project(Cartography)

cmake_minimum_required(VERSION 3.10)
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules)
include(FeatureSummary)

########################
## Enable C++20 and all warnings

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Werror")

########################
## Enable testing

enable_testing()

########################
## Define version

set(CARTOGRAPHY_MAJOR_VERSION 2)
set(CARTOGRAPHY_MINOR_VERSION 0)
set(CARTOGRAPHY_PATCH_VERSION 0)
set(CARTOGRAPHY_VERSION ${CARTOGRAPHY_MAJOR_VERSION}.${CARTOGRAPHY_MINOR_VERSION}.${CARTOGRAPHY_PATCH_VERSION})

########################
## Define install dirs

set(INSTALL_LIB_DIR     lib${LIB_SUFFIX}/ CACHE PATH "Installation directory for libraries")
set(INSTALL_INCLUDE_DIR include/          CACHE PATH "Installation directory for headers")
set(INSTALL_BIN_DIR     bin/              CACHE PATH "Installation directory for executables")
set(INSTALL_SHARE_DIR   share/Cartography/        CACHE PATH "Installation directory for data")
set(INSTALL_DOC_DIR     share/doc/Cartography/    CACHE PATH "Installation directory for documentation")
set(INSTALL_QML_DIR     lib/qt6/qml       CACHE PATH "Installation directory for QML plugin")

if(WIN32 AND NOT CYGWIN)
  set(DEF_INSTALL_CMAKE_DIR cmake)
else()
  set(DEF_INSTALL_CMAKE_DIR lib/cmake/Cartography)
endif()
set(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR} CACHE PATH "Installation directory for CMake files")

# Make relative paths absolute (needed later on)
foreach(p LIB BIN INCLUDE SHARE DOC CMAKE QML)
  set(var INSTALL_${p}_DIR)
  if(NOT IS_ABSOLUTE "${${var}}")
    set(${var} "${CMAKE_INSTALL_PREFIX}/${${var}}")
  endif()
endforeach()

#
# Define INSTALL_TARGETS_DEFAULT_ARGS to be used as install target for program and library.
# It will do the right thing on all platform
#
set(INSTALL_TARGETS_DEFAULT_ARGS  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin
                                  LIBRARY DESTINATION "${INSTALL_LIB_DIR}" COMPONENT shlib
                                  ARCHIVE DESTINATION "${INSTALL_LIB_DIR}" COMPONENT Devel )

########################
# RPATH

SET(CMAKE_INSTALL_RPATH "${INSTALL_LIB_DIR}")
SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

########################
## Find libcmaes

find_package(libcmaes CONFIG)

set_package_properties(libcmaes PROPERTIES
    DESCRIPTION "libcmaes"
    URL "https://github.com/beniz/libcmaes"
    TYPE OPTIONAL
    PURPOSE "Optional for Weighted Graph Polygon Decomposition")

########################
## Find GDAL

find_package(GDAL)

set_package_properties(GDAL PROPERTIES
    DESCRIPTION "GDAL - Geospatial Data Abstraction Library."
    URL "https://www.gdal.org/"
    TYPE OPTIONAL
    PURPOSE "Required for integration between Geometry and GDAL")

include_directories(${GDAL_INCLUDE_DIR} )

if(GDAL_FOUND)
  set(CARTOGRAPHY_HAVE_GDAL TRUE)
else()
  set(CARTOGRAPHY_HAVE_GDAL)
endif()

########################
## Find Mapnik

find_package(Mapnik )

set_package_properties(MAPNIK PROPERTIES
    DESCRIPTION "mapnik combines pixel-perfect image output with lightning-fast cartographic algorithms, and exposes interfaces in C++, Python, and Node."
    URL "https://mapnik.org/"
    TYPE OPTIONAL
    PURPOSE "Required for the mapnik QML module")

add_definitions(${MAPNIK_DEFINITIONS})
set(HAVE_MAPNIK MAPNIK_FOUND)
########################
## Find Cyqlops::Toolbox

find_package(Cyqlops CONFIG COMPONENTS Toolbox)

set_package_properties(Cyqlops PROPERTIES
    DESCRIPTION "Cyqlops: is a set of components for Qt."
    URL "https://gitlab.com/cyloncore/Cyqlops"
    TYPE OPTIONAL
    PURPOSE "Required for Cartographer")

########################
## Find Euclid

if(${CARTOGRAPHY_HAVE_GDAL})
  find_package(euclid REQUIRED COMPONENTS GDAL GEOS Eigen3 Proj CONFIG)
else()
  find_package(euclid REQUIRED COMPONENTS GEOS Eigen3 Proj CONFIG)
endif()


set_package_properties(EUCLID PROPERTIES
    DESCRIPTION ""
    URL "https://gitlab.com/cyloncore/euclid"
    TYPE OPTIONAL
    PURPOSE "Required for the Algorithms module")

########################
## Find clog
find_package(clog REQUIRED)
set_package_properties(clog PROPERTIES
    DESCRIPTION "C++ Logging Library."
    TYPE OPTIONAL
    URL "https://gitlab.com/cyloncore/cext"
    PURPOSE "Manage logging messages")

########################
## Find cres
find_package(cres REQUIRED)
set_package_properties(cres PROPERTIES
    DESCRIPTION "C++ Result Library."
    TYPE OPTIONAL
    URL "https://gitlab.com/cyloncore/cext"
    PURPOSE "Manage result values.")

########################
## Find Qt6

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

find_package(Qt6 COMPONENTS Positioning Gui Quick 3DRender 3DQuickExtras CONFIG)
find_package(Qt6 REQUIRED COMPONENTS Core Test CONFIG)

########################
## Find Python

find_package(pybind11-qt CONFIG)

set_package_properties(pybind11-qt PROPERTIES
    DESCRIPTION "pybind11-qt"
    URL "http://gitlab.com/cyloncore/pybind11-qt"
    TYPE OPTIONAL
    PURPOSE "Required by the Python bindings")

##########################
## List enabled features

set(BUILD_CARTOGRAPHY_ALGORITHMS OFF CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_GEOMETRY ON CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_RASTER ON CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_TERRAIN OFF CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_QUICK_ALGORITHMS OFF CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_QUICK_CARTOGRAPHER OFF CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_QUICK_GEOMETRY OFF CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_QUICK_MAPNIK OFF CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_QUICK_RASTER OFF CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_QUICK_TERRAIN OFF CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_QUICK_TERRAIN_3D OFF CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_PYTHON OFF CACHE INTERNAL "")
set(BUILD_CARTOGRAPHY_MQTT OFF CACHE INTERNAL "")

if(MAPNIK_FOUND AND Qt6Quick_FOUND AND Qt6Positioning_FOUND)
  set(BUILD_CARTOGRAPHY_QUICK_MAPNIK ON CACHE INTERNAL "")
endif()

if(EUCLID_FOUND)
  set(BUILD_CARTOGRAPHY_ALGORITHMS ON CACHE INTERNAL "")
endif()

if(Qt6Gui_FOUND)
  set(BUILD_CARTOGRAPHY_TERRAIN ON CACHE INTERNAL "")
endif()

if(Qt6Quick_FOUND AND BUILD_CARTOGRAPHY_ALGORITHMS)
  set(BUILD_CARTOGRAPHY_QUICK_ALGORITHMS ON CACHE INTERNAL "")
endif()

if(BUILD_CARTOGRAPHY_QUICK_MAPNIK AND Cyqlops_FOUND)
  set(BUILD_CARTOGRAPHY_QUICK_CARTOGRAPHER ON CACHE INTERNAL "")
endif()

if(Qt6Quick_FOUND)
  set(BUILD_CARTOGRAPHY_QUICK_GEOMETRY ON CACHE INTERNAL "")
endif()

if(Qt6Quick_FOUND AND BUILD_CARTOGRAPHY_RASTER)
  set(BUILD_CARTOGRAPHY_QUICK_RASTER ON CACHE INTERNAL "")
endif()

if(Qt6Quick_FOUND AND BUILD_CARTOGRAPHY_TERRAIN)
  set(BUILD_CARTOGRAPHY_QUICK_TERRAIN ON CACHE INTERNAL "")
endif()

if(Qt63DRender_FOUND AND Qt63DQuickExtras_FOUND)
  set(BUILD_CARTOGRAPHY_QUICK_TERRAIN_3D ON CACHE INTERNAL "")
endif()

if (pybind11-qt_FOUND)
  set(BUILD_CARTOGRAPHY_PYTHON ON CACHE INTERNAL "")
endif()

if (CYQLOPS_MQTT_FOUND)
  set(BUILD_CARTOGRAPHY_MQTT ON CACHE INTERNAL "")
endif()

add_feature_info(CartographyAlgorithms        BUILD_CARTOGRAPHY_ALGORITHMS          "Cartography Algorithms")
add_feature_info(CartographyGeometry          BUILD_CARTOGRAPHY_GEOMETRY            "Cartography Geometry")
add_feature_info(CartographyTerrain           BUILD_CARTOGRAPHY_TERRAIN             "Cartography Terrain")
add_feature_info(CartographyQuickAlgorithms   BUILD_CARTOGRAPHY_QUICK_ALGORITHMS    "Cartography Quick Algorithms")
add_feature_info(CartographyQuickCartographer BUILD_CARTOGRAPHY_QUICK_CARTOGRAPHER  "Cartography Quick Cartographer")
add_feature_info(CartographyQuickMapnik       BUILD_CARTOGRAPHY_QUICK_MAPNIK        "Cartography Quick Mapnik")
add_feature_info(CartographyQuickTerrain      BUILD_CARTOGRAPHY_QUICK_TERRAIN       "Cartography Quick Terrain")
add_feature_info(CartographyQuickTerrain3D    BUILD_CARTOGRAPHY_QUICK_TERRAIN_3D    "Cartography Quick Terrain 3D")
add_feature_info(CartographyMqtt              BUILD_CARTOGRAPHY_MQTT                "Cartography MQTT Server")
add_feature_info(Python                       BUILD_CARTOGRAPHY_PYTHON              "Cartography python binding")

########################
## subdirectories

include_directories(BEFORE ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR})

add_subdirectory(Cartography/)
add_subdirectory(examples)
add_subdirectory(tests)

if(BUILD_CARTOGRAPHY_PYTHON)
add_subdirectory(python)
endif()

########################
## Config files

export(TARGETS ${PROJECT_EXPORTED_TARGETS}
  FILE "${PROJECT_BINARY_DIR}/CartographyTargets.cmake")

file(RELATIVE_PATH REL_INCLUDE_DIR "${INSTALL_CMAKE_DIR}"
   "${INSTALL_INCLUDE_DIR}")

file(RELATIVE_PATH REL_BIN_DIR "${INSTALL_CMAKE_DIR}"
   "${INSTALL_BIN_DIR}")

# ... for the install tree
set(CONF_INCLUDE_DIRS "\${CARTOGRAPHY_CMAKE_DIR}/${REL_INCLUDE_DIR}")
set(CONF_BIN_DIR "\${CARTOGRAPHY_CMAKE_DIR}/${REL_BIN_DIR}")

configure_file(CartographyConfig.cmake.in
  "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CartographyConfig.cmake" @ONLY)
# ... for both
configure_file(CartographyConfigVersion.cmake.in
  "${PROJECT_BINARY_DIR}/CartographyConfigVersion.cmake" @ONLY)

# Install the CartographyConfig.cmake and CartographyConfigVersion.cmake
install(FILES
  "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CartographyConfig.cmake"
  "${PROJECT_BINARY_DIR}/CartographyConfigVersion.cmake"
  DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev)

# Install the export set for use with the install-tree
install(EXPORT CartographyTargets DESTINATION
  "${INSTALL_CMAKE_DIR}" COMPONENT dev)

########################
## Feature summary

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
