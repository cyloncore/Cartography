
set(CARTOGRAPHY_ALGORITHMS_SRCS
  LawnMowerPattern.cpp
  WeightedGraphPolygonDecomposition.cpp
  WeightedPolygonDecomposition.cpp
  )

add_library(CartographyAlgorithms SHARED ${CARTOGRAPHY_ALGORITHMS_SRCS} )
target_link_libraries(CartographyAlgorithms PUBLIC Qt6::Core euclid::GEOS CartographyRaster PRIVATE euclid::Eigen3)
set_target_properties(CartographyAlgorithms PROPERTIES VERSION ${CARTOGRAPHY_VERSION} SOVERSION ${CARTOGRAPHY_MAJOR_VERSION})

if(libcmaes_FOUND)
target_link_libraries(CartographyAlgorithms PRIVATE libcmaes::cmaes)
endif()

install(TARGETS CartographyAlgorithms EXPORT CartographyTargets ${INSTALL_TARGETS_DEFAULT_ARGS} )

install( FILES
  LawnMowerPattern.h
  WeightedGraphPolygonDecomposition.h
  WeightedPolygonDecomposition.h
  DESTINATION ${INSTALL_INCLUDE_DIR}/Cartography/Algorithms )
