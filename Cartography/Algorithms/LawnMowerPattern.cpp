#include "LawnMowerPattern.h"

#include <euclid/eigen3>
#include <euclid/extra/longest_segment>
#include <euclid/ops>

#include "Utils_p.h"

namespace
{
  inline double line_distance(const euclid::simple::point& _pt, const euclid::simple::segment& _seg)
  {
    double x1 = _seg.first().x();
    double y1 = _seg.first().y();
    double x2 = _seg.second().x();
    double y2 = _seg.second().y();
    double x0 = _pt.x();
    double y0 = _pt.y();

    return std::abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1))
           / std::sqrt(pow2(x2 - x1) + pow2(y2 - y1));
  }

  inline double segment_to_segment_point_distance(const euclid::simple::segment& _seg1,
                                                  const euclid::simple::segment& _seg2)
  {
    return std::max({line_distance(_seg1.first(), _seg2), line_distance(_seg1.second(), _seg2),
                     line_distance(_seg2.first(), _seg2), line_distance(_seg2.second(), _seg2)});
  }
  inline void add_to_builder(euclid::geos::line_string::builder& _builder,
                             const euclid::simple::point& _pt1, const euclid::simple::point& _pt2,
                             double _offset)
  {
    if(_offset > 0.0 or _offset < 0.0)
    {
      Eigen::Vector2d p1 = euclid::eigen3::to_vector(_pt1).head<2>();
      Eigen::Vector2d p2 = euclid::eigen3::to_vector(_pt2).head<2>();
      Eigen::Vector2d offset = (p2 - p1).normalized() * _offset;
      _builder.add_point(
        euclid::eigen3::to_point<euclid::simple::point>(Eigen::Vector2d(p1 + offset)));
      _builder.add_point(
        euclid::eigen3::to_point<euclid::simple::point>(Eigen::Vector2d(p2 + offset)));
    }
    else
    {
      _builder.add_point(_pt1);
      _builder.add_point(_pt2);
    }
  }
} // namespace

namespace Cartography::Algorithms
{
  template<typename _T_>
  void LawnMowerPatternParameters::readImpl(const _T_& _values)
  {
    for(typename _T_::ConstIterator cit = _values.begin(); cit != _values.end(); ++cit)
    {
      QString k = cit.key();
      QVariant v = cit.value();
      IF_KEY_TRY_CONVERT(strideWidth, stride_width)
      else IF_KEY_TRY_CONVERT(sensingLength, sensing_length) else IF_KEY_TRY_CONVERT(
        startOffset, start_offset) else IF_KEY_TRY_CONVERT(precision,
                                                           precision) else if(k == "startPoint"
                                                                              or k == "start_point")
      {
        QPointF pt;
        if(v.canConvert<QVariantMap>())
        {
          QVariantMap vm = v.toMap();
          pt = QPointF(vm.value("x", 0.0).toDouble(), vm.value("y", 0.0).toDouble());
        }
        else
        {
          tryConvert(k, &pt, v);
        }
        startPoint = euclid::simple::point(pt.x(), pt.y());
      }
      else
      {
        clog_error("Invalid key '{}' with value '{}'", k, v);
      }
    }
  }

  void LawnMowerPatternParameters::read(const QVariantHash& _values) { readImpl(_values); }

  void LawnMowerPatternParameters::read(const QVariantMap& _values) { readImpl(_values); }

  euclid::geos::line_string lawnMowerPattern(const euclid::geos::polygon& _geometry,
                                             const LawnMowerPatternParameters& _parameters)
  {
    QList<euclid::simple::segment> segments;
    QList<euclid::geos::polygon> geometries;
    geometries.push_back(_geometry);

    clog_debug_vn(euclid::io::to_wkt(_geometry, {14, 2}));
    euclid::simple::box b = euclid::ops::envelope(_geometry);
    const double max_length = std::sqrt(b.width() * b.width() + b.height() * b.height());
    const double offsetting = _parameters.precision * _parameters.strideWidth;

    // First compute the segments
    while(not geometries.isEmpty())
    {
      const euclid::geos::polygon poly = geometries.takeFirst();
      const euclid::simple::segment longest_segment
        = euclid::extra::longest_segment(poly.exterior_ring());
      const Eigen::Vector2d start = euclid::eigen3::to_vector(longest_segment.first()).head<2>();
      const Eigen::Vector2d end = euclid::eigen3::to_vector(longest_segment.second()).head<2>();

      const Eigen::Vector2d segment_direction = (end - start).normalized();
      const Eigen::Vector2d segment_normal
        = Eigen::Vector2d(segment_direction.y(), -segment_direction.x());
      const Eigen::Vector2d translation = _parameters.strideWidth * segment_normal;

      // Generate a big polylon that covers the area to use for the section
      const Eigen::Vector2d stride_segment_start
        = start - segment_direction * max_length - 0.0001 * segment_normal;
      const Eigen::Vector2d stride_segment_end
        = end + segment_direction * max_length - 0.0001 * segment_normal;

      const Eigen::Vector2d stride_start = start + translation - segment_direction * max_length;
      const Eigen::Vector2d stride_end = end + translation + segment_direction * max_length;

      namespace ee3 = euclid::eigen3;

      const euclid::geos::polygon over_stride_poly
        = euclid::geos::polygon::builder()
            .add_point(ee3::to_point<euclid::simple::point>(stride_segment_start))
            .add_point(ee3::to_point<euclid::simple::point>(stride_segment_end))
            .add_point(ee3::to_point<euclid::simple::point>(stride_end))
            .add_point(ee3::to_point<euclid::simple::point>(stride_start))
            .create();

      const euclid::geos::geometry intersection = euclid::ops::intersection(poly, over_stride_poly);
      clog_debug_vn(euclid::io::to_wkt(poly, {14, 2}),
                    euclid::io::to_wkt(over_stride_poly, {14, 2}));
      clog_assert(not intersection.is_null());
      euclid::geos::polygon intersection_poly;
      if(intersection.is<euclid::geos::polygon>())
      {
        intersection_poly = intersection.cast<euclid::geos::polygon>();
      }
      else if(intersection.is<euclid::geos::multi_polygon>())
      {
        euclid::geos::multi_polygon mp = intersection.cast<euclid::geos::multi_polygon>();
        double largest_area = 0.0;
        for(std::size_t i = 0; i < mp.size(); ++i)
        {
          euclid::geos::polygon mpp = mp.at(i);
          double area = euclid::ops::area(mpp);
          if(area > largest_area)
          {
            largest_area = area;
            intersection_poly = mpp;
          }
        }
      }
      clog_assert(not intersection_poly.is_null());
      // Find the extent of the polygon
      double min = std::numeric_limits<double>::max();
      double max = std::numeric_limits<double>::lowest();
      euclid::geos::linear_ring intersection_poly_er = intersection_poly.exterior_ring();
      for(euclid::geos::point_iterator<euclid::geos::linear_ring> it
          = intersection_poly_er.points().begin();
          it != intersection_poly_er.points().end(); ++it)
      {
        // projection
        const Eigen::Vector2d pt = euclid::eigen3::to_vector(*it).head<2>();
        qreal d = (pt - start).dot(segment_direction);
        min = std::min(min, d);
        max = std::max(max, d);
      }

      // Handle the sensing length (for instance if using a camera)
      if(min + _parameters.sensingLength > max - _parameters.sensingLength)
      {
        min = 0.5 * (min + max) - 0.0001;
        max = min + 0.0002;
      }
      else
      {
        min += _parameters.sensingLength;
        max -= _parameters.sensingLength;
      }

      Eigen::Vector2d translation_compensation = Eigen::Vector2d::Zero();

      // Remove from the polygon
      const euclid::geos::geometry substracted
        = euclid::ops::difference(poly, euclid::ops::offsetting(intersection_poly, offsetting));
      if(not substracted.is_null() and not substracted.is_empty())
      {
        if(substracted.is<euclid::geos::polygon>())
        {
          geometries.append(substracted.cast<euclid::geos::polygon>());
        }
        else if(substracted.is<euclid::geos::multi_polygon>())
        {
          const euclid::geos::multi_polygon mp = substracted.cast<euclid::geos::multi_polygon>();
          for(std::size_t i = 0; i < mp.size(); ++i)
          {
            geometries.append(mp.at(i));
          }
        }
        else if(substracted.is<euclid::geos::multi_geometry>())
        {
          const euclid::geos::multi_geometry col = substracted.cast<euclid::geos::multi_geometry>();
          for(std::size_t i = 0; i < col.size(); ++i)
          {
            const euclid::geos::geometry g = col.at(i);
            if(g.is<euclid::geos::polygon>())
            {
              geometries.append(g.cast<euclid::geos::polygon>());
            }
            else if(not g.is<euclid::geos::line_string>())
            {
              // ignore line string and asserts on the rest
              clog_fatal("Result of difference should be collection of polygon and line string");
            }
          }
        }
        else
        {
          clog_fatal("Result of difference should be polygon, or multi polygon or null");
        }
      }
      else
      {
        // If the area is empty, make the segment go through the middle of the polygon to cover more
        // of the area of interest
        Eigen::Vector2d middle = Eigen::Vector2d::Zero();
        int count = 0;
        for(euclid::geos::point_iterator<euclid::geos::linear_ring> it
            = intersection_poly_er.points().begin();
            it != intersection_poly_er.points().end(); ++it)
        {
          middle += euclid::eigen3::to_vector(*it).head<2>();
          ++count;
        }
        middle /= count;

        qreal d = (middle - start).dot(segment_direction);
        Eigen::Vector2d middle_projection = d * segment_direction + start;
        translation_compensation = middle - middle_projection - 0.5 * translation;
      }

      // Add the segment

      const Eigen::Vector2d seg_start
        = start + segment_direction * min + 0.5 * translation + translation_compensation;
      const Eigen::Vector2d seg_end
        = start + segment_direction * max + 0.5 * translation + translation_compensation;

      segments.append(euclid::eigen3::to_segment(seg_start, seg_end));
    }

    // Second connect them together

    euclid::geos::line_string::builder builder;
    euclid::simple::point current_point = _parameters.startPoint;

    if(segments.size() < 2)
    {
      euclid::simple::segment seg = segments.takeFirst();

      if(euclid::simple::distance<2>(current_point, seg.first())
         < euclid::simple::distance<2>(current_point, seg.second()))
      {
        add_to_builder(builder, seg.first(), seg.second(), _parameters.startOffset);
      }
      else
      {
        add_to_builder(builder, seg.second(), seg.first(), _parameters.startOffset);
      }
    }
    else
    {
      // First select the two furthest segments, one of them will act as first segment.

      std::size_t index1 = -1;
      std::size_t index2 = -1;
      double furthest_distance = 0.0;

      for(int i = 0; i < segments.size() - 1; ++i)
      {
        for(int j = i + 1; j < segments.size(); ++j)
        {
          double distance = segment_to_segment_point_distance(segments[i], segments[j]);
          if(distance > furthest_distance)
          {
            furthest_distance = distance;
            index1 = i;
            index2 = j;
          }
        }
      }

      // If a start point was defined
      if(current_point.is_valid())
      {
        euclid::simple::segment seg1 = segments.at(index1);
        euclid::simple::segment seg2 = segments.at(index2);
        // Select the closest start point
        double d1 = euclid::simple::distance<2>(current_point, seg1.first());
        double d2 = euclid::simple::distance<2>(current_point, seg1.second());
        double d3 = euclid::simple::distance<2>(current_point, seg2.first());
        double d4 = euclid::simple::distance<2>(current_point, seg2.second());

        std::size_t selected_index = 0;
        bool first_is_start = false;

        if(d1 < d2 and d1 < d3 and d1 < d4)
        {
          selected_index = index1;
          first_is_start = true;
        }
        else if(d2 < d3 and d2 < d4)
        {
          selected_index = index1;
          first_is_start = false;
        }
        else if(d3 < d4)
        {
          selected_index = index2;
          first_is_start = true;
        }
        else
        {
          selected_index = index2;
          first_is_start = false;
        }

        // Create the first segment in the builder
        euclid::simple::segment seg = segments.takeAt(selected_index);

        if(first_is_start)
        {
          add_to_builder(builder, seg.first(), seg.second(), _parameters.startOffset);
          current_point = seg.second();
        }
        else
        {
          add_to_builder(builder, seg.second(), seg.first(), _parameters.startOffset);
          current_point = seg.first();
        }
      }
      else
      {
        // No point was defined, then select the index1 segment arbitrarily
        euclid::simple::segment seg = segments.takeAt(index1);
        add_to_builder(builder, seg.first(), seg.second(), _parameters.startOffset);
        current_point = seg.second();
      }

      // As long as we have more segments, select the closest one as the next point
      while(not segments.isEmpty())
      {
        QList<euclid::simple::segment>::iterator closest_segment_it;
        double closest_distance = std::numeric_limits<double>::max();
        // Find the closest segment
        for(auto it = segments.begin(); it != segments.end(); ++it)
        {
          double distance = std::min(euclid::simple::distance<2>(current_point, it->first()),
                                     euclid::simple::distance<2>(current_point, it->second()));
          if(distance < closest_distance)
          {
            closest_segment_it = it;
            closest_distance = distance;
          }
        }
        // Add it to the builder
        clog_assert(closest_distance < std::numeric_limits<double>::max());
        euclid::simple::point p1 = closest_segment_it->first();
        euclid::simple::point p2 = closest_segment_it->second();
        double distance_1 = euclid::simple::distance<2>(current_point, p1);
        double distance_2 = euclid::simple::distance<2>(current_point, p2);

        // Select a point on either side of the segment
        if(distance_1 < distance_2)
        {
          add_to_builder(builder, p1, p2, _parameters.startOffset);
          current_point = p2;
        }
        else
        {
          add_to_builder(builder, p2, p1, _parameters.startOffset);
          current_point = p1;
        }
        segments.erase(closest_segment_it);
      }
    }
    return builder.create();
  }

} // namespace Cartography::Algorithms
