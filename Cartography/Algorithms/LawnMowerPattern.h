#include <QMetaType>

#include <Cartography/CoordinateSystem.h>
#include <Cartography/GeometryObject.h>

namespace Cartography::Algorithms
{
  struct LawnMowerPatternParameters
  {
    /// Width between two strides in the pattern
    double strideWidth = 1.0;
    /// Length covered by the sensor
    double sensingLength = 0.000;
    /// Add an offset to the start of a segment in the pattern
    double startOffset = 0.0;
    double precision = 1.0e-2;
    /// start point, used to select the start of the law mower pattern (default is no start point
    /// and use the longest segment as start
    euclid::simple::point startPoint = euclid::simple::point::invalid();
    /**
     * Read the \ref _values and set the field of the parameters
     * \private
     */
    void read(const QVariantHash& _values);
    /**
     * Read the \ref _values and set the field of the parameters
     * \private
     */
    void read(const QVariantMap& _values);
  private:
    template<typename _T_>
    void readImpl(const _T_& _values);
  };
  EuclidSystem::line_string lawnMowerPattern(const EuclidSystem::polygon& _geometry,
                                             const LawnMowerPatternParameters& _parameters);
  GeometryObject lawnMowerPattern(const GeometryObject& _geometry,
                                  const LawnMowerPatternParameters& _parameters)
  {
    return GeometryObject(
      _geometry.coordinateSystem(),
      lawnMowerPattern(_geometry.geometry().cast<EuclidSystem::polygon>(), _parameters));
  }
  GeometryObject lawnMowerPattern(const GeometryObject& _geometry, const QVariantHash& _parameters)
  {
    LawnMowerPatternParameters lmpp;
    lmpp.read(_parameters);
    return lawnMowerPattern(_geometry, lmpp);
  }
} // namespace Cartography::Algorithms
