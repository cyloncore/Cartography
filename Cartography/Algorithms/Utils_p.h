#include <cmath>
#include <memory>

#include <clog_qt>

#include <QLineF>
#include <QPointF>
#include <QVariant>

#include <Cartography/Global.h>

#include <Cartography/EuclidSystem.h>

#include <euclid/extra/curve_value>

namespace
{
  template<typename _T_>
  constexpr inline _T_ pow2(_T_ _t)
  {
    return _t * _t;
  }
  struct CurvilinearCoordinates
  {
    const Cartography::EuclidSystem::linear_ring linearRing;
    const double length;
    CurvilinearCoordinates(const Cartography::EuclidSystem::linear_ring& _linearRing)
        : linearRing(_linearRing), length(euclid::ops::length(linearRing))
    {
    }
    euclid::simple::point operator()(double _c) const
    {
      return euclid::extra::curve_value(linearRing, _c * length);
    }
  };

  CARTOGRAPHY_GLIBC_CONSTEXPR inline qreal distance2(const QPointF& _p1, const QPointF& _p2)
  {
    return std::sqrt(pow2(_p1.x() - _p2.x()) + pow2(_p1.y() - _p2.y()));
  }
  constexpr inline qreal manhattan(const QPointF& _p1, const QPointF& _p2)
  {
    return (_p1 - _p2).manhattanLength();
  }

  inline qreal distance(const QPointF& p, const QLineF& line)
  {
    // transform to loocal coordinates system (0,0) - (lx, ly)
    QPointF p1 = line.p1();
    QPointF p2 = line.p2();
    qreal x = p.x() - p1.x();
    qreal y = p.y() - p1.y();
    qreal x2 = p2.x() - p1.x();
    qreal y2 = p2.y() - p1.y();

    // if line is a point (nodes are the same) =>
    // just return distance between point and one line node
    qreal norm = std::sqrt(x2 * x2 + y2 * y2);
    if(norm <= std::numeric_limits<int>::epsilon())
      return std::sqrt(x * x + y * y);

    // distance
    return std::abs(x * y2 - y * x2) / norm;
  }
  template<typename _T_>
  inline bool tryConvert(const QString& _k, _T_* _dst, const QVariant& _v)
  {
    if(_v.canConvert<_T_>())
    {
      *_dst = _v.value<_T_>();
      return true;
    }
    else
    {
      clog_error("Cannot convert key '%1' to type with value '%2' with type '%3'", _k, _v,
                 _v.typeName());
      return false;
    }
  }
} // namespace

#define IF_KEY_TRY_CONVERT(kn, altK)                                                               \
  if(k == #kn or k == #altK)                                                                       \
  {                                                                                                \
    tryConvert(k, &kn, v);                                                                         \
  }

#define IF_KEY_TRY_CONVERT_ENUM(kn, altK, toEnum)                                                  \
  if(k == #kn or k == #altK)                                                                       \
  {                                                                                                \
    QString e;                                                                                     \
    if(tryConvert(k, &e, v))                                                                       \
    {                                                                                              \
      toEnum(k, &kn, e);                                                                           \
    }                                                                                              \
  }
