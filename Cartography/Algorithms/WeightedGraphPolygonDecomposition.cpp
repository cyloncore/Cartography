#define CLOG_FORCE_ASSERT

#include "WeightedGraphPolygonDecomposition.h"

#include <optional>

#include <euclid/extra/fix_it>
#include <euclid/geos>
#include <euclid/io>
#include <euclid/ops>

#include <clog_qt>

#include <QList>
#include <QRectF>
#include <QSet>

#include <Cartography/config_p.h>

#include "Utils_p.h"
#include <euclid/extra/line_string_fitting>

#include <Cartography/CoordinateSystem.h>
#include <Cartography/Point.h>

#ifdef HAVE_LIBCMAES
// Both gdal and libcames uses generic names for defines...
#undef PACKAGE_NAME
#undef PACKAGE_STRING
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION
#include <libcmaes/cmaes.h>
#else
#include <Eigen/Core>
#endif

namespace Cartography::Algorithms::Details
{
  euclid::simple::point toSimplePoint(const QPointF& _pt)
  {
    return euclid::simple::point(_pt.x(), _pt.y());
  }
  QPointF toQPointF(const euclid::simple::point& _pt) { return QPointF(_pt.x(), _pt.y()); }
#define CONVERT_ENUM(b, e)                                                                         \
  if(_v == #e)                                                                                     \
  {                                                                                                \
    *_d = b::e;                                                                                    \
    return true;                                                                                   \
  }
  bool toOptimisationEnum(const QString& _k,
                          WeightedGraphPolygonDecompositionParameters::Optimisation* _d,
                          const QString& _v)
  {
    CONVERT_ENUM(WeightedGraphPolygonDecompositionParameters::Optimisation, None)
    else CONVERT_ENUM(
      WeightedGraphPolygonDecompositionParameters::Optimisation,
      PotentialField) else CONVERT_ENUM(WeightedGraphPolygonDecompositionParameters::Optimisation,
                                        CMAES) else CONVERT_ENUM(WeightedGraphPolygonDecompositionParameters::
                                                                   Optimisation,
                                                                 RandomSearch) else
    {
      clog_error("Unknown enum value '{}' for key '{}'", _v, _k);
      return false;
    }
  }
  bool toObjectiveFunctionFatnessEnum(
    const QString& _k, WeightedGraphPolygonDecompositionParameters::ObjectiveFunctionFatness* _d,
    const QString& _v)
  {
    CONVERT_ENUM(WeightedGraphPolygonDecompositionParameters::ObjectiveFunctionFatness,
                 Schwartzberg)
    else CONVERT_ENUM(WeightedGraphPolygonDecompositionParameters::ObjectiveFunctionFatness,
                      PolsbyPopper) else
    {
      clog_error("Unknown enum value '{}' for key '{}'", _v, _k);
      return false;
    }
  }
  bool toNormEnum(const QString& _k, WeightedGraphPolygonDecompositionParameters::Norm* _d,
                  const QString& _v)
  {
    CONVERT_ENUM(WeightedGraphPolygonDecompositionParameters::Norm, Norm1)
    else CONVERT_ENUM(WeightedGraphPolygonDecompositionParameters::Norm, Norm2) else
    {
      clog_error("Unknown enum value '{}' for key '{}'", _v, _k);
      return false;
    }
  }
} // namespace Cartography::Algorithms::Details

using namespace Cartography::Algorithms::Details;

template<typename _T_>
void Cartography::Algorithms::WeightedGraphPolygonDecompositionParameters::readImpl(
  const _T_& _values)
{
  for(typename _T_::ConstIterator cit = _values.begin(); cit != _values.end(); ++cit)
  {
    QString k = cit.key();
    QVariant v = cit.value();
    IF_KEY_TRY_CONVERT(maxIterations, max_iterations)
    else IF_KEY_TRY_CONVERT(raster, raster) else IF_KEY_TRY_CONVERT(rasterBand, raster_mBand) else IF_KEY_TRY_CONVERT_ENUM(
      optimisation1, optimisation_1,
      toOptimisationEnum) else IF_KEY_TRY_CONVERT_ENUM(optimisation2, optimisation_2,
                                                       toOptimisationEnum) else IF_KEY_TRY_CONVERT_ENUM(objectiveFunctionFatness,
                                                                                                        objective_function_fatness,
                                                                                                        toObjectiveFunctionFatnessEnum) else
    {
      clog_error("Invalid key '{}' with value '{}'", k, v);
    }
  }
}

void Cartography::Algorithms::WeightedGraphPolygonDecompositionParameters::read(
  const QVariantHash& _values)
{
  readImpl(_values);
}

void Cartography::Algorithms::WeightedGraphPolygonDecompositionParameters::read(
  const QVariantMap& _values)
{
  readImpl(_values);
}

namespace Cartography
{
  namespace Algorithms
  {
    namespace Details
    {
      struct WeightedGraphPolygonDecompositionAreaInfo;
      struct WeightedGraphPolygonDecompositionBoundaryNode;
      struct WeightedGraphPolygonDecompositionGridCell;

      struct WeightedGraphPolygonDecompositionBoundaryEdge
      {
        QList<QPointF> points;
        bool canSimplify;
        WeightedGraphPolygonDecompositionAreaInfo* areaInfo1;
        WeightedGraphPolygonDecompositionAreaInfo* areaInfo2;
        WeightedGraphPolygonDecompositionBoundaryNode* start;
        WeightedGraphPolygonDecompositionBoundaryNode* end;
        WeightedGraphPolygonDecompositionBoundaryEdge* cannotConnectTo
          = nullptr; /// indicates that the other e

        WeightedGraphPolygonDecompositionBoundaryNode*
          other(const WeightedGraphPolygonDecompositionBoundaryNode* n) const
        {
          return n == start ? end : start;
        }
      };

      struct WeightedGraphPolygonDecompositionBoundaryNode
      {
        WeightedGraphPolygonDecompositionGridCell* topLeft = nullptr;
        WeightedGraphPolygonDecompositionGridCell* topRight = nullptr;
        WeightedGraphPolygonDecompositionGridCell* bottomLeft = nullptr;
        WeightedGraphPolygonDecompositionGridCell* bottomRight = nullptr;

        typedef QHash<WeightedGraphPolygonDecompositionBoundaryNode*,
                      QList<WeightedGraphPolygonDecompositionBoundaryEdge*>>
          ConnectionsT;

        ConnectionsT connections = ConnectionsT();

        WeightedGraphPolygonDecompositionBoundaryEdge* leftEdge = nullptr;
        WeightedGraphPolygonDecompositionBoundaryEdge* topEdge = nullptr;
        WeightedGraphPolygonDecompositionBoundaryEdge* rightEdge = nullptr;
        WeightedGraphPolygonDecompositionBoundaryEdge* bottomEdge = nullptr;

        QPointF coordinate() const;

        int connectionTo(WeightedGraphPolygonDecompositionAreaInfo* ai) const
        {
          int count = 0;
          if(leftEdge and (leftEdge->areaInfo1 == ai or leftEdge->areaInfo2 == ai))
            ++count;
          if(topEdge and (topEdge->areaInfo1 == ai or topEdge->areaInfo2 == ai))
            ++count;
          if(rightEdge and (rightEdge->areaInfo1 == ai or rightEdge->areaInfo2 == ai))
            ++count;
          if(bottomEdge and (bottomEdge->areaInfo1 == ai or bottomEdge->areaInfo2 == ai))
            ++count;
          return count;
        }

        //         bool canConnect(WeightedGraphPolygonDecompositionBoundaryNode* _dst, const
        //         QList<const WeightedGraphPolygonDecompositionBoundaryEdge*>& _used_edges,
        //         WeightedGraphPolygonDecompositionAreaInfo* _areaInfo)
        //         {
        //           if(connections.contains(_dst))
        //           {
        //             for(const WeightedGraphPolygonDecompositionBoundaryEdge* e :
        //             connections.value(_dst))
        //             {
        //               if(not _used_edges.contains(e) and (e->areaInfo1 == _areaInfo or
        //               e->areaInfo2 == _areaInfo))
        //               {
        //                 return true;
        //               }
        //             }
        //           }
        //           return false;
        //         }

        std::tuple<QList<QPointF>, const WeightedGraphPolygonDecompositionAreaInfo*,
                   const WeightedGraphPolygonDecompositionBoundaryEdge*>
          getConnectionsPoints(
            WeightedGraphPolygonDecompositionBoundaryNode* _dst,
            QList<const WeightedGraphPolygonDecompositionBoundaryEdge*>* _used_edges,
            WeightedGraphPolygonDecompositionAreaInfo* _areaInfo,
            const WeightedGraphPolygonDecompositionAreaInfo* _previous_area,
            const WeightedGraphPolygonDecompositionBoundaryEdge* _previous_edge) const
        {
          std::tuple<int, WeightedGraphPolygonDecompositionAreaInfo*,
                     const WeightedGraphPolygonDecompositionBoundaryEdge*>
            candidate(10, nullptr, nullptr);

          for(const WeightedGraphPolygonDecompositionBoundaryEdge* e : connections.value(_dst))
          {
            clog_assert(e);
            clog_assert((e->end == this and e->start == _dst)
                        or (e->start == this and e->end == _dst));
            WeightedGraphPolygonDecompositionAreaInfo* other_area
              = e->areaInfo1 == _areaInfo ? e->areaInfo2 : e->areaInfo1;

            if(not _used_edges->contains(e)
               and (e->areaInfo1 == _areaInfo or e->areaInfo2 == _areaInfo)
               and (not _previous_edge
                    or (other_area != _previous_area and e->cannotConnectTo != _previous_edge)))
            {
              int connection = e->other(this)->connectionTo(_areaInfo);
              if(connection < std::get<0>(candidate))
              {
                candidate = std::make_tuple(connection, other_area, e);
              }
            }
          }
          if(std::get<2>(candidate))
          {
            auto [cccc, other_area, e] = candidate;
            Q_UNUSED(cccc);

            _used_edges->append(e);
            QList<QPointF> pts = e->points;
            if(e->end == this)
              std::reverse(pts.begin(), pts.end());
            return std::make_tuple(pts, other_area, e);
          }
          return std::make_tuple(QList<QPointF>(), nullptr, nullptr);
        }
        void addConnections(WeightedGraphPolygonDecompositionBoundaryNode* _dst,
                            WeightedGraphPolygonDecompositionBoundaryEdge* _edge)
        {
          QList<WeightedGraphPolygonDecompositionBoundaryEdge*>& cs = connections[_dst];
          cs.append(_edge);
          std::sort(cs.begin(), cs.end(),
                    [](WeightedGraphPolygonDecompositionBoundaryEdge* _a,
                       WeightedGraphPolygonDecompositionBoundaryEdge* _b)
                    { return _a->points.size() < _b->points.size(); });
        }
      };

      struct WeightedGraphPolygonDecompositionGridCell
      {
        qsizetype index;
        QPointF center;
        qreal gridSize;
        qreal area; // area of the grid cell = interesection of the rectangle with the polygon
        WeightedGraphPolygonDecompositionGridCell* top = nullptr;
        WeightedGraphPolygonDecompositionGridCell* bottom = nullptr;
        WeightedGraphPolygonDecompositionGridCell* left = nullptr;
        WeightedGraphPolygonDecompositionGridCell* right = nullptr;

        WeightedGraphPolygonDecompositionBoundaryNode* topLeft = nullptr;
        WeightedGraphPolygonDecompositionBoundaryNode* topRight = nullptr;
        WeightedGraphPolygonDecompositionBoundaryNode* bottomLeft = nullptr;
        WeightedGraphPolygonDecompositionBoundaryNode* bottomRight = nullptr;

        WeightedGraphPolygonDecompositionAreaInfo* currentOwner
          = nullptr; // is the owner of the current assignment
        WeightedGraphPolygonDecompositionAreaInfo* owner
          = nullptr; // is the owner according to the best assignement

        bool holeClustered = false;
        bool clustered = false;

        bool checkBorder(WeightedGraphPolygonDecompositionAreaInfo* _ai)
        {
          return (top and top->owner == _ai) or (bottom and bottom->owner == _ai)
                 or (left and left->owner == _ai) or (right and right->owner == _ai);
        }
        int borderCount(WeightedGraphPolygonDecompositionAreaInfo* _ai)
        {
          int count = 0;
          if(top and top->owner == _ai)
            ++count;
          if(bottom and bottom->owner == _ai)
            ++count;
          if(left and left->owner == _ai)
            ++count;
          if(right and right->owner == _ai)
            ++count;
          return count;
        }
        QPointF topLeftCoordinate() const
        {
          return center + QPointF(-gridSize * 0.5, -gridSize * 0.5);
        }
        QPointF topRightCoordinate() const
        {
          return center + QPointF(gridSize * 0.5, -gridSize * 0.5);
        }
        QPointF bottomLeftCoordinate() const
        {
          return center + QPointF(-gridSize * 0.5, gridSize * 0.5);
        }
        QPointF bottomRightCoordinate() const
        {
          return center + QPointF(gridSize * 0.5, gridSize * 0.5);
        }
      };

      struct WeightedGraphPolygonDecompositionBoundaryInfo
      {
        QList<WeightedGraphPolygonDecompositionGridCell*> nodes;
        WeightedGraphPolygonDecompositionGridCell* extremity1 = nullptr;
        WeightedGraphPolygonDecompositionGridCell* extremity2 = nullptr;

        QLineF line;

        void computeExtremeties()
        {
          if(nodes.size() == 1)
          {
            extremity1 = nodes.first();
            extremity2 = nodes.first();
          }
          else
          {
            qreal distance_sum_1 = 0;
            qreal distance_sum_2 = 0;
            for(WeightedGraphPolygonDecompositionGridCell* n : nodes)
            {
              qreal distance_sum = 0.0;
              for(WeightedGraphPolygonDecompositionGridCell* n2 : nodes)
              {
                distance_sum += ::distance2(n->center, n2->center);
              }
              if(distance_sum > distance_sum_1)
              {
                extremity2 = extremity1;
                distance_sum_2 = distance_sum_1;
                extremity1 = n;
                distance_sum_1 = distance_sum;
              }
              else if(distance_sum > distance_sum_2)
              {
                extremity2 = n;
                distance_sum_2 = distance_sum;
              }
              //             clog_assert(extremity1 != extremity2);
            }
            clog_assert(extremity1 != extremity2);
          }
          line.setPoints(extremity1->center, extremity2->center);
        }
        void append(WeightedGraphPolygonDecompositionGridCell* node)
        {
          if(not nodes.contains(node))
          {
            nodes.append(node);
          }
        }
        void remove(WeightedGraphPolygonDecompositionGridCell* node) { nodes.removeOne(node); }
      };

      struct WeightedGraphPolygonDecompositionAreaInfo
      {
        QPointF startPoint;
        QPointF currentOriginTmp;
        QPointF currentOrigin2;
        qreal weigth;
        double targetArea;
        int targetCellCount;
        double expectedRadius;
        qreal currentFactor;

        int currentCellCountTmp
          = 0; // cell count during optimisation step (ie number of cell.currentOwner == this)
        int ownedCellCountTmp = 0;  // cell count owned (ie number of cell.owner == this)
        qreal currentSurface = 0.0; // surface during optimisation step (ie sum of surface of
                                    // cell.#define HAVE_LIBCMAEScurrentOwner == this)
        qreal ownedSurface = 0.0;   // surface owned (ie sum of surface of cell.owner == this)
        qreal new_origin_x_sum = 0.0;
        qreal new_origin_y_sum = 0.0;

        QList<QList<WeightedGraphPolygonDecompositionGridCell*>> holeClusters
          = QList<QList<WeightedGraphPolygonDecompositionGridCell*>>();
        QList<WeightedGraphPolygonDecompositionBoundaryNode*> boundaryNodes
          = QList<WeightedGraphPolygonDecompositionBoundaryNode*>();

        QHash<WeightedGraphPolygonDecompositionAreaInfo*,
              WeightedGraphPolygonDecompositionBoundaryInfo>
          boundaryInfos = QHash<WeightedGraphPolygonDecompositionAreaInfo*,
                                WeightedGraphPolygonDecompositionBoundaryInfo>();
      };

      // Implementation
      QPointF WeightedGraphPolygonDecompositionBoundaryNode::coordinate() const
      {
        if(topLeft)
          return topLeft->bottomRightCoordinate();
        if(topRight)
          return topRight->bottomLeftCoordinate();
        if(bottomLeft)
          return bottomLeft->topRightCoordinate();
        clog_assert(
          bottomRight); // Actually should never reach that point, it has at least three neighbor...
        return bottomRight->topLeftCoordinate();
      }

      // Score functions

      typedef std::function<qreal(const QPointF&, const QPointF&)> NormT;
      /// This function is used in the first assignment step (lower is better)
      inline qreal computeAssignmentScore(const WeightedGraphPolygonDecompositionGridCell* node,
                                          const QPointF& _origin, const qreal currentRadius,
                                          NormT _norm)
      {
        const qreal distance = _norm(node->center, _origin);
        return distance / currentRadius;
      }

      /// This function is used during reassingment to handle holes (lower is better)
      inline qreal computeReAssignmentScore(const WeightedGraphPolygonDecompositionGridCell* node,
                                            const WeightedGraphPolygonDecompositionAreaInfo* area,
                                            NormT _norm)
      {
        const qreal distance = _norm(node->center, area->currentOrigin2);
        const qreal score = area->ownedSurface / area->targetArea * distance / area->expectedRadius;
        return score;
      }

      QHash<WeightedGraphPolygonDecompositionAreaInfo*,
            QList<WeightedGraphPolygonDecompositionAreaInfo*>>
        weightedGraphPolygonDecompositionAreaCurrentNeighbours(
          const QList<WeightedGraphPolygonDecompositionGridCell*>& gridCells)
      {
        QHash<WeightedGraphPolygonDecompositionAreaInfo*,
              QList<WeightedGraphPolygonDecompositionAreaInfo*>>
          result;

        for(WeightedGraphPolygonDecompositionGridCell* cell : gridCells)
        {
          QList<WeightedGraphPolygonDecompositionAreaInfo*>& infos = result[cell->currentOwner];
          auto f = [&infos, cell](WeightedGraphPolygonDecompositionGridCell* _other_cell)
          {
            if(_other_cell and _other_cell->currentOwner != cell->currentOwner
               and not infos.contains(_other_cell->currentOwner))
            {
              infos.append(_other_cell->currentOwner);
            }
            else if(not _other_cell and not infos.contains(nullptr))
            {
              infos.append(nullptr);
            }
          };
          f(cell->left);
          f(cell->top);
          f(cell->right);
          f(cell->bottom);
        }

        return result;
      }

      void weightedGraphPolygonDecompositionUpdateAssignment(
        const QList<WeightedGraphPolygonDecompositionAreaInfo*>& areas,
        const QList<WeightedGraphPolygonDecompositionGridCell*>& gridCells, NormT norm)
      {
        for(WeightedGraphPolygonDecompositionAreaInfo* areaInfo : areas)
        {
          areaInfo->currentSurface = 0.0;
          areaInfo->currentCellCountTmp = 0;
          areaInfo->new_origin_x_sum = 0.0;
          areaInfo->new_origin_y_sum = 0.0;
        }
        for(WeightedGraphPolygonDecompositionGridCell* node : gridCells)
        {
          WeightedGraphPolygonDecompositionAreaInfo* bestArea = nullptr;
          qreal best_score = std::numeric_limits<qreal>::max();
          for(WeightedGraphPolygonDecompositionAreaInfo* area : areas)
          {
            const qreal score = computeAssignmentScore(
              node, area->currentOriginTmp, area->expectedRadius / area->currentFactor, norm);
            if(score < best_score)
            {
              bestArea = area;
              best_score = score;
            }
          }
          bestArea->new_origin_x_sum += node->center.x();
          bestArea->new_origin_y_sum += node->center.y();
          bestArea->currentSurface += node->area;
          ++bestArea->currentCellCountTmp;
          node->currentOwner = bestArea;
        }
        for(WeightedGraphPolygonDecompositionAreaInfo* areaInfo : areas)
        {
          if(areaInfo->currentCellCountTmp == 0)
          { // Steal the closed grid cell
            WeightedGraphPolygonDecompositionGridCell* closestNode = nullptr;
            qreal closestNodeDistance = std::numeric_limits<qreal>::max();
            for(WeightedGraphPolygonDecompositionGridCell* node : gridCells)
            {
              if(node->currentOwner->currentCellCountTmp > 1)
              {
                const qreal dist = norm(node->center, areaInfo->currentOriginTmp);
                if(dist < closestNodeDistance)
                {
                  closestNodeDistance = dist;
                  closestNode = node;
                }
              }
            }
            clog_assert(closestNode);
            --closestNode->currentOwner->currentCellCountTmp;
            closestNode->currentOwner->new_origin_x_sum -= closestNode->center.x();
            closestNode->currentOwner->new_origin_y_sum -= closestNode->center.y();
            closestNode->currentOwner->currentSurface -= closestNode->area;
            areaInfo->currentCellCountTmp = 1;
            areaInfo->new_origin_x_sum = closestNode->center.x();
            areaInfo->new_origin_y_sum = closestNode->center.y();
            areaInfo->currentSurface = closestNode->area;
            closestNode->currentOwner = areaInfo;
          }
        }
      }

      void weightedGraphPolygonDecompositionPotentialField(
        const QList<WeightedGraphPolygonDecompositionAreaInfo*>& areas,
        const QList<WeightedGraphPolygonDecompositionGridCell*>& gridCells, NormT norm,
        const WeightedGraphPolygonDecompositionParameters& _parameters, const QRectF& _boundingRect)
      {
        qreal best_assignment_error = std::numeric_limits<qreal>::max();
        for(int i = 0; i < _parameters.maxIterations; ++i)
        {
          weightedGraphPolygonDecompositionUpdateAssignment(areas, gridCells, norm);
          QHash<WeightedGraphPolygonDecompositionAreaInfo*,
                QList<WeightedGraphPolygonDecompositionAreaInfo*>>
            neighborInfo = weightedGraphPolygonDecompositionAreaCurrentNeighbours(gridCells);

          bool all_has_converged = true;
          qreal factor_sum = 0.0;
          qreal assignment_error = 0.0;
          bool valid_assignment = true;
          for(WeightedGraphPolygonDecompositionAreaInfo* areaInfo : areas)
          {
            if(areaInfo->currentCellCountTmp > 0)
            {
              areaInfo->currentOriginTmp.setX(areaInfo->new_origin_x_sum
                                              / areaInfo->currentCellCountTmp);
              areaInfo->currentOriginTmp.setY(areaInfo->new_origin_y_sum
                                              / areaInfo->currentCellCountTmp);
            }
            qreal convergence_error
              = std::max<qreal>(0.0001, areaInfo->currentSurface) / qreal(areaInfo->targetArea);
            qreal adjustment_factor
              = (1
                 + (_parameters.maxIterations - i) / qreal(_parameters.maxIterations)
                     * (convergence_error - 1) * 0.5);
            areaInfo->currentFactor *= adjustment_factor;
            assignment_error += pow2(convergence_error - 1);

            bool has_converged = std::abs(convergence_error - 1) <= _parameters.tol;
            all_has_converged = all_has_converged and has_converged;
            if(neighborInfo[areaInfo].size() == 1 and neighborInfo[areaInfo][0] != nullptr)
            {
              QPointF f = areaInfo->currentOriginTmp - _boundingRect.topLeft();
              f = QPointF(f.x() / _boundingRect.width(), f.y() / _boundingRect.height());
              QPointF fs(f.x() < 0.5 ? f.x() : 1.0 - f.x(), f.y() < 0.5 ? f.y() : 1.0 - f.y());
              if(fs.x() > fs.y())
              {
                if(f.x() < 0.5)
                  areaInfo->currentOriginTmp.setX(_boundingRect.left());
                else
                  areaInfo->currentOriginTmp.setX(_boundingRect.right());
              }
              else
              {
                if(f.y() < 0.5)
                  areaInfo->currentOriginTmp.setY(_boundingRect.top());
                else
                  areaInfo->currentOriginTmp.setY(_boundingRect.bottom());
              }
              if(areaInfo->currentCellCountTmp == 0)
              {
                areaInfo->currentFactor *= 2;
              }
              valid_assignment = false;
            }
            factor_sum += areaInfo->currentFactor * areaInfo->currentFactor;
          }
          if(assignment_error < best_assignment_error and valid_assignment)
          {
            best_assignment_error = assignment_error;
            for(WeightedGraphPolygonDecompositionGridCell* node : gridCells)
            {
              node->owner = node->currentOwner;
            }
            for(WeightedGraphPolygonDecompositionAreaInfo* area : areas)
            {
              area->ownedCellCountTmp = area->currentCellCountTmp;
              area->ownedSurface = area->currentSurface;
              area->currentOrigin2 = area->currentOriginTmp;
            }
            if(all_has_converged)
            {
              clog_info("Converged in %{} iterations!", i);
              break;
            }
          }
          // Normalise factors
          qreal factor_norm = std::sqrt(factor_sum);
          for(WeightedGraphPolygonDecompositionAreaInfo* areaInfo : areas)
          {
            areaInfo->currentFactor /= factor_norm;
          }
        }
      }

      struct Fitness
      {
        QList<WeightedGraphPolygonDecompositionAreaInfo*> areas;
        QList<WeightedGraphPolygonDecompositionGridCell*> gridCells;
        NormT norm;
        WeightedGraphPolygonDecompositionParameters::ObjectiveFunctionFatness objectiveFunction;
        qreal tolerance;

        struct CMAESAssignmentInfo
        {
          QPointF center;
          qreal expectedRadius;
          qreal area = 0.0;
          int assigned = 0;
          qreal sum_x = 0.0;
          qreal sum_y = 0.0;
          QPointF newCenter = QPointF();
          qreal perimeter = 0.0;
        };

        static void updateCurrentAreaPerimeters(
          const QList<WeightedGraphPolygonDecompositionGridCell*>& gridCells,
          const std::vector<CMAESAssignmentInfo*>& gridCellsToArea)
        {
          clog_assert(gridCells.size() == (int)gridCellsToArea.size());
          for(int i = 0; i < gridCells.size(); ++i)
          {
            WeightedGraphPolygonDecompositionGridCell* cell = gridCells[i];
            CMAESAssignmentInfo* ai = gridCellsToArea[i];
            if(not cell->top or gridCellsToArea[cell->top->index] != ai)
              ai->perimeter += cell->gridSize;
            if(not cell->bottom or gridCellsToArea[cell->bottom->index] != ai)
              ai->perimeter += cell->gridSize;
            if(not cell->left or gridCellsToArea[cell->left->index] != ai)
              ai->perimeter += cell->gridSize;
            if(not cell->right or gridCellsToArea[cell->right->index] != ai)
              ai->perimeter += cell->gridSize;
          }
        }

        double operator()(const double* x, const int& N) const
        {
          clog_assert(N == areas.size() * 3);
          std::vector<CMAESAssignmentInfo> assignmentInfos;
          assignmentInfos.reserve(areas.size());
          for(int i = 0; i < areas.size(); ++i)
          {
            assignmentInfos.push_back(
              CMAESAssignmentInfo{QPointF(x[3 * i], x[3 * i + 1]), x[3 * i + 2]});
          }
          std::vector<CMAESAssignmentInfo*> gridCellsToArea;
          gridCellsToArea.reserve(gridCells.size());
          // 1) compute assignement
          for(WeightedGraphPolygonDecompositionGridCell* node : gridCells)
          {
            CMAESAssignmentInfo* best_area = nullptr;
            qreal best_score = std::numeric_limits<qreal>::max();
            for(CMAESAssignmentInfo& area : assignmentInfos)
            {
              const qreal score
                = computeAssignmentScore(node, area.center, area.expectedRadius, norm);
              if(score < best_score)
              {
                best_area = &area;
                best_score = score;
              }
            }
            best_area->area += node->area;
            best_area->sum_x += node->center.x();
            best_area->sum_y += node->center.y();
            ++best_area->assigned;
            gridCellsToArea.push_back(best_area);
          }
          // 2) compute size fitness
          qreal fitness_size = 0.0;
          qreal max_error = 0.0;
          for(int i = 0; i < areas.size(); ++i)
          {
            qreal err = (areas[i]->targetArea - assignmentInfos[i].area) / areas[i]->targetArea;
            max_error = std::max(err, max_error);
            fitness_size += err * err;
            assignmentInfos[i].newCenter
              = QPointF(assignmentInfos[i].sum_x, assignmentInfos[i].sum_y)
                / assignmentInfos[i].assigned;
          }
          // 3) Compute fatness fitness
          qreal fitness_fatness = 0.0;

          switch(objectiveFunction)
          {
          case WeightedGraphPolygonDecompositionParameters::ObjectiveFunctionFatness::Schwartzberg:
          {
            updateCurrentAreaPerimeters(gridCells, gridCellsToArea);
            for(int i = 0; i < areas.size(); ++i)
            {
              fitness_fatness += 2.0 * M_PI * std::sqrt(assignmentInfos[i].area * M_1_PI)
                                 / assignmentInfos[i].perimeter;
            }
            break;
          }
          case WeightedGraphPolygonDecompositionParameters::ObjectiveFunctionFatness::PolsbyPopper:
          {
            updateCurrentAreaPerimeters(gridCells, gridCellsToArea);
            for(int i = 0; i < areas.size(); ++i)
            {
              fitness_fatness
                += 4.0 * M_PI
                   * (assignmentInfos[i].area
                      / (assignmentInfos[i].perimeter * assignmentInfos[i].perimeter));
            }
            break;
          }
          }
          return std::sqrt(fitness_size) - fitness_fatness
                 + 1e6 * pow2(std::max(0.0, std::abs(max_error) - tolerance));
        }
      };

      void weightedGraphPolygonDecompositionStateToStructure(
        const double* x, const QList<WeightedGraphPolygonDecompositionAreaInfo*>& areas,
        const QList<WeightedGraphPolygonDecompositionGridCell*>& gridCells, NormT norm)
      {
        for(int i = 0; i < areas.size(); ++i)
        {
          areas[i]->currentOriginTmp = QPointF(x[3 * i], x[3 * i + 1]);
          areas[i]->currentOrigin2 = areas[i]->currentOriginTmp;
          areas[i]->currentFactor = areas[i]->expectedRadius / x[3 * i + 2];
        }
        // TODO this is a bit ugly...
        // Generate solution
        weightedGraphPolygonDecompositionUpdateAssignment(areas, gridCells, norm);
        // Copy current to actual
        for(WeightedGraphPolygonDecompositionGridCell* node : gridCells)
        {
          node->owner = node->currentOwner;
        }
        for(WeightedGraphPolygonDecompositionAreaInfo* area : areas)
        {
          area->ownedCellCountTmp = area->currentCellCountTmp;
          area->ownedSurface = area->currentSurface;
        }
      }

#ifdef HAVE_LIBCMAES
      void weightedGraphPolygonDecompositionCMAES(
        const QList<WeightedGraphPolygonDecompositionAreaInfo*>& areas,
        const QList<WeightedGraphPolygonDecompositionGridCell*>& gridCells, NormT norm,
        const WeightedGraphPolygonDecompositionParameters& _parameters)
      {
        std::vector<double> x0;
        x0.reserve(areas.size() * 3);
        for(WeightedGraphPolygonDecompositionAreaInfo* area : areas)
        {
          x0.push_back(area->currentOrigin2.x());
          x0.push_back(area->currentOrigin2.y());
          x0.push_back(area->expectedRadius / area->currentFactor);
        }

        double sigma = 10;
        libcmaes::FitFunc fitness(
          Fitness{areas, gridCells, norm, _parameters.objectiveFunctionFatness});
        libcmaes::CMAParameters<> cmaparams(x0, sigma);
        libcmaes::CMASolutions cmasols = libcmaes::cmaes<>(fitness, cmaparams);
        clog_info("status = %{} iters = %{} nevals = %{}", cmasols.status_msg(), cmasols.niter(),
                  cmasols.nevals());
        std::vector<double> x_r_2 = cmasols.get_best_seen_candidate().get_x();
        fitness(x_r_2.data(), 3 * areas.size());
        weightedGraphPolygonDecompositionStateToStructure(x_r_2.data(), areas, gridCells, norm);
      }
#endif
      void weightedGraphPolygonDecompositionRandomSearch(
        const QList<WeightedGraphPolygonDecompositionAreaInfo*>& areas,
        const QList<WeightedGraphPolygonDecompositionGridCell*>& gridCells, NormT norm,
        const WeightedGraphPolygonDecompositionParameters& _parameters)
      {
        Eigen::VectorXd x(areas.size() * 3);
        for(int i = 0; i < areas.size(); ++i)
        {
          WeightedGraphPolygonDecompositionAreaInfo* area = areas[i];
          x[i * 3] = area->currentOrigin2.x();
          x[i * 3 + 1] = area->currentOrigin2.y();
          x[i * 3 + 2] = area->expectedRadius / area->currentFactor;
        }
        Fitness fitness{areas, gridCells, norm, _parameters.objectiveFunctionFatness};
        double best_fitness = fitness(x.data(), x.size());

        for(std::size_t i = 0; i < 10000; ++i)
        {
          Eigen::VectorXd cx = x + 100 * Eigen::VectorXd::Random(x.size());
          double nfitness = fitness(cx.data(), cx.size());
          if(nfitness < best_fitness)
          {
            best_fitness = nfitness;
            x = cx;
          }
        }
        weightedGraphPolygonDecompositionStateToStructure(x.data(), areas, gridCells, norm);
      }

      void weightedGraphPolygonDecompositionOptimisation(
        WeightedGraphPolygonDecompositionParameters::Optimisation _opt,
        const QList<WeightedGraphPolygonDecompositionAreaInfo*>& areas,
        const QList<WeightedGraphPolygonDecompositionGridCell*>& gridCells, NormT norm,
        const WeightedGraphPolygonDecompositionParameters& _parameters, const QRectF& _boundingRect)
      {
        switch(_opt)
        {
        case WeightedGraphPolygonDecompositionParameters::Optimisation::None:
          return;
        case WeightedGraphPolygonDecompositionParameters::Optimisation::PotentialField:
          weightedGraphPolygonDecompositionPotentialField(areas, gridCells, norm, _parameters,
                                                          _boundingRect);
          break;
        case WeightedGraphPolygonDecompositionParameters::Optimisation::CMAES:
#ifdef HAVE_LIBCMAES
          weightedGraphPolygonDecompositionCMAES(areas, gridCells, norm, _parameters);
#else
          clog_fatal("Cartography is build without CMAES support!");
#endif
          break;
        case WeightedGraphPolygonDecompositionParameters::Optimisation::RandomSearch:
          weightedGraphPolygonDecompositionRandomSearch(areas, gridCells, norm, _parameters);
          break;
        default:
          clog_fatal("Unimplemented optimisation technique!");
        }
      }
    } // namespace Details
    EuclidSystem::multi_polygon weightedGraphPolygonDecomposition(
      const EuclidSystem::polygon& _source, const CoordinateSystem& _geometry_cs,
      const WeightedGraphPolygonDecompositionParameters& _parameters,
      WeightedGraphPolygonDecompositionStatistics* _statistics)
    {
      if(_parameters.weights.size() == 1)
      {
        EuclidSystem::multi_polygon::builder b;
        b.add_element(_source);
        return b.create();
      }
      NormT norm;
      switch(_parameters.norm)
      {
      case WeightedGraphPolygonDecompositionParameters::Norm::Norm1:
        norm = &::manhattan;
        break;
      case WeightedGraphPolygonDecompositionParameters::Norm::Norm2:
        norm = &::distance2;
        break;
      }
      CurvilinearCoordinates cc(_source.exterior_ring());
#if 0 
      EuclidSystem::multi_point points;
      for(euclid::simple::point pt : _source.exterior_ring())
      {
      points.addGeometry(&pt);
    }
#endif
      // BEGIN Helpers
      auto getOwner = [](WeightedGraphPolygonDecompositionGridCell* _cell)
      { return _cell ? _cell->owner : nullptr; };
      auto getTopLeftCell = [](WeightedGraphPolygonDecompositionGridCell* _cell) {
        return _cell->top ? _cell->top->left : _cell->left ? _cell->left->top : nullptr;
      };
      auto getTopRightCell = [](WeightedGraphPolygonDecompositionGridCell* _cell) {
        return _cell->top ? _cell->top->right : _cell->right ? _cell->right->top : nullptr;
      };
      auto getBottomLeftCell = [](WeightedGraphPolygonDecompositionGridCell* _cell) {
        return _cell->bottom ? _cell->bottom->left : _cell->left ? _cell->left->bottom : nullptr;
      };
      auto getBottomRightCell = [](WeightedGraphPolygonDecompositionGridCell* _cell) {
        return _cell->bottom ? _cell->bottom->right : _cell->right ? _cell->right->bottom : nullptr;
      };
      auto getTopLeft = [](WeightedGraphPolygonDecompositionGridCell* _cell)
      { return _cell ? &_cell->topLeft : nullptr; };
      auto getTopRight = [](WeightedGraphPolygonDecompositionGridCell* _cell)
      { return _cell ? &_cell->topRight : nullptr; };
      auto getBottomLeft = [](WeightedGraphPolygonDecompositionGridCell* _cell)
      { return _cell ? &_cell->bottomLeft : nullptr; };
      auto getBottomRight = [](WeightedGraphPolygonDecompositionGridCell* _cell)
      { return _cell ? &_cell->bottomRight : nullptr; };
      // END Helpers
      //  Steps
      // BEGIN 1) Grid decomposition
      euclid::simple::box envelope = euclid::ops::envelope(_source);
      QRectF boundingRect(envelope.left(), envelope.top(), envelope.width(), envelope.height());

      //     const double grid_size = std::min(boundingRect.width(), boundingRect.height()) *
      //     _parameters.tol;
      const double min_weight
        = *std::min_element(_parameters.weights.begin(), _parameters.weights.end());
      const double grid_size = std::sqrt(_parameters.tol * euclid::ops::area(_source) * min_weight);
      //         const double grid_area = grid_size * grid_size;

      QList<WeightedGraphPolygonDecompositionGridCell*> gridCells;

      QList<WeightedGraphPolygonDecompositionGridCell*> previous_row;

      qreal total_surface = 0.0;

      CoordinateSystem geometry_cs = _geometry_cs;
      if(not geometry_cs.isValid())
      {
        clog_warning("Geometry has no spatial reference, assuming WGS84!");
        geometry_cs = CoordinateSystem::wgs84;
      }

      double rasterMinValue = 0.0, rasterMaxValue = 0.0;
      if(_parameters.raster.isValid())
      {
        rasterMinValue = _parameters.raster.minimumValue(_parameters.rasterBand);
        rasterMaxValue = _parameters.raster.maximumValue(_parameters.rasterBand);
      }

      for(double y = envelope.top(); y < envelope.bottom(); y += grid_size)
      {
        QList<WeightedGraphPolygonDecompositionGridCell*> current_row;
        WeightedGraphPolygonDecompositionGridCell* previous_node = nullptr;
        for(double x = envelope.left(); x < envelope.right(); x += grid_size)
        {
          euclid::geos::polygon geos_polygon = euclid::geos::polygon::builder()
                                                 .add_point(x, y)
                                                 .add_point(x + grid_size, y)
                                                 .add_point(x + grid_size, y + grid_size)
                                                 .add_point(x, y + grid_size)
                                                 .add_point(x, y)
                                                 .create();

          QPointF center(x + 0.5 * grid_size, y + 0.5 * grid_size);
          WeightedGraphPolygonDecompositionGridCell* new_node = nullptr;

          euclid::geos::geometry geos_geom = euclid::ops::intersection(_source, geos_polygon);
          double geom_surface = euclid::ops::area(geos_geom);

          if(geom_surface > 0)
          {
            if(_parameters.raster.isValid())
            {
              Point tL(center.x() - 0.5 * grid_size, center.y() - 0.5 * grid_size, geometry_cs);
              Point bR(center.x() + 0.5 * grid_size, center.y() + 0.5 * grid_size, geometry_cs);

              geom_surface = _parameters.raster.sampleRect(
                _parameters.rasterBand, tL, bR, Raster::Raster::SampleMode::AveragePositive);
              if(std::isnan(geom_surface) or geom_surface > rasterMaxValue
                 or geom_surface < rasterMinValue)
                geom_surface = 0;
              clog_assert(geom_surface >= 0);
            }
            new_node = new WeightedGraphPolygonDecompositionGridCell{gridCells.size(), center,
                                                                     grid_size, geom_surface};
            total_surface += geom_surface;
          }

          if(new_node)
          {
            WeightedGraphPolygonDecompositionGridCell* previous_row_node
              = (current_row.size() < previous_row.size()) ? previous_row[current_row.size()]
                                                           : nullptr;
            if(previous_row_node)
            {
              clog_assert(previous_row_node->bottom == nullptr);
              previous_row_node->bottom = new_node;
              new_node->top = previous_row_node;
            }
            if(previous_node)
            {
              previous_node->right = new_node;
              new_node->left = previous_node;
            }
            gridCells.append(new_node);
          }
          previous_node = new_node;
          current_row.append(new_node);
        }
        clog_assert(previous_row.size() == 0 or previous_row.size() == current_row.size());
        previous_row = current_row;
      }

      if(gridCells.size() < _parameters.weights.size())
      {
        clog_error("Too high tolerance, not enough grid cells...");
        return EuclidSystem::multi_polygon();
      }
      clog_assert(total_surface > 0);
      if(not _parameters.raster.isValid())
      {
        clog_near_equal_assert(euclid::ops::area(_source), total_surface,
                               (1e-3 * euclid::ops::area(_source)));
      }
      // END grid decompostion
      // BEGIN 2) start points
      QList<WeightedGraphPolygonDecompositionAreaInfo*> areas;
      for(int i = 0; i < _parameters.weights.size(); ++i)
      {
        euclid::simple::point start_ogr = i < _parameters.starts.size()
                                            ? _parameters.starts[i]
                                            : cc(i / double(_parameters.weights.size()));
        QPointF start(start_ogr.x(), start_ogr.y());
        qreal target_surface = total_surface * _parameters.weights[i];
        qreal expected_radius = std::sqrt(target_surface * M_1_PI);
        areas.append(new WeightedGraphPolygonDecompositionAreaInfo{
          start, start, start, _parameters.weights[i], target_surface,
          std::max(1, int(std::round(target_surface / total_surface * gridCells.size()))),
          expected_radius, qreal(1.0) / _parameters.weights.size()});
        clog_assert(std::isfinite(areas.last()->expectedRadius));
      }
      // END start points
      // BEGIN 3) Assignements
      //  3a) pre-optimisation
      clog_assert(_parameters.optimisation1
                  != WeightedGraphPolygonDecompositionParameters::Optimisation::None);
      weightedGraphPolygonDecompositionOptimisation(_parameters.optimisation1, areas, gridCells,
                                                    norm, _parameters, boundingRect);
      weightedGraphPolygonDecompositionOptimisation(_parameters.optimisation2, areas, gridCells,
                                                    norm, _parameters, boundingRect);

      // END assignments
      // BEGIN 4) Make sure all division are continuous
      //  4a) Cluster decomposition nodes
      for(WeightedGraphPolygonDecompositionGridCell* node : gridCells)
      {
        if(not node->holeClustered)
        {
          QList<WeightedGraphPolygonDecompositionGridCell*> cluster;
          QList<WeightedGraphPolygonDecompositionGridCell*> to_investigate;
          auto cluster_check
            = [&cluster, &to_investigate](WeightedGraphPolygonDecompositionGridCell* node,
                                          WeightedGraphPolygonDecompositionGridCell* source_node)
          {
            if(node and not node->holeClustered and node->owner == source_node->owner)
            {
              to_investigate.append(node);
              cluster.append(node);
              node->holeClustered = true;
            }
          };
          cluster.append(node);
          to_investigate.append(node);
          node->holeClustered = true;
          while(not to_investigate.isEmpty())
          {
            WeightedGraphPolygonDecompositionGridCell* n = to_investigate.takeFirst();
            cluster_check(n->top, n);
            cluster_check(n->bottom, n);
            cluster_check(n->left, n);
            cluster_check(n->right, n);
          }
          node->owner->holeClusters.append(cluster);
        }
      }

      // 4b) remove the hole
      QList<WeightedGraphPolygonDecompositionGridCell*> discarded;
      for(WeightedGraphPolygonDecompositionAreaInfo* areaInfo : areas)
      {
        int cellCount = 0;
        if(areaInfo->holeClusters.size() > 1)
        {
          if(_statistics)
          {
            ++_statistics->discountinuityCount;
          }
          // Keep the biggest one
          int keep_index = 0;
          int keep_size = areaInfo->holeClusters[0].size();
          for(int i = 1; i < areaInfo->holeClusters.size(); ++i)
          {
            if(areaInfo->holeClusters[i].size() > keep_size)
            {
              keep_size = areaInfo->holeClusters[i].size();
              keep_index = i;
            }
          }
          for(int i = 0; i < areaInfo->holeClusters.size(); ++i)
          {
            if(i != keep_index)
            {
              cellCount += areaInfo->holeClusters[i].size();
              for(WeightedGraphPolygonDecompositionGridCell* node : areaInfo->holeClusters[i])
              {
                node->owner = nullptr; // disown
                areaInfo->ownedSurface -= node->area;
              }
              areaInfo->ownedCellCountTmp -= areaInfo->holeClusters[i].size();
              discarded.append(areaInfo->holeClusters[i]);
            }
          }
        }
        if(_statistics)
        {
          _statistics->gridCellExchangedCounts.append(cellCount);
        }
      }
      while(not discarded.isEmpty())
      {
        int current_discarded_size = discarded.size();
        for(int i = 0; i < discarded.size();)
        {
          WeightedGraphPolygonDecompositionAreaInfo* bestAreaInfo = nullptr;
          qreal bestScore = std::numeric_limits<qreal>::max();
          ;
          WeightedGraphPolygonDecompositionGridCell* node = discarded[i];
          clog_assert(node->owner == nullptr);
          auto discard_check = [node, &bestAreaInfo, &bestScore,
                                norm](WeightedGraphPolygonDecompositionGridCell* n_node)
          {
            if(n_node and n_node->owner)
            {
              qreal score = computeReAssignmentScore(node, n_node->owner, norm);
              if(score < bestScore)
              {
                bestScore = score;
                bestAreaInfo = n_node->owner;
              }
            }
          };

          discard_check(node->top);
          discard_check(node->bottom);
          discard_check(node->left);
          discard_check(node->right);

          if(bestAreaInfo)
          {
            node->owner = bestAreaInfo;
            bestAreaInfo->ownedCellCountTmp++;
            bestAreaInfo->ownedSurface += node->area;
            discarded.removeAt(i);
          }
          else
          {
            ++i;
          }
        }
        clog_assert(current_discarded_size != discarded.size()); // Check we do remove nodes
      }
      // END hole removal
      // BEGIN 5) Build the boundary nodes
      for(WeightedGraphPolygonDecompositionGridCell* node : gridCells)
      {
        auto check_boundary = [node](WeightedGraphPolygonDecompositionGridCell* otherNode)
        {
          if(otherNode and node->owner != otherNode->owner)
          {
            node->owner->boundaryInfos[otherNode->owner].append(node);
          }
        };
        check_boundary(node->top);
        check_boundary(node->bottom);
        check_boundary(node->left);
        check_boundary(node->right);
      }

      for(WeightedGraphPolygonDecompositionAreaInfo* ai : areas)
      {
        for(auto it = ai->boundaryInfos.begin(); it != ai->boundaryInfos.end(); ++it)
        {
          it->computeExtremeties();
        }
      }
      // END
      // BEGIN 6) adjust boundaries, if possible
      QList<WeightedGraphPolygonDecompositionAreaInfo*> sortedAreaInfo = areas;
      auto sortAreaFunction = [](WeightedGraphPolygonDecompositionAreaInfo* _a,
                                 WeightedGraphPolygonDecompositionAreaInfo* _b)
      {
        qreal a_score = _a->ownedSurface / _a->targetArea;
        qreal b_score = _b->ownedSurface / _b->targetArea;
        return a_score < b_score; // we want to order the areas according to the one that is missing
                                  // the most, and have at the bottom the one that has too much
      };
      std::sort(sortedAreaInfo.begin(), sortedAreaInfo.end(), sortAreaFunction);

      while(true)
      {
        bool has_done_something = false;
        for(WeightedGraphPolygonDecompositionAreaInfo* ai : sortedAreaInfo)
        {
          if(std::abs(1.0 - ai->ownedSurface / ai->targetArea) < _parameters.tol)
          {
            break;
          }
          WeightedGraphPolygonDecompositionAreaInfo* best_nai = nullptr;
          qreal best_score = 0;
          for(WeightedGraphPolygonDecompositionAreaInfo* ai2 : ai->boundaryInfos.keys())
          {
            clog_assert(ai != ai2);
            if(ai2->currentCellCountTmp > 1 and ai2->ownedSurface > ai2->targetArea)
            {
              qreal score = ai2->ownedSurface / ai2->targetArea;
              if(score > best_score)
              {
                best_nai = ai2;
                score = best_score;
              }
            }
          }
          if(best_nai)
          {
            WeightedGraphPolygonDecompositionGridCell* nodeToSteal = nullptr;
            int bestBorderCount = 4;
            qreal nodeToStealScore = std::numeric_limits<qreal>::max();
            QList<WeightedGraphPolygonDecompositionGridCell*> nodes
              = best_nai->boundaryInfos[ai].nodes;
            for(WeightedGraphPolygonDecompositionGridCell* node : nodes)
            {
              int borderCount = node->borderCount(best_nai);
              if(std::abs(1.0 - node->owner->ownedSurface / node->owner->targetArea) > std::abs(
                   1.0 - (node->owner->ownedSurface - node->area) / node->owner->targetArea))
              { // only allow stealing if it would not degrade the error too much TODO could improve
                // with checking the overall error
                if(borderCount <= bestBorderCount and node->borderCount(ai) > 1)
                {
                  WeightedGraphPolygonDecompositionAreaInfo* current_owner
                    = getOwner(getTopLeftCell(node));
                  int count = 0;
                  auto countOwnerChanges = [&current_owner, node, &count, getOwner](
                                             WeightedGraphPolygonDecompositionGridCell* _neighb)
                  {
                    WeightedGraphPolygonDecompositionAreaInfo* new_owner = getOwner(_neighb);
                    if(new_owner != current_owner)
                    {
                      if(new_owner == node->owner or current_owner == node->owner)
                      {
                        ++count;
                        current_owner = new_owner;
                      }
                    }
                  };
                  countOwnerChanges(node->top);
                  countOwnerChanges(getTopRightCell(node));
                  countOwnerChanges(node->right);
                  countOwnerChanges(getBottomRightCell(node));
                  countOwnerChanges(node->bottom);
                  countOwnerChanges(getBottomLeftCell(node));
                  countOwnerChanges(node->left);
                  countOwnerChanges(getTopLeftCell(node));
                  clog_assert(count > 0);
                  if(count == 1)
                  {
                    const qreal distance
                      = ::distance(node->center, best_nai->boundaryInfos[ai].line);
                    if(distance < nodeToStealScore or borderCount < bestBorderCount)
                    {
                      nodeToSteal = node;
                      nodeToStealScore = distance;
                      bestBorderCount = borderCount;
                    }
                  }
                }
              }
            }
            if(nodeToSteal)
            {
              nodeToSteal->owner = ai;
              auto updateBorder = [ai, best_nai](WeightedGraphPolygonDecompositionGridCell* n)
              {
                if(n)
                {
                  if(n->owner == ai and not n->checkBorder(best_nai))
                  {
                    ai->boundaryInfos[best_nai].remove(n);
                  }
                  if(n->owner == best_nai and n->checkBorder(ai))
                  {
                    best_nai->boundaryInfos[ai].append(n);
                  }
                }
              };
              updateBorder(nodeToSteal->top);
              updateBorder(nodeToSteal->bottom);
              updateBorder(nodeToSteal->left);
              updateBorder(nodeToSteal->right);
              ++ai->ownedCellCountTmp;
              --best_nai->ownedCellCountTmp;
              ai->ownedSurface += nodeToSteal->area;
              best_nai->ownedSurface -= nodeToSteal->area;
              has_done_something = true;
              break;
            }
          }
        }
        if(not has_done_something)
          break;
        std::sort(sortedAreaInfo.begin(), sortedAreaInfo.end(), sortAreaFunction); // Resort
      }
      // END
      // BEGIN 7) Generate graph of borders
      QList<WeightedGraphPolygonDecompositionBoundaryNode*> nodes;
      QList<WeightedGraphPolygonDecompositionBoundaryEdge*> edges;

      // 7a) Find all the points that borders more than two polygon (interesction points of the
      // polylines)
      auto setNode = [](const std::function<WeightedGraphPolygonDecompositionBoundaryNode**(
                          WeightedGraphPolygonDecompositionGridCell*)>& _getNode,
                        WeightedGraphPolygonDecompositionGridCell* _cell,
                        WeightedGraphPolygonDecompositionBoundaryNode* _node)
      {
        if(_getNode(_cell))
          *_getNode(_cell) = _node;
      };

      auto createBoundaryNode
        = [&nodes, areas, setNode, getTopLeft, getTopRight, getBottomLeft, getBottomRight,
           getOwner](WeightedGraphPolygonDecompositionBoundaryNode** _node,
                     WeightedGraphPolygonDecompositionGridCell* _cellTL,
                     WeightedGraphPolygonDecompositionGridCell* _cellTR,
                     WeightedGraphPolygonDecompositionGridCell* _cellBL,
                     WeightedGraphPolygonDecompositionGridCell* _cellBR)
      {
        if(not *_node)
        {
          QList<WeightedGraphPolygonDecompositionAreaInfo*> parents;
          WeightedGraphPolygonDecompositionAreaInfo* pTL = getOwner(_cellTL);
          parents.append(pTL);
          WeightedGraphPolygonDecompositionAreaInfo* pTR = getOwner(_cellTR);
          if(not parents.contains(pTR))
            parents.append(pTR);
          WeightedGraphPolygonDecompositionAreaInfo* pBL = getOwner(_cellBL);
          if(not parents.contains(pBL))
            parents.append(pBL);
          WeightedGraphPolygonDecompositionAreaInfo* pBR = getOwner(_cellBR);
          if(not parents.contains(pBR))
            parents.append(pBR);
          if(parents.count() >= 3)
          {
            *_node = new WeightedGraphPolygonDecompositionBoundaryNode{_cellTL, _cellTR, _cellBL,
                                                                       _cellBR};
            nodes.append(*_node);
            setNode(getBottomRight, _cellTL, *_node);
            setNode(getBottomLeft, _cellTR, *_node);
            setNode(getTopRight, _cellBL, *_node);
            setNode(getTopLeft, _cellBR, *_node);
            for(WeightedGraphPolygonDecompositionAreaInfo* areaInfo : parents)
            {
              if(areaInfo)
              {
                areaInfo->boundaryNodes.append(*_node);
              }
            }
          }
        }
      };

      for(WeightedGraphPolygonDecompositionGridCell* node : gridCells)
      {
        createBoundaryNode(&node->topLeft,
                           node->left  ? node->left->top
                           : node->top ? node->top->left
                                       : nullptr,
                           node->top, node->left, node);
        createBoundaryNode(&node->topRight, node->top,
                           node->right ? node->right->top
                           : node->top ? node->top->right
                                       : nullptr,
                           node, node->right);
        createBoundaryNode(&node->bottomLeft, node->left, node,
                           node->left     ? node->left->bottom
                           : node->bottom ? node->bottom->left
                                          : nullptr,
                           node->bottom);
        createBoundaryNode(&node->bottomRight, node, node->right, node->bottom,
                           node->right    ? node->right->bottom
                           : node->bottom ? node->bottom->right
                                          : nullptr);
      }

      // 7b create edge connections
      struct ExploreR;
      typedef std::function<ExploreR(WeightedGraphPolygonDecompositionBoundaryEdge * e,
                                     const QPointF& _point,
                                     WeightedGraphPolygonDecompositionGridCell* _left,
                                     WeightedGraphPolygonDecompositionGridCell* _right)>
        ExploreF;
      struct ExploreR
      {
        WeightedGraphPolygonDecompositionBoundaryNode* endnode;
        QPointF pt;
        WeightedGraphPolygonDecompositionGridCell* _1;
        WeightedGraphPolygonDecompositionGridCell* _2;
        ExploreF func;
      };

      ExploreF exploreUp, exploreDown, exploreLeft, exploreRight;

      exploreUp = [grid_size, getOwner, &exploreUp, &exploreLeft, &exploreRight](
                    WeightedGraphPolygonDecompositionBoundaryEdge* e, const QPointF& _point,
                    WeightedGraphPolygonDecompositionGridCell* _left,
                    WeightedGraphPolygonDecompositionGridCell* _right)
      {
        clog_assert(_left or _right);
        clog_assert(getOwner(_left) != getOwner(_right));
        clog_assert(not _left or _left->right == _right);

        // Check termination
        if(_left and _left->topRight)
        {
          _left->topRight->bottomEdge = e;
          return ExploreR{_left->topRight, QPointF(), nullptr, nullptr, nullptr};
        }
        if(_right and _right->topLeft)
        {
          _right->topLeft->bottomEdge = e;
          return ExploreR{_right->topLeft, QPointF(), nullptr, nullptr, nullptr};
        }

        // Add point
        QPointF p = _point + QPointF(0, -grid_size);
        e->points.push_back(p);

        WeightedGraphPolygonDecompositionAreaInfo* leftOwner = getOwner(_left);
        WeightedGraphPolygonDecompositionAreaInfo* rightOwner = getOwner(_right);

        WeightedGraphPolygonDecompositionGridCell* leftTop = _left         ? _left->top
                                                             : _right->top ? _right->top->left
                                                                           : nullptr;
        WeightedGraphPolygonDecompositionGridCell* rightTop = _right       ? _right->top
                                                              : _left->top ? _left->top->right
                                                                           : nullptr;

        WeightedGraphPolygonDecompositionAreaInfo* leftTopOwner = getOwner(leftTop);
        WeightedGraphPolygonDecompositionAreaInfo* rightTopOwner = getOwner(rightTop);

        if(leftOwner != leftTopOwner)
        {
          clog_assert(leftTopOwner == rightOwner);
          clog_assert(rightTopOwner == rightOwner);
          return ExploreR{nullptr, p, _left, leftTop, exploreLeft};
        }
        else if(leftTopOwner != rightTopOwner)
        {
          clog_assert(leftTopOwner == leftOwner);
          clog_assert(rightTopOwner == rightOwner);
          return ExploreR{nullptr, p, leftTop, rightTop, exploreUp};
        }
        else
        {
          clog_assert(rightOwner != rightTopOwner);
          clog_assert(leftTopOwner == leftOwner);
          clog_assert(rightTopOwner == leftOwner);
          return ExploreR{nullptr, p, rightTop, _right, exploreRight};
        }
      };

      exploreRight = [grid_size, getOwner, &exploreUp, &exploreDown, &exploreRight](
                       WeightedGraphPolygonDecompositionBoundaryEdge* e, const QPointF& _point,
                       WeightedGraphPolygonDecompositionGridCell* _top,
                       WeightedGraphPolygonDecompositionGridCell* _bottom)
      {
        clog_assert(_top or _bottom);
        clog_assert(getOwner(_top) != getOwner(_bottom));
        clog_assert(not _top or _top->bottom == _bottom);

        // Check termination
        if(_top and _top->bottomRight)
        {
          _top->bottomRight->leftEdge = e;
          return ExploreR{_top->bottomRight, QPointF(), nullptr, nullptr, nullptr};
        }
        if(_bottom and _bottom->topRight)
        {
          _bottom->topRight->leftEdge = e;
          return ExploreR{_bottom->topRight, QPointF(), nullptr, nullptr, nullptr};
        }

        // Add point
        QPointF p = _point + QPointF(grid_size, 0);
        e->points.push_back(p);

        WeightedGraphPolygonDecompositionAreaInfo* topOwner = getOwner(_top);
        WeightedGraphPolygonDecompositionAreaInfo* bottomOwner = getOwner(_bottom);

        WeightedGraphPolygonDecompositionGridCell* topRight = _top             ? _top->right
                                                              : _bottom->right ? _bottom->right->top
                                                                               : nullptr;
        WeightedGraphPolygonDecompositionGridCell* bottomRight = _bottom       ? _bottom->right
                                                                 : _top->right ? _top->right->bottom
                                                                               : nullptr;

        WeightedGraphPolygonDecompositionAreaInfo* topRightOwner = getOwner(topRight);
        WeightedGraphPolygonDecompositionAreaInfo* bottomRightOwner = getOwner(bottomRight);

        if(topOwner != topRightOwner)
        {
          clog_assert(topRightOwner == bottomOwner);
          clog_assert(bottomRightOwner == bottomOwner);
          return ExploreR{nullptr, p, _top, topRight, exploreUp};
        }
        else if(topRightOwner != bottomRightOwner)
        {
          clog_assert(topRightOwner == topOwner);
          clog_assert(bottomRightOwner == bottomOwner);
          return ExploreR{nullptr, p, topRight, bottomRight, exploreRight};
        }
        else
        {
          clog_assert(bottomOwner != bottomRightOwner);
          clog_assert(topRightOwner == topOwner);
          clog_assert(bottomRightOwner == topOwner);
          return ExploreR{nullptr, p, bottomRight, _bottom, exploreDown};
        }
      };

      exploreDown = [grid_size, getOwner, &exploreDown, &exploreLeft, &exploreRight](
                      WeightedGraphPolygonDecompositionBoundaryEdge* e, const QPointF& _point,
                      WeightedGraphPolygonDecompositionGridCell* _right,
                      WeightedGraphPolygonDecompositionGridCell* _left)
      {
        clog_assert(_right or _left);
        clog_assert(getOwner(_right) != getOwner(_left));
        clog_assert(not _right or _right->left == _left);

        // Check termination
        if(_right and _right->bottomLeft)
        {
          _right->bottomLeft->topEdge = e;
          return ExploreR{_right->bottomLeft, QPointF(), nullptr, nullptr, nullptr};
        }
        if(_left and _left->bottomRight)
        {
          _left->bottomRight->topEdge = e;
          return ExploreR{_left->bottomRight, QPointF(), nullptr, nullptr, nullptr};
        }

        // Add point
        QPointF p = _point + QPointF(0, grid_size);
        e->points.push_back(p);

        WeightedGraphPolygonDecompositionAreaInfo* rightOwner = getOwner(_right);
        WeightedGraphPolygonDecompositionAreaInfo* leftOwner = getOwner(_left);

        WeightedGraphPolygonDecompositionGridCell* rightBottom = _right ? _right->bottom
                                                                 : _left->bottom
                                                                   ? _left->bottom->right
                                                                   : nullptr;
        WeightedGraphPolygonDecompositionGridCell* leftBottom = _left ? _left->bottom
                                                                : _right->bottom
                                                                  ? _right->bottom->left
                                                                  : nullptr;

        WeightedGraphPolygonDecompositionAreaInfo* rightBottomOwner = getOwner(rightBottom);
        WeightedGraphPolygonDecompositionAreaInfo* leftBottomOwner = getOwner(leftBottom);

        if(rightOwner != rightBottomOwner)
        {
          clog_assert(rightBottomOwner == leftOwner);
          clog_assert(leftBottomOwner == leftOwner);
          return ExploreR{nullptr, p, _right, rightBottom, exploreRight};
        }
        else if(rightBottomOwner != leftBottomOwner)
        {
          clog_assert(rightBottomOwner == rightOwner);
          clog_assert(leftBottomOwner == leftOwner);
          return ExploreR{nullptr, p, rightBottom, leftBottom, exploreDown};
        }
        else
        {
          clog_assert(leftOwner != leftBottomOwner);
          clog_assert(rightBottomOwner == rightOwner);
          clog_assert(leftBottomOwner == rightOwner);
          return ExploreR{nullptr, p, leftBottom, _left, exploreLeft};
        }
      };

      exploreLeft = [grid_size, getOwner, &exploreUp, &exploreDown, &exploreLeft](
                      WeightedGraphPolygonDecompositionBoundaryEdge* e, const QPointF& _point,
                      WeightedGraphPolygonDecompositionGridCell* _bottom,
                      WeightedGraphPolygonDecompositionGridCell* _top)
      {
        clog_assert(_bottom or _top);
        clog_assert(getOwner(_bottom) != getOwner(_top));
        clog_assert(not _bottom or _bottom->top == _top);

        // Check termination
        if(_bottom and _bottom->topLeft)
        {
          _bottom->topLeft->rightEdge = e;
          return ExploreR{_bottom->topLeft, QPointF(), nullptr, nullptr, nullptr};
        }
        if(_top and _top->bottomLeft)
        {
          _top->bottomLeft->rightEdge = e;
          return ExploreR{_top->bottomLeft, QPointF(), nullptr, nullptr, nullptr};
        }

        // Add point
        QPointF p = _point + QPointF(-grid_size, 0);
        e->points.push_back(p);

        WeightedGraphPolygonDecompositionAreaInfo* bottomOwner = getOwner(_bottom);
        WeightedGraphPolygonDecompositionAreaInfo* topOwner = getOwner(_top);

        WeightedGraphPolygonDecompositionGridCell* bottomLeft = _bottom      ? _bottom->left
                                                                : _top->left ? _top->left->bottom
                                                                             : nullptr;
        WeightedGraphPolygonDecompositionGridCell* topLeft = _top            ? _top->left
                                                             : _bottom->left ? _bottom->left->top
                                                                             : nullptr;

        WeightedGraphPolygonDecompositionAreaInfo* bottomLeftOwner = getOwner(bottomLeft);
        WeightedGraphPolygonDecompositionAreaInfo* topLeftOwner = getOwner(topLeft);

        if(bottomOwner != bottomLeftOwner)
        {
          clog_assert(bottomLeftOwner == topOwner);
          clog_assert(topLeftOwner == topOwner);
          return ExploreR{nullptr, p, _bottom, bottomLeft, exploreDown};
        }
        else if(bottomLeftOwner != topLeftOwner)
        {
          clog_assert(bottomLeftOwner == bottomOwner);
          clog_assert(topLeftOwner == topOwner);
          return ExploreR{nullptr, p, bottomLeft, topLeft, exploreLeft};
        }
        else
        {
          clog_assert(topOwner != topLeftOwner);
          clog_assert(bottomLeftOwner == bottomOwner);
          clog_assert(topLeftOwner == bottomOwner);
          return ExploreR{nullptr, p, topLeft, _top, exploreUp};
        }
      };

      auto explore = [](WeightedGraphPolygonDecompositionBoundaryEdge* e, const ExploreR& _start)
      {
        e->canSimplify = _start._1 and _start._2;
        ExploreR current = _start;
        clog_assert(_start.func);
        while(true)
        {
          current = current.func(e, current.pt, current._1, current._2);
          clog_assert(current.endnode or current.func);
          if(current.endnode)
          {
            e->end = current.endnode;
            e->start->addConnections(e->end, e);
            e->end->addConnections(e->start, e);
            return;
          }
        }
      };
      clog_assert(nodes.size() > 0);
      clog_assert(exploreUp);
      clog_assert(exploreDown);
      clog_assert(exploreLeft);
      clog_assert(exploreRight);
      for(WeightedGraphPolygonDecompositionBoundaryNode* n : nodes)
      {
        WeightedGraphPolygonDecompositionAreaInfo* pTL = getOwner(n->topLeft);
        WeightedGraphPolygonDecompositionAreaInfo* pTR = getOwner(n->topRight);
        WeightedGraphPolygonDecompositionAreaInfo* pBL = getOwner(n->bottomLeft);
        WeightedGraphPolygonDecompositionAreaInfo* pBR = getOwner(n->bottomRight);

        if(not n->topEdge and pTL != pTR)
        {
          WeightedGraphPolygonDecompositionBoundaryEdge* e
            = new WeightedGraphPolygonDecompositionBoundaryEdge;
          n->topEdge = e;
          e->start = n;
          e->areaInfo1 = pTL;
          e->areaInfo2 = pTR;
          edges.append(e);
          explore(e, ExploreR{nullptr, n->coordinate(), n->topLeft, n->topRight, exploreUp});
        }
        if(not n->rightEdge and pTR != pBR)
        {
          WeightedGraphPolygonDecompositionBoundaryEdge* e
            = new WeightedGraphPolygonDecompositionBoundaryEdge;
          n->rightEdge = e;
          e->start = n;
          e->areaInfo1 = pTR;
          e->areaInfo2 = pBR;
          edges.append(e);
          explore(e, ExploreR{nullptr, n->coordinate(), n->topRight, n->bottomRight, exploreRight});
        }
        if(not n->bottomEdge and pBR != pBL)
        {
          WeightedGraphPolygonDecompositionBoundaryEdge* e
            = new WeightedGraphPolygonDecompositionBoundaryEdge;
          n->bottomEdge = e;
          e->start = n;
          e->areaInfo1 = pBR;
          e->areaInfo2 = pBL;
          edges.append(e);
          explore(e,
                  ExploreR{nullptr, n->coordinate(), n->bottomRight, n->bottomLeft, exploreDown});
        }
        if(not n->leftEdge and pBL != pTL)
        {
          WeightedGraphPolygonDecompositionBoundaryEdge* e
            = new WeightedGraphPolygonDecompositionBoundaryEdge;
          n->leftEdge = e;
          e->start = n;
          e->areaInfo1 = pBL;
          e->areaInfo2 = pTL;
          edges.append(e);
          explore(e, ExploreR{nullptr, n->coordinate(), n->bottomLeft, n->topLeft, exploreLeft});
        }
        int edgeCount = 0;
        if(n->topEdge)
          ++edgeCount;
        if(n->rightEdge)
          ++edgeCount;
        if(n->bottomEdge)
          ++edgeCount;
        if(n->leftEdge)
          ++edgeCount;
        if(edgeCount == 4)
        {
          n->topEdge->cannotConnectTo = n->bottomEdge;
          n->bottomEdge->cannotConnectTo = n->topEdge;
          n->leftEdge->cannotConnectTo = n->rightEdge;
          n->rightEdge->cannotConnectTo = n->leftEdge;
        }
      }
      // 7c prevent simplification for areas that have only two boundary nodes and have long length
      // of unprotected boundary
      //    this can happen with very small weights and very sharp polygons
      for(WeightedGraphPolygonDecompositionAreaInfo* areaInfo : areas)
      {
        if(areaInfo->boundaryNodes.size() == 2)
        {
          QSet<WeightedGraphPolygonDecompositionBoundaryEdge*> areaEdges;
          for(WeightedGraphPolygonDecompositionBoundaryNode* node : areaInfo->boundaryNodes)
          {
            if(node->leftEdge
               and (node->leftEdge->areaInfo1 == areaInfo or node->leftEdge->areaInfo2 == areaInfo))
              areaEdges.insert(node->leftEdge);
            if(node->rightEdge
               and (node->rightEdge->areaInfo1 == areaInfo
                    or node->rightEdge->areaInfo2 == areaInfo))
              areaEdges.insert(node->rightEdge);
            if(node->topEdge
               and (node->topEdge->areaInfo1 == areaInfo or node->topEdge->areaInfo2 == areaInfo))
              areaEdges.insert(node->topEdge);
            if(node->bottomEdge
               and (node->bottomEdge->areaInfo1 == areaInfo
                    or node->bottomEdge->areaInfo2 == areaInfo))
              areaEdges.insert(node->bottomEdge);
          }
          clog_assert(areaEdges.size() == 2);
          int protected_lenght = 0;
          int unprotected_lenght = 0;
          for(WeightedGraphPolygonDecompositionBoundaryEdge* edge : areaEdges)
          {
            if(edge->canSimplify)
            {
              unprotected_lenght += edge->points.size();
            }
            else
            {
              protected_lenght += edge->points.size();
            }
          }
          if(protected_lenght < unprotected_lenght)
          {
            for(WeightedGraphPolygonDecompositionBoundaryEdge* edge : areaEdges)
            {
              edge->canSimplify = false;
            }
          }
        }
      }

      // 7d Simplify
      for(WeightedGraphPolygonDecompositionBoundaryEdge* e : edges)
      {
        if(e->canSimplify)
        {
          QList<QPointF> before = e->points;
          std::vector<euclid::simple::point> pts;
          pts.push_back(toSimplePoint(e->start->coordinate()));
          for(const QPointF& pt : e->points)
          {
            pts.push_back(toSimplePoint(pt));
          }
          pts.push_back(toSimplePoint(e->end->coordinate()));
          pts = euclid::extra::line_string_fitting(pts, grid_size);
          e->points = QList<QPointF>();
          for(const euclid::simple::point& pt : pts)
          {
            e->points.append(toQPointF(pt));
          }
          e->points.removeFirst();
          e->points.removeLast();
        }
      }
      // END
      // BEGIN 8 generate output
      QList<EuclidSystem::polygon> result_polygon;
      QList<EuclidSystem::polygon> residuals;
      for(WeightedGraphPolygonDecompositionAreaInfo* areaInfo : areas)
      {
        /* For each area, build a cluster:
         * - starting from the first boundary node
         * - and then moving through the connection
         */
        EuclidSystem::linear_ring::builder cluster_builder;
        clog_assert(areaInfo->boundaryNodes.size() > 1);
        if(areaInfo->boundaryNodes.isEmpty())
        {
          clog_error("Empty decomposition, most likely, too big tolerance.");
          result_polygon.clear();
          residuals.clear();
          break;
        }
        WeightedGraphPolygonDecompositionBoundaryNode* startNode = areaInfo->boundaryNodes.first();
        QPointF sc = startNode->coordinate();
        // Add the start point
        cluster_builder.add_point(sc.x(), sc.y());
        // Use connections to connect all the points
        int start_node_idx = 0;
        QList<WeightedGraphPolygonDecompositionBoundaryNode*> nodes = areaInfo->boundaryNodes;
        nodes.removeOne(startNode);
        WeightedGraphPolygonDecompositionBoundaryNode* currentNode = startNode;
        QList<const WeightedGraphPolygonDecompositionBoundaryEdge*> used_edges;
        const WeightedGraphPolygonDecompositionAreaInfo* other_info = nullptr;
        const WeightedGraphPolygonDecompositionBoundaryEdge* other_edge = nullptr;
        while(not nodes.isEmpty())
        {
          int s = nodes.size();
          for(WeightedGraphPolygonDecompositionBoundaryNode* node : nodes)
          {
            auto [points, ai, edge] = currentNode->getConnectionsPoints(node, &used_edges, areaInfo,
                                                                        other_info, other_edge);
            if(edge)
            {
              nodes.removeOne(node);
              other_info = ai;
              other_edge = edge;
              for(const QPointF& _pt : points)
              {
                cluster_builder.add_point(_pt.x(), _pt.y());
              }
              QPointF nc = node->coordinate();
              cluster_builder.add_point(nc.x(), nc.y());
              currentNode = node;
              break;
            }
          }
          if(s == nodes.size())
          {
            ++start_node_idx;
            if(start_node_idx >= areaInfo->boundaryNodes.size())
            {
              clog_error("Failed to generate boundary, most likely, numerical error, try different "
                         "tolerance.");
              return EuclidSystem::multi_polygon();
            }
            nodes = areaInfo->boundaryNodes;
            startNode = nodes.takeAt(start_node_idx);
            currentNode = startNode;
            used_edges.clear();
            cluster_builder = EuclidSystem::linear_ring::builder();
            sc = startNode->coordinate();
            // Add the start point
            cluster_builder.add_point(sc.x(), sc.y());
          }
          clog_assert(s != nodes.size()); // Check that a connection happened
        }
        // Connect back to starting point
        {
          auto [points, ai, edge] = currentNode->getConnectionsPoints(
            startNode, &used_edges, areaInfo, other_info, other_edge);
          Q_UNUSED(ai);
          if(edge)
          {
            for(const QPointF& _pt : points)
            {
              cluster_builder.add_point(_pt.x(), _pt.y());
            }
          }
          else
          {
            clog_error("Failed to generate boundary, most likely, numerical error, try different "
                       "tolerance.");
            return EuclidSystem::multi_polygon();
          }
        }
        EuclidSystem::linear_ring cluster = cluster_builder.create();
        EuclidSystem::polygon polygon(cluster);
        if(not euclid::ops::is_valid(polygon))
        {
          euclid::geos::linear_ring lr = euclid::extra::fix_it::correct_self_intersection(cluster);
          polygon = EuclidSystem::polygon(lr);
        }
        if(not euclid::ops::is_valid(polygon))
        {
          clog_error("Boundary generated an invalid polygon, most likely, numerical error, try "
                     "different tolerance.");
          return EuclidSystem::multi_polygon();
        }
        EuclidSystem::geometry intersection = euclid::ops::intersection(_source, polygon);
        if(not intersection.is<EuclidSystem::polygon>())
        {
          clog_assert(intersection.is<EuclidSystem::multi_polygon>());
          EuclidSystem::multi_polygon mp = intersection.cast<EuclidSystem::multi_polygon>();
          clog_assert(not mp.is_null());
          EuclidSystem::polygon selected_poly;
          qreal selected_poly_area = 0.0;
          for(EuclidSystem::polygon poly : mp)
          {
            qreal area = euclid::ops::area(poly);
            if(area > selected_poly_area)
            {
              if(not selected_poly.is_null())
              {
                residuals.push_back(selected_poly);
              }
              selected_poly = poly;
              selected_poly_area = area;
            }
            else
            {
              residuals.push_back(poly);
            }
          }
          intersection = selected_poly;
        }
        clog_assert(not intersection.is_null());
        EuclidSystem::polygon intersection_poly = intersection.cast<EuclidSystem::polygon>();
        EuclidSystem::polygon simplified
          = euclid::ops::simplify(intersection_poly, _parameters.tol);
        if(simplified.is_null())
        {
          result_polygon.append(intersection_poly);
        }
        else
        {
          result_polygon.append(simplified);
        }
        if(_statistics)
        {
          _statistics->areas.append(areaInfo->ownedSurface);
        }
      }
      for(const EuclidSystem::polygon& poly_residual : residuals)
      {
        bool merged = false;
        for(int i = 0; i < result_polygon.size(); ++i)
        {
          const EuclidSystem::polygon& poly_result = result_polygon[i];
          if(euclid::ops::touches(poly_result, poly_residual))
          {
            EuclidSystem::geometry fuse = euclid::ops::union_(poly_result, poly_residual);
            std::cout << fuse.is<EuclidSystem::polygon>() << std::endl;
            if(fuse.is<EuclidSystem::polygon>())
            {
              EuclidSystem::polygon poly_fuse = fuse.cast<EuclidSystem::polygon>();
              result_polygon[i] = poly_fuse;
              merged = true;
              break;
            }
          }
        }
        clog_assert(merged);
      }
      EuclidSystem::multi_polygon::builder result_builder;
      for(const EuclidSystem::polygon& poly : result_polygon)
      {
        result_builder.add_element(poly);
      }
      // END
      //  8) Freeup memory
      return result_builder.create();
    }
  } // namespace Algorithms
} // namespace Cartography
