#include <QVariantHash>

#include <Cartography/CoordinateSystem.h>
#include <Cartography/EuclidSystem.h>
#include <Cartography/GeometryObject.h>
#include <Cartography/Raster/Raster.h>

namespace Cartography
{
  namespace Algorithms
  {
    /**
     * Parameters to control the \ref weightedGraphPolygonDecomposition algorithm
     */
    struct WeightedGraphPolygonDecompositionParameters
    {
      /// \private
      QList<qreal> weights;

      /// \private
      double tol;

      /// \private
      QList<euclid::simple::point> starts;

      /// maximum number of iterations
      int maxIterations = 100;
      enum class Norm
      {
        Norm1,
        Norm2
      };
      // Type of norm used to compute distances
      Norm norm = Norm::Norm2;
      enum class Optimisation
      {
        None,
        PotentialField,
        CMAES,
        RandomSearch
      };
      /// First optimisation
      Optimisation optimisation1 = Optimisation::PotentialField;
      /// Second optimisation (using the result of \ref optimisation1
      Optimisation optimisation2 = Optimisation::None;

      enum class ObjectiveFunctionFatness
      {
        Schwartzberg,
        PolsbyPopper
      };
      /// Type of fatness function used for the objectve function
      ObjectiveFunctionFatness objectiveFunctionFatness = ObjectiveFunctionFatness::Schwartzberg;

      /// Use a raster to define the weights of the grid cells (ie population density)
      Raster::Raster raster = Raster::Raster();

      /// Index of the band in the raster to use
      std::size_t rasterBand = 0;

      /**
       * Read the \ref _values and set the field of the parameters
       * \private
       */
      void read(const QVariantHash& _values);
      /**
       * Read the \ref _values and set the field of the parameters
       * \private
       */
      void read(const QVariantMap& _values);
    private:
      template<typename _T_>
      void readImpl(const _T_& _values);
    };
    /**
     * This structure is used to collect some statistics during the execution of the algorithm.
     * Mostly for debug purposes.
     */
    struct WeightedGraphPolygonDecompositionStatistics
    {
      /// Number of time a discountinuity occured during assignment
      int discountinuityCount = 0;
      /// Number of grid cell exchanged during rebalancing
      QList<int> gridCellExchangedCounts;
      /// Areas assigned to each partitions
      QList<qreal> areas;
    };
    /**
     * @param _grid_size represent the number of points used in the grid
     * @param _splits how much the segment of the polygon needs to be splited
     *
     * A higher number of @param _grid_size and @param _splits give better accuracy, but increase
     * computation time
     */
    EuclidSystem::multi_polygon weightedGraphPolygonDecomposition(
      const EuclidSystem::polygon& _source, const CoordinateSystem& _geometry_cs,
      const WeightedGraphPolygonDecompositionParameters& _parameters,
      WeightedGraphPolygonDecompositionStatistics* _statistics = nullptr);
    GeometryObject weightedGraphPolygonDecomposition(
      const GeometryObject& _source, const WeightedGraphPolygonDecompositionParameters& _parameters,
      WeightedGraphPolygonDecompositionStatistics* _statistics = nullptr)
    {
      return GeometryObject(
        _source.coordinateSystem(),
        weightedGraphPolygonDecomposition(_source.geometry().cast<EuclidSystem::polygon>(),
                                          _source.coordinateSystem(), _parameters, _statistics));
    }
    GeometryObject weightedGraphPolygonDecomposition(
      const GeometryObject& _source, const QVariantHash& _parameters,
      WeightedGraphPolygonDecompositionStatistics* _statistics = nullptr)
    {
      WeightedGraphPolygonDecompositionParameters lmpp;
      lmpp.read(_parameters);
      return weightedGraphPolygonDecomposition(_source, lmpp, _statistics);
    }
  } // namespace Algorithms
} // namespace Cartography

#include <clog_format>

clog_format_declare_enum_formatter(
  Cartography::Algorithms::WeightedGraphPolygonDecompositionParameters::Optimisation, None,
  PotentialField, CMAES, RandomSearch);
