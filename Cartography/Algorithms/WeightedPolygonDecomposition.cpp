#include "WeightedPolygonDecomposition.h"

#include <clog_qt>

#include <QList>

#include <euclid/extra/grid_decomposition>
#include <euclid/extra/is_clock_wise>
#include <euclid/extra/linear_ring>
#include <euclid/extra/reverse_order>
#include <euclid/ops>

#include "Utils_p.h"

namespace Cartography
{
  namespace Algorithms
  {
    namespace Details
    {
      struct WeightedPolygonDecompositionAreaInfo
      {
        euclid::simple::point startPoint;
        qreal weigth;
        double targetArea;
        double currentArea = 0.0;

        // Cache for the best local candidate
        EuclidSystem::linear_ring best_local_candidate = EuclidSystem::linear_ring();
        qreal best_local_area = -1.0;
        qreal best_local_score = 0.0;
        int best_local_candidate_index = -1;

        EuclidSystem::linear_ring linearRing = EuclidSystem::linear_ring();
      };
    } // namespace Details
    EuclidSystem::multi_geometry
      weightedPolygonDecomposition(const EuclidSystem::polygon& _source,
                                   const QList<qreal>& _weights, double _tol,
                                   const QList<euclid::simple::point>& _starts)
    {
      using namespace Details;
      // Steps
      // 1) Grid decomposition
      euclid::simple::box envelope = euclid::ops::envelope(_source);

      const double grid_h_acc = envelope.width() * _tol;
      const double grid_v_acc = envelope.height() * _tol;

      EuclidSystem::multi_polygon initial_decomposition
        = euclid::extra::grid_decomposition(_source, std::min(grid_h_acc, grid_v_acc));

      QList<EuclidSystem::polygon> candidates;
      std::copy(initial_decomposition.begin(), initial_decomposition.end(),
                std::back_inserter(candidates));

      // 2) start points
      CurvilinearCoordinates cc(_source.exterior_ring());
      QList<WeightedPolygonDecompositionAreaInfo> areas;
      const qreal total_surface = euclid::ops::area(_source);
      for(int i = 0; i < _weights.size(); ++i)
      {
        euclid::simple::point start
          = i < _starts.size() ? _starts[i] : cc(i / double(_weights.size()));
        areas.append({start, _weights[i], total_surface * _weights[i]});
      }

      // 3) merge operations

      for(WeightedPolygonDecompositionAreaInfo& areaInfo : areas)
      {
        int closest_polygon_index = -1;
        qreal closest_polygon_distance = std::numeric_limits<qreal>::max();
        for(int j = 0; j < candidates.size(); ++j)
        {
          qreal dist
            = euclid::ops::distance(EuclidSystem::point(areaInfo.startPoint), candidates[j]);
          if(dist < closest_polygon_distance)
          {
            closest_polygon_distance = dist;
            closest_polygon_index = j;
          }
        }
        if(closest_polygon_index == -1)
        {
          clog_error("Failed to assign a starting triangle! Increase polygon splits.");
          return EuclidSystem::multi_geometry();
        }
        areaInfo.linearRing = candidates[closest_polygon_index].exterior_ring();
        areaInfo.currentArea = euclid::ops::area(areaInfo.linearRing);
        candidates.removeAt(closest_polygon_index);
      }

      while(candidates.size() > 0)
      {
        //           clog_debug("Remaining candiates: %1", candidates.size());
        WeightedPolygonDecompositionAreaInfo* best_areaInfo = nullptr;
        qreal best_score = -std::numeric_limits<qreal>::max();
        int best_candidate_index = -1;
        EuclidSystem::linear_ring best_linearRing;

        for(WeightedPolygonDecompositionAreaInfo& areaInfo : areas)
        {
          if(not areaInfo.best_local_candidate.is_null())
          {
            areaInfo.best_local_score = std::numeric_limits<qreal>::max();
            areaInfo.best_local_candidate_index = -1;

            for(int j = 0; j < candidates.size(); ++j)
            {
              std::size_t edge1;
              std::size_t edge2;
              bool reversed;
              EuclidSystem::linear_ring candiate_ring = candidates[j].exterior_ring();
              if(euclid::extra::linear_ring::share_edge(areaInfo.linearRing, candiate_ring, _tol,
                                                        &edge1, &edge2, &reversed))
              {
                EuclidSystem::linear_ring combined = euclid::extra::linear_ring::combine(
                  areaInfo.linearRing, edge1, candiate_ring, edge2, reversed);
                if(not euclid::extra::linear_ring::has_duplicate_points(combined, _tol))
                {
                  if(not euclid::extra::is_clock_wise(combined))
                  {
                    combined = euclid::extra::reverse_order(combined);
                  }
#if 0
                  qreal pt_x = 0;
                  qreal pt_y = 0;
                  for_ogr_point(pt, combined)
                  {
                  pt_x += pt.getX();
                  pt_y += pt.getY();
                }
                pt_x /= combined->getNumPoints();
                pt_y /= combined->getNumPoints();
#else
                  qreal pt_x = areaInfo.startPoint.x();
                  qreal pt_y = areaInfo.startPoint.y();
#endif

                  QList<qreal> distances;
                  qreal avg_distance = 0;
                  for(euclid::simple::point pt : combined.points())
                  {
                    qreal u = pt.x() - pt_x;
                    qreal v = pt.y() - pt_y;

                    qreal d = std::sqrt(u * u + v * v);
                    distances.append(d);
                    avg_distance += d;
                  }
                  avg_distance /= combined.points().size();
                  qreal std_dev = 0.0;
                  for(qreal d : distances)
                  {
                    qreal a = d - avg_distance;
                    std_dev += a * a;
                  }
                  std_dev = std::sqrt(std_dev / (combined.points().size() - 1));
                  qreal score = std_dev;
#if 0
                auto [ min, max, median, avg, std_dev] = angleStatistics(combined);
                Q_UNUSED(min);
                Q_UNUSED(max);
                Q_UNUSED(median);
                Q_UNUSED(avg);
                //                   qreal distance = candidates[j]->Distance(&areaInfo.startPoint);
                qreal score = std_dev /** distance*/;
#endif
                  if(score < areaInfo.best_local_score)
                  {
                    areaInfo.best_local_area = -1.0;
                    areaInfo.best_local_score = score;
                    areaInfo.best_local_candidate = combined;
                    areaInfo.best_local_candidate_index = j;
                  }
                }
              }
            }
          }
          if(not areaInfo.best_local_candidate.is_null())
          {
            if(areaInfo.best_local_area < 0.0)
              areaInfo.best_local_area = euclid::ops::area(areaInfo.best_local_candidate);
            qreal score = (areaInfo.targetArea - areaInfo.best_local_area) / areaInfo.targetArea;
            if(score < 0)
              score *= areaInfo.best_local_score;
            else
              score /= std::max(0.001, areaInfo.best_local_score);
            if(score > best_score)
            {
              best_score = score;
              best_areaInfo = &areaInfo;
              best_candidate_index = areaInfo.best_local_candidate_index;
              best_linearRing = areaInfo.best_local_candidate;
            }
          }
        }
        if(not best_areaInfo)
          break;
        clog_assert(best_areaInfo);
        clog_assert(not best_linearRing.is_null());

        clog_assert(euclid::ops::equals(best_linearRing, best_areaInfo->best_local_candidate));

        best_areaInfo->linearRing = best_linearRing;
        best_areaInfo->currentArea = euclid::ops::area(best_linearRing);
        candidates.removeAt(best_candidate_index);
        best_areaInfo->best_local_candidate = EuclidSystem::linear_ring();
        for(WeightedPolygonDecompositionAreaInfo& areaInfo : areas)
        {
          if(areaInfo.best_local_candidate_index == best_candidate_index)
          { // Same candidate index, delete
          }
          else if(areaInfo.best_local_candidate_index > best_candidate_index)
          { // Update index that are after the one removed
            --areaInfo.best_local_candidate_index;
          }
        }
      }

      EuclidSystem::multi_geometry::builder collection_b;
      for(const WeightedPolygonDecompositionAreaInfo& areaInfo : areas)
      {
#ifndef NDEBUG
        auto [min, max, median, avg, std_dev]
          = euclid::extra::linear_ring::angle_statistics(areaInfo.linearRing);
        clog_debug("area: %1 area target: %2 area got: %3", (void*)&areaInfo, areaInfo.targetArea,
                   areaInfo.currentArea);
        clog_debug("min: %1 max: %2 median: %3 avg: %4 std_dev: %5", min, max, median, avg,
                   std_dev);
        clog_debug("polyline: %1", euclid::io::to_wkt(areaInfo.linearRing));
#endif
        collection_b.add_element(areaInfo.linearRing);
      }

      return collection_b.create();
    }
  } // namespace Algorithms
} // namespace Cartography
