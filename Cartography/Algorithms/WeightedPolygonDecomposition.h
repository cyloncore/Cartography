#include <QList>

#include <Cartography/EuclidSystem.h>

namespace Cartography::Algorithms
{
  /**
   * @param _grid_size represent the number of points used in the grid
   * @param _splits how much the segment of the polygon needs to be splited
   *
   * A higher number of @param _grid_size and @param _splits give better accuracy, but increase
   * computation time
   */
  EuclidSystem::multi_geometry weightedPolygonDecomposition(
    const EuclidSystem::polygon& _source, const QList<qreal>& _weight, double _tol,
    const QList<euclid::simple::point>& _starts = QList<euclid::simple::point>());
} // namespace Cartography::Algorithms
