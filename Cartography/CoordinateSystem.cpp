#include "CoordinateSystem.h"

#include <QPointF>

#include <proj.h>

#include <clog_qt>

#include "GeoPoint.h"

using namespace Cartography;

struct CoordinateSystem::Private : public QSharedData
{
  PJ* proj = nullptr;

  ~Private() { proj_destroy(proj); }
};

namespace
{
  int getZone(const GeoPoint& _point)
  {
    if(_point.latitude() >= 56.0 && _point.latitude() < 64.0 && _point.longitude() >= 3.0
       && _point.longitude() < 12.0)
      return 32;

    // Special zones for Svalbard
    if(_point.latitude() >= 72.0 && _point.latitude() < 84.0)
    {
      if(_point.longitude() >= 0.0 && _point.longitude() < 9.0)
        return 31;
      else if(_point.longitude() >= 9.0 && _point.longitude() < 21.0)
        return 33;
      else if(_point.longitude() >= 21.0 && _point.longitude() < 33.0)
        return 35;
      else if(_point.longitude() >= 33.0 && _point.longitude() < 42.0)
        return 37;
    }
    return int(_point.longitude() + 186.0) / 6;
  }
} // namespace

CoordinateSystem CoordinateSystem::utm(const GeoPoint& _point)
{
  const int zone = getZone(_point);
  const int srid = (_point.longitude() < 0.0 ? 32700 : 32600) + zone;
  return CoordinateSystem(srid);
}

CoordinateSystem::CoordinateSystem() : d(new Private) {}
//
CoordinateSystem::CoordinateSystem(const char* _wkt) : d(new Private)
{
  d->proj = proj_create(PJ_DEFAULT_CTX, _wkt);
}

CoordinateSystem::CoordinateSystem(const QByteArray& _wkt) : CoordinateSystem(_wkt.data()) {}

CoordinateSystem::CoordinateSystem(const QString& _wkt) : CoordinateSystem(_wkt.toLatin1()) {}

CoordinateSystem::CoordinateSystem(unsigned int _srid) : d(new Private)
{
  d->proj = proj_create(PJ_DEFAULT_CTX, qPrintable(QString("EPSG:%1").arg(_srid)));
}

CoordinateSystem CoordinateSystem::wgs84 = CoordinateSystem(
  "GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS "
  "84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]],PRIMEM["
  "\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.0174532925199433,AUTHORITY["
  "\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4326\"]]");

CoordinateSystem::CoordinateSystem(const CoordinateSystem& _rhs) : d(_rhs.d) {}

CoordinateSystem& CoordinateSystem::operator=(const CoordinateSystem& _rhs)
{
  d = _rhs.d;
  return *this;
}

CoordinateSystem::~CoordinateSystem() {}

bool CoordinateSystem::operator==(const CoordinateSystem& _rhs) const
{
  return srid() == _rhs.srid();
}

bool CoordinateSystem::isValid() const { return d->proj; }

QString CoordinateSystem::authority() const
{
  return QString::fromLatin1(proj_get_id_auth_name(d->proj, 0));
}

unsigned int CoordinateSystem::srid() const
{
  return d->proj ? (unsigned int)atoi(proj_get_id_code(d->proj, 0)) : 0;
}

QString CoordinateSystem::toWkt() const
{
  return QString::fromLatin1(proj_as_wkt(PJ_DEFAULT_CTX, d->proj, PJ_WKT2_2019, NULL));
}

QString CoordinateSystem::toProj() const
{
  return QString::fromLatin1(proj_as_proj_string(PJ_DEFAULT_CTX, d->proj, PJ_PROJ_4, NULL));
}

void* Cartography::CoordinateSystem::projection() const { return d->proj; }

std::ostream& Cartography::operator<<(std::ostream& os, CoordinateSystem const& value)
{
  os << value.srid();
  return os;
}
