#pragma once

#include <QSharedPointer>

#include "Forward.h"

class QPointF;

namespace Cartography
{
  /**
   * This class represents a coordinate system (UTM, WSG84...).
   */
  class CoordinateSystem
  {
    friend class Point;
  public:
    unsigned int WGS84_SRID = 4326;
  public:
    /**
     * Static WGS 85 \ref CoordinateSystem
     */
    static CoordinateSystem wgs84;
    /**
     * @return the utm \ref CoordinateSystem that best matches the given \p _point
     */
    static CoordinateSystem utm(const GeoPoint& _point);
    /**
     * Construct an invalid reference system
     */
    CoordinateSystem();
    /**
     * Create a coordinate system from a WKT representation
     */
    CoordinateSystem(const QByteArray& _wkt);
    CoordinateSystem(const char* _wkt);
    CoordinateSystem(const QString& _wkt);
    /**
     * Construct one according to the SRID (Spatial Reference System Identifier) number
     */
    explicit CoordinateSystem(unsigned int _srid);
    CoordinateSystem(const CoordinateSystem& _rhs);
    CoordinateSystem& operator=(const CoordinateSystem& _rhs);
    ~CoordinateSystem();
    bool operator==(const CoordinateSystem& _rhs) const;
    bool operator!=(const CoordinateSystem& _rhs) const { return not(*this == _rhs); }
    bool isValid() const;
    /**
     * \return the authority name (query for AUTHORITY[] within the WKT representation)
     */
    QString authority() const;
    /**
     * \return the SRID (Spatial Reference System Identifier)
     */
    unsigned int srid() const;
    /**
     * \return the WKT representation
     */
    QString toWkt() const;
    /**
     * \return the proj representation
     */
    QString toProj() const;
    bool isWgs84() const { return srid() == WGS84_SRID; }
  private:
    /**
     * \return the proj structure
     */
    void* projection() const;
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
  std::ostream& operator<<(std::ostream& os, CoordinateSystem const& value);
} // namespace Cartography

inline Cartography::CoordinateSystem operator"" _cartographyCS(unsigned long long int _srid)
{
  return Cartography::CoordinateSystem(_srid);
}

#include <clog_format>

clog_format_declare_formatter(Cartography::CoordinateSystem)
{
  return clog_format::to_string(ctx.out(), p.toWkt());
}

Q_DECLARE_METATYPE(Cartography::CoordinateSystem);
