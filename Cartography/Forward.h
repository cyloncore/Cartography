namespace Cartography
{
  class CoordinateSystem;
  class GeoPoint;
  class GeometryObject;
  class Point;

  namespace Geometry
  {
    class AbstractFeaturesSource;
    class Collection;
    class Feature;
    class FeaturesSet;
    class Geometry;
    class LinearRing;
    class Point;
    class Polygon;
  } // namespace Geometry
  namespace Raster
  {
    class AbstractImageWritter;
    class AbstractRasterSource;
    class Raster;
  } // namespace Raster
  namespace Terrain
  {
    class HeightMap;
  }
  namespace Quick
  {
    namespace Raster
    {
      class Raster;
    }
  } // namespace Quick
} // namespace Cartography
