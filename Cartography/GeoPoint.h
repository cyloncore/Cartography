#pragma once

#include <clog_qt>

#include "CoordinateSystem.h"
#include "NamedTypes.h"
#include "Point.h"

namespace Cartography
{
  /**
   * GeoPoint is a @ref Point whose coordinateSystem is wgs84
   */
  class GeoPoint : public Point
  {
  public:
    GeoPoint() : Point() {}
    /**
     * Create a \ref GeoPoint from a set of longitude, latitude and an optional altitude.
     */
    GeoPoint(const Longitude& _longitude, const Latitude& _latitude,
             const Altitude& _altitude = Altitude(std::numeric_limits<double>::quiet_NaN()))
        : Point(_longitude, _latitude, _altitude, CoordinateSystem::wgs84)
    {
    }
  private:
    /**
     * Create a \ref GeoPoint from a WGS84 \ref Point (if the point is in a different coordinate
     * system, use \ref transformFrom),
     */
    GeoPoint(const Point& _rhs) : Point(_rhs)
    {
      clog_assert(coordinateSystem() == CoordinateSystem::wgs84);
    }
  public:
    /**
     * Create a \ref GeoPoint from a \ref Point in a different coordinate system than WGS84
     */
    static GeoPoint from(const Point& _rhs)
    {
      return GeoPoint(_rhs.transform(CoordinateSystem::wgs84));
    }
    ~GeoPoint() {}
    Longitude longitude() const { return Longitude(x()); }
    Latitude latitude() const { return Latitude(y()); }
    Altitude altitude() const { return Altitude(z()); }
  };
} // namespace Cartography

#include <QMetaType>

Q_DECLARE_METATYPE(Cartography::GeoPoint);
