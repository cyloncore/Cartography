#ifndef _CARTOGRAPHERML_ABSTRACTFEATURESSOURCE_H_
#define _CARTOGRAPHERML_ABSTRACTFEATURESSOURCE_H_

#include <QObject>

#include <Cartography/Forward.h>

namespace Cartography::Geometry
{
  class AbstractFeaturesSource : public QObject
  {
    Q_OBJECT
  public:
    AbstractFeaturesSource(QObject* parent = 0);
    virtual ~AbstractFeaturesSource();
    Q_INVOKABLE virtual Cartography::Geometry::FeaturesSet* features(const QRectF& _rect) = 0;
    Q_INVOKABLE virtual Cartography::Geometry::FeaturesSet* featuresAt(const QPointF& _rect,
                                                                       qreal _tol = 1e-6)
      = 0;
    Q_INVOKABLE Cartography::Geometry::FeaturesSet* featuresAt(qreal _x, qreal _y,
                                                               qreal _tol = 1e-6);
    Q_INVOKABLE virtual bool record(Cartography::Geometry::Feature* _feature) = 0;
    Q_INVOKABLE virtual QRectF envelope() = 0;
    Q_INVOKABLE virtual Cartography::Geometry::Feature* createFeature() = 0;
  signals:
    void featuresChanged();
  };
} // namespace Cartography::Geometry

#endif
