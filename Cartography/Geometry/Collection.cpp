#include "Collection.h"

#include "Geometry_p.h"
#include "rect_utils_p.h"

using namespace Cartography::Geometry;

struct Collection::Private : Geometry::Private
{
  Private() : Geometry::Private(Geometry::Type::Collection) {}

  QList<Geometry*> elements;
  Type elementsType;
};

CARTOGRAPHY_D_FUNC_DEF(Collection)

Collection::Collection(QObject* parent) : Geometry(new Private, parent) {}

Collection::~Collection() {}

void Collection::append(Geometry* _geom)
{
  if(D()->elements.empty())
  {
    D()->elementsType = _geom->type();
    emit(elementsTypeChanged());
  }
  else if(D()->elementsType != Type::Undefined and D()->elementsType != _geom->type())
  {
    D()->elementsType = Type::Undefined;
    emit(elementsTypeChanged());
  }
  D()->elements.append(_geom);
  emit(elementsChanged());
}

void Collection::append(QList<Geometry*> _geom)
{
  for(Geometry* g : _geom)
    append(g);
}

QList<Geometry*> Collection::elements() const { return D()->elements; }

QList<QObject*> Collection::elementsAsQObject()
{
  return Cartography::Private::list_cast<QObject*>(D()->elements);
}

Geometry::Type Collection::elementsType() const { return D()->elementsType; }

QRectF Collection::envelope() const
{
  if(D()->elements.isEmpty())
    return QRectF();
  QRectF r = D()->elements.first()->envelope();
  for(Geometry* g : D()->elements)
  {
    r = unite(r, g->envelope());
  }
  return r;
}

Geometry::Dimension Collection::dimension() const
{
  Dimension dim = Dimension::Zero;
  for(Geometry* g : D()->elements)
  {
    dim = max(dim, g->dimension());
    if(dim == Dimension::Three)
      return dim;
  }
  return dim;
}

Geometry* Collection::clone(QObject* _parent) const
{
  Collection* c = new Collection(_parent);
  Private* cd = static_cast<Private*>(c->d);
  cd->elementsType = D()->elementsType;
  for(const Geometry* g : D()->elements)
  {
    cd->elements.append(g->clone());
  }
  return c;
}

#include "moc_Collection.cpp"
