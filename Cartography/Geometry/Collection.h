#pragma once

#include <Cartography/Global.h>

#include "Geometry.h"

namespace Cartography::Geometry
{
  class Collection : public Geometry
  {
    Q_OBJECT
    Q_PROPERTY(QList<QObject*> elements READ elementsAsQObject NOTIFY elementsChanged)
    Q_PROPERTY(Type elementsType READ elementsType NOTIFY elementsTypeChanged)
  public:
    explicit Collection(QObject* parent = 0);
    virtual ~Collection();
    void append(Geometry* _geom);
    void append(QList<Geometry*> _geom);
    QList<Geometry*> elements() const;
    Type elementsType() const;
    QRectF envelope() const override;
    Dimension dimension() const override;
    Geometry* clone(QObject* _parent = nullptr) const override;
  private:
    QList<QObject*> elementsAsQObject();
  signals:
    void elementsChanged();
    void elementsTypeChanged();
  private:
    CARTOGRAPHY_D_DECL();
  };
} // namespace Cartography::Geometry
