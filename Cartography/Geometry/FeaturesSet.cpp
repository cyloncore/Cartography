#include "FeaturesSet.h"

#include <QRectF>

#include "Feature.h"
#include "Geometry.h"
#include <Cartography/Global.h>

using namespace Cartography::Geometry;

struct FeaturesSet::Private
{
  QList<Feature*> features;
};

FeaturesSet::FeaturesSet(QObject* parent) : QObject(parent), d(new Private) {}

FeaturesSet::FeaturesSet(const QList<Feature*>& _features, QObject* parent)
    : QObject(parent), d(new Private)
{
  d->features = _features;
  for(Feature* f : d->features)
  {
    f->setParent(this);
  }
}

FeaturesSet::~FeaturesSet() { delete d; }

Feature* FeaturesSet::feature(int _index) const { return d->features.at(_index); }

Feature* FeaturesSet::takeFeature(int _index) const
{
  Feature* f = d->features.at(_index);
  d->features.removeAt(_index);
  f->setParent(nullptr);
  return f;
}

QList<Feature*> FeaturesSet::features() const { return d->features; }

int FeaturesSet::featuresCount() const { return d->features.size(); }

QList<QObject*> FeaturesSet::featuresAsQObject()
{
  return Cartography::Private::list_cast<QObject*>(d->features);
}

QRectF FeaturesSet::envelope() const
{
  if(d->features.isEmpty())
    return QRectF();
  QRectF rect = d->features.first()->geometry()->envelope();
  for(Feature* f : d->features)
  {
    rect |= f->geometry()->envelope();
  }
  return rect;
}

#include "moc_FeaturesSet.cpp"
