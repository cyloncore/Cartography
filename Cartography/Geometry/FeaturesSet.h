#ifndef _GEOMETRYML_FEATURESSET_H_
#define _GEOMETRYML_FEATURESSET_H_

#include <QObject>

namespace Cartography::Geometry
{
  class Feature;
  class FeaturesSet : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QRectF envelope READ envelope NOTIFY featuresChanged)
    Q_PROPERTY(int featuresCount READ featuresCount NOTIFY featuresChanged)
    Q_PROPERTY(QList<QObject*> features READ featuresAsQObject NOTIFY featuresChanged)
  public:
    explicit FeaturesSet(QObject* parent = 0);
    explicit FeaturesSet(const QList<Feature*>& _features, QObject* parent = 0);
    virtual ~FeaturesSet();
    int featuresCount() const;
    Q_INVOKABLE Cartography::Geometry::Feature* feature(int _index) const;
    Q_INVOKABLE Cartography::Geometry::Feature* takeFeature(int _index) const;
    QList<Feature*> features() const;
    QRectF envelope() const;
  private:
    QList<QObject*> featuresAsQObject();
  signals:
    void featuresChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Geometry

#endif
