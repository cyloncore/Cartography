#include "Geometry_p.h"

#include <QRectF>

using namespace Cartography::Geometry;

Geometry::Geometry(Geometry::Private* _d, QObject* parent) : QObject(parent), d(_d) {}

Geometry::~Geometry() { delete d; }

Geometry::Type Geometry::type() const { return d->type; }

Cartography::CoordinateSystem Geometry::coordinateSystem() const { return d->coordinateSystem; }

void Geometry::setCoordinateSystem(const Cartography::CoordinateSystem& _coordinateSystem)
{
  d->coordinateSystem = _coordinateSystem;
  emit(coordinateSystemChanged());
}

#include "moc_Geometry.cpp"
