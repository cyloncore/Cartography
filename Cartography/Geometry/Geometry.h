#pragma once

#include <QObject>

#include <Cartography/Forward.h>

namespace Cartography::Geometry
{
  class GeometryAttachedProperty;
  class Geometry : public QObject
  {
    Q_OBJECT
  protected:
    struct Private;
    Private* const d;
  public:
    enum class Type
    {
      Undefined,
      Collection,
      LineString,
      LinearRing,
      Point,
      Polygon
    };
    Q_ENUMS(Type)
    Q_PROPERTY(Type type READ type CONSTANT)
    Q_PROPERTY(QRectF envelope READ envelope NOTIFY geometryChanged)
    Q_PROPERTY(Cartography::CoordinateSystem coordinateSystem READ coordinateSystem WRITE
                 setCoordinateSystem NOTIFY coordinateSystemChanged)
    enum class Dimension
    {
      Zero = 0,
      Two = 2,
      Three = 3
    };
  protected:
    explicit Geometry(Private* _d, QObject* parent = 0);
  signals:
    void geometryChanged();
    void coordinateSystemChanged();
  public:
    virtual ~Geometry();
    Type type() const;
    virtual QRectF envelope() const = 0;
    virtual Dimension dimension() const = 0;
    virtual Geometry* clone(QObject* _parent = nullptr) const = 0;
    template<typename _TR_, typename _TA_ = _TR_>
    static QList<_TR_*> clone(const QList<_TA_*>& _geom, QObject* _parent = nullptr)
      requires(std::is_convertible_v<_TA_*, _TR_*>);
    Cartography::CoordinateSystem coordinateSystem() const;
    void setCoordinateSystem(const Cartography::CoordinateSystem& _coordinateSystem);
    /**
     * Load a \ref Geometry from the givem \p _filename.
     * If Cartography was built with GDAL, it will first attempt to load the file with GDAL,
     * otherwise it fallbacks to using \ref euclid::io::from_wkt.
     * @return the loaded \ref Geometry (the responsability to delete belongs to the caller) or a
     * nullptr in case of failure
     */
    static Geometry* load(const QString& _filename);
  public:
    static GeometryAttachedProperty* qmlAttachedProperties(QObject* object);
  protected:
    static inline Dimension max(Dimension _d1, Dimension _d2);
  };

  template<typename _TR_, typename _TA_>
  QList<_TR_*> Geometry::clone(const QList<_TA_*>& _geom, QObject* _parent)
    requires(std::is_convertible_v<_TA_*, _TR_*>)
  {
    QList<_TR_*> res;
    for(_TR_* g : _geom)
      res.append(static_cast<_TR_*>(g->clone(_parent)));
    return res;
  }

} // namespace Cartography::Geometry
