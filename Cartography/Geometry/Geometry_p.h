#include "Geometry.h"

#include <Cartography/CoordinateSystem.h>

namespace Cartography::Geometry
{
  struct Geometry::Private
  {
    Private(Type _type) : type(_type) {}
    const Type type;
    Cartography::CoordinateSystem coordinateSystem;
  };

  inline Geometry::Dimension Geometry::max(Geometry::Dimension _d1, Geometry::Dimension _d2)
  {
    switch(_d1)
    {
    case Dimension::Zero:
      return _d2;
    case Dimension::Two:
      switch(_d2)
      {
      case Dimension::Zero:
        Q_FALLTHROUGH();
      case Dimension::Two:
        return _d1;
      case Dimension::Three:
        return _d2;
      }
      break;
    case Dimension::Three:
      return _d1;
    }
    qFatal("impossible");
  }
} // namespace Cartography::Geometry
