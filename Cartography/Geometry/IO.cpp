#include "Geometry.h"

#include <QFile>

#include <Cartography/config_p.h>

#include <euclid/io>

#ifdef CARTOGRAPHY_HAVE_GDAL
#include <gdal_version.h>

#include <cpl_conv.h>
#include <gdal_priv.h>
#include <ogr_feature.h>
#include <ogrsf_frmts.h>

#include "Interface/Gdal.h"
#endif

#include <Cartography/EuclidSystem.h>

#include "Interface/Euclid.h"

namespace Cartography::Geometry
{
  Geometry* Geometry::load(const QString& _filename)
  {
#ifdef CARTOGRAPHY_HAVE_GDAL
    {

      GDALAllRegister();
      GDALDataset* dataset
        = (GDALDataset*)GDALOpenEx(qPrintable(_filename), GDAL_OF_VECTOR, NULL, NULL, NULL);

      if(dataset)
      {
        QList<OGRGeometry*> objects;
        for(int i = 0; i < dataset->GetLayerCount(); ++i)
        {
          OGRLayer* layer = dataset->GetLayer(i);
          OGRFeature* feature;
          while((feature = layer->GetNextFeature()) != 0)
          {
            objects.append(feature->GetGeometryRef());
          }
        }
        Geometry* result = nullptr;
        switch(objects.size())
        {
        case 0:
          // Empty dataset
          break;
        case 1:
          result = Cartography::Geometry::Interface::fromGdal(objects.first());
          break;
        default:
        {
          Cartography::Geometry::Collection* c = new Cartography::Geometry::Collection;
          for(OGRGeometry* go : objects)
          {
            c->append(Cartography::Geometry::Interface::fromGdal(go));
          }
          result = c;
          break;
        }
        }
        delete dataset;

        return result;
      }
      else
      {
        clog_error("Failed to open file '{}' with GDAL.", _filename);
      }
    }
#endif
    QFile file(_filename);
    if(file.open(QIODevice::ReadOnly))
    {
      Cartography::EuclidSystem::geometry object
        = euclid::io::from_wkt<Cartography::EuclidSystem::geometry>(file.readAll().toStdString());
      if(not object.is_null())
      {
        return Cartography::Geometry::Interface::fromEuclid(object,
                                                            Cartography::CoordinateSystem::wgs84);
      }
      else
      {
        clog_error("Failed to parse file '{}' as WKT", _filename);
      }
    }
    else
    {
      clog_error("Failed to open file '{}'", _filename);
    }
    return nullptr;
  }
} // namespace Cartography::Geometry
