#pragma once

#include <clog_qt>

#include <euclid/operation>

#include <Cartography/Geometry/Collection.h>
#include <Cartography/Geometry/LinearRing.h>
#include <Cartography/Geometry/Point.h>
#include <Cartography/Geometry/Polygon.h>

namespace Cartography::Geometry::Interface
{
  namespace details
  {
    template<bool _add_last_point, typename _builder_t_>
    void to_euclid_line_string(_builder_t_& _builder, const LineString* _geom)
    {
      QList<Point*> points = _geom->points();
      for(int i = 0; i < points.size() - (_add_last_point ? 0 : 1); ++i)
      {
        _builder.add_point(points[i]->x(), points[i]->y(), points[i]->z());
      }
    }
    template<typename _T_>
    _T_* setCS(const Cartography::CoordinateSystem& _cs, _T_* _geometry)
    {
      _geometry->setCoordinateSystem(_cs);
      return _geometry;
    }
  } // namespace details

  template<typename _system_>
  inline typename _system_::geometry toEuclid(const Geometry* _geometry)
  {
    if(not _geometry)
      return typename _system_::geometry();
    using GT = Geometry::Type;
    switch(_geometry->type())
    {
    case GT::Undefined:
      return typename _system_::geometry();
    case GT::LineString:
    {
      const LineString* lr = static_cast<const LineString*>(_geometry);
      typename _system_::line_string::builder builder;
      details::to_euclid_line_string<true>(builder, lr);
      return builder.create();
    }
    case GT::Point:
    {
      const Point* pt = static_cast<const Point*>(_geometry);
      return typename _system_::point(pt->x(), pt->y(), pt->z());
    }
    case GT::Collection:
    {
      const Collection* lr = static_cast<const Collection*>(_geometry);
      typename _system_::multi_geometry::builder builder;
      for(const Geometry* g : lr->elements())
      {
        builder.add_element(toEuclid<_system_>(g));
      }
      return builder.create();
    }
    case GT::LinearRing:
    {
      const LinearRing* lr = static_cast<const LinearRing*>(_geometry);
      typename _system_::linear_ring::builder builder;
      details::to_euclid_line_string<false>(builder, lr);
      return builder.create();
    }
    case GT::Polygon:
    {
      const Polygon* polygon = static_cast<const Polygon*>(_geometry);
      if(polygon->exteriorRing() and not polygon->exteriorRing()->points().empty())
      {
        typename _system_::polygon::builder builder;
        details::to_euclid_line_string<false>(builder, polygon->exteriorRing());
        for(const LinearRing* ring : polygon->holes())
        {
          builder.start_interior_ring();
          details::to_euclid_line_string<false>(builder, ring);
        }

        return builder.create();
      }
    }
    break;
    }

    return typename _system_::geometry();
  }

  template<typename _system_>
  inline Geometry* fromEuclid(const euclid::geometry::geometry<_system_>& _geometry,
                              const Cartography::CoordinateSystem& _cs);

  template<typename _system_>
  inline Point* fromEuclid(const euclid::geometry::point<_system_>& _point,
                           const Cartography::CoordinateSystem& _cs)
  {
    return details::setCS(_cs, new Point(_point.x(), _point.y(), _point.z()));
  }
  template<typename _system_>
  inline LineString* fromEuclid(const euclid::geometry::line_string<_system_>& _line_string,
                                const Cartography::CoordinateSystem& _cs)
  {
    QList<Point*> pts;
    for(auto it = _line_string.points().begin(); it != _line_string.points().end(); ++it)
    {
      euclid::simple::point pt = *it;
      pts.append(new Point(pt.x(), pt.y(), pt.z()));
    }
    return details::setCS(_cs, new LineString(pts));
  }
  template<typename _system_>
  inline LinearRing* fromEuclid(const euclid::geometry::linear_ring<_system_>& _linear_ring,
                                const Cartography::CoordinateSystem& _cs)
  {
    QList<Point*> pts;
    for(auto it = _linear_ring.points().begin(); it != _linear_ring.points().end(); ++it)
    {
      euclid::simple::point pt = *it;
      pts.append(new Point(pt.x(), pt.y(), pt.z()));
    }
    return details::setCS(_cs, new LinearRing(pts));
  }
  template<typename _system_>
  inline Polygon* fromEuclid(const euclid::geometry::polygon<_system_>& _polygon,
                             const Cartography::CoordinateSystem& _cs)
  {
    Polygon* poly = new Polygon(fromEuclid(_polygon.exterior_ring(), _cs));
    for(euclid::geometry::linear_ring<_system_> lr : _polygon.interior_rings())
    {
      poly->appendHole(fromEuclid(lr, _cs));
    }
    return details::setCS(_cs, poly);
  }
  template<typename _system_, template<typename> class _collection_>
    requires(euclid::geometry::traits::is_collection_v<_collection_<_system_>>)
  inline Geometry* fromEuclid(const _collection_<_system_>& _collection,
                              const Cartography::CoordinateSystem& _cs)
  {
    Collection* col = new Collection;

    for(auto e : _collection)
    {
      col->append(fromEuclid(e, _cs));
    }

    return details::setCS(_cs, col);
  }

  namespace details
  {
    template<typename _system_>
    class from_euclid_dispatch_op
        : public euclid::operation::operation<from_euclid_dispatch_op<_system_>, Geometry*>
    {
    public:
      using return_type = Geometry*;
      using euclid::operation::operation<from_euclid_dispatch_op<_system_>, return_type>::operation;

      template<typename _T_>
      return_type operator()(const _T_& _t, const Cartography::CoordinateSystem& _cs)
      {
        return fromEuclid(_t, _cs);
      }
    };
  } // namespace details

  template<typename _system_>
  inline Geometry* fromEuclid(const euclid::geometry::geometry<_system_>& _geometry,
                              const Cartography::CoordinateSystem& _cs)
  {
    using dispatch_op = details::from_euclid_dispatch_op<_system_>;
    Geometry* geom = euclid::operation::dispatch<dispatch_op>(_geometry, _cs);
    clog_assert(geom);
    return geom;
  }
} // namespace Cartography::Geometry::Interface
