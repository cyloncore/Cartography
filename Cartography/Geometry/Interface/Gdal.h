#pragma once

#include <Cartography/CoordinateSystem.h>

class OGRFeature;
class OGRFeatureDefn;
class OGRGeometry;
class OGRPoint;
class OGRSpatialReference;

class QByteArray;

namespace Cartography::Geometry::Interface
{
  // GDAL
  Feature* fromGdal(OGRFeature* _feature);
  Feature* fromGdal(OGRFeatureDefn* _definition);
  Geometry* fromGdal(const OGRGeometry* _geometry,
                     const Cartography::CoordinateSystem& _cs = Cartography::CoordinateSystem());
  Point* fromGdal(const OGRPoint* _point);
  OGRFeature* toGdal(const Feature* _feature, OGRFeatureDefn* _definition);
  /**
   * @param _multi_surface indicates wehter a collection is converted to \ref OGRGeometryCollection
   * or to a more specific collection type (such as \ref OGRMultiPoint...)
   */
  OGRGeometry* toGdal(const Geometry* _geometry, bool _multi_surface,
                      OGRSpatialReference* _sr = nullptr);
  OGRPoint* toGdal(const Point* _point);

} // namespace Cartography::Geometry::Interface
