#include "Mapnik.h"

#include <mapnik/featureset.hpp>

#include <QDateTime>
#include <QDebug>
#include <QRectF>
#include <QTimeZone>
#include <QVariant>

#include <Cartography/Geometry/AbstractFeaturesSource.h>
#include <Cartography/Geometry/Collection.h>
#include <Cartography/Geometry/Feature.h>
#include <Cartography/Geometry/FeaturesSet.h>
#include <Cartography/Geometry/LinearRing.h>
#include <Cartography/Geometry/Point.h>
#include <Cartography/Geometry/Polygon.h>

namespace Cartography::Geometry::Interface
{
  mapnik::feature_ptr toMapnik(const Feature* _feature)
  {
    mapnik::feature_ptr f(
      new mapnik::feature_impl(std::make_shared<mapnik::context_type>(), _feature->id()));
    f->set_geometry(toMapnik(_feature->geometry()));
    QVariantHash attr = _feature->attributes();
    for(QVariantHash::const_iterator it = attr.begin(); it != attr.end(); ++it)
    {
      mapnik::value val;
      switch(it.value().typeId())
      {
      case QVariant::Bool:
        val = mapnik::value(it.value().toBool());
        break;
      case QVariant::Int:
        val = mapnik::value(it.value().toInt());
        break;
      case QVariant::Double:
        val = mapnik::value(it.value().toDouble());
        break;
      default:
        val = mapnik::value(mapnik::value_unicode_string(it.value().toString().toUtf8().data()));
        break;
      }
      f->put_new(it.key().toStdString(), val);
    }
    return f;
  }
  namespace details
  {
    void toMapnik_line_string(const LineString* ls, mapnik::geometry::line_string<double>* mls)
    {
      for(Point* pt : ls->points())
      {
        mls->add_coord(pt->x(), pt->y());
      }
    }
    mapnik::geometry::linear_ring<double> toMapnik(const LinearRing* _lr)
    {
      mapnik::geometry::linear_ring<double> mls;
      toMapnik_line_string(_lr, &mls);
      return mls;
    }
    template<typename _MT_>
    _MT_ toMapnik_(const Geometry* _geometry);

    template<>
    mapnik::geometry::point<double>
      toMapnik_<mapnik::geometry::point<double>>(const Geometry* _geometry)
    {
      const Point* pt = qobject_cast<const Point*>(_geometry);
      return mapnik::geometry::point<double>(pt->x(), pt->y());
    }
    template<>
    mapnik::geometry::line_string<double>
      toMapnik_<mapnik::geometry::line_string<double>>(const Geometry* _geometry)
    {
      const LineString* ls = qobject_cast<const LineString*>(_geometry);
      mapnik::geometry::line_string<double> mls;
      details::toMapnik_line_string(ls, &mls);
      return mls;
    }
    template<>
    mapnik::geometry::polygon<double>
      toMapnik_<mapnik::geometry::polygon<double>>(const Geometry* _geometry)
    {
      const Polygon* p = qobject_cast<const Polygon*>(_geometry);
      mapnik::geometry::polygon<double> poly;
      poly.set_exterior_ring(details::toMapnik(p->exteriorRing()));
      for(const LinearRing* lr : p->holes())
      {
        poly.add_hole(details::toMapnik(lr));
      }
      return poly;
    }

    template<typename _MCT_>
    _MCT_ toMapnik_multi(const Collection* _collection)
    {
      _MCT_ r;
      for(const Geometry* g : _collection->elements())
      {
        r.push_back(toMapnik_<typename _MCT_::value_type>(g));
      }
      return r;
    }

  } // namespace details
  mapnik::geometry::geometry<double> toMapnik(const Geometry* _geometry)
  {
    switch(_geometry->type())
    {
    case Geometry::Type::Point:
    {
      return details::toMapnik_<mapnik::geometry::point<double>>(_geometry);
    }
    case Geometry::Type::LineString:
    {
      return details::toMapnik_<mapnik::geometry::line_string<double>>(_geometry);
    }
    case Geometry::Type::LinearRing:
    {
      return details::toMapnik(qobject_cast<const LinearRing*>(_geometry));
    }
    case Geometry::Type::Polygon:
    {
      return details::toMapnik_<mapnik::geometry::polygon<double>>(_geometry);
    }
    case Geometry::Type::Collection:
    {
      const Collection* c = qobject_cast<const Collection*>(_geometry);
      switch(c->elementsType())
      {
      case Geometry::Type::Point:
        return details::toMapnik_multi<mapnik::geometry::multi_point<double>>(c);
      case Geometry::Type::LineString:
        return details::toMapnik_multi<mapnik::geometry::multi_line_string<double>>(c);
      case Geometry::Type::Polygon:
        return details::toMapnik_multi<mapnik::geometry::multi_polygon<double>>(c);
      default:
      {
        mapnik::geometry::geometry_collection<double> gc;
        for(const Geometry* g : c->elements())
        {
          gc.push_back(toMapnik(g));
        }
        return gc;
      }
      }
    }
    case Geometry::Type::Undefined:
      return mapnik::geometry::geometry<double>();
    }
    qFatal("Internal error");
  }

} // namespace Cartography::Geometry::Interface

using namespace Cartography::Geometry::Interface::Mapnik;

DataSource::DataSource(Cartography::Geometry::AbstractFeaturesSource* _source)
    : mapnik::datasource(mapnik::parameters()), m_source(_source)
{
}

mapnik::box2d<double> DataSource::envelope() const
{
  QRectF r = m_source->envelope();
  return mapnik::box2d<double>(r.left(), r.top(), r.right(), r.bottom());
}

mapnik::featureset_ptr DataSource::features(const mapnik::query& q) const
{
  return mapnik::featureset_ptr(
    new FeatureSet(m_source->features(QRectF(QPointF(q.get_bbox().minx(), q.get_bbox().miny()),
                                             QPointF(q.get_bbox().maxx(), q.get_bbox().maxy())))));
}

mapnik::featureset_ptr DataSource::features_at_point(const mapnik::coord2d& pt, double tol) const
{
  return mapnik::featureset_ptr(new FeatureSet(m_source->featuresAt(QPointF(pt.x, pt.y), tol)));
}

mapnik::layer_descriptor DataSource::get_descriptor() const
{
  return mapnik::layer_descriptor("name", "encoding");
}

mapnik::datasource::datasource_t DataSource::type() const { return datasource_t::Vector; }

boost::optional<mapnik::datasource_geometry_t> DataSource::get_geometry_type() const
{
  return mapnik::datasource_geometry_t::Collection;
}

FeatureSet::FeatureSet(Cartography::Geometry::FeaturesSet* _features_Set)
    : m_index(0), m_features_set(_features_Set)
{
}

FeatureSet::~FeatureSet() { delete m_features_set; }

mapnik::feature_ptr FeatureSet::next()
{
  if(m_index >= m_features_set->featuresCount())
  {
    return mapnik::feature_ptr(nullptr);
  }
  else
  {
    return Cartography::Geometry::Interface::toMapnik(m_features_set->features()[m_index++]);
  }
}
