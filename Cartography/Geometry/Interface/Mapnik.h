#pragma once

#include <mapnik/datasource.hpp>

#include <Cartography/Forward.h>

namespace Cartography::Geometry::Interface
{
  mapnik::feature_ptr toMapnik(const Feature* _feature);
  mapnik::geometry::geometry<double> toMapnik(const Geometry* _geometry);

  namespace Mapnik
  {
    class DataSource : public ::mapnik::datasource
    {
    public:
      DataSource(Cartography::Geometry::AbstractFeaturesSource* _source);
      virtual mapnik::box2d<double> envelope() const;
      virtual mapnik::featureset_ptr features(const mapnik::query& q) const;
      virtual mapnik::featureset_ptr features_at_point(const mapnik::coord2d& pt,
                                                       double tol = 0) const;
      virtual mapnik::layer_descriptor get_descriptor() const;
      virtual boost::optional<mapnik::datasource_geometry_t> get_geometry_type() const;
      virtual datasource_t type() const;
    private:
      Cartography::Geometry::AbstractFeaturesSource* m_source;
    };
    class FeatureSet : public mapnik::Featureset
    {
    public:
      FeatureSet(Cartography::Geometry::FeaturesSet* _features_set);
      virtual ~FeatureSet();
      virtual mapnik::feature_ptr next();
    private:
      int m_index;
      Cartography::Geometry::FeaturesSet* m_features_set;
    };
  } // namespace Mapnik
} // namespace Cartography::Geometry::Interface
