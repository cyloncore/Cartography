#include <qglobal.h>

class QByteArray;

namespace Cartography::Geometry
{
  class Geometry;
  QByteArray toSpatialite(const Geometry* _point, quint32 _srid);
  Geometry* fromSpatialite(const QByteArray& _data, quint32* _srid);
} // namespace Cartography::Geometry
