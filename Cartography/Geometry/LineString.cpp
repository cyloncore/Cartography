#include "LineString_p.h"

#include "Point.h"
#include "rect_utils_p.h"

using namespace Cartography::Geometry;

CARTOGRAPHY_D_FUNC_DEF(LineString)

LineString::LineString(QObject* parent) : LineString(new Private, parent) {}

LineString::LineString(LineString::Private* _d, QObject* parent) : Geometry(_d, parent) {}

LineString::LineString(const QList<Point*>& _points, LineString::Private* _d, QObject* parent)
    : LineString(_d, parent)
{
  D()->points = _points;
  for(Point* pt : _points)
  {
    pt->setParent(this);
  }
}

LineString::LineString(const QList<Point*>& _points, QObject* parent)
    : LineString(_points, new Private, parent)
{
}

void LineString::append(const QPointF& _pt)
{
  D()->points.append(new Point(_pt.x(), _pt.y(), this));
  emit(pointsChanged());
  emit(geometryChanged());
}

void LineString::append(Point* from_gdal)
{
  from_gdal->setParent(this);
  D()->points.append(from_gdal);
  emit(pointsChanged());
  emit(geometryChanged());
}

void LineString::append(qreal _x, qreal _y)
{
  D()->points.append(new Point(_x, _y, this));
  emit(pointsChanged());
  emit(geometryChanged());
}

void LineString::append(qreal _x, qreal _y, qreal _z)
{
  D()->points.append(new Point(_x, _y, _z, this));
  emit(pointsChanged());
  emit(geometryChanged());
}

void LineString::clear()
{
  qDeleteAll(D()->points);
  D()->points.clear();
  emit(pointsChanged());
  emit(geometryChanged());
}

QList<Point*> LineString::points() const { return D()->points; }

QList<QObject*> LineString::pointsAsQObject() const
{
  return Cartography::Private::list_cast<QObject*>(D()->points);
}

QRectF LineString::envelope() const
{
  if(D()->points.isEmpty())
    return QRectF();
  QRectF r(D()->points.first()->toPoint2D(), D()->points.first()->toPoint2D());
  for(Point* p : D()->points)
  {
    QPointF pt = p->toPoint2D();
    r = unite(r, QRectF(pt, pt));
  }
  return r;
}

Geometry::Dimension LineString::dimension() const
{
  Dimension dim = Dimension::Zero;
  for(Point* p : D()->points)
  {
    dim = max(dim, p->dimension());
    if(dim == Dimension::Three)
      return dim;
  }
  return dim;
}

Geometry* LineString::clone(QObject* _parent) const
{
  LineString* ls = new LineString(_parent);
  static_cast<LineString::Private*>(ls->d)->points = Geometry::clone<Point>(D()->points, ls);
  return ls;
}

#include "moc_LineString.cpp"
