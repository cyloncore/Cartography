#ifndef _CARTOGRAPHYQUICK_LINESEGMENT_H_
#define _CARTOGRAPHYQUICK_LINESEGMENT_H_

#include <Cartography/Global.h>

#include "Geometry.h"

namespace Cartography::Geometry
{
  class Point;
  class LineString : public Geometry
  {
    Q_OBJECT
    Q_PROPERTY(QList<QObject*> points READ pointsAsQObject NOTIFY pointsChanged);
  protected:
    CARTOGRAPHY_D_DECL();
    explicit LineString(Private* _d, QObject* parent = 0);
    explicit LineString(const QList<Point*>& _points, Private* _d, QObject* parent = 0);
  public:
    explicit LineString(QObject* parent = 0);
    explicit LineString(const QList<Point*>& _points, QObject* parent = 0);
    Q_INVOKABLE void append(const QPointF& _pt);
    Q_INVOKABLE void append(Point* _pt);
    Q_INVOKABLE void append(qreal _x, qreal _y);
    Q_INVOKABLE void append(qreal _x, qreal _y, qreal _z);
    Q_INVOKABLE void clear();
    virtual QList<Point*> points() const;
    QRectF envelope() const override;
    Dimension dimension() const override;
    Geometry* clone(QObject* _parent = nullptr) const override;
  private:
    QList<QObject*> pointsAsQObject() const;
  signals:
    void pointsChanged();
  };
} // namespace Cartography::Geometry

#endif
