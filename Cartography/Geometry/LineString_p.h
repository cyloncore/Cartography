#include "LineString.h"

#include "Geometry_p.h"

namespace Cartography::Geometry
{
  struct LineString::Private : Geometry::Private
  {
    Private(Type type) : Geometry::Private(type) {}
    Private() : Private(Geometry::Type::LineString) {}
    QList<Point*> points;
  };
} // namespace Cartography::Geometry
