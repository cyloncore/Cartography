#include "LinearRing.h"

#include "LineString_p.h"
#include "Point.h"

using namespace Cartography::Geometry;

LinearRing::LinearRing(QObject* parent)
    : LineString(new LineString::Private(Type::LinearRing), parent)
{
}

LinearRing::LinearRing(const QList<Point*>& _points, QObject* parent)
    : LineString(_points, new LineString::Private(Type::LinearRing), parent)
{
}

LinearRing::~LinearRing() {}

QList<Point*> LinearRing::points() const
{
  QList<Point*> pts = LineString::points();
  if(not pts.isEmpty())
  {
    pts.append(pts.first());
  }
  return pts;
}

Geometry* LinearRing::clone(QObject* _parent) const
{
  LinearRing* ls = new LinearRing(_parent);
  static_cast<LineString::Private*>(ls->d)->points
    = Geometry::clone<Point>(LineString::points(), ls);
  return ls;
}

#include "moc_LinearRing.cpp"
