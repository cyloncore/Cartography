#ifndef _CARTOGRAPHYQUICK_LINEARRING_H_
#define _CARTOGRAPHYQUICK_LINEARRING_H_

#include "LineString.h"

namespace Cartography::Geometry
{
  class Point;
  class LinearRing : public LineString
  {
    Q_OBJECT
  public:
    explicit LinearRing(QObject* parent = 0);
    explicit LinearRing(const QList<Point*>& _points, QObject* parent = 0);
    virtual ~LinearRing();
    Geometry* clone(QObject* _parent = nullptr) const override;
  public:
    QList<Point*> points() const override;
  };
} // namespace Cartography::Geometry

#endif
