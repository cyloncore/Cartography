#include "Point.h"

#include <QRectF>

#include "Geometry_p.h"

using namespace Cartography::Geometry;

struct Point::Private : Geometry::Private
{
  Private() : Geometry::Private(Geometry::Type::Point) {}

  qreal x = 0.0, y = 0.0, z = 0.0;
  Dimension dim = Dimension::Zero;
};

CARTOGRAPHY_D_FUNC_DEF(Point)

Point::Point(Private* _d, QObject* parent) : Geometry(_d, parent) {}

Point::Point(QObject* parent) : Geometry(new Private, parent) {}

Point::Point(qreal _x, qreal _y, qreal _z, QObject* parent) : Geometry(new Private, parent)
{
  D()->dim = Dimension::Three;
  D()->x = _x;
  D()->y = _y;
  D()->z = _z;
}

Point::Point(qreal _x, qreal _y, QObject* parent) : Geometry(new Private, parent)
{
  D()->dim = Dimension::Two;
  D()->x = _x;
  D()->y = _y;
}

Point::Point(const QPointF& _pt, QObject* parent) : Point(_pt.x(), _pt.y(), parent) {}

Point::Point(const QPointF& _pt, qreal _z, QObject* parent) : Point(_pt.x(), _pt.y(), _z, parent) {}

Point::~Point() {}

Point::Dimension Point::dimension() const { return D()->dim; }

#define UPDATE_DIMENSION(_dim_)                                                                    \
  if(Dimension::_dim_ != D()->dim)                                                                 \
  {                                                                                                \
    D()->dim = Dimension::_dim_;                                                                   \
    emit(dimensionChanged());                                                                      \
  }

qreal Point::x() const { return D()->x; }

void Point::setX(qreal _x)
{
  if(_x != D()->x)
  {
    D()->x = _x;
    emit(xChanged());
    emit(geometryChanged());
  }
  UPDATE_DIMENSION(Two)
}

qreal Point::y() const { return D()->y; }

void Point::setY(qreal _y)
{
  if(_y != D()->y)
  {
    D()->y = _y;
    emit(yChanged());
    emit(geometryChanged());
  }
  UPDATE_DIMENSION(Two)
}

qreal Point::z() const { return D()->z; }

void Point::setZ(qreal _z)
{
  if(_z != D()->z)
  {
    D()->z = _z;
    emit(zChanged());
    emit(geometryChanged());
  }
  UPDATE_DIMENSION(Three)
}

QRectF Point::envelope() const
{
  QPointF pt = toPoint2D();
  return QRectF(pt, pt);
}

QPointF Point::toPoint2D() const { return QPointF(D()->x, D()->y); }

Geometry* Point::clone(QObject* _parent) const { return new Point(new Private(*D()), _parent); }

#include "moc_Point.cpp"
