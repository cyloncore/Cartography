#ifndef _CARTOGRAPHYQUICK_POINT_H_
#define _CARTOGRAPHYQUICK_POINT_H_

#include <Cartography/Global.h>

#include "Geometry.h"

namespace Cartography::Geometry
{
  class Point : public Geometry
  {
    Q_OBJECT
  public:
    Q_ENUMS(Dimension)
    Q_PROPERTY(Dimension dimension READ dimension NOTIFY dimensionChanged)
    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged)
    Q_PROPERTY(qreal z READ z WRITE setZ NOTIFY zChanged)
  public:
    explicit Point(QObject* parent = 0);
    explicit Point(qreal _x, qreal _y, QObject* parent = 0);
    explicit Point(qreal _x, qreal _y, qreal _z, QObject* parent = 0);
    explicit Point(const QPointF& _pt, QObject* parent = 0);
    explicit Point(const QPointF& _pt, qreal _z, QObject* parent = 0);
    virtual ~Point();
    Dimension dimension() const override;
    qreal x() const;
    void setX(qreal _x);
    qreal y() const;
    void setY(qreal _y);
    qreal z() const;
    void setZ(qreal _z);
    virtual QRectF envelope() const;
    QPointF toPoint2D() const;
    Geometry* clone(QObject* _parent = nullptr) const override;
  signals:
    void dimensionChanged();
    void xChanged();
    void yChanged();
    void zChanged();
  private:
    CARTOGRAPHY_D_DECL();
    Point(Private* _d, QObject* parent);
  };
} // namespace Cartography::Geometry

#endif
