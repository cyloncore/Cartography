#include "Polygon.h"

#include <QRectF>

#include "Geometry_p.h"
#include "LinearRing.h"

using namespace Cartography::Geometry;

struct Polygon::Private : public Geometry::Private
{
  Private() : Geometry::Private(Geometry::Type::Polygon) {}
  LinearRing* exterior_ring = nullptr;
  QList<LinearRing*> holes;
};

CARTOGRAPHY_D_FUNC_DEF(Polygon)

Polygon::Polygon(QObject* parent) : Geometry(new Private, parent) {}

Polygon::Polygon(LinearRing* _ring, QObject* parent) : Polygon(parent) { setExteriorRing(_ring); }

Polygon::~Polygon() {}

void Polygon::setExteriorRing(LinearRing* _ring)
{
  if(D()->exterior_ring)
  {
    disconnect(D()->exterior_ring, SIGNAL(geometryChanged()), this, SIGNAL(geometryChanged()));
  }
  delete D()->exterior_ring;
  D()->exterior_ring = _ring;
  if(D()->exterior_ring)
  {
    D()->exterior_ring->setParent(this);
  }
  connect(_ring, SIGNAL(geometryChanged()), this, SIGNAL(geometryChanged()));
  emit(exteriorRingChanged());
  emit(geometryChanged());
}

LinearRing* Polygon::exteriorRing() const { return D()->exterior_ring; }

LinearRing* Polygon::createHole()
{
  LinearRing* lr = new LinearRing;
  appendHole(lr);
  return lr;
}

void Polygon::clearHoles()
{
  for(LinearRing* lr : D()->holes)
    lr->deleteLater();
  D()->holes.clear();
  emit(holesChanged());
  emit(geometryChanged());
}

void Polygon::appendHole(LinearRing* _ring)
{
  _ring->setParent(this);
  D()->holes.append(_ring);
  connect(_ring, SIGNAL(geometryChanged()), this, SIGNAL(geometryChanged()));
  emit(holesChanged());
  emit(geometryChanged());
}

QList<LinearRing*> Polygon::holes() const { return D()->holes; }

QList<QObject*> Polygon::holesAsQObject() const
{
  return Cartography::Private::list_cast<QObject*>(D()->holes);
}

QRectF Polygon::envelope() const
{
  if(D()->exterior_ring)
  {
    return D()->exterior_ring->envelope();
  }
  else
  {
    return QRectF();
  }
}

Geometry::Dimension Polygon::dimension() const
{
  if(D()->exterior_ring)
  {
    Dimension dim = D()->exterior_ring->dimension();
    for(LinearRing* h : D()->holes)
    {
      if(dim == Dimension::Three)
        return dim;
      dim = max(dim, h->dimension());
    }
    return dim;
  }
  else
  {
    return Dimension::Three;
  }
}

Geometry* Polygon::clone(QObject* _parent) const
{
  Polygon* p = new Polygon(_parent);
  Private* pd = static_cast<Private*>(p->d);
  pd->exterior_ring = static_cast<LinearRing*>(D()->exterior_ring->clone(p));
  pd->holes = Geometry::clone<LinearRing>(D()->holes, p);
  return p;
}

#include "moc_Polygon.cpp"
