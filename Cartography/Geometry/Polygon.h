#ifndef _CARTOGRAPHYQUICK_POLYGON_H_
#define _CARTOGRAPHYQUICK_POLYGON_H_

#include <Cartography/Global.h>

#include "Geometry.h"

namespace Cartography::Geometry
{
  class LinearRing;
  class Polygon : public Geometry
  {
    Q_OBJECT
    Q_PROPERTY(Cartography::Geometry::LinearRing* exteriorRing READ exteriorRing WRITE
                 setExteriorRing NOTIFY exteriorRingChanged)
    Q_PROPERTY(QList<QObject*> holes READ holesAsQObject NOTIFY holesChanged)
  public:
    explicit Polygon(QObject* parent = 0);
    explicit Polygon(LinearRing* _ring, QObject* parent = 0);
    virtual ~Polygon();
    void setExteriorRing(LinearRing* _ring);
    LinearRing* exteriorRing() const;
    Q_INVOKABLE Cartography::Geometry::LinearRing* createHole();
    Q_INVOKABLE void clearHoles();
    Q_INVOKABLE void appendHole(Cartography::Geometry::LinearRing* _ring);
    QList<LinearRing*> holes() const;
    QRectF envelope() const override;
    Dimension dimension() const override;
    Geometry* clone(QObject* _parent = nullptr) const override;
  signals:
    void exteriorRingChanged();
    void holesChanged();
  private:
    QList<QObject*> holesAsQObject() const;
  private:
    CARTOGRAPHY_D_DECL();
  };
} // namespace Cartography::Geometry

#endif
