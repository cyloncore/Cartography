#include "GDALFeaturesSource.h"

#include <QRectF>
#include <QUrl>

#include <gdal/gdal_priv.h>
#include <gdal/ogrsf_frmts.h>

#include <Cartography/Geometry/Feature.h>
#include <Cartography/Geometry/FeaturesSet.h>
#include <Cartography/Geometry/Interface/Gdal.h>

using namespace Cartography::Geometry::Sources;

struct GDALFeaturesSource::Private
{
  QUrl url;
  QString errorMessage;
#if GDAL_VERSION_MAJOR >= 2
  GDALDataset* gdalDataset = nullptr;
#else
  OGRDataset* gdalDataset = nullptr;
#endif
};

GDALFeaturesSource::GDALFeaturesSource(QObject* parent)
    : AbstractFeaturesSource(parent), d(new Private)
{
}

GDALFeaturesSource::~GDALFeaturesSource()
{
  delete d->gdalDataset;
  delete d;
}

QString GDALFeaturesSource::errorMessage() const { return d->errorMessage; }

QRectF GDALFeaturesSource::envelope()
{
  if(d->gdalDataset)
  {
    OGREnvelope env;
    OGRErr err = d->gdalDataset->GetLayer(0)->GetExtent(&env);
    Q_UNUSED(err);
    return QRectF(QPointF(env.MinX, env.MinY), QPointF(env.MaxX, env.MaxY));
  }
  else
  {
    return QRectF();
  }
}

Cartography::Geometry::FeaturesSet* GDALFeaturesSource::features(const QRectF& _rect)
{
  QList<Cartography::Geometry::Feature*> res;

  if(d->gdalDataset)
  {
    OGRLayer* layer = d->gdalDataset->GetLayer(0);
    layer->SetSpatialFilterRect(_rect.left(), _rect.top(), _rect.right(), _rect.bottom());
    layer->ResetReading();
    while(OGRFeature* ofeature = layer->GetNextFeature())
    {
      res.append(Cartography::Geometry::Interface::fromGdal(ofeature));
    }
  }
  return new Cartography::Geometry::FeaturesSet(res);
}

Cartography::Geometry::FeaturesSet* GDALFeaturesSource::featuresAt(const QPointF& _rect, qreal _tol)
{
  QPointF tol(_tol, _tol);
  return features(QRectF(_rect - tol, _rect + tol));
}

bool GDALFeaturesSource::load()
{
  if(d->gdalDataset)
  {
    delete d->gdalDataset;
    d->gdalDataset = nullptr;
    emit(validChanged());
  }
#if GDAL_VERSION_MAJOR >= 2
  d->gdalDataset = (GDALDataset*)GDALOpenEx(d->url.toLocalFile().toLatin1().constData(),
                                            GDAL_OF_VECTOR | GDAL_OF_UPDATE, NULL, NULL, NULL);
#else
  d->gdalDataset = OGRSFDriverRegistrar::Open(d->url.toLocalFile().toLatin1().constData(), TRUE);
#endif
  emit(validChanged());

  if(d->gdalDataset == nullptr)
  {
    d->errorMessage = "Failed to open file!";
    emit(validChanged());
    emit(errorMessageChanged());
    return false;
  }

  if(d->gdalDataset->GetLayerCount() != 1)
  {
    delete d->gdalDataset;
    d->gdalDataset = nullptr;
    d->errorMessage = "Invalid number of layers!";
    emit(validChanged());
    emit(errorMessageChanged());
    return false;
  }

  emit(featuresChanged());
  return true;
}

bool GDALFeaturesSource::save()
{
  if(d->gdalDataset)
  {
#if GDAL_VERSION_MAJOR >= 2
    d->gdalDataset->FlushCache();
    return true;
#else
    if(d->gdalDataset->SyncToDisk() == 0)
    {
      return true;
    }
    else
    {
      d->errorMessage = "Failed to sync to disk.";
      emit(errorMessageChanged());
      return false;
    }
#endif
  }
  else
  {
    d->errorMessage = "No dataset.";
    emit(errorMessageChanged());
    return false;
  }
}
#include <QDebug>
bool GDALFeaturesSource::create(const QString& _driver)
{
  GDALDriver* poDriver = GetGDALDriverManager()->GetDriverByName(qPrintable(_driver));
  if(not poDriver)
  {
    d->errorMessage = QString("Failed to find '%1' driver").arg(_driver);
    emit(errorMessageChanged());
    return false;
  }
  QString filename = d->url.toLocalFile();
  if(d->url.scheme().isEmpty())
  {
    filename = d->url.toString();
  }
  d->gdalDataset = poDriver->Create(filename.toLatin1().constData(), 0, 0, 0, GDT_Unknown, nullptr);
  emit(validChanged());
  if(d->gdalDataset == nullptr)
  {
    d->errorMessage = "Failed to create file: " + filename;
    emit(errorMessageChanged());
    return false;
  }
  d->gdalDataset->CreateLayer("layer", nullptr, wkbPolygon, NULL);
  return true;
}

bool GDALFeaturesSource::record(Cartography::Geometry::Feature* _feature)
{
  OGRErr err;
  OGRLayer* layer = d->gdalDataset->GetLayer(0);
  OGRFeature* ogr_feature
    = Cartography::Geometry::Interface::toGdal(_feature, layer->GetLayerDefn());
  if(_feature->id() != Cartography::Geometry::Feature::NO_ID)
  {
    err = layer->SetFeature(ogr_feature);
  }
  else
  {
    err = layer->CreateFeature(ogr_feature);
    _feature->setId(ogr_feature->GetFID());
  }
  delete ogr_feature;
  emit(featuresChanged());
  return err == OGRERR_NONE;
}

Cartography::Geometry::Feature* GDALFeaturesSource::createFeature()
{
  OGRLayer* layer = d->gdalDataset->GetLayer(0);
  return Cartography::Geometry::Interface::fromGdal(layer->GetLayerDefn());
}

void GDALFeaturesSource::setUrl(const QUrl& _name)
{
  d->url = _name;
  emit(urlChanged());
}

QUrl GDALFeaturesSource::url() const { return d->url; }

bool GDALFeaturesSource::valid() const { return d->gdalDataset; }

#include "moc_GDALFeaturesSource.cpp"
