#ifndef _CARTOGRAPHERML_OGRFEATURESSOURCE_H_
#define _CARTOGRAPHERML_OGRFEATURESSOURCE_H_

#include <Cartography/Geometry/AbstractFeaturesSource.h>

namespace Cartography::Geometry::Sources
{
  class GDALFeaturesSource : public AbstractFeaturesSource
  {
    Q_OBJECT
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString errorMessage READ errorMessage NOTIFY errorMessageChanged)
    Q_PROPERTY(bool valid READ valid NOTIFY validChanged)
  public:
    GDALFeaturesSource(QObject* parent = 0);
    ~GDALFeaturesSource();
  public:
    // List of drivers https://gdal.org/drivers/vector/index.html
    Q_INVOKABLE bool create(const QString& _driver);
    Q_INVOKABLE bool save();
    Q_INVOKABLE bool load();
    QString errorMessage() const;
    QUrl url() const;
    void setUrl(const QUrl& _name);
    bool valid() const;
  public: // AbstractFeaturesSource
    Cartography::Geometry::FeaturesSet* features(const QRectF& _rect) override;
    Cartography::Geometry::FeaturesSet* featuresAt(const QPointF& _rect,
                                                   qreal _tol = 1e-6) override;
    bool record(Cartography::Geometry::Feature* _feature) override;
    QRectF envelope() override;
    Cartography::Geometry::Feature* createFeature() override;
  signals:
    void urlChanged();
    void errorMessageChanged();
    void validChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Geometry::Sources

#endif
