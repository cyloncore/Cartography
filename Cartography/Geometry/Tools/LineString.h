class QPointF;

namespace Cartography::Geometry
{
  class LineString;
  namespace Tools
  {
    namespace LineString
    {
      QPointF tangeant(const Cartography::Geometry::LineString* _line, int _index);
    }
  } // namespace Tools
} // namespace Cartography::Geometry
