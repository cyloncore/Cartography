#include <QList>

class QPointF;

namespace Cartography::Geometry
{
  class LineString;
  class Polygon;
  namespace Tools
  {
    namespace Polygon
    {
      Cartography::Geometry::Polygon* from(const LineString* _line, const QList<qreal>& _thickness);
      Cartography::Geometry::Polygon* from(const QList<QPointF>& _line,
                                           const QList<qreal>& _thickness);
      Cartography::Geometry::Polygon* from(const QList<QPointF>& _line, qreal _thickness);
    } // namespace Polygon
  } // namespace Tools
} // namespace Cartography::Geometry
