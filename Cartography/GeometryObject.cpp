#include "GeometryObject.h"

#include <clog_qt>

#include <QJsonDocument>
#include <QString>

#include <Cartography/config_p.h>

#include <geos_c.h>

#ifdef CARTOGRAPHY_HAVE_GDAL
#include <gdal_version.h>

#include <cpl_conv.h>
#include <gdal_priv.h>
#include <ogr_feature.h>
#include <ogrsf_frmts.h>

#include "Interface/Gdal.h"
#endif

#include <euclid/convert>
#include <euclid/format>
#include <euclid/gdal>
#include <euclid/ops>
#include <euclid/proj>

#include <euclid/extra/filter>
#include <euclid/extra/fix_it>

#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <QRectF>

#include "CoordinateSystem.h"

#include "Point.h"

using namespace Cartography;

struct GeometryObject::Private : public QSharedData
{
  Private() = default;
  Private(const Private& _rhs) : QSharedData()
  {
    coordinateSystem = _rhs.coordinateSystem;
    geometry = _rhs.geometry;
  }
  ~Private() {}
  Cartography::CoordinateSystem coordinateSystem;
  EuclidSystem::geometry geometry;
};

GeometryObject::GeometryObject() : d(new Private) {}

GeometryObject::GeometryObject(const Cartography::CoordinateSystem& _cs,
                               const EuclidSystem::geometry& _geometry)
    : GeometryObject()
{
  d->coordinateSystem = _cs;
  d->geometry = _geometry;
}

GeometryObject::GeometryObject(const Point& _point)
    : GeometryObject(_point.coordinateSystem(),
                     EuclidSystem::point(_point.x(), _point.y(), _point.z()))
{
}

GeometryObject::GeometryObject(const GeometryObject& _rhs) : d(_rhs.d) {}
GeometryObject::~GeometryObject() {}

GeometryObject& GeometryObject::operator=(const GeometryObject& _rhs)
{
  d = _rhs.d;
  return *this;
}

bool GeometryObject::operator==(const GeometryObject& _rhs) const
{
  return d == _rhs.d
         or (isValid() and _rhs.isValid()
             and euclid::ops::near_equals(d->geometry, _rhs.d->geometry, 1e-6));
}

bool GeometryObject::isValid() const
{
  return not d->geometry.is_null() and euclid::ops::is_valid(d->geometry);
}

bool GeometryObject::isEmpty() const { return d->geometry.is_empty(); }

QList<GeometryObject> GeometryObject::toList() const
{
  QList<GeometryObject> r;
  if(d->geometry.is_collection())
  {
    for(auto it = euclid::geometry::begin<EuclidSystem::geometry>(d->geometry);
        it != euclid::geometry::end<EuclidSystem::geometry>(d->geometry); ++it)
    {
      r.append(GeometryObject(d->coordinateSystem, *it));
    }
  }
  else
  {
    r.append(*this);
  }
  return r;
}

QString GeometryObject::toWKT(int _precision) const
{
  return QString::fromStdString(euclid::io::to_wkt(d->geometry, {_precision, 2}));
}

QString GeometryObject::toEWKT(int _precision) const
{
  QString wkt = toWKT(_precision);
  Cartography::CoordinateSystem cs = coordinateSystem();
  if(cs.isValid())
  {
    wkt = QString("SRID=%1;%2").arg(cs.srid()).arg(wkt);
  }
  return wkt;
}

cres_qresult<GeometryObject> GeometryObject::fromWKT(const QString& _data)
{
  GeometryObject obj;
  QByteArray arr = _data.toLatin1();
  // Check if EWKT with SRID, GDAL does not handle that properly
  if(arr.startsWith("SRID="))
  {
    int pos = arr.indexOf(';');
    if(pos < 0)
    {
      return cres_failure("Missing ';' in ewkt: '{}'", _data);
    }
    int srid = arr.mid(5, pos - 5).toInt();

    obj.d->coordinateSystem = Cartography::CoordinateSystem(srid);
    arr = arr.right(arr.size() - pos - 1);
  }
  else
  {
    obj.d->coordinateSystem = Cartography::CoordinateSystem::wgs84;
  }

  obj.d->geometry = euclid::io::from_wkt<EuclidSystem::geometry>(arr.toStdString());
  if(obj.d->geometry.is_null())
  {
    return cres_failure("Got a null geometry from '{}' when parsing from WKT!", _data);
  }
  else
  {
    return cres_success(obj);
  }
}

QString GeometryObject::toJson() const
{
#if defined(EUCLID_GEOS_HAS_JSON)

  EuclidSystem::geometry geom = d->geometry;
  if(d->coordinateSystem != CoordinateSystem::wgs84)
  {
    geom = euclid::proj::transform(d->geometry, d->coordinateSystem.toProj().toStdString(),
                                   CoordinateSystem::wgs84.toProj().toStdString());
  }

  return QString::fromStdString(euclid::io::to_json(geom));
#elif defined(CARTOGRAPHY_HAVE_GDAL)
  OGRGeometry* geom = Interface::toGdal(*this);
  char* txt = geom->exportToJson();
  QString qtxt = QString::fromUtf8(txt);
  CPLFree(txt);
  delete geom;
  return qtxt;
#else
  clog_fatal("Cartography was built without JSON support.");
#endif
}

cres_qresult<GeometryObject> GeometryObject::fromJson(const QString& _data)
{
#if defined(EUCLID_GEOS_HAS_JSON)
  GeometryObject obj;
  obj.d->coordinateSystem = CoordinateSystem::wgs84;
  obj.d->geometry = euclid::io::from_json<EuclidSystem::geometry>(_data.toStdString());
  if(obj.d->geometry.is_null())
  {
    return cres_failure("Got a null geometry from '{}' when parsing using JSON!", _data);
  }
  else
  {
    return cres_success(obj);
  }
#elif defined(CARTOGRAPHY_HAVE_GDAL)
  QByteArray arr = _data.toLatin1();
  char* data_str = const_cast<char*>(arr.data());
  OGRGeometry* geom = reinterpret_cast<OGRGeometry*>(OGR_G_CreateGeometryFromJson(data_str));
  if(geom)
  {
    GeometryObject obj = Interface::fromGdal(geom);
    delete geom;
    if(obj.isValid())
    {
      return cres_success(obj);
    }
    else
    {
      return cres_failure("invalid json");
    }
  }
  else
  {
    return cres_failure("invalid json");
  }
#else
  clog_fatal("Cartography was built without JSON support.");
#endif
}

QJsonValue GeometryObject::toJsonValue() const
{
  QString json = toJson();
  QJsonParseError err;
  QJsonDocument doc = QJsonDocument::fromJson(json.toUtf8(), &err);
  if(doc.isNull())
    return QJsonValue();
  if(doc.isArray())
    return doc.array();
  if(doc.isObject())
    return doc.object();
  qFatal("should not happen");
}

cres_qresult<GeometryObject> GeometryObject::fromJsonValue(const QJsonValue& _data)
{
  QJsonDocument doc;
  if(_data.isArray())
    doc.setArray(_data.toArray());
  if(_data.isObject())
    doc.setObject(_data.toObject());
  return fromJson(doc.toJson(QJsonDocument::Compact));
}

QString GeometryObject::toGML() const
{
#ifdef CARTOGRAPHY_HAVE_GDAL
  OGRGeometry* geom = Interface::toGdal(*this);
  char* txt = geom->exportToGML();
  QString qtxt = QString::fromLatin1(txt);
  CPLFree(txt);
  delete geom;
  return qtxt;
#else
  clog_fatal("Cartography was built without GML support.");
#endif
}

cres_qresult<GeometryObject> GeometryObject::fromGML(const QString& _data)
{
#ifdef CARTOGRAPHY_HAVE_GDAL
  QByteArray arr = _data.toLatin1();
  char* data_str = const_cast<char*>(arr.data());
  OGRGeometry* geom = OGRGeometryFactory::createFromGML(data_str);
  GeometryObject obj = Interface::fromGdal(geom);
  delete geom;
  return cres_success(obj);
#else
  clog_fatal("Cartography was built without GML support.");
#endif
}

cres_qresult<GeometryObject> GeometryObject::fromGeoVariant(const QVariant& _data)
{
  QJsonDocument doc = QJsonDocument::fromVariant(_data);
  if(doc.isNull())
  {
    return cres_failure("Invalid GeometryObject: %1", _data);
  }
  else
  {
    return fromJson(doc.toJson(QJsonDocument::Compact));
  }
}

cres_qresult<GeometryObject> GeometryObject::polygon(const QVariantList& _exteriorRing,
                                                     const QVariantList& _interiorRings)
{
  QVariantHash h;
  h["type"] = "Polygon";
  QVariantList coordinates;
  QVariantList eR;
  eR.append(QVariant(_exteriorRing));
  coordinates.append(eR);
  coordinates += _interiorRings;
  h["coordinates"] = coordinates;
  return fromGeoVariant(h);
}

QVariant GeometryObject::toGeoVariant() const
{
  QString json = toJson();
  QJsonDocument doc = QJsonDocument::fromJson(json.toUtf8());
  return doc.toVariant();
}

QByteArray GeometryObject::toWKB(bool _extended) const
{
  if(not d->geometry.is_null())
  {
    auto [hGEOSCtxt, hThisGeosGeom_c] = euclid::to_geos(d->geometry);
    GEOSGeometry* hThisGeosGeom = GEOSGeom_clone_r(hGEOSCtxt, hThisGeosGeom_c);
    GEOSWKBWriter* wkbWriter = GEOSWKBWriter_create_r(hGEOSCtxt);
    if(_extended)
    {
      if(coordinateSystem().isValid())
      {
        GEOSSetSRID_r(hGEOSCtxt, hThisGeosGeom, coordinateSystem().srid());
      }
      GEOSWKBWriter_setIncludeSRID_r(hGEOSCtxt, wkbWriter, true);
    }
    GEOSWKBWriter_setOutputDimension_r(hGEOSCtxt, wkbWriter, d->geometry.dimensions());
    std::size_t size;
    unsigned char* data = GEOSWKBWriter_write_r(hGEOSCtxt, wkbWriter, hThisGeosGeom, &size);
    QByteArray res((const char*)data, size);
    GEOSFree_r(hGEOSCtxt, data);

    GEOSGeom_destroy_r(hGEOSCtxt, hThisGeosGeom);
    GEOSWKBWriter_destroy_r(hGEOSCtxt, wkbWriter);

    return res;
  }
  return QByteArray();
}

cres_qresult<GeometryObject> GeometryObject::fromWKB(const QByteArray& _data, bool _extended)
{
  GEOSContextHandle_t hGEOSCtxt = euclid::to_geos(euclid::geos::context::get_context());

  GEOSWKBReader* wkbReader = GEOSWKBReader_create_r(hGEOSCtxt);
  GEOSGeometry* hThisGeosGeom
    = GEOSWKBReader_read_r(hGEOSCtxt, wkbReader, (const unsigned char*)_data.data(), _data.size());

  GeometryObject obj;
  obj.d->geometry = euclid::from_geos(hThisGeosGeom);

  if(_extended and hThisGeosGeom)
  {
    int srid = GEOSGetSRID_r(hGEOSCtxt, hThisGeosGeom);
    if(srid > 0)
    {
      obj.d->coordinateSystem = Cartography::CoordinateSystem(srid);
    }
  }
  GEOSWKBReader_destroy_r(hGEOSCtxt, wkbReader);

  if(obj.isValid())
  {
    return cres_success(obj);
  }
  else
  {
    return cres_failure("invalid wkb");
  }
}

cres_qresult<GeometryObject> GeometryObject::fromString(const QString& _data)
{
  {
    auto const& [success, value, errmsg] = fromWKT(_data);
    if(success)
    {
      return cres_success(value.value());
    }
    else
    {
      clog_error("Not WKT: {}", errmsg.value());
    }
  }
  {
    auto const& [success, value, errmsg] = fromJson(_data);
    if(success)
    {
      return cres_success(value.value());
    }
    else
    {
      clog_error("Not JSON: {}", errmsg.value());
    }
  }
  return cres_failure("neither valid WKT nor valid JSON");
}

EuclidSystem::geometry GeometryObject::geometry() const { return d->geometry; }

bool GeometryObject::contains(const Point& _pt) const
{
  return not d->geometry.is_null()
         and euclid::ops::contains(d->geometry, EuclidSystem::point(_pt.toEuclid()));
}

bool GeometryObject::within(const GeometryObject& _rhs) const
{
  return euclid::ops::within(d->geometry, _rhs.d->geometry);
}

bool GeometryObject::contains(const GeometryObject& _rhs) const
{
  return euclid::ops::contains(d->geometry, _rhs.d->geometry);
}

bool GeometryObject::overlaps(const GeometryObject& _rhs) const
{
  return euclid::ops::overlaps(d->geometry, _rhs.d->geometry);
}

bool GeometryObject::intersects(const GeometryObject& _rhs) const
{
  return euclid::ops::intersects(d->geometry, _rhs.d->geometry);
}

bool GeometryObject::touches(const GeometryObject& _rhs) const
{
  return euclid::ops::touches(d->geometry, _rhs.d->geometry);
}

bool GeometryObject::disjoint(const GeometryObject& _rhs) const
{
  return euclid::ops::disjoint(d->geometry, _rhs.d->geometry);
}

GeometryObject GeometryObject::intersection(const GeometryObject& _rhs) const
{
  return GeometryObject(
    d->coordinateSystem,
    euclid::ops::intersection(d->geometry, _rhs.transform(d->coordinateSystem).d->geometry));
}

GeometryObject GeometryObject::difference(const GeometryObject& _rhs) const
{
  return GeometryObject(
    d->coordinateSystem,
    euclid::ops::difference(d->geometry, _rhs.transform(d->coordinateSystem).d->geometry));
}

GeometryObject GeometryObject::fixIt(FixItFlags flags, double _threshold)
{
  if(isEmpty())
  { // An empty geometry is already valid
    return *this;
  }
  EuclidSystem::geometry g = d->geometry;

  if(flags.testFlag(FixItFlag::RemoveDuplicatePoints))
  {
    g = euclid::extra::fix_it::remove_duplicate_points(
      g, _threshold * euclid::extra::fix_it::remove_duplicate_points_default_tolerance);
  }
  if(flags.testFlag(FixItFlag::RemoveDeadAngle))
  {
    g = euclid::extra::fix_it::remove_dead_angle(
      g, _threshold * euclid::extra::fix_it::remove_dead_angle_default_distance_tolerance,
      _threshold * euclid::extra::fix_it::remove_dead_angle_default_angle_tolerance);
  }
  if(flags.testFlag(FixItFlag::RemoveTangeantHoles))
  {
    g = euclid::extra::fix_it::remove_tangeant_holes(
      g, _threshold * euclid::extra::fix_it::remove_tangeant_holes_default_tolerance);
    if(flags.testFlag(FixItFlag::RemoveDeadAngle))
    {
      g = euclid::extra::fix_it::remove_dead_angle(
        g, _threshold * euclid::extra::fix_it::remove_dead_angle_default_distance_tolerance,
        _threshold * euclid::extra::fix_it::remove_dead_angle_default_angle_tolerance);
    }
  }
  if(flags.testFlag(FixItFlag::Split))
  {
    g = euclid::extra::fix_it::split(g,
                                     _threshold * euclid::extra::fix_it::split_default_tolerance);
    if(flags.testFlag(FixItFlag::RemoveDeadAngle))
    {
      g = euclid::extra::fix_it::remove_dead_angle(
        g, _threshold * euclid::extra::fix_it::remove_dead_angle_default_distance_tolerance,
        _threshold * euclid::extra::fix_it::remove_dead_angle_default_angle_tolerance);
    }
  }
  if(flags.testFlag(FixItFlag::CorrectSelfIntersection))
  {
    g = euclid::extra::fix_it::correct_self_intersection(
      g, _threshold * euclid::extra::fix_it::correct_self_intersection_default_tolerance);
  }
  return GeometryObject(d->coordinateSystem, g);
}

QRectF GeometryObject::envelope() const
{
  euclid::simple::box e = euclid::ops::envelope(d->geometry);

  return QRectF(e.left(), e.top(), e.width(), e.height());
}

double GeometryObject::area() const { return euclid::ops::area(d->geometry); }

GeometryObject
  GeometryObject::filter(const std::function<bool(const GeometryObject& _object)>& _filter)
{
  return filter([this, _filter](const EuclidSystem::geometry& _geom)
                { return _filter(GeometryObject(d->coordinateSystem, _geom)); });
}

GeometryObject
  GeometryObject::filter(const std::function<bool(const EuclidSystem::geometry& _object)>& _filter)
{
  if(d->geometry.is_collection())
  {
    return GeometryObject(d->coordinateSystem, euclid::extra::filter(d->geometry, _filter));
  }
  else if(_filter(d->geometry))
  {
    return *this;
  }
  else
  {
    return GeometryObject(d->coordinateSystem, EuclidSystem::multi_geometry::create_empty());
  }
}

Cartography::CoordinateSystem GeometryObject::coordinateSystem() const
{
  return d->coordinateSystem;
}

GeometryObject GeometryObject::transform(const Cartography::CoordinateSystem& _cs) const
{
  if(_cs == coordinateSystem())
    return *this;

  EuclidSystem::geometry geom = euclid::proj::transform(
    d->geometry, d->coordinateSystem.toProj().toStdString(), _cs.toProj().toStdString());

  return GeometryObject(_cs, geom);
}

QList<GeometryObject> GeometryObject::load(const QString& _filename)
{
#ifdef CARTOGRAPHY_HAVE_GDAL
  {

    GDALAllRegister();
    GDALDataset* dataset
      = (GDALDataset*)GDALOpenEx(qPrintable(_filename), GDAL_OF_VECTOR, NULL, NULL, NULL);

    if(dataset)
    {
      QList<GeometryObject> objects;
      for(int i = 0; i < dataset->GetLayerCount(); ++i)
      {
        OGRLayer* layer = dataset->GetLayer(i);
        OGRFeature* feature;
        while((feature = layer->GetNextFeature()) != 0)
        {
          objects.append(Interface::fromGdal(feature->GetGeometryRef()));
        }
      }
      return objects;
    }
    else
    {
      clog_error("Failed to open file '{}' with GDAL.", _filename);
    }
  }
#endif
  QFile file(_filename);
  if(file.open(QIODevice::ReadOnly))
  {
    auto const& [success, geometry, errorMessage] = GeometryObject::fromWKT(file.readAll());
    if(success)
    {
      return {geometry.value()};
    }
    else
    {
      clog_error("Failed to parse file '{}' as WKT: {}", _filename, errorMessage.value());
    }
  }
  else
  {
    clog_error("Failed to open file '{}'", _filename);
  }
  return QList<GeometryObject>();
}

#include <QDebug>

QDebug operator<<(QDebug dbg, const GeometryObject& l)
{
  if(l.isValid())
  {
    dbg << l.toWKT();
  }
  else
  {
    dbg << "(invalid geometry)";
  }
  return dbg;
}
