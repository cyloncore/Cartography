#pragma once

#include <clog_qt>
#include <cres_qt>

#include <QSharedDataPointer>
#include <QVariant>

#include "EuclidSystem.h"
#include "Forward.h"

class QString;

namespace Cartography
{
  /**
   * Encapsulate a \ref EuclidSystem::Geometry object and associate it with a \ref CoordinateSystem.
   */
  class GeometryObject
  {
  public:
    GeometryObject();
    /**
     * Create a \ref GeometryObject from a GDAL object
     */
    GeometryObject(const Cartography::CoordinateSystem& _cs, const EuclidSystem::geometry&);
    template<template<typename _system_> class _T_>
    GeometryObject(const Cartography::CoordinateSystem& _cs, const _T_<EuclidSystem>& _object)
        : GeometryObject(_cs, EuclidSystem::geometry(_object))
    {
    }
    template<template<typename _system_> class _C_, template<typename _system_> class _T_>
    GeometryObject(const Cartography::CoordinateSystem& _cs, const _C_<_T_<EuclidSystem>>& _object)
        : GeometryObject(_cs, EuclidSystem::geometry(_object))
    {
    }
    /**
     * Construct a \ref GeometryObject from a point.
     */
    GeometryObject(const Point& _point);
    GeometryObject(const GeometryObject& _rhs);
    ~GeometryObject();
    GeometryObject& operator=(const GeometryObject& _rhs);
    bool isValid() const;
  public:
    // BEGIN operators
    bool operator==(const GeometryObject& _object) const;
    /**
     * @return true if @arg _pt is inside the geometrys
     */
    bool contains(const Point& _pt) const;
    /**
     * @return true if this contains \ref _rhs
     */
    bool contains(const GeometryObject& _rhs) const;
    /**
     * @return true if this overlaps with \ref _rhs
     */
    bool overlaps(const GeometryObject& _rhs) const;
    /**
     * @return true if this intersects with \ref _rhs
     */
    bool intersects(const GeometryObject& _rhs) const;
    /**
     * @return true if this is within \ref _rhs
     */
    bool within(const GeometryObject& _rhs) const;
    /**
     * @return true if this touches with \ref _rhs
     */
    bool touches(const GeometryObject& _rhs) const;
    /**
     * @return true if this is disjoint \ref _rhs
     */
    bool disjoint(const GeometryObject& _rhs) const;
    /**
     * @return the intersection between this and \ref _rhs.
     */
    GeometryObject intersection(const GeometryObject& _rhs) const;
    /**
     * @return this minus \ref _rhs.
     */
    GeometryObject difference(const GeometryObject& _rhs) const;
    // END operators
    enum class FixItFlag
    {
      RemoveDuplicatePoints = 0x1,
      RemoveDeadAngle
      = 0x2, //< attempt to remove dead angle i.e. vertices where the angle is almost null
      RemoveTangeantHoles = 0x4,
      Split = 0x8,                    //< split polygons that have two overlapping segments
      CorrectSelfIntersection = 0x10, //< fix self intersection
      All = RemoveDuplicatePoints | RemoveDeadAngle | RemoveTangeantHoles | Split
            | CorrectSelfIntersection
    };
    Q_DECLARE_FLAGS(FixItFlags, FixItFlag)
    /**
     * Attempt to fix a \ref GeometryObject by applying the operations defined by \p flags.
     * The @p _threshold_scale can be used to increase or lower the thresholds used by the fixing
     * algorithm.
     */
    GeometryObject fixIt(FixItFlags flags = FixItFlag::All, double = 1.0);
  public:
    /**
     * @return the envelope for the \ref GeometryObject
     */
    QRectF envelope() const;
    double area() const;
    /**
     * @return a \ref GeometryObject filtered according to \p _filter
     *
     * For a collection, if \p _filter return true the object is kept otherwise it is removed from a
     * collection. For non collect, if \p _filter return true the object is returned otherwise, an
     * empty object is returned.
     */
    GeometryObject filter(const std::function<bool(const GeometryObject& _object)>& _filter);
    GeometryObject
      filter(const std::function<bool(const EuclidSystem::geometry& _object)>& _filter);
  public:
    /**
     * @return true if the geometry object is empty
     */
    bool isEmpty() const;
    /**
     * @return a list of GeometryObject if this is object is a collection, otherwise return a list
     * with the current object.
     */
    QList<GeometryObject> toList() const;
  public:
    QString toWKT(int precision = 15) const;
    QString toGML() const;
    QString toEWKT(int precision = 15) const;
    QByteArray toWKB(bool _extented = false) const;
    QByteArray toEWKB() const { return toWKB(true); }
    QString toJson() const;
    QJsonValue toJsonValue() const;
    /**
     * Convenient function that return a variant following the GeoJSON convension
     */
    QVariant toGeoVariant() const;
    static cres_qresult<GeometryObject> fromEWKB(const QByteArray& _data)
    {
      return fromWKB(_data, true);
    }
    static cres_qresult<GeometryObject> fromWKB(const QByteArray& _data, bool _extented = false);
    static cres_qresult<GeometryObject> fromWKT(const QString& _data);
    static cres_qresult<GeometryObject> fromGML(const QString& _data);
    static cres_qresult<GeometryObject> fromJson(const QString& _data);
    static cres_qresult<GeometryObject> fromJsonValue(const QJsonValue& _data);
    /**
     * Try to create a GeometryObject from a string, first try with WKT, then JSON.
     */
    static cres_qresult<GeometryObject> fromString(const QString& _data);
    /**
     * Convenient function that return a GeometryObject from a variant following the GeoJSON
     * convension
     */
    static cres_qresult<GeometryObject> fromGeoVariant(const QVariant& _data);
    static cres_qresult<GeometryObject> polygon(const QVariantList& _exteriorRing,
                                                const QVariantList& _interiorRings
                                                = QVariantList());

    /**
     * @return the geometry object contains in this object
     */
    EuclidSystem::geometry geometry() const;
    /**
     * @return the \ref CoordinateSystem for this object
     */
    CoordinateSystem coordinateSystem() const;
    /**
     * Transform the object to a different coordinateSystem.
     */
    GeometryObject transform(const CoordinateSystem& _cs) const;
  public:
    static QList<GeometryObject> load(const QString& _filename);
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace Cartography

QDebug operator<<(QDebug, const Cartography::GeometryObject&);
Q_DECLARE_METATYPE(Cartography::GeometryObject);
