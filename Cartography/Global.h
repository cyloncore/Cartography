#pragma once

#include <type_traits>

#include <QList>
#include <QSharedPointer>

#ifdef __GNU_LIBRARY__
#ifndef __clang__
#define CARTOGRAPHY_GLIBC_CONSTEXPR constexpr
#endif
#endif

#ifndef CARTOGRAPHY_GLIBC_CONSTEXPR
#define CARTOGRAPHY_GLIBC_CONSTEXPR
#endif

// BEGIN list_cast
namespace Cartography::Private
{
  template<typename _T2_, typename _T_>
    requires(std::is_base_of_v<_T2_, _T_>)
  QList<_T2_> list_cast(const QList<_T_>& _list)
  {
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
    return *reinterpret_cast<const QList<_T2_*>*>(&_list);
#pragma GCC diagnostic error "-Wstrict-aliasing"
  }
  template<typename _T2_, typename _T_>
    requires(not std::is_base_of_v<_T2_*, _T_*>)
  QList<_T2_> list_cast(const QList<_T_>& _list)
  {
    QList<_T2_> transitions;
    for(const _T_& t : _list)
    {
      transitions.append(t);
    }
    return transitions;
  }
} // namespace Cartography::Private
// END list_cast

// BEGIN is_instance
namespace Cartography::Private
{
  //
  // The following template can be used to detect if a class is an instance of atempolate
  //
  namespace
  {
    template<typename, template<typename...> typename>
    struct is_instance_impl : public std::false_type
    {
    };

    template<template<typename...> typename U, typename... Ts>
    struct is_instance_impl<U<Ts...>, U> : public std::true_type
    {
    };
  } // namespace

  template<typename T, template<typename...> typename U>
  using is_instance = is_instance_impl<std::decay_t<T>, U>;
  template<typename T, template<typename...> typename U>
  inline constexpr bool is_instance_v = is_instance<T, U>::value;
} // namespace Cartography::Private
// END¨ is_instance

// BEGIN Cartography D-Pointer Utilities

namespace Cartography::Private
{
  template<typename _TD_, typename _TS_, class Enable = void>
  struct CastD
  {
    typedef _TD_* ReturnT;
    static ReturnT get(_TS_ _t) { return static_cast<_TD_*>(_t); }
  };
  template<typename _TD_, typename _TS_>
    requires(not std::is_const_v<_TD_>)
  struct CastD<_TD_, const _TS_>
  {
    typedef _TD_* ReturnT;
    static ReturnT get(_TS_ _t) { return const_cast<_TD_*>(static_cast<const _TD_*>(_t)); }
  };
  template<typename _TD_, typename _TS_>
  struct CastD<_TD_, QSharedPointer<_TS_>>
  {
    typedef std::remove_const_t<_TD_> TD;
    typedef QSharedPointer<TD> ReturnT;
    static ReturnT get(const QSharedPointer<_TS_> _t) { return _t.template staticCast<TD>(); }
  };
  template<typename _TD_, typename _TS_>
  struct CastD<_TD_, QExplicitlySharedDataPointer<_TS_>>
  {
    typedef std::remove_const_t<_TD_> TD;
    typedef QExplicitlySharedDataPointer<TD> ReturnT;
    static ReturnT get(const QExplicitlySharedDataPointer<_TS_> _t)
    {
      return ReturnT(const_cast<TD*>(static_cast<_TD_*>(_t.data())));
    }
  };
  template<typename _TD_, typename _TS_>
  typename CastD<_TD_, _TS_>::ReturnT castD(const _TS_& _t)
  {
    return CastD<_TD_, _TS_>::get(_t);
  };
} // namespace Cartography::Private

/**
 * @internal
 * Macro use to declare the D() functions for casting the private pointer
 */
#define __CARTOGRAPHY_D_FUNC_DECL()                                                                \
  inline decltype(Cartography::Private::castD<Private>(d)) D();                                    \
  inline decltype(Cartography::Private::castD<const Private>(d)) CD();                             \
  inline decltype(Cartography::Private::castD<Private>(d)) NCD() const;                            \
  inline decltype(Cartography::Private::castD<const Private>(d)) D() const

#define CARTOGRAPHY_D_DECL()                                                                       \
  struct Private;                                                                                  \
  __CARTOGRAPHY_D_FUNC_DECL()

#define CARTOGRAPHY_D_FUNC_DEF(_KLASS_)                                                            \
  decltype(Cartography::Private::castD<_KLASS_::Private>(static_cast<_KLASS_*>(nullptr)->d))       \
    _KLASS_::D()                                                                                   \
  {                                                                                                \
    return Cartography::Private::castD<Private>(d);                                                \
  }                                                                                                \
  decltype(Cartography::Private::castD<const _KLASS_::Private>(static_cast<_KLASS_*>(nullptr)->d)) \
    _KLASS_::CD()                                                                                  \
  {                                                                                                \
    return Cartography::Private::castD<const Private>(d);                                          \
  }                                                                                                \
  decltype(Cartography::Private::castD<_KLASS_::Private>(static_cast<_KLASS_*>(nullptr)->d))       \
    _KLASS_::NCD() const                                                                           \
  {                                                                                                \
    return Cartography::Private::castD<Private>(d);                                                \
  }                                                                                                \
  decltype(Cartography::Private::castD<const _KLASS_::Private>(static_cast<_KLASS_*>(nullptr)->d)) \
    _KLASS_::D() const                                                                             \
  {                                                                                                \
    return Cartography::Private::castD<const Private>(d);                                          \
  }

// END Cartography D-Pointer Utilities
