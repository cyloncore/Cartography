#include "Gdal.h"

#include <clog_qt>

#include <ogr_geometry.h>
#include <ogr_spatialref.h>

#include <euclid/convert>
#include <euclid/gdal>

#include <Cartography/CoordinateSystem.h>
#include <Cartography/GeometryObject.h>

namespace Cartography::Interface
{
  CoordinateSystem fromGdal(const OGRSpatialReference* _sr)
  {
    return _sr ? CoordinateSystem(
                   QString("%1:%2").arg(_sr->GetAuthorityName(NULL), _sr->GetAuthorityCode(NULL)))
               : CoordinateSystem();
  }
  OGRSpatialReference* toGdal(const CoordinateSystem& _cs)
  {
    OGRSpatialReference* sr = new OGRSpatialReference;
    OGRErr err = sr->importFromEPSG(_cs.srid());
    if(err != OGRERR_NONE)
    {
      clog_error("Failed to import SRID: %1 with error code %2", _cs.srid(), err);
      delete sr;
      return nullptr;
    }
    else
    {
      return sr;
    }
  }
  GeometryObject fromGdal(const OGRGeometry* _geom)
  {
    return GeometryObject(
      fromGdal(_geom->getSpatialReference()),
      euclid::convert<EuclidSystem>(euclid::temporary_from_gdal<euclid::gdal::geometry>(_geom)));
  }
  OGRGeometry* toGdal(const GeometryObject& _geom)
  {
    euclid::gdal::geometry geometry_gdal = euclid::convert<euclid::gdal>(_geom.geometry());
    OGRGeometry* geom = euclid::to_gdal(geometry_gdal)->clone();
    geom->assignSpatialReference(toGdal(_geom.coordinateSystem()));
    return geom;
  }
} // namespace Cartography::Interface
