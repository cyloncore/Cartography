#pragma once

#include <Cartography/Forward.h>

class OGRGeometry;
class OGRSpatialReference;

namespace Cartography::Interface
{
  CoordinateSystem fromGdal(const OGRSpatialReference* _sr);
  OGRSpatialReference* toGdal(const CoordinateSystem& _cs);
  GeometryObject fromGdal(const OGRGeometry* _geom);
  /**
   * Convert \p _geom to a GDAL \ref OGRGeometry*. Deleting the returned
   * value is the responsability of the caller.
   */
  OGRGeometry* toGdal(const GeometryObject& _geom);
} // namespace Cartography::Interface

#include <clog_format>

#include <cpl_error.h>
#include <ogr_core.h>

clog_format_declare_enum_formatter(CPLErr, CE_None, CE_Debug, CE_Warning, CE_Failure, CE_Fatal);

clog_format_declare_enum_formatter(
  OGRwkbGeometryType, wkbUnknown, wkbPoint, wkbLineString, wkbPolygon, wkbMultiPoint,
  wkbMultiLineString, wkbMultiPolygon, wkbGeometryCollection, wkbCircularString, wkbCompoundCurve,
  wkbCurvePolygon, wkbMultiCurve, wkbMultiSurface, wkbCurve, wkbSurface, wkbPolyhedralSurface,
  wkbTIN, wkbTriangle, wkbNone, wkbLinearRing, wkbCircularStringZ, wkbCompoundCurveZ,
  wkbCurvePolygonZ, wkbMultiCurveZ, wkbMultiSurfaceZ, wkbCurveZ, wkbSurfaceZ, wkbPolyhedralSurfaceZ,
  wkbTINZ, wkbTriangleZ, wkbPointM, wkbLineStringM, wkbPolygonM, wkbMultiPointM,
  wkbMultiLineStringM, wkbMultiPolygonM, wkbGeometryCollectionM, wkbCircularStringM,
  wkbCompoundCurveM, wkbCurvePolygonM, wkbMultiCurveM, wkbMultiSurfaceM, wkbCurveM, wkbSurfaceM,
  wkbPolyhedralSurfaceM, wkbTINM, wkbTriangleM, wkbPointZM, wkbLineStringZM, wkbPolygonZM,
  wkbMultiPointZM, wkbMultiLineStringZM, wkbMultiPolygonZM, wkbGeometryCollectionZM,
  wkbCircularStringZM, wkbCompoundCurveZM, wkbCurvePolygonZM, wkbMultiCurveZM, wkbMultiSurfaceZM,
  wkbCurveZM, wkbSurfaceZM, wkbPolyhedralSurfaceZM, wkbTINZM, wkbTriangleZM, wkbPoint25D,
  wkbLineString25D, wkbPolygon25D, wkbMultiPoint25D, wkbMultiLineString25D, wkbMultiPolygon25D,
  wkbGeometryCollection25D);

#include <gdal.h>

clog_format_declare_enum_formatter(GDALDataType, GDT_Unknown, GDT_Byte, GDT_Int8, GDT_UInt16,
                                   GDT_Int16, GDT_UInt32, GDT_Int32, GDT_UInt64, GDT_Int64,
                                   GDT_Float32, GDT_Float64, GDT_CInt32, GDT_CFloat32, GDT_CFloat64,
                                   GDT_TypeCount);
