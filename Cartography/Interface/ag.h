#include <Cartography/CoordinateSystem.h>
#include <ag/qt.h>

namespace ag
{
  template<>
  struct TypeSupport<Cartography::CoordinateSystem, false>
  {
    static void hash(QCryptographicHash* _hash, const Cartography::CoordinateSystem& _t)
    {
      unsigned int srid = _t.srid();
      _hash->addData(reinterpret_cast<const char*>(&srid), sizeof(decltype(srid)));
    }
    static Cartography::CoordinateSystem init() { return Cartography::CoordinateSystem(); }
    static void clean(Cartography::CoordinateSystem* _t) {}
    static Cartography::CoordinateSystem clone(const Cartography::CoordinateSystem& _t)
    {
      return _t;
    }
  };
  template<>
  struct SerialisationSupport<Cartography::CoordinateSystem, false>
  {
    static QJsonValue toJson(const Cartography::CoordinateSystem& _t) { return qint64(_t.srid()); }
    static bool fromJson(Cartography::CoordinateSystem* _t, const QJsonValue& _value, QString*)
    {
      *_t = Cartography::CoordinateSystem(_value.toInt());
      return true;
    }
    static QCborValue toCbor(const Cartography::CoordinateSystem& _t) { return qint64(_t.srid()); }
    static bool fromCbor(Cartography::CoordinateSystem* _t, const QCborValue& _value,
                         QString* _errorMessage)
    {
      *_t = Cartography::CoordinateSystem(_value.toInteger());
      return true;
    }
  };
} // namespace ag
