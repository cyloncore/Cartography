#pragma once

#include <cext_nt>

#include <QtGlobal>

namespace Cartography
{
  using Latitude = cext_named_type<qreal, struct LatitudeTag>;
  using Longitude = cext_named_type<qreal, struct LongitudeTag>;
  using Altitude = cext_named_type<qreal, struct AltitudeTag>;
} // namespace Cartography
