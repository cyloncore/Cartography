#include "Point.h"

#include <clog_qt>
#include <euclid/proj>

#include <QCborMap>
#include <QJsonObject>

#include "CoordinateSystem.h"
#include "GeoPoint.h"

using namespace Cartography;

struct Point::Private : public QSharedData
{
  bool valid = false;
  euclid::simple::point pt;
  CoordinateSystem cs;
  bool operator==(const Private& _rhs) const { return pt == _rhs.pt and cs == _rhs.cs; }
};

Point::Point(const euclid::simple::point& _opt) : Point(_opt, CoordinateSystem::wgs84) {}

Point::Point(const euclid::simple::point& _opt, const CoordinateSystem& _cs)
    : Point(_opt.x(), _opt.y(), _opt.z(), _cs)
{
}

Point::Point(const EuclidSystem::point& _opt) : Point(_opt, CoordinateSystem::wgs84) {}

Point::Point(const EuclidSystem::point& _opt, const CoordinateSystem& _cs)
    : Point(_opt.x(), _opt.y(), _opt.z(), _cs)
{
  clog_assert(d->cs.isValid());
}

Point::Point() : d(new Private) {}

Point::Point(double _x, double _y, double _z, const CoordinateSystem& _cs) : d(new Private)
{
  d->pt = euclid::simple::point(_x, _y, _z);
  d->cs = _cs;
  d->valid = true;
}

Point::Point(double _x, double _y, const CoordinateSystem& _cs)
    : Point(_x, _y, std::numeric_limits<double>::quiet_NaN(), _cs)
{
}

Point::Point(const Point& _rhs) : d(_rhs.d) {}

Point& Point::operator=(const Point& _rhs)
{
  d = _rhs.d;
  return *this;
}

Point::~Point() {}

euclid::simple::point Point::toEuclid() const { return d->pt; }

CoordinateSystem Point::coordinateSystem() const { return d->cs; }

double Point::x() const { return d->pt.x(); }

double Point::y() const { return d->pt.y(); }

double Point::z() const { return d->pt.z(); }

bool Point::operator==(const Point& _rhs) const { return *d == *_rhs.d; }

Point Point::transform(const CoordinateSystem& _coordinateSystem) const
{
  return Point(euclid::proj::transform(d->pt, static_cast<PJ*>(d->cs.projection()),
                                       static_cast<PJ*>(_coordinateSystem.projection()), true),
               _coordinateSystem);
}

namespace
{
  double pow2(double v) { return v * v; }
} // namespace

double Point::distanceTo(const Point& _point)
{
  Point src = *this;
  if(coordinateSystem() == CoordinateSystem::wgs84)
  {
    CoordinateSystem utm = CoordinateSystem::utm(GeoPoint::from(src));
    src = src.transform(utm);
  }
  Point dst = _point.transform(src.coordinateSystem());
  if(std::isfinite(src.z()) and std::isfinite(dst.z()))
  {
    return std::sqrt(pow2(src.x() - dst.x()) + pow2(src.y() - dst.y()) + pow2(src.z() - dst.z()));
  }
  else
  {
    return std::sqrt(pow2(src.x() - dst.x()) + pow2(src.y() - dst.y()));
  }
}

bool Point::isValid() const { return d->valid; }

std::ostream& Cartography::operator<<(std::ostream& os, Point const& value)
{
  os << value.toEuclid() << "@" << value.coordinateSystem();
  return os;
}
