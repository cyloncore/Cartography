#pragma once

#include <QSharedDataPointer>

#include "EuclidSystem.h"
#include "Forward.h"

namespace Cartography
{
  class Point
  {
  public:
    Point();
    Point(const euclid::simple::point& _opt);
    Point(const euclid::simple::point& _opt, const CoordinateSystem& _cs);
    Point(const EuclidSystem::point& _opt);
    Point(const EuclidSystem::point& _opt, const CoordinateSystem& _cs);
    Point(double _x, double _y, double _z, const CoordinateSystem& _cs);
    Point(double _x, double _y, const CoordinateSystem& _cs);
    Point(const Point& _rhs);
    Point& operator=(const Point& _rhs);
    ~Point();
    euclid::simple::point toEuclid() const;
    double x() const;
    double y() const;
    double z() const;
    CoordinateSystem coordinateSystem() const;
    bool operator==(const Point& _rhs) const;
    Point transform(const CoordinateSystem& _coordinateSystem) const;
    bool isValid() const;
    double distanceTo(const Point& _point);
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
  std::ostream& operator<<(std::ostream& os, Point const& value);
} // namespace Cartography

#include <QMetaType>

Q_DECLARE_METATYPE(Cartography::Point);
