#include <clog_qt>

#include "AlgorithmsSingleton.h"

#include <random>

#include <ogr_geometry.h>

#include <euclid/extra/compactness>
#include <euclid/extra/convex_polygon_decomposition>
#include <euclid/extra/fix_it>
#include <euclid/extra/grid_decomposition>
#include <euclid/extra/line_string_fitting>
#include <euclid/geos>

#include <Cartography/Algorithms/LawnMowerPattern.h>
#include <Cartography/Algorithms/WeightedGraphPolygonDecomposition.h>
#include <Cartography/Algorithms/WeightedPolygonDecomposition.h>
#include <Cartography/CoordinateSystem.h>

#include <Cartography/Geometry/Interface/Euclid.h>

using namespace Cartography::Quick::Algorithms;

struct AlgorithmsSingleton::Private
{
  QVariant stats;
  QString lastError;
};

AlgorithmsSingleton::AlgorithmsSingleton(QObject* _parent) : QObject(_parent), d(new Private) {}

Cartography::Geometry::Geometry*
  AlgorithmsSingleton::voronoiDiagram(Cartography::Geometry::Geometry* _geometry,
                                      Cartography::Geometry::Geometry* _envelope, qreal _tol)
{
  Cartography::EuclidSystem::geometry egr
    = Cartography::Geometry::Interface::toEuclid<Cartography::EuclidSystem>(_geometry);
  if(not egr.is_null())
  {
    Cartography::EuclidSystem::geometry envelop_egr
      = Cartography::Geometry::Interface::toEuclid<Cartography::EuclidSystem>(_envelope);
    if(not envelop_egr.is_null())
    {
      std::cout << euclid::io::to_wkt(egr) << std::endl;
      std::cout << egr.is_collection() << std::endl;
      std::cout << euclid::io::to_wkt(envelop_egr) << std::endl;
      Cartography::EuclidSystem::multi_geometry voronoid_egr = euclid::ops::voronoi_diagram(
        euclid::geometry::cast<Cartography::EuclidSystem::multi_point>(egr), envelop_egr, _tol);
      return Cartography::Geometry::Interface::fromEuclid(voronoid_egr,
                                                          _geometry->coordinateSystem());
    }
    else
    {
      return nullptr;
    }
  }
  else
  {
    return nullptr;
  }
}

Cartography::Geometry::Geometry*
  AlgorithmsSingleton::delaunayTriangulation(Cartography::Geometry::Geometry* _geometry, qreal _tol)
{
  Cartography::EuclidSystem::geometry egr
    = Cartography::Geometry::Interface::toEuclid<Cartography::EuclidSystem>(_geometry);
  if(not egr.is_null())
  {
    Cartography::EuclidSystem::geometry triangles_egr
      = euclid::ops::delaunay_triangulation(egr, _tol);
    return Cartography::Geometry::Interface::fromEuclid(triangles_egr,
                                                        _geometry->coordinateSystem());
  }
  else
  {
    return nullptr;
  }
}

Cartography::Geometry::Geometry*
  AlgorithmsSingleton::intersection(Cartography::Geometry::Geometry* _geometry1,
                                    Cartography::Geometry::Geometry* _geometry2)
{
  Cartography::EuclidSystem::geometry egr1
    = Cartography::Geometry::Interface::toEuclid<Cartography::EuclidSystem>(_geometry1);
  Cartography::EuclidSystem::geometry egr2
    = Cartography::Geometry::Interface::toEuclid<Cartography::EuclidSystem>(_geometry2);
  if(not egr1.is_null() and not egr2.is_null())
  {
    return Cartography::Geometry::Interface::fromEuclid(euclid::ops::intersection(egr1, egr2),
                                                        _geometry1->coordinateSystem());
  }
  else
  {
    return nullptr;
  }
}

Cartography::Geometry::Geometry*
  AlgorithmsSingleton::convexPolygonDecomposition(Cartography::Geometry::Geometry* _geometry,
                                                  qreal _tol)
{
  Cartography::EuclidSystem::geometry egr
    = Cartography::Geometry::Interface::toEuclid<Cartography::EuclidSystem>(_geometry);
  if(not egr.is_null())
  {
    Cartography::EuclidSystem::multi_geometry ogr_result
      = euclid::extra::convex_polygon_decomposition(egr, _tol);
    if(not ogr_result.is_null())
    {
      return Cartography::Geometry::Interface::fromEuclid(ogr_result,
                                                          _geometry->coordinateSystem());
    }
    return nullptr;
  }
  else
  {
    return nullptr;
  }
}

Cartography::Geometry::Geometry* AlgorithmsSingleton::weightedGraphPolygonDecomposition(
  Cartography::Geometry::Geometry* _geometry, const QVariantList& _weights, qreal _tol,
  const QVariantList& _start, const QVariantMap& _options, bool _extendedStatistics)
{
  Cartography::EuclidSystem::geometry egr
    = Cartography::Geometry::Interface::toEuclid<Cartography::EuclidSystem>(_geometry);
  if(egr.is<Cartography::EuclidSystem::polygon>())
  {
    Cartography::EuclidSystem::polygon poly_egr = egr.cast<Cartography::EuclidSystem::polygon>();
#if 1
    if(not euclid::ops::is_valid(poly_egr))
    {
      setLastError("Invalid polygon, most likely self-intersecting.");
      return nullptr;
    }
#else
    poly_egr = euclid::extra::fix_it::correct_self_intersection(poly_egr);
#endif
    QList<qreal> weights;
    qreal weights_count = 0.0;
    for(const QVariant& w : _weights)
    {
      qreal ww = w.value<qreal>();
      weights.append(ww);
      weights_count += ww;
    }
    for(qreal& w : weights)
    {
      w /= weights_count;
    }
    QList<euclid::simple::point> starts;
    for(const QVariant& p : _start)
    {
      QVariantList l = p.toList();
      if(l.size() != 2)
      {
        clog_warning("Invalid start point: %1", p);
        return nullptr;
      }
      starts.append(euclid::simple::point(l[0].toReal(), l[1].toReal()));
    }
    Cartography::Algorithms::WeightedGraphPolygonDecompositionParameters parameters{weights, _tol,
                                                                                    starts};

    parameters.read(_options);
    Cartography::Algorithms::WeightedGraphPolygonDecompositionStatistics stats;

    Cartography::EuclidSystem::multi_polygon egr_result
      = Cartography::Algorithms::weightedGraphPolygonDecomposition(
        poly_egr, _geometry->coordinateSystem(), parameters, &stats);
    if(egr_result.is_null())
    {
      setLastError("No decomposition was generated, most likely due to numerical instabilities.");
      return nullptr;
    }
    QVariantMap map;
    QVariantList list_ar, polsby_popper;
    qreal sum_ar = 0.0;
    for(int i = 0; i < stats.areas.length(); ++i)
    {
      qreal ar = stats.areas[i];
      sum_ar += ar;
      list_ar.append(ar);
      if(_extendedStatistics)
      {
        std::cout << euclid::io::to_wkt(egr_result.at(i)) << std::endl;
        polsby_popper.append((double)euclid::extra::compactness::polsby_popper(egr_result.at(i)));
      }
    }
    map["areas"] = list_ar;
    map["areas_sum"] = sum_ar;
    if(_extendedStatistics)
    {
      map["polsby_popper"] = polsby_popper;
    }
    d->stats = map;
    emit(lastStatisticsChanged());
    return Cartography::Geometry::Interface::fromEuclid(egr_result, _geometry->coordinateSystem());
  }
  else
  {
    if(egr.is_null())
    {
      setLastError("Geometry argument to 'weightedGraphPolygonDecomposition' is null");
    }
    else
    {
      setLastError(
        QString(
          "Geometry argument to 'weightedGraphPolygonDecomposition' is not a polygon, got: '%1'")
          .arg(QString::fromStdString(euclid::io::to_wkt(egr))));
    }
    return nullptr;
  }
}

Cartography::Geometry::Geometry*
  AlgorithmsSingleton::weightedPolygonDecomposition(Cartography::Geometry::Geometry* _geometry,
                                                    const QVariantList& _weights, qreal _tol,
                                                    const QVariantList& _start)
{
  Cartography::EuclidSystem::geometry egr
    = Cartography::Geometry::Interface::toEuclid<Cartography::EuclidSystem>(_geometry);
  if(egr.is<Cartography::EuclidSystem::polygon>())
  {
    Cartography::EuclidSystem::polygon poly_egr = egr.cast<Cartography::EuclidSystem::polygon>();
    QList<qreal> weights;
    for(const QVariant& w : _weights)
      weights.append(w.value<qreal>());
    QList<euclid::simple::point> starts;
    for(const QVariant& p : _start)
    {
      QVariantList l = p.toList();
      if(l.size() != 2)
      {
        clog_warning("Invalid start point: %1", p);
        return nullptr;
      }
      starts.append(euclid::simple::point(l[0].toReal(), l[1].toReal()));
    }
    Cartography::EuclidSystem::multi_geometry egr_result
      = Cartography::Algorithms::weightedPolygonDecomposition(poly_egr, weights, _tol, starts);
    return Cartography::Geometry::Interface::fromEuclid(egr_result, _geometry->coordinateSystem());
  }
  else
  {
    return nullptr;
  }
}

Cartography::Geometry::Geometry*
  AlgorithmsSingleton::gridPolygonDecomposition(Cartography::Geometry::Geometry* _geometry,
                                                qreal _grid_size)
{
  Cartography::EuclidSystem::geometry egr
    = Cartography::Geometry::Interface::toEuclid<Cartography::EuclidSystem>(_geometry);
  if(egr.is<Cartography::EuclidSystem::polygon>())
  {
    Cartography::EuclidSystem::polygon poly_egr = egr.cast<Cartography::EuclidSystem::polygon>();
    Cartography::EuclidSystem::multi_polygon egr_result
      = euclid::extra::grid_decomposition(poly_egr, _grid_size);
    return Cartography::Geometry::Interface::fromEuclid(egr_result, _geometry->coordinateSystem());
  }
  else
  {
    return nullptr;
  }
}

Cartography::Geometry::Geometry*
  AlgorithmsSingleton::polylineFitting(Cartography::Geometry::Geometry* _geometry, qreal _tol)
{
  if(not _geometry)
    return nullptr;
  Cartography::EuclidSystem::geometry egr
    = Cartography::Geometry::Interface::toEuclid<Cartography::EuclidSystem>(_geometry);
  if(egr.is<Cartography::EuclidSystem::multi_geometry>())
  {
    Cartography::EuclidSystem::multi_geometry mp_egr
      = egr.cast<Cartography::EuclidSystem::multi_geometry>();
    Cartography::EuclidSystem::line_string egr_result
      = euclid::extra::line_string_fitting(mp_egr, _tol);
    return Cartography::Geometry::Interface::fromEuclid(egr_result, _geometry->coordinateSystem());
  }
  else
  {
    clog_warning("Null geometry or not a collection in polylineFitting!");
    return nullptr;
  }
}

Cartography::Geometry::Geometry*
  AlgorithmsSingleton::lawnMowerPattern(Cartography::Geometry::Geometry* _geometry,
                                        qreal _span_width)
{
  euclid::geos::geometry geom = Cartography::Geometry::Interface::toEuclid<euclid::geos>(_geometry);

  euclid::geos::polygon polygon;

  if(not geom.is_null())
  {
    if(geom.is<euclid::geos::polygon>())
    {
      polygon = geom.cast<euclid::geos::polygon>();
    }
    else if(geom.is<euclid::geos::linear_ring>())
    {
      polygon = euclid::geos::polygon(geom.cast<euclid::geos::linear_ring>());
    }
  }

  if(polygon.is_null())
  {
    clog_warning("Null geometry or not a polygon");
  }
  else
  {
    euclid::geos::line_string result
      = Cartography::Algorithms::lawnMowerPattern(polygon, {_span_width});
    return Cartography::Geometry::Interface::fromEuclid(result, _geometry->coordinateSystem());
  }
  return nullptr;
}

Cartography::Geometry::Geometry*
  AlgorithmsSingleton::fixIt(Cartography::Geometry::Geometry* _geometry)
{
  euclid::geos::geometry geom = Cartography::Geometry::Interface::toEuclid<euclid::geos>(_geometry);

  if(not geom.is_null())
  {
    Cartography::GeometryObject go(_geometry->coordinateSystem(), geom);
    return Cartography::Geometry::Interface::fromEuclid(go.fixIt().geometry(),
                                                        _geometry->coordinateSystem());
  }
  return nullptr;
}

QVariant AlgorithmsSingleton::randomPoints(int _points, qreal _minX, qreal _maxX, qreal _minY,
                                           qreal _maxY)
{
  std::random_device r;
  std::default_random_engine e1(r());
  std::uniform_real_distribution<qreal> uniform_dist_x(_minX, _maxX);
  std::uniform_real_distribution<qreal> uniform_dist_y(_minY, _maxY);
  QVariantList points;
  while(points.size() < _points)
  {
    QVariantList pt;
    pt << uniform_dist_x(e1) << uniform_dist_y(e1);
    points.append(QVariant(pt));
  }
  return points;
}

QVariant AlgorithmsSingleton::gridPoints(int _points, qreal _minX, qreal _maxX, qreal _minY,
                                         qreal _maxY)
{
  QVariantList points;

  const int per_line = std::round(std::sqrt(_points));
  const int rows = std::round(_points / qreal(per_line));
  const int last_row = _points - rows * per_line + per_line;

  const qreal v_spacing = (_maxY - _minY) / (rows - 1);

  for(int y = 0; y < rows; ++y)
  {
    const int count = (y == rows - 1) ? last_row : per_line;
    qreal h_spacing = count == 1 ? 0 : (_maxX - _minX) / (count - 1);
    for(int x = 0; x < count; ++x)
    {
      qreal pt_x = (_minX + x * h_spacing);
      qreal pt_y = (y * v_spacing + _minY);
      clog_assert(not std::isnan(pt_x));
      clog_assert(not std::isnan(pt_y));
      QVariantList pt;
      pt << pt_x << pt_y;
      points.append(QVariant(pt));
    }
  }

  return points;
}

QVariant AlgorithmsSingleton::equalWeights(int _count)
{
  QVariantList weights;

  for(int i = 0; i < _count; ++i)
  {
    weights.append(1.0 / qreal(_count));
  }
  return weights;
}

QVariant AlgorithmsSingleton::lastStatistics() const { return d->stats; }

QString AlgorithmsSingleton::lastError() const { return d->lastError; }

void AlgorithmsSingleton::setLastError(const QString& _lastError)
{
  d->lastError = _lastError;
  emit(lastErrorChanged());
}

#include "moc_AlgorithmsSingleton.cpp"
