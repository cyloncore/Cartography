#pragma once

#include <QObject>
#include <QVariantHash>

#include <Cartography/Forward.h>

namespace Cartography::Quick::Algorithms
{
  class AlgorithmsSingleton : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QVariant lastStatistics READ lastStatistics NOTIFY lastStatisticsChanged)
    Q_PROPERTY(QVariant lastError READ lastError NOTIFY lastErrorChanged)
  public:
    AlgorithmsSingleton(QObject* _parent = nullptr);
    Q_INVOKABLE Cartography::Geometry::Geometry*
      delaunayTriangulation(Cartography::Geometry::Geometry* _geometry, qreal _tol);
    Q_INVOKABLE Cartography::Geometry::Geometry*
      intersection(Cartography::Geometry::Geometry* _geometry1,
                   Cartography::Geometry::Geometry* _geometry2);
    Q_INVOKABLE Cartography::Geometry::Geometry*
      convexPolygonDecomposition(Cartography::Geometry::Geometry* _geometry, qreal _tol);
    Q_INVOKABLE Cartography::Geometry::Geometry*
      voronoiDiagram(Cartography::Geometry::Geometry* _geometry,
                     Cartography::Geometry::Geometry* _envelope, qreal _tol);
    Q_INVOKABLE Cartography::Geometry::Geometry* weightedGraphPolygonDecomposition(
      Cartography::Geometry::Geometry* _geometry, const QVariantList& _weights, qreal _tol,
      const QVariantList& _start = QVariantList(), const QVariantMap& _options = QVariantMap(),
      bool _advancedStatistics = false);
    Q_INVOKABLE Cartography::Geometry::Geometry*
      weightedPolygonDecomposition(Cartography::Geometry::Geometry* _geometry,
                                   const QVariantList& _weights, qreal _tol,
                                   const QVariantList& _start = QVariantList());
    Q_INVOKABLE Cartography::Geometry::Geometry*
      gridPolygonDecomposition(Cartography::Geometry::Geometry* _geometry, qreal _grid_size);
    Q_INVOKABLE Cartography::Geometry::Geometry*
      polylineFitting(Cartography::Geometry::Geometry* _geometry, qreal _tol);
    Q_INVOKABLE Cartography::Geometry::Geometry*
      lawnMowerPattern(Cartography::Geometry::Geometry* _geometry, qreal _span_width);
    Q_INVOKABLE Cartography::Geometry::Geometry* fixIt(Cartography::Geometry::Geometry* _geometry);
    Q_INVOKABLE QVariant randomPoints(int _points, qreal _minX, qreal _maxX, qreal _minY,
                                      qreal _maxY);
    Q_INVOKABLE QVariant gridPoints(int _points, qreal _minX, qreal _maxX, qreal _minY,
                                    qreal _maxY);
    Q_INVOKABLE QVariant equalWeights(int _count);
    QString lastError() const;
    QVariant lastStatistics() const;
  signals:
    void lastErrorChanged();
    void lastStatisticsChanged();
  private:
    void setLastError(const QString& _lastError);
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Quick::Algorithms
