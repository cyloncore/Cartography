#include "CartographyAlgorithmsPlugin.h"

#include <QtQml>

#include "AlgorithmsSingleton.h"

#if(QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
template<typename _T_>
void qmlRegisterAnonymousType(const char*, int)
{
  qmlRegisterType<_T_>();
}
#endif

using namespace Cartography::Quick::Algorithms;

void CartographyAlgorithmsPlugin::registerTypes(const char* /*uri*/)
{
  const char* uri = "Cartography.Algorithms";

  qmlRegisterSingletonType<AlgorithmsSingleton>(uri, 2, 0, "Algorithms",
                                                [](QQmlEngine*, QJSEngine*) -> QObject*
                                                { return new AlgorithmsSingleton; });
}
