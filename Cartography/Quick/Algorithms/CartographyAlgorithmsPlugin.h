#pragma once

#include <QQmlExtensionPlugin>

class CartographyAlgorithmsPlugin : public QQmlExtensionPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "Cartography/Algorithms/2.0")
public:
  void registerTypes(const char* uri) override;
};
