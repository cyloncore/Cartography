#include "CartographyCartographerPlugin.h"

#include <gdal/gdal.h>
#include <gdal/ogrsf_frmts.h>

#include <QtQml>

#include "EditableDatasource.h"
#include "MapViewInterface.h"

using namespace Cartography::Quick::Cartographer;

void CartographyCartographerPlugin::registerTypes(const char* /*uri*/)
{
  GDALAllRegister();
  OGRRegisterAll();

  const char* uri = "Cartography.Cartographer";

  qmlRegisterType<EditableDatasource>(uri, 2, 0, "EditableDatasource");
  qmlRegisterType<MapViewInterface>(uri, 2, 0, "MapViewInterface");
}
