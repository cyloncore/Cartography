#ifndef _CARTOGRAPHERML_EDITABLE_DATASOURCE_H_
#define _CARTOGRAPHERML_EDITABLE_DATASOURCE_H_

#include <Cartography/Forward.h>
#include <Cartography/Quick/Mapnik/AbstractDatasource.h>

namespace Cartography::Quick::Cartographer
{
  class EditableDatasource : public Cartography::Quick::Mapnik::AbstractDatasource
  {
    Q_OBJECT
    Q_PROPERTY(Cartography::Geometry::AbstractFeaturesSource* featuresSource READ featuresSource
                 WRITE setFeaturesSource NOTIFY featuresSourceChanged)
  public:
    explicit EditableDatasource(QObject* parent = 0);
    virtual ~EditableDatasource();
  public:
    Cartography::Geometry::AbstractFeaturesSource* featuresSource() const;
    void setFeaturesSource(Cartography::Geometry::AbstractFeaturesSource* _afs);
  public:
    virtual mapnik::datasource_ptr mapnikDatasource() const;
  signals:
    void featuresSourceChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Quick::Cartographer

#endif
