#include "MapViewInterface.h"

#include <Cartography/Quick/Mapnik/MapView.h>
#include <Cartography/Quick/Mapnik/ViewTransform.h>

using namespace Cartography::Quick::Cartographer;

struct MapViewInterface::Private
{
  Cartography::Quick::Mapnik::MapView* mapView = nullptr;
};

MapViewInterface::MapViewInterface(QObject* _parent) : QObject(_parent), d(new Private) {}

MapViewInterface::~MapViewInterface() {}

Cartography::Quick::Mapnik::MapView* MapViewInterface::mapView() const { return d->mapView; }

void MapViewInterface::setMapView(Cartography::Quick::Mapnik::MapView* _mapView)
{
  d->mapView = _mapView;
  emit(mapViewChanged());
}

QPointF MapViewInterface::fromView(const QPointF& _point) const
{
  return d->mapView->viewTransform()->toMap(_point);
}

QPointF MapViewInterface::toView(const QPointF& _point) const
{
  return d->mapView->viewTransform()->fromMap(_point);
}

#include "moc_MapViewInterface.cpp"
