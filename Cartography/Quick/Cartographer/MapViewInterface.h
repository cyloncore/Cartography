#pragma once

#include <Cyqlops/Toolbox/AbstractViewInterface.h>
#include <QObject>

namespace Cartography::Quick::Mapnik
{
  class MapView;
}

namespace Cartography::Quick::Cartographer
{
  class MapViewInterface : public QObject, public Cyqlops::Toolbox::AbstractViewInterface
  {
    Q_OBJECT
    Q_PROPERTY(Cartography::Quick::Mapnik::MapView* mapView READ mapView WRITE setMapView NOTIFY
                 mapViewChanged)
    Q_INTERFACES(Cyqlops::Toolbox::AbstractViewInterface)
  public:
    MapViewInterface(QObject* _parent = 0);
    ~MapViewInterface();
    Cartography::Quick::Mapnik::MapView* mapView() const;
    void setMapView(Cartography::Quick::Mapnik::MapView* _mapView);
  public: // Cyqlops::Toolbox::AbstractViewInterface implementation
    Q_INVOKABLE QPointF toView(const QPointF& _point) const override;
    Q_INVOKABLE QPointF fromView(const QPointF& _point) const override;
  signals:
    void mapViewChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Quick::Cartographer
