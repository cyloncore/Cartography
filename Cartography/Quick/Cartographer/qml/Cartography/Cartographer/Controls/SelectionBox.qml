import QtQuick
import Cartography.Geometry

Rectangle
{
  id: _root_
  property /*Feature*/ QtObject feature: null
  property /*Geometry*/ QtObject geometry: _root_.feature ? _root_.feature.geometry : null
  property rect area: _root_.geometry ? mapView.viewTransform.fromMap(_root_.geometry.envelope) : Qt.rect(0,0,0,0)
  x: area.x-5
  y: area.y-5
  width: area.width+10
  height: area.height+10
  visible: _root_.geometry
  color: "#551DB1E1"
  border.color: "white"
  border.width: 1
}

