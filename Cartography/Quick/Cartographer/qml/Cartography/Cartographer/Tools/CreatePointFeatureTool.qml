import QtQuick

import Cartography.Geometry
import Cartography.Cartographer
import Cartography.Cartographer.Components
import Cyqlops.Toolbox

Tool
{
  id: root
  
  property QtObject feature
  property QtObject featuresSource

  overlayComponent: Item
  {
    SelectionBox
    {
      feature: root.feature
    }
  }
  optionsComponent: FeatureAttributesTable
  {
    feature: root.feature
    onFeatureAttributesChanged: featuresSource.record(root.feature)
  }  
  onPressed: mouse =>
  {
    var feature = featuresSource.createFeature()
    feature.geometry = Qt.createQmlObject('import Cartography.Geometry; Point {}', featuresSource, "CreatePointTool.qml")
    feature.geometry.x = mouse.x
    feature.geometry.y = mouse.y
    console.log(pt, feature)
    if(featuresSource.record(feature))
    {
      root.feature = feature
    } else {
      root.feature = null
    }
  }
}
