import QtQuick

import Cartography.Geometry
import Cartography.Cartographer
import Cartography.Cartographer.Components
import Cyqlops.Toolbox

ProxyTool
{
  id: root
  
  property QtObject feature
  property QtObject featuresSource
  property MapViewInterface viewInterface

  function __record_feature()
  {
    if(root.feature && !featuresSource.record(root.feature))
    {
      root.feature = null
    }
  }
  onFeatureChanged:
  {
    if(root.feature == null)
    {
      tool = defaultTool
    }
  }
  
  optionsComponent: FeatureAttributesTable
  {
    feature: root.feature
    onFeatureAttributesChanged: featuresSource.record(root.feature)
  }
  overlayComponent: root.tool.overlayComponent
  
  Tool
  {
    id: defaultTool
    overlayComponent: Item {
      SelectionBox
      {
        feature: root.feature
      }
    }

    onPressed: mouse =>
    {
      root.feature = root.featuresSource.createFeature()
      feature.geometry = Qt.createQmlObject('import Cartography.Geometry; Polygon {}', feature, "CreatePolygonFeatureTool.qml")
      feature.geometry.exteriorRing = Qt.createQmlObject('import Cartography.Geometry; LinearRing {}', feature, "CreatePolygonFeatureTool.qml")
      root.tool = lineStringTool
      lineStringTool.geometry = root.feature.geometry.exteriorRing
      lineStringTool.geometry.append(Qt.point(mouse.x, mouse.y))
      if(root.feature && !root.featuresSource.record(root.feature))
      {
        // TODO report error
        root.geometry = null
        root.feature    = null
      } else {
        mouse.accepted = false // so that the onPressed of CreateLineStringGeometryTool is also called
      }
    }
  }
  CreateLineStringGeometryTool
  {
    id: lineStringTool
    viewInterface: root.viewInterface
    onGeometryChanged:
    {
      if(lineStringTool.geometry == null)
      {
        root.tool = defaultTool
      }
      if(root.feature && !root.featuresSource.record(root.feature))
      {
        // TODO report error
        root.geometry = null
        root.feature    = null
      }
    }
  }
  tool: defaultTool
  
}

