import QtQuick

import Cartography.Geometry
import Cartography.Mapnik
import Cartography.Cartographer
import Cartography.Cartographer.Components
import Cyqlops.Toolbox

Tool
{
  id: _root_
  
  property QtObject feature
  property QtObject featuresSource
  
  overlayComponent: Item
  {
    SelectionBox
    {
      feature: _root_.feature
    }
  }
  optionsComponent: FeatureAttributesTable
  {
    feature: tool.feature
    onFeatureAttributesChanged: featuresSource.record(tool.feature)
  }  
  onPressed: mouse =>
  {
    var features = featuresSource.featuresAt(mouse.x, mouse.y, 2)
    if(features.featuresCount > 0)
    {
      var f = features.takeFeature(0)
      if(_root_.feature != null && f.id == _root_.feature.id)
      {
        _root_.feature = null
      } else {
        _root_.feature = f
      }
    } else {
      _root_.feature = null
    }
  }
}
