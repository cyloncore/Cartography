#include "CartographyGeometryPlugin.h"

#include <QtQml>

#include <Cartography/Geometry/AbstractFeaturesSource.h>
#include <Cartography/Geometry/Collection.h>
#include <Cartography/Geometry/Feature.h>
#include <Cartography/Geometry/FeatureAttributesModel.h>
#include <Cartography/Geometry/FeaturesSet.h>
#include <Cartography/Geometry/LinearRing.h>
#include <Cartography/Geometry/Point.h>
#include <Cartography/Geometry/Polygon.h>
#include <Cartography/GeometryObject.h>
#include <Cartography/config_p.h>

#include "GeometryFactorySingleton.h"

#ifdef CARTOGRAPHY_HAVE_GDAL
#include <Cartography/Geometry/Sources/GDALFeaturesSource.h>
#endif

void CartographyGeometryPlugin::registerTypes(const char* /*uri*/)
{
  using namespace Cartography::Geometry;
  using namespace Cartography::Quick::Geometry;

  const char* uri = "Cartography.Geometry";
  qRegisterMetaType<Cartography::GeometryObject>("Cartography::GeometryObject");

  qmlRegisterUncreatableType<AbstractFeaturesSource>(uri, 2, 0, "AbstractFeaturesSource",
                                                     "It is a virtual class");
  qmlRegisterAnonymousType<FeaturesSet>(uri, 2);
  qmlRegisterAnonymousType<Feature>(uri, 2);
  qmlRegisterUncreatableType<Cartography::Geometry::Geometry>(uri, 1, 0, "Geometry",
                                                              "Abstract class");
  qmlRegisterType<Collection>(uri, 2, 0, "Collection");
  qmlRegisterType<FeatureAttributesModel>(uri, 2, 0, "FeatureAttributesModel");
  qmlRegisterType<LinearRing>(uri, 2, 0, "LinearRing");
  qmlRegisterType<LineString>(uri, 2, 0, "LineString");
  qmlRegisterType<Cartography::Geometry::Point>(uri, 2, 0, "Point");
  qmlRegisterType<Polygon>(uri, 2, 0, "Polygon");
  qmlRegisterSingletonType<GeometryFactorySingleton>(uri, 2, 0, "GeometryFactory",
                                                     [](QQmlEngine*, QJSEngine*) -> QObject*
                                                     { return new GeometryFactorySingleton; });

#ifdef CARTOGRAPHY_HAVE_GDAL
  qmlRegisterType<Sources::GDALFeaturesSource>(uri, 2, 0, "GDALFeaturesSource");
#endif
}
