#pragma once

#include <QQmlExtensionPlugin>

class CartographyGeometryPlugin : public QQmlExtensionPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "Cartography/Geometry/2.0")
public:
  void registerTypes(const char* uri) override;
};
