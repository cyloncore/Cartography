#include "GeometryFactorySingleton.h"

#include <QUrl>
#include <QVariant>

#include <clog_qt>

#include <ogr_geometry.h>

#include <Cartography/EuclidSystem.h>

#include <Cartography/Geometry/Collection.h>
#include <Cartography/Geometry/Interface/Euclid.h>
#include <Cartography/Geometry/Interface/Gdal.h>
#include <Cartography/Geometry/LinearRing.h>
#include <Cartography/Geometry/Point.h>
#include <Cartography/Geometry/Polygon.h>
#include <Cartography/GeometryObject.h>

using namespace Cartography::Quick::Geometry;

GeometryFactorySingleton::GeometryFactorySingleton(QObject* _parent) : QObject(_parent) {}

GeometryFactorySingleton::~GeometryFactorySingleton() {}

namespace
{
  void toPointsCollection(const Cartography::Geometry::Geometry* _geometry,
                          Cartography::Geometry::Collection* _destination)
  {
    switch(_geometry->type())
    {
    case Cartography::Geometry::Geometry::Type::Point:
    {
      _destination->append(_geometry->clone(_destination));
      break;
    }
    case Cartography::Geometry::Geometry::Type::LineString:
      Q_FALLTHROUGH();
    case Cartography::Geometry::Geometry::Type::LinearRing:
      _destination->append(Cartography::Geometry::Geometry::clone<Cartography::Geometry::Geometry,
                                                                  Cartography::Geometry::Point>(
        static_cast<const Cartography::Geometry::LineString*>(_geometry)->points(), _destination));
      break;
    case Cartography::Geometry::Geometry::Type::Polygon:
    {
      const Cartography::Geometry::Polygon* polygon
        = static_cast<const Cartography::Geometry::Polygon*>(_geometry);
      toPointsCollection(polygon->exteriorRing(), _destination);
      for(const Cartography::Geometry::LinearRing* lr : polygon->holes())
      {
        toPointsCollection(lr, _destination);
      }
      break;
    }
    case Cartography::Geometry::Geometry::Type::Collection:
    {
      const Cartography::Geometry::Collection* collection
        = static_cast<const Cartography::Geometry::Collection*>(_geometry);
      for(Cartography::Geometry::Geometry* g : collection->elements())
      {
        toPointsCollection(g, _destination);
      }
      break;
    }
    case Cartography::Geometry::Geometry::Type::Undefined:
    {
      clog_warning("Undefined geometry");
      break;
    }
    }
  }
} // namespace

Cartography::Geometry::Collection* GeometryFactorySingleton::points(const QVariant& _points)
{
  if(_points.canConvert<Cartography::Geometry::Geometry*>())
  {
    Cartography::Geometry::Collection* c = new Cartography::Geometry::Collection;
    toPointsCollection(_points.value<Cartography::Geometry::Geometry*>(), c);
    return c;
  }
  else if(_points.canConvert<Cartography::Geometry::Collection*>())
  {
    return _points.value<Cartography::Geometry::Collection*>();
  }
  else if(_points.canConvert<QVariantList>())
  {
    Cartography::Geometry::Collection* coll = new Cartography::Geometry::Collection();
    QVariantList points_listt = _points.toList();
    for(const QVariant& point_var : points_listt)
    {
      Cartography::Geometry::Point* pt = point(point_var);
      if(pt)
      {
        coll->append(pt);
      }
    }
    return coll;
  }
  clog_warning("Cannot convert '{}' to point collection", _points);
  return nullptr;
}

Cartography::Geometry::Point* GeometryFactorySingleton::point(const QVariant& _point)
{
  if(_point.canConvert<Cartography::Geometry::Point*>())
  {
    return _point.value<Cartography::Geometry::Point*>();
  }
  else if(_point.canConvert<QVariantList>())
  {
    QVariantList coordinates = _point.toList();
    switch(coordinates.size())
    {
    case 2:
      return new Cartography::Geometry::Point(coordinates[0].toDouble(), coordinates[1].toDouble());
    case 3:
      return new Cartography::Geometry::Point(coordinates[0].toDouble(), coordinates[1].toDouble(),
                                              coordinates[2].toDouble());
    default:
      return nullptr;
    }
  }
  clog_warning("Cannot convert '{}' to LinearRing", _point);
  return nullptr;
}

Cartography::Geometry::LinearRing* GeometryFactorySingleton::linearRing(const QVariant& _ring)
{
  if(_ring.canConvert<Cartography::Geometry::LinearRing*>())
  {
    return _ring.value<Cartography::Geometry::LinearRing*>();
  }
  else if(_ring.canConvert<QVariantList>())
  {
    QList<Cartography::Geometry::Point*> points;
    for(const QVariant& var : _ring.toList())
    {
      points.append(point(var));
    }
    return new Cartography::Geometry::LinearRing(points);
  }
  else
  {
    clog_warning("Cannot convert '{}' to LinearRing", _ring);
    return nullptr;
  }
}

Cartography::Geometry::Polygon* GeometryFactorySingleton::polygon(const QVariant& _exterior_ring,
                                                                  const QVariantList& _holes)
{
  Cartography::Geometry::Polygon* polygon
    = new Cartography::Geometry::Polygon(linearRing(_exterior_ring));

  for(const QVariant& v : _holes)
  {
    polygon->appendHole(linearRing(v));
  }

  return polygon;
}

Cartography::Geometry::Geometry*
  GeometryFactorySingleton::fromGeometryObject(const Cartography::GeometryObject& _go)
{
  if(_go.isValid())
  {
    return Cartography::Geometry::Interface::fromEuclid(_go.geometry(), _go.coordinateSystem());
  }
  else
  {
    return nullptr;
  }
}

Cartography::Geometry::Geometry* GeometryFactorySingleton::fromWKT(const QString& _wkt)
{
  QByteArray arr = _wkt.toLatin1();
#if(GDAL_VERSION_MAJOR >= 2 && GDAL_VERSION_MINOR >= 3) || GDAL_VERSION_MAJOR > 2
  const char* data_str = arr.data();
#else
  char* data_str = const_cast<char*>(arr.data());
#endif
  OGRGeometry* ogr = nullptr;
  OGRGeometryFactory::createFromWkt(&data_str, nullptr, &ogr);
  if(ogr)
  {
    return Cartography::Geometry::Interface::fromGdal(ogr);
  }
  else
  {
    clog_warning("Invalid WKT!");
    return nullptr;
  }
}

QString GeometryFactorySingleton::toWKT(Cartography::Geometry::Geometry* _geometry)
{
  Cartography::EuclidSystem::geometry geometry
    = Cartography::Geometry::Interface::toEuclid<Cartography::EuclidSystem>(_geometry);
  return QString::fromStdString(euclid::io::to_wkt(geometry));
}

Cartography::Geometry::Geometry* GeometryFactorySingleton::load(const QUrl& _url)
{
  return Cartography::Geometry::Geometry::load(_url.toLocalFile());
}
