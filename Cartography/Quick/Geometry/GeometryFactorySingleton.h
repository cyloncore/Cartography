#pragma once

#include <QObject>
#include <QVariant>

#include <Cartography/Forward.h>

namespace Cartography::Quick::Geometry
{
  class GeometryFactorySingleton : public QObject
  {
    Q_OBJECT
  public:
    GeometryFactorySingleton(QObject* _parent = nullptr);
    ~GeometryFactorySingleton();

    /**
     * Take a list of point or any geometry and convert it to a collection of points
     */
    Q_INVOKABLE Cartography::Geometry::Collection* points(const QVariant& _points);
    Q_INVOKABLE Cartography::Geometry::Point* point(const QVariant& _point);
    Q_INVOKABLE Cartography::Geometry::LinearRing* linearRing(const QVariant& _ring);
    Q_INVOKABLE Cartography::Geometry::Polygon* polygon(const QVariant& _exterior_ring,
                                                        const QVariantList& _holes);
    Q_INVOKABLE Cartography::Geometry::Geometry*
      fromGeometryObject(const Cartography::GeometryObject& _wkt);
    Q_INVOKABLE Cartography::Geometry::Geometry* fromWKT(const QString& _wkt);
    Q_INVOKABLE Cartography::Geometry::Geometry* load(const QUrl& _url);
    Q_INVOKABLE QString toWKT(Cartography::Geometry::Geometry* _geometry);
  };
} // namespace Cartography::Quick::Geometry
