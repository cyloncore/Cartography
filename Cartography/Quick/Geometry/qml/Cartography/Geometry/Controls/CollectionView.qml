import QtQuick
import Cartography.Geometry

Item
{
  id: root
  property Geometry geometry
  property alias collection: root.geometry
  
  property variant strokeStyle: null
  property variant fillStyle: null
  property real lineWidth: 10
  property point offset: Qt.point(0, 0)
  property real scale: 1
  
  Repeater
  {
    model: geometry.elements
    GeometryView
    {
      strokeStyle: root.strokeStyle
      fillStyle: root.fillStyle
      lineWidth: root.lineWidth
      geometry: modelData
      offset: root.offset
      scale: root.scale
    }
  }
}
