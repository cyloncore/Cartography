import QtQuick
import Cartography.Geometry

Loader
{
  id: root
  property Geometry geometry
  sourceComponent: __component_for_geometry(geometry)

  property variant strokeStyle: null
  property variant fillStyle: null
  property real lineWidth: 1
  property point offset: Qt.point(0,0)
  property real scale: 1
  
  function __component_for_geometry(_geo)
  {
    if(_geo == null) return null;
    switch(_geo.type)
    {
      case Geometry.Collection:
        return Qt.createComponent("CollectionView.qml");
      case Geometry.LineString:
      case Geometry.LinearRing:
        return Qt.createComponent("LineStringView.qml");
      case Geometry.Polygon:
        return Qt.createComponent("PolygonView.qml");
      case Geometry.Point:
        return Qt.createComponent("PointView.qml");
    }
    console.log("In GeometryView.qml: unsupported geometry type: ", _geo.type)
    return null
  }
  onLoaded:
  {
    root.item.geometry    = Qt.binding(function () { return root.geometry })
    root.item.strokeStyle = Qt.binding(function () { return root.strokeStyle })
    root.item.fillStyle   = Qt.binding(function () { return root.fillStyle })
    root.item.lineWidth   = Qt.binding(function () { return root.lineWidth })
    root.item.offset      = Qt.binding(function () { return root.offset })
    root.item.scale       = Qt.binding(function () { return root.scale })
  }
}
