import QtQuick
import Cartography.Geometry

Item
{
  id: root
  property Geometry geometry
  property rect __geometry_rect: geometry ? geometry.envelope : Qt.rect(0, 0, 0, 0)

  property variant strokeStyle: null
  property variant fillStyle: null
  property real lineWidth: 10
  property point offset: Qt.point(0, 0)
  property real scale: 1

  Canvas
  {
    id: _canvas_
    x: __geometry_rect.x * root.scale - lineWidth + offset.x * root.scale
    y: __geometry_rect.y * root.scale - lineWidth + offset.y * root.scale
    width: __geometry_rect.width * root.scale + 2 * lineWidth
    height: __geometry_rect.height * root.scale + 2 * lineWidth
    
    onPaint:
    {
      var ctx = getContext("2d");
      ctx.reset()
      ctx.translate(-x + offset.x * root.scale, -y + offset.y * root.scale)
      root.paint(ctx)
    }
  }
  
  function __strokeFill(ctx)
  {
    if(root.strokeStyle)
    {
      ctx.strokeStyle=root.strokeStyle
      ctx.lineWidth=root.lineWidth
      ctx.stroke()
    }
    if(root.fillStyle)
    {
      ctx.fillStyle=root.fillStyle
      ctx.stroke()
    }
  }
  function requestPaint()
  {
    _canvas_.requestPaint()
  }
  signal paint(variant ctx)
}

