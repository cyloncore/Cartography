import QtQuick 2.0

GeometryViewBase
{
  id: root
  property alias lineString: root.geometry
  
  onPaint: ctx =>
  {
    if(root.lineString && root.lineString.points.length > 0)
    {
      var points = root.lineString.points
      ctx.moveTo(points[0].x * root.scale, points[0].y * root.scale)
      for(var i = 1; i < points.length; ++i)
      {
        ctx.lineTo(points[i].x * root.scale, points[i].y * root.scale)
      }
      root.__strokeFill(ctx)
    } else {
      console.log("empty line string")
    }
  }
}
