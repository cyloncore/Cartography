import QtQuick 2.0

GeometryViewBase
{
  id: root
  property alias point: root.geometry
  
  onPaint: ctx =>
  {
    if(root.point && ctx.strokeStyle)
    {
      ctx.arc(root.point.x * root.scale, root.point.y * root.scale, root.lineWidth, 0, 2*Math.PI);
      ctx.fillStyle = root.strokeStyle
      ctx.fill()
    }
  }
}
