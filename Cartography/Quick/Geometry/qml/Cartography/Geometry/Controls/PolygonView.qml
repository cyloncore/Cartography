import QtQuick 2.0

GeometryViewBase
{
  id: root
  property alias polygon: root.geometry
  
  onPaint: ctx =>
  {
    if(root.polygon)
    {
      var points = root.polygon.exteriorRing.points
      if(points.length > 0)
      {
        ctx.moveTo(points[0].x * root.scale, points[0].y * root.scale)
        for(var i = 1; i < points.length; ++i)
        {
          ctx.lineTo(points[i].x * root.scale, points[i].y * root.scale)
        }
        ctx.lineTo(points[0].x * root.scale, points[0].y * root.scale)
      }
      
      var interiors = root.polygon.holes
      for(var j = 0; j < interiors.length; ++j)
      {
        var points = interiors[j].points
        if(points.length > 0)
        {
          ctx.moveTo(points[0].x * root.scale, points[0].y * root.scale)
          for(var i = 1; i < points.length; ++i)
          {
            ctx.lineTo(points[i].x * root.scale, points[i].y * root.scale)
          }
          ctx.lineTo(points[0].x * root.scale, points[0].y * root.scale)
        }
      }
      
      root.__strokeFill(ctx)
    }
  }
  Connections
  {
    target: root.polygon
    function onHolesChanged()
    {
      root.requestPaint()
    }
  }
  Repeater
  {
    model: root.polygon.holes
    Item
    {
      Connections
      {
        target: modelData
        function onPointsChanged()
        {
          root.requestPaint()
        }
      }
    }
  }
  Connections
  {
    target: root.polygon.exteriorRing
    function onPointsChanged()
    {
      root.requestPaint()
    }
  }
}
