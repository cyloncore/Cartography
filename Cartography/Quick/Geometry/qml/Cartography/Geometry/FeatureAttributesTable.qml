import QtQuick.Controls
import Cartography.Geometry
import Cyqlops.Controls

TableView
{
  id: _root_
  delegate: EditableDelegate { }
  property alias feature: model.feature
  signal featureAttributesChanged()
  model: FeatureAttributesModel
  {
    id: model
    feature: tool.feature
    onDataChanged: _root_.featureAttributesChanged()
  }
  horizontalHeaderModel: ["Name", "Value"]
  horizontalHeaderVisible: true
}
