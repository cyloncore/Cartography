import QtQuick
import QtLocation

import Cartography.Geometry

MapItemGroup
{
  id: _root_
  property Geometry geometry
  property alias pointDelegate: _pt_mi_.pointDelegate

  property color color
  property alias border: _poly_mi_.border

  property var activeItem

  function __item_for_geometry(_geo)
  {
    if(_geo == null) return null;
    switch(_geo.type)
    {
      case Geometry.Collection:
        console.log("In GeometryView.qml: unimplemented geometry type: ", _geo.type)
        return null
      case Geometry.LineString:
      case Geometry.LinearRing:
        console.log("In GeometryView.qml: unimplemented geometry type: ", _geo.type)
        return null
      case Geometry.Polygon:
        return _poly_mi_
      case Geometry.Point:
        return _pt_mi_
    }
    console.log("In GeometryView.qml: unsupported geometry type: ", _geo.type)
    return null
  }
  onGeometryChanged:
  {
    if(_root_.activeItem)
    {
      _root_.activeItem.geometry = null
      _root_.activeItem.visible = false
    }
    _root_.activeItem = __item_for_geometry(_root_.geometry);
    if(_root_.activeItem)
    {
      _root_.activeItem.geometry = _root_.geometry
      _root_.activeItem.visible  = true
    }
  }
  PolygonMapItem
  {
    id: _poly_mi_
    color: _root_.color
    visible: false
  }
  PointMapItem
  {
    id: _pt_mi_
    visible: false
  }
}