import QtQuick
import QtLocation
import Cartography.Geometry

MapQuickItem
{
  id: _root_
  property Geometry geometry
  property Component pointDelegate: Rectangle { color: "red"; width: 2.0; height: 2.0; property point anchorPoint: Qt.point(1, 1) }
  coordinate.latitude: geometry.y
  coordinate.longitude: geometry.x
  anchorPoint: sourceItem.item ? sourceItem.item.anchorPoint : Qt.point(0, 0)
  sourceItem: Loader
  {
    sourceComponent: _root_.pointDelegate
  }
}
