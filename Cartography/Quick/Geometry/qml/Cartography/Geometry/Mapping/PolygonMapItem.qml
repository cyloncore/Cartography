import QtQuick
import QtPositioning
import QtLocation
import Cartography.Geometry

MapPolygon
{
  id: _root_
  property Geometry geometry

  onGeometryChanged:
  {
    var npath = []
    console.log(_root_.geometry)
    if(_root_.geometry)
    {
      var points = _root_.geometry.exteriorRing.points
      for(var i = 0; i < points.length; ++i)
      {
        npath.push(QtPositioning.coordinate(points[i].y, points[i].x))
      }
    }
    _root_.path = npath
  }
}
