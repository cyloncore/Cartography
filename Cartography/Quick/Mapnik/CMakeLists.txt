# Build QuickMapnik library

set(MAPNIKML_SRCS
  AbstractDatasource.cpp
  Datasource.cpp
  Keys.cpp
  Layer.cpp
  Map.cpp
  MapElement.cpp
  MapView.cpp
  Markers.cpp
  Rule.cpp
  Symbolizers.cpp
  Style.cpp
  ViewTransform.cpp
)

add_library(CartographyQuickMapnik SHARED ${MAPNIKML_SRCS})
target_link_libraries(CartographyQuickMapnik PUBLIC ${MAPNIK_LIBRARIES} Qt6::Quick)
install(TARGETS CartographyQuickMapnik EXPORT CartographyTargets ${INSTALL_TARGETS_DEFAULT_ARGS} )
set_target_properties(CartographyQuickMapnik PROPERTIES VERSION ${CARTOGRAPHY_VERSION} SOVERSION ${CARTOGRAPHY_MAJOR_VERSION})

add_library(Cartography::Quick::Mapnik INTERFACE IMPORTED)
set_target_properties(Cartography::Quick::Mapnik PROPERTIES INTERFACE_LINK_LIBRARIES CartographyQuickMapnik)

install( FILES
  AbstractDatasource.h
  Datasource.h
  DESTINATION ${INSTALL_INCLUDE_DIR}/Cartography/Quick/Mapnik )

# Build plugin

set(MAPNIKML_PLUGIN_SRCS
  Datasource.cpp
  CartographyMapnikPlugin.cpp
  GeoTileDatasource.cpp
  GridView.cpp
  MapnikEnums_p.cpp
  Utils.cpp
  UtilsAttachedProperty.cpp
  )

add_library(CartographyMapnikPlugin SHARED ${MAPNIKML_PLUGIN_SRCS})
target_link_libraries(CartographyMapnikPlugin CartographyQuickMapnik Qt6::Positioning ${GDAL_LIBRARY})

install(TARGETS CartographyMapnikPlugin
  RUNTIME DESTINATION ${INSTALL_QML_DIR}/Cartography/Mapnik
  LIBRARY DESTINATION ${INSTALL_QML_DIR}/Cartography/Mapnik
)

install(DIRECTORY qml/
        DESTINATION ${INSTALL_QML_DIR}/)

install (CODE "execute_process(COMMAND qmlplugindump Cartography.Mapnik 2.0 ${INSTALL_QML_DIR}  OUTPUT_FILE ${INSTALL_QML_DIR}/Cartography/Mapnik/plugin.qmltypes)")
