#include "CartographyMapnikPlugin.h"

#include <QtQml>

#include <mapnik/color.hpp>
#include <mapnik/datasource_cache.hpp>
#include <mapnik/symbolizer.hpp>

#include "Datasource.h"
#include "GeoTileDatasource.h"
#include "GridView.h"
#include "Keys.h"
#include "Layer.h"
#include "Map.h"
#include "MapView.h"
#include "MapnikEnums_p.h"
#include "Markers.h"
#include "Rule.h"
#include "Style.h"
#include "Symbolizers.h"
#include "Utils.h"
#include "UtilsAttachedProperty.h"
#include "ViewTransform.h"
#include <Cartography/config_p.h>

template<typename _T_>
void qmlRegisterSymbolizer(const char* _uri, int versionMajor, int versionMinor,
                           const char* _qmlName)
{
  qmlRegisterType<Cartography::Quick::Mapnik::Symbolizer<_T_>>(_uri, versionMajor, versionMinor,
                                                               _qmlName);
}

template<mapnik::keys _T_, typename _TValue_>
void qmlRegisterKey(const char* _uri, int versionMajor, int versionMinor, const char* _qmlName)
{
  qmlRegisterType<Cartography::Quick::Mapnik::Key<_T_, _TValue_>>(_uri, versionMajor, versionMinor,
                                                                  _qmlName);
}

using namespace Cartography::Quick::Mapnik;

void CartographyMapnikPlugin::registerTypes(const char* /*uri*/)
{
  if(not mapnik::datasource_cache::instance().register_datasources(MAPNIK_INPUT_PLUGINS_DIR))
  {
    qWarning() << "Failed to register Mapnik datasources " << MAPNIK_INPUT_PLUGINS_DIR;
  }

  const char* uri = "Cartography.Mapnik";
  qmlRegisterUncreatableType<MapnikEnums>(uri, 2, 0, "Mapnik", "Just for enums");
  qmlRegisterUncreatableType<AbstractDatasource>(uri, 2, 0, "AbstractDatasource",
                                                 "Is a pure virtual class.");
  qmlRegisterAnonymousType<ViewTransform>(uri, 1);
  qmlRegisterType<Datasource>(uri, 2, 0, "Datasource");
  qmlRegisterType<GeoTileDatasource>(uri, 2, 0, "GeoTileDatasource");
  //   qmlRegisterType<GridView  >(uri, 2, 0, "GridView");
  qmlRegisterType<Layer>(uri, 2, 0, "Layer");
  qmlRegisterType<Map>(uri, 2, 0, "Map");
  qmlRegisterType<MapView>(uri, 2, 0, "MapView");
  qmlRegisterType<Markers>(uri, 2, 0, "Markers");
  qmlRegisterType<Rule>(uri, 2, 0, "Rule");
  qmlRegisterType<Style>(uri, 2, 0, "Style");
  qmlRegisterType<Utils>(uri, 2, 0, "Utils");

  qmlRegisterSymbolizer<mapnik::dot_symbolizer>(uri, 2, 0, "DotSymbolizer");
  qmlRegisterSymbolizer<mapnik::line_symbolizer>(uri, 2, 0, "LineSymbolizer");
  qmlRegisterSymbolizer<mapnik::markers_symbolizer>(uri, 2, 0, "MarkersSymbolizer");
  qmlRegisterSymbolizer<mapnik::point_symbolizer>(uri, 2, 0, "PointSymbolizer");
  qmlRegisterSymbolizer<mapnik::polygon_symbolizer>(uri, 2, 0, "PolygonSymbolizer");
  qmlRegisterSymbolizer<mapnik::raster_symbolizer>(uri, 2, 0, "RasterSymbolizer");
  qmlRegisterSymbolizer<mapnik::text_symbolizer>(uri, 2, 0, "TextSymbolizer");

  qmlRegisterKey<mapnik::keys::width, double>(uri, 2, 0, "WidthKey");
  qmlRegisterKey<mapnik::keys::height, double>(uri, 2, 0, "HeightKey");
  qmlRegisterKey<mapnik::keys::scaling, mapnik::scaling_method_e>(uri, 2, 0, "ScalingKey");
  qmlRegisterKey<mapnik::keys::file, std::string>(uri, 2, 0, "FileKey");
  qmlRegisterKey<mapnik::keys::fill, mapnik::color>(uri, 2, 0, "FillKey");
  qmlRegisterKey<mapnik::keys::fill_opacity, double>(uri, 2, 0, "FillOpacityKey");
  qmlRegisterKey<mapnik::keys::stroke, mapnik::color>(uri, 2, 0, "StrokeKey");
  qmlRegisterKey<mapnik::keys::stroke_linecap, mapnik::line_cap_enum>(uri, 2, 0,
                                                                      "StrokeLinecapKey");
  qmlRegisterKey<mapnik::keys::stroke_linejoin, mapnik::line_join_enum>(uri, 2, 0,
                                                                        "StrokeLinejoinKey");
}
