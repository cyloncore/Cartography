#pragma once

#include <QQmlExtensionPlugin>

class CartographyMapnikPlugin : public QQmlExtensionPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "Cartography/Mapnik/1.0")
public:
  void registerTypes(const char* uri) override;
};
