#include "GeoTileDatasource.h"

#include <mapnik/feature_factory.hpp>
#include <mapnik/image_reader.hpp>
#include <mapnik/proj_transform.hpp>
#include <mapnik/projection.hpp>
#include <mapnik/raster.hpp>

#include <QCoreApplication>
#include <QCryptographicHash>
#include <QDir>
#include <QFileInfo>
#include <QFuture>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QPromise>
#include <QStandardPaths>
#include <QThread>
#include <QTimer>
#include <QWaitCondition>

using namespace Cartography::Quick::Mapnik;

namespace Cartography::Quick::Mapnik
{
  int long2tilex(double lon, int z) { return (int)(floor((lon + 180.0) / 360.0 * (1 << z))); }

  int lat2tiley(double lat, int z)
  {
    double latrad = lat * M_PI / 180.0;
    return (int)(floor((1.0 - asinh(tan(latrad)) / M_PI) / 2.0 * (1 << z)));
  }

  double tilex2long(int x, int z) { return x / (double)(1 << z) * 360.0 - 180; }

  double tiley2lat(int y, int z)
  {
    double n = M_PI - 2.0 * M_PI * y / (double)(1 << z);
    return 180.0 / M_PI * atan(0.5 * (exp(n) - exp(-n)));
  }

  int zoomLevel(double _denom)
  {
    // from https://github.com/openstreetmap/mapnik-stylesheets/blob/master/zoom-to-scale.txt

    if(_denom > 279541132.014)
      return 0;
    if(_denom > 139770566.007)
      return 1;
    if(_denom > 69885283.0036)
      return 2;
    if(_denom > 34942641.5018)
      return 3;
    if(_denom > 17471320.7509)
      return 4;
    if(_denom > 8735660.37545)
      return 5;
    if(_denom > 4367830.18772)
      return 6;
    if(_denom > 2183915.09386)
      return 7;
    if(_denom > 1091957.54693)
      return 8;
    if(_denom > 545978.773466)
      return 9;
    if(_denom > 272989.386733)
      return 10;
    if(_denom > 136494.693366)
      return 11;
    if(_denom > 68247.3466832)
      return 12;
    if(_denom > 34123.6733416)
      return 13;
    if(_denom > 17061.8366708)
      return 14;
    if(_denom > 8530.9183354)
      return 15;
    if(_denom > 4265.4591677)
      return 16;
    if(_denom > 2132.72958385)
      return 17;
    if(_denom > 1066.36479192)
      return 18;
    // if(_denom > 533.182395962) return 19;
    return 19;
  }

  class geo_tile_downloader
  {
  public:
    geo_tile_downloader(const QStringList& _sources, const QString& _userAgent)
        : m_sources(_sources), m_userAgent(_userAgent)
    {
      setSources(_sources);
      // Setup network access manager
      m_networkAccessManager = new QNetworkAccessManager();
      m_networkAccessManager->moveToThread(&m_thread);
      m_thread.start();
      QObject::connect(m_networkAccessManager, &QNetworkAccessManager::finished,
                       [this](QNetworkReply* _reply) { handleReply(_reply); });
    }
    ~geo_tile_downloader()
    {
      m_thread.exit();
      m_thread.wait();
      delete m_networkAccessManager;
    }
    void setNotifyUpdate(const std::function<void()>& _update) { m_notifyUpdate = _update; }
    void setSources(const QStringList& _sources)
    {
      m_sources = _sources;
      QCryptographicHash ch(QCryptographicHash::Md5);
      for(const QString& s : _sources)
      {
        ch.addData(s.toUtf8());
      }
      m_cachedir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/"
                   + ch.result().toHex() + "/";
    }
    QStringList sources() const { return m_sources; }
    void setUserAgent(const QString& _userAgent) { m_userAgent = _userAgent; }
    QString userAgent() const { return m_userAgent; }
    QString requestTile(int _x, int _y, int _z, bool _download)
    {
      QString cachefile = QString::fromStdString(
        std::format("{}/{}/{}/{}.png", m_cachedir.toStdString(), _z, _x, _y));
      QFileInfo cachefileinfo(cachefile);
      if(not cachefileinfo.exists() and _download)
      {
        QDir().mkpath(cachefileinfo.path());
        QString url = m_sources[0];
        url.replace("{z}", QString::number(_z))
          .replace("{x}", QString::number(_x))
          .replace("{y}", QString::number(_y));
        QMutexLocker l(&m_mutex);
        if(not m_caches.contains(url))
        {
          m_caches[url] = cachefile;
          l.unlock();
          // Do the request
          QNetworkRequest request(url);
          request.setRawHeader("User-Agent", m_userAgent.toUtf8());

          // Use a timer to send the request from the thread
          QTimer* t = new QTimer;
          t->moveToThread(&m_thread);
          QObject::connect(t, &QTimer::timeout,
                           [request, t, this]()
                           {
                             m_networkAccessManager->get(request);
                             t->deleteLater();
                           });
          QMetaObject::invokeMethod(t, "start", Qt::QueuedConnection);
        }
      }

      return cachefile;
    }
  private:
    void handleReply(QNetworkReply* reply)
    {
      QMutexLocker l(&m_mutex);
      QFile cache_f(m_caches[reply->request().url()]);
      cache_f.open(QIODevice::WriteOnly);
      cache_f.write(reply->readAll());
      m_caches.remove(reply->request().url());
      m_notifyUpdate();
    }
  private:
    QStringList m_sources;
    QString m_userAgent;
    QString m_cachedir;
    QThread m_thread;
    QNetworkAccessManager* m_networkAccessManager;
    std::function<void()> m_notifyUpdate;
    // protected by mutex
    QMutex m_mutex;
    QHash<QUrl, QString> m_caches;
  };

  class geo_tile_featureset : public mapnik::Featureset
  {
  public:
    geo_tile_featureset(const mapnik::query& _query, geo_tile_downloader* _downloader)
        : m_query(_query), m_ctx(std::make_shared<mapnik::context_type>()),
          m_proj_trans(
            mapnik::projection("epsg:4326"),
            mapnik::projection("+proj=merc +a=6378137 +b=6378137 +lat_ts=0 +lon_0=0 +x_0=0 +y_0=0 "
                               "+k=1 +units=m +nadgrids=@null +wktext +no_defs +type=crs")),
          m_downloader(_downloader)
    {
      m_query_bbox = _query.get_bbox();
      m_proj_trans.backward(m_query_bbox, PROJ_ENVELOPE_POINTS);

      m_final_zoom_level = zoomLevel(_query.scale_denominator());
      if(m_final_zoom_level == 0)
      {
        m_zoom_level = m_final_zoom_level;
        m_download = true;
      }
      else
      {
        m_zoom_level = m_final_zoom_level - 1;
        m_download = false;
      }
      prepareCoordinates();
    }
    void prepareCoordinates()
    {
      m_start_x = long2tilex(m_query_bbox.minx(), m_zoom_level);
      m_next_x = m_start_x;
      m_next_y = lat2tiley(m_query_bbox.maxy(), m_zoom_level);

      m_last_x = long2tilex(m_query_bbox.maxx(), m_zoom_level);
      m_last_y = lat2tiley(m_query_bbox.miny(), m_zoom_level);
    }
    ~geo_tile_featureset() {}
    virtual mapnik::feature_ptr next()
    {
      if(m_finished)
      {
        return mapnik::feature_ptr();
      }
      // Get the tile
      QString cachefile = m_downloader->requestTile(m_next_x, m_next_y, m_zoom_level, m_download);

      QFileInfo cachefileinfo(cachefile);
      mapnik::feature_ptr feature = nullptr;
      if(cachefileinfo.exists() and m_zoom_level <= m_final_zoom_level)
      {
        // Generate the feature
        feature = mapnik::feature_factory::create(m_ctx, m_feature_id++);
        try
        {
          std::unique_ptr<mapnik::image_reader> reader(
            mapnik::get_image_reader(cachefile.toStdString()));

          if(reader.get())
          {
            mapnik::image_any data = reader->read(0, 0, reader->width(), reader->height());
            mapnik::box2d raster_extent{
              tilex2long(m_next_x, m_zoom_level), tiley2lat(m_next_y, m_zoom_level),
              tilex2long(m_next_x + 1, m_zoom_level), tiley2lat(m_next_y + 1, m_zoom_level)};
            m_proj_trans.forward(raster_extent, PROJ_ENVELOPE_POINTS);
            mapnik::raster_ptr raster = std::make_shared<mapnik::raster>(
              raster_extent, std::move(data), m_query.get_filter_factor());
            feature->set_raster(raster);
          }
        }
        catch(mapnik::image_reader_exception const& ex)
        {
          // qWarning() << "GeoTileDatasource: image reader exception caught: " << ex.what() << "
          // for "
          //  << cachefile;
        }
      }

      // Compute the coordinate for the next tile
      ++m_next_x;
      if(m_next_x > m_last_x)
      {
        m_next_x = m_start_x;
        ++m_next_y;
        if(m_next_y > m_last_y)
        {
          if(m_zoom_level >= m_final_zoom_level + 1 or m_zoom_level == 19)
          {
            m_finished = true;
          }
          else
          {
            ++m_zoom_level;
            prepareCoordinates();
            m_download = true;
          }
        }
      }
      if(feature)
      {
        return feature;
      }
      else
      {
        return next();
      }
    }
  private:
    int m_feature_id = 0;
    int m_next_x, m_next_y, m_start_x, m_last_x, m_last_y;
    int m_final_zoom_level, m_zoom_level;
    mapnik::query m_query;
    mapnik::context_ptr m_ctx;
    bool m_finished = false;
    mapnik::proj_transform m_proj_trans;
    geo_tile_downloader* m_downloader;
    bool m_download;
    mapnik::box2d<double> m_query_bbox;
  };

  class geo_tile_datasource : public mapnik::datasource
  {
  public:
    geo_tile_datasource(geo_tile_downloader* _downloader)
        : mapnik::datasource(mapnik::parameters()), m_downloader(_downloader)
    {
    }
    virtual ~geo_tile_datasource() {}
    datasource_t type() const override { return Raster; }
    boost::optional<mapnik::datasource_geometry_t> get_geometry_type() const override
    {
      return boost::optional<mapnik::datasource_geometry_t>();
    }
    mapnik::featureset_ptr features(mapnik::query const& q) const override
    {
      return std::make_shared<geo_tile_featureset>(q, m_downloader);
    }
    mapnik::featureset_ptr features_at_point(mapnik::coord2d const& pt,
                                             double tol = 0) const override
    {
      Q_UNUSED(pt);
      Q_UNUSED(tol);
      qWarning() << "GeoTileDatasource does not support features_at_point.";
      return mapnik::make_invalid_featureset();
    }
    mapnik::box2d<double> envelope() const override
    {
      // bounds of web mercartor https://epsg.io/3857
      return {-20037508.34, -20048966.1, 20037508.34, 20048966.1};
    }
    mapnik::layer_descriptor get_descriptor() const
    {
      return mapnik::layer_descriptor("geo_tile", "utf-8");
    }
  private:
    geo_tile_downloader* m_downloader = nullptr;
  };
} // namespace Cartography::Quick::Mapnik

struct GeoTileDatasource::Private
{
  std::shared_ptr<geo_tile_datasource> datasource;
  geo_tile_downloader* downloader;
};

GeoTileDatasource::GeoTileDatasource(QObject* parent) : AbstractDatasource(parent), d(new Private)
{
  d->downloader = new geo_tile_downloader(QStringList(), QCoreApplication::applicationName());
  d->downloader->setNotifyUpdate([this]() { emit(tilesUpdated()); });
  d->datasource = std::make_shared<geo_tile_datasource>(d->downloader);
}

GeoTileDatasource::~GeoTileDatasource()
{
  delete d->downloader;
  delete d;
}

QStringList GeoTileDatasource::sources() const { return d->downloader->sources(); }

void GeoTileDatasource::setSources(const QStringList& _sources)
{
  d->downloader->setSources(_sources);
  emit(sourcesChanged());
}

QString GeoTileDatasource::userAgent() const { return d->downloader->userAgent(); }

void GeoTileDatasource::setUserAgent(const QString& _userAgent)
{
  d->downloader->setUserAgent(_userAgent);
  emit(userAgentChanged());
}

mapnik::datasource_ptr GeoTileDatasource::mapnikDatasource() const { return d->datasource; }
