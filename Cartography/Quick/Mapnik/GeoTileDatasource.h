#pragma once

#include "AbstractDatasource.h"

namespace Cartography::Quick::Mapnik
{
  class GeoTileDatasource : public AbstractDatasource
  {
    Q_OBJECT
    Q_PROPERTY(QStringList sources READ sources WRITE setSources NOTIFY sourcesChanged)
    Q_PROPERTY(QString userAgent READ userAgent WRITE setUserAgent NOTIFY userAgentChanged)
  public:
    explicit GeoTileDatasource(QObject* parent = 0);
    virtual ~GeoTileDatasource();
    QStringList sources() const;
    void setSources(const QStringList& _sources);
    QString userAgent() const;
    void setUserAgent(const QString& _userAgent);
  public:
    virtual mapnik::datasource_ptr mapnikDatasource() const;
  signals:
    void sourcesChanged();
    void userAgentChanged();
    void tilesUpdated();
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Quick::Mapnik
