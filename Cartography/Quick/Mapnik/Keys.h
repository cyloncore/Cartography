#ifndef _MAPNIKML_KEYS_H_
#define _MAPNIKML_KEYS_H_

#include <QColor>
#include <QDebug>
#include <QObject>
#include <QVariant>

#include <mapnik/symbolizer.hpp>

namespace Cartography::Quick::Mapnik
{
  class KeyBase : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QVariant value READ value WRITE setValue NOTIFY valueChanged)
    Q_CLASSINFO("DefaultProperty", "value")
  public:
    explicit KeyBase(mapnik::keys _t, QObject* parent = 0);
    explicit KeyBase(QObject* parent = 0) : KeyBase(mapnik::keys{}, parent) {}
    virtual ~KeyBase();
  public:
    mapnik::keys key() const;
    virtual mapnik::symbolizer_base::value_type mapnikValue() const = 0;
  public:
    QVariant value() const;
    void setValue(const QVariant& _value);
  signals:
    void valueChanged();
  private:
    struct Private;
    Private* const d;
  };
  namespace details
  {
    template<typename _TValue_>
    inline mapnik::symbolizer_base::value_type toMapnikValue(const QVariant& _variant);
    template<>
    inline mapnik::symbolizer_base::value_type
      toMapnikValue<mapnik::scaling_method_e>(const QVariant& _variant)
    {
      if(_variant.typeId() == QVariant::Int)
      {
        return mapnik::enumeration_wrapper(mapnik::scaling_method_e(_variant.value<int>()));
      }
      else
      {
        boost::optional<mapnik::scaling_method_e> sme
          = mapnik::scaling_method_from_string(_variant.toString().toStdString());
        if(sme)
        {
          return mapnik::enumeration_wrapper(*sme);
        }
        else
        {
          return mapnik::enumeration_wrapper(mapnik::SCALING_NEAR);
        }
      }
    }
    template<>
    inline mapnik::symbolizer_base::value_type
      toMapnikValue<mapnik::color>(const QVariant& _variant)
    {
      QColor c = _variant.value<QColor>();
      return mapnik::color(c.red(), c.green(), c.blue(), c.alpha());
    }
    template<>
    inline mapnik::symbolizer_base::value_type toMapnikValue<double>(const QVariant& _variant)
    {
      return _variant.toDouble();
    }
    template<>
    inline mapnik::symbolizer_base::value_type
      toMapnikValue<mapnik::line_cap_enum>(const QVariant& _variant)
    {
      return mapnik::enumeration_wrapper(mapnik::line_cap_enum(_variant.value<int>()));
    }
    template<>
    inline mapnik::symbolizer_base::value_type
      toMapnikValue<mapnik::line_join_enum>(const QVariant& _variant)
    {
      return mapnik::enumeration_wrapper(mapnik::line_join_enum(_variant.value<int>()));
    }
    template<>
    inline mapnik::symbolizer_base::value_type toMapnikValue<std::string>(const QVariant& _variant)
    {
      return _variant.toString().toStdString();
    }
  } // namespace details

  template<mapnik::keys _T_, typename _TValue_>
  class Key : public KeyBase
  {
  public:
    explicit Key(QObject* parent = 0) : KeyBase(_T_, parent) {}
    virtual ~Key() {}
    virtual mapnik::symbolizer_base::value_type mapnikValue() const
    {
      return details::toMapnikValue<_TValue_>(value());
    }
  };
} // namespace Cartography::Quick::Mapnik

#endif
