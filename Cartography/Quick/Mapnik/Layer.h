#ifndef _MAPNIKML_LAYER_H_
#define _MAPNIKML_LAYER_H_

#include "MapElement.h"

namespace mapnik
{
  class Map;
}

namespace Cartography::Quick::Mapnik
{
  class AbstractDatasource;
  class Layer : public MapElement
  {
    friend class Map;
    Q_OBJECT
    Q_PROPERTY(Cartography::Quick::Mapnik::AbstractDatasource* datasource READ datasource WRITE
                 setDatasource NOTIFY datasourceChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString srs READ srs WRITE setSrs NOTIFY srsChanged)
    Q_PROPERTY(QStringList styles READ styles WRITE setStyles NOTIFY stylesChanged)
    Q_PROPERTY(bool visible READ visible WRITE setVisible NOTIFY visibleChanged)
  public:
    explicit Layer(QObject* parent = 0);
    virtual ~Layer();
    AbstractDatasource* datasource() const;
    void setDatasource(AbstractDatasource* _datasource);
    QString name() const;
    void setName(const QString& _name);
    QString srs() const;
    /**
     * Set the Spatial reference system
     */
    void setSrs(const QString& _srs);
    QStringList styles() const;
    void setStyles(const QStringList& _styles);
    bool visible() const;
    void setVisible(bool _v);
  private:
    void setMapnikLayer(mapnik::Map* _map, std::size_t _index);
    void applyStyles();
  signals:
    void nameChanged();
    void srsChanged();
    void datasourceChanged();
    void stylesChanged();
    void mapnikLayerChanged();
    void visibleChanged();
  private slots:
    void updateDatasource();
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Quick::Mapnik

#endif
