#include "MapView.h"

#include <QDebug>
#include <QMutex>
#include <QQuickWindow>
#include <QSGSimpleTextureNode>
#include <QSGTexture>
#include <QThreadPool>
#include <QTimer>
#include <QWaitCondition>

#include <mapnik/agg_renderer.hpp>
#include <mapnik/box2d.hpp>
#include <mapnik/color.hpp>
#include <mapnik/feature_type_style.hpp>
#include <mapnik/layer.hpp>
#include <mapnik/map.hpp>
#include <mapnik/rule.hpp>
#include <mapnik/symbolizer.hpp>

#include "Map.h"
#include "ViewTransform.h"

using namespace Cartography::Quick::Mapnik;

constexpr static int TILE_WIDTH = 500;
constexpr static int TILE_HEIGHT = 500;
constexpr static int TILE_COUNT = 100;

namespace Cartography::Quick::Mapnik
{
  struct MapTile
  {
    MapView* self;
    // map
    QMutex map_mutex;
    qreal map_zoom;
    int left_index = std::numeric_limits<int>::max();
    int top_index = std::numeric_limits<int>::max();
    // QRectF map_extent;
    mapnik::box2d<double> map_extent;
    mapnik::Map map;
    bool image_need_update = false;

    // image buffer
    QMutex image_buffer_mutex;
    mapnik::image_rgba8* image_buffer_rendering = nullptr;

    // image
    QMutex image_mutex;
    bool texture_need_update = false;
    mapnik::image_rgba8* image_buffer_qimage = nullptr;
    QImage image;
    qreal image_zoom;
    // QRectF image_extent;
    mapnik::box2d<double> image_extent;

    // Should only be changed in MapView::updatePaintNode
    QSGTexture* texture = nullptr;
    QSGSimpleTextureNode* textureNode = nullptr;

    // Functions
    void update()
    {
      QMutexLocker l_map(&map_mutex);
      if(image_need_update)
      {
        // Take thje info we need
        image_need_update = false;
        mapnik::Map map = this->map;
        // QRectF map_extent = this->map_extent;
        mapnik::box2d<double> map_extent = this->map_extent;
        double map_zoom = this->map_zoom;
        l_map.unlock();

        // Render
        QMutexLocker l_buf(&image_buffer_mutex);
        mapnik::agg_renderer<mapnik::image_rgba8> ren(map, *image_buffer_rendering);
        ren.apply();

        // Update qimage
        QMutexLocker l_img(&image_mutex);
        std::swap(image_buffer_rendering, image_buffer_qimage);
        l_buf.unlock();
        image = QImage((uchar*)image_buffer_qimage->data(), TILE_WIDTH, TILE_HEIGHT,
                       QImage::Format_RGBA8888);
        image_extent = map_extent;
        image_zoom = map_zoom;
        texture_need_update = true;
        // Update the display
        QMetaObject::invokeMethod(self, "update", Qt::QueuedConnection);
      }
    }
  };

} // namespace Cartography::Quick::Mapnik

struct MapView::Private
{
  Map* map;
  QMetaObject::Connection mapChangedConnection;

  qreal zoom = 1.0;
  qreal pan_x = 0.5;
  qreal pan_y = 0.5;

  qreal pixelScaleX = 1.0;
  qreal pixelScaleY = 1.0;

  ViewTransform* viewTransform;

  QThreadPool pool;

  QMutex tiles_mutex;
  QList<MapTile*> tiles;

  bool should_update = true;
  bool force_update = false;

  QTimer updateTimer;
};

MapView::MapView(QQuickItem* parent) : QQuickItem(parent), d(new Private)
{
  setFlag(QQuickItem::ItemHasContents);
  d->map = nullptr;
  d->viewTransform = new ViewTransform(width(), height(), QRectF(), this);

  for(int i = 0; i < TILE_COUNT; ++i)
  {
    MapTile* mt = new MapTile;
    mt->self = this;
    mt->image_buffer_rendering = new mapnik::image_rgba8(TILE_WIDTH, TILE_HEIGHT);
    mt->image_buffer_qimage = new mapnik::image_rgba8(TILE_WIDTH, TILE_HEIGHT);
    d->tiles.append(mt);
  }
  connect(&d->updateTimer, &QTimer::timeout, [this]() { updateTiles(); });
}

MapView::~MapView()
{
  d->pool.clear();
  d->pool.waitForDone();
  for(MapTile* mt : d->tiles)
  {
    delete mt->image_buffer_rendering;
    delete mt->image_buffer_qimage;
  }
  qDeleteAll(d->tiles);
  delete d;
}

namespace
{
  int sort_index(double _zoom, double _zoom_ref)
  {
    if(_zoom == _zoom_ref)
      return 0;
    return -1000 + _zoom;
  }
} // namespace

QSGNode* MapView::updatePaintNode(QSGNode* _oldNode, UpdatePaintNodeData* _upnd)
{
  QSGNode* rootNode = _oldNode;
  if(not rootNode)
  {
    rootNode = new QSGNode;
  }

  QMutexLocker l_tiles(&d->tiles_mutex);
  QList<MapTile*> tiles = d->tiles;
  l_tiles.unlock();
  // Remove the nodes, so they can be put back in order
  for(MapTile* tile : tiles)
  {
    if(tile->textureNode)
    {
      rootNode->removeChildNode(tile->textureNode);
    }
  }
  // Sort the nodes, so that the nodes for current level are in front,
  // the others are in the back with the nodes with more details shown
  // in front of the ones with less
  std::sort(tiles.begin(), tiles.end(), [this](MapTile* t1, MapTile* t2)
            { return sort_index(t1->image_zoom, d->zoom) < sort_index(t2->image_zoom, d->zoom); });
  for(MapTile* tile : tiles)
  {
    if(not tile->textureNode)
    {
      tile->textureNode = new QSGSimpleTextureNode;
    }
    QMutexLocker l_image(&tile->image_mutex);

    QRectF image_extent(tile->image_extent.minx(), tile->image_extent.miny(),
                        tile->image_extent.width(), tile->image_extent.height());

    tile->textureNode->setRect(d->viewTransform->fromMap(image_extent));
    if(tile->texture_need_update)
    {
      if(tile->texture)
      {
        tile->texture->deleteLater();
      }
      tile->texture = window()->createTextureFromImage(tile->image);
      tile->textureNode->setTexture(tile->texture);
      tile->texture_need_update = false;
    }
    if(not tile->image.isNull())
    {
      rootNode->appendChildNode(tile->textureNode);
    }
  }

  return rootNode;
}

void MapView::geometryChange(const QRectF& newGeometry, const QRectF& oldGeometry)
{
  updateMap(false);
  QQuickItem::geometryChange(newGeometry, oldGeometry);
}

Map* MapView::map() const { return d->map; }

void MapView::setMap(Map* _map)
{
  if(d->map)
  {
    disconnect(d->mapChangedConnection);
  }
  d->map = _map;
  emit(mapChanged());
  updateMap(true);
  d->mapChangedConnection
    = connect(d->map, &Map::mapnikMapChanged, std::bind(&MapView::updateMap, this, true));
}

ViewTransform* MapView::viewTransform() const { return d->viewTransform; }

qreal MapView::zoom() const { return d->zoom; }

void MapView::zoomToFit()
{
  setCenterX(0);
  setCenterY(0);
  setZoom(1);
}

void MapView::setZoom(qreal _zoom)
{
  if(_zoom <= 0)
  {
    qWarning() << "Got zoom level" << _zoom << ", expecting strictly positive number";
    return;
  }
  if(std::abs(_zoom - d->zoom) < 1e-3)
  {
    return;
  }
  _zoom = std::max(_zoom, 1.0);
  d->zoom = _zoom;
  emit(zoomChanged());
  updateMap(false);
}

void MapView::zoomIn(qreal _factor) { setZoom(d->zoom * _factor); }

void MapView::zoomOut(qreal _factor) { setZoom(d->zoom / _factor); }

void MapView::centerTo(qreal _pt_x, qreal _pt_y, qreal _pan_factor)
{
  setCenterX((1.0 - _pan_factor) * d->pan_x + _pan_factor * _pt_x);
  setCenterY((1.0 - _pan_factor) * d->pan_y + _pan_factor * _pt_y);
}

void MapView::zoomTo(qreal _pt_x, qreal _pt_y, qreal _zoom, qreal _pan_factor)
{
  centerTo(_pt_x, _pt_y, _pan_factor);
  setZoom(_zoom);
}

qreal MapView::centerX() const { return d->pan_x; }

void MapView::setCenterX(qreal _pan_x)
{
  d->pan_x = _pan_x;
  emit(centerXChanged());
  updateMap(false);
}

qreal MapView::centerY() const { return d->pan_y; }

void MapView::setCenterY(qreal _pan_y)
{
  d->pan_y = _pan_y;
  emit(centerYChanged());
  updateMap(false);
}

qreal MapView::pixelScaleX() const { return d->pixelScaleX; }

qreal MapView::pixelScaleY() const { return d->pixelScaleY; }

void MapView::updateMap(bool _force)
{
  d->should_update = true;
  d->force_update = d->force_update or _force;
  d->updateTimer.start(0);
}

void MapView::updateTiles()
{
  if(d->should_update and d->map)
  {
    d->should_update = false;
    bool force = d->force_update;
    d->force_update = false;

    QMutexLocker l(&d->tiles_mutex);

    // Apply zoom
    mapnik::Map map = d->map->map();
    map.set_width(width());
    map.set_height(height());
    QRectF full_extent = d->map->fullExtent();

    QPointF center{d->pan_x, d->pan_y};
    QSizeF size = full_extent.size() / d->zoom;

    QRectF zoomed_extent(center.x() - 0.5 * size.width(), center.y() - 0.5 * size.height(),
                         size.width(), size.height());
    map.zoom_to_box(mapnik::box2d<double>{zoomed_extent.left(), zoomed_extent.top(),
                                          zoomed_extent.right(), zoomed_extent.bottom()});

    d->pixelScaleX = zoomed_extent.width() / width();
    d->pixelScaleY = zoomed_extent.height() / height();
    emit(pixelScaleXChanged());
    emit(pixelScaleYChanged());

    QRectF extent(map.get_buffered_extent().minx(), map.get_buffered_extent().miny(),
                  map.get_buffered_extent().width(), map.get_buffered_extent().height());
    d->viewTransform->set(width(), height(), extent, d->map->srs());
    emit(viewTransformChanged());

    int left_index = std::floor(d->pan_x / TILE_WIDTH);
    int top_index = std::floor(d->pan_y / TILE_HEIGHT);

    for(int v_idx = top_index; v_idx <= std::ceil((d->pan_y + height()) / TILE_HEIGHT); ++v_idx)
    {
      for(int h_idx = left_index; h_idx <= std::ceil((d->pan_x + width()) / TILE_WIDTH); ++h_idx)
      {
        MapTile* selected_tile = nullptr;
        for(MapTile* tile : d->tiles)
        {
          if(tile->left_index == h_idx and tile->top_index == v_idx and tile->map_zoom == d->zoom)
          {
            selected_tile = tile;
            break;
          }
        }

        if(force or selected_tile == nullptr)
        {
          MapTile* tile = selected_tile;
          if(selected_tile == nullptr)
          {
            tile = d->tiles.takeFirst();
          }
          QMutexLocker l(&tile->map_mutex);
          tile->map = map;
          tile->map.set_aspect_fix_mode(mapnik::Map::aspect_fix_mode::RESPECT);
          tile->map.set_width(TILE_WIDTH);
          tile->map.set_height(TILE_HEIGHT);
          QRectF map_extent = d->viewTransform->toMap(QRectF(h_idx * TILE_WIDTH - d->pan_x,
                                                             v_idx * TILE_HEIGHT - d->pan_y,
                                                             TILE_WIDTH, TILE_HEIGHT));
          mapnik::box2d<double> map_extent_m{map_extent.left(), map_extent.top(),
                                             map_extent.right(), map_extent.bottom()};
          tile->map.zoom_to_box(map_extent_m);
          tile->map_extent = map_extent_m;
          tile->left_index = h_idx;
          tile->top_index = v_idx;
          tile->map_zoom = d->zoom;
          tile->image_need_update = true;
          if(selected_tile == nullptr)
          {
            d->tiles.append(tile);
          }
          d->pool.start([tile]() { tile->update(); });
        }
      }
    }
    QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
  }
}

#include "moc_MapView.cpp"
