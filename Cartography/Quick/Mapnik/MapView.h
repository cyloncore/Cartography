#ifndef _MAPNIKML_MAPVIEW_H_
#define _MAPNIKML_MAPVIEW_H_

#include <QQuickItem>

namespace Cartography::Quick::Mapnik
{
  class Map;
  class ViewTransform;
  class MapView : public QQuickItem
  {
    Q_OBJECT
    Q_PROPERTY(Cartography::Quick::Mapnik::Map* map READ map WRITE setMap NOTIFY mapChanged)
    Q_PROPERTY(qreal zoom READ zoom WRITE setZoom NOTIFY zoomChanged)
    Q_PROPERTY(qreal centerX READ centerX WRITE setCenterX NOTIFY centerXChanged)
    Q_PROPERTY(qreal centerY READ centerY WRITE setCenterY NOTIFY centerYChanged)
    Q_PROPERTY(qreal pixelScaleX READ pixelScaleX NOTIFY pixelScaleXChanged)
    Q_PROPERTY(qreal pixelScaleY READ pixelScaleY NOTIFY pixelScaleYChanged)
    Q_PROPERTY(Cartography::Quick::Mapnik::ViewTransform* viewTransform READ viewTransform NOTIFY
                 viewTransformChanged)
  public:
    MapView(QQuickItem* parent = 0);
    virtual ~MapView();
  public:
    Map* map() const;
    void setMap(Map* _map);
    ViewTransform* viewTransform() const;
  public: // zoom and pan
    qreal zoom() const;
    Q_INVOKABLE void zoomToFit();
    Q_INVOKABLE void setZoom(qreal _zoom);
    Q_INVOKABLE void zoomIn(qreal _factor);
    Q_INVOKABLE void zoomOut(qreal _factor);
    Q_INVOKABLE void zoomTo(qreal _pt_x, qreal _pt_y, qreal _zoom, qreal _pan_factor);
    Q_INVOKABLE void centerTo(qreal _pt_x, qreal _pt_y, qreal _pan_factor = 1.0);
    qreal centerX() const;
    void setCenterX(qreal _center_x);
    qreal centerY() const;
    void setCenterY(qreal _center_y);
    qreal pixelScaleX() const;
    qreal pixelScaleY() const;
  protected:
    QSGNode* updatePaintNode(QSGNode* _oldNode, UpdatePaintNodeData* _upnd) override;
    void geometryChange(const QRectF& newGeometry, const QRectF& oldGeometry) override;
  signals:
    void mapChanged();
    void zoomChanged();
    void centerXChanged();
    void centerYChanged();
    void viewTransformChanged();
    void pixelScaleXChanged();
    void pixelScaleYChanged();
  public slots:
    void updateMap(bool _force);
  private:
    void updateTiles();
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Quick::Mapnik

#endif
