#include "Markers.h"

#include <QQmlContext>
#include <QQmlEngine>

#include "ViewTransform.h"

using namespace Cartography::Quick::Mapnik;

struct Markers::Private
{
  QList<QPair<QQmlContext*, QQuickItem*>> items;
  QVariantList model;
  QQmlComponent* delegate = nullptr;
  ViewTransform* view_transform;
  void update(Markers* _self);
};

void Markers::Private::update(Markers* _self)
{
  if(delegate and view_transform)
  {
    while(items.size() > model.size())
    {
      // TODO should we remove the context too?
      delete items.takeLast().second;
    }
    for(qsizetype i = 0; i < model.size(); ++i)
    {
      if(i >= items.size())
      {
        QQmlContext* context = new QQmlContext(QQmlEngine::contextForObject(_self));
        context->setContextProperty("modelData", model[i]);
        items.append({context, qobject_cast<QQuickItem*>(delegate->create(context))});
        items.last().second->setParentItem(_self);
      }
      else
      {
        items[i].first->setContextProperty("modelData", model[i]);
      }
      QVariantHash data = model[i].toHash();
      qreal longitude = data["longitude"].toReal();
      qreal latitude = data["latitude"].toReal();
      QPointF coord = view_transform->fromWgs84(longitude, latitude);
      items[i].second->setPosition(coord);
    }
  }
}

Markers::Markers(QQuickItem* _parent) : QQuickItem(_parent), d(new Private)
{
  setFlags(QQuickItem::ItemHasContents);
}
Markers::~Markers() { delete d; }

QQmlComponent* Markers::delegate() const { return d->delegate; }

void Markers::setDelegate(QQmlComponent* _component)
{
  if(d->delegate)
  {
    d->delegate->deleteLater();
  }
  for(auto it : d->items)
  {
    it.first->deleteLater();
    it.second->deleteLater();
  }
  d->delegate = _component;
  if(d->delegate)
  {
    d->delegate->setParent(this);
  }
  emit(delegateChanged());
  d->update(this);
}
ViewTransform* Markers::viewTransform() const { return d->view_transform; }

void Markers::setViewTransform(ViewTransform* _view_transform)
{
  d->view_transform = _view_transform;
  emit(viewTransformChanged());
  d->update(this);
}

QVariant Markers::model() const { return d->model; }

void Markers::setModel(const QVariant& _model)
{
  d->model = _model.toList();
  emit(modelChanged());
  d->update(this);
}

#include "moc_Markers.cpp"
