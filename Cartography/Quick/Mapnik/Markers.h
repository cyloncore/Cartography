#pragma once

#include <QQuickItem>

namespace Cartography::Quick::Mapnik
{
  class ViewTransform;
  class Markers : public QQuickItem
  {
    Q_OBJECT
    Q_PROPERTY(QQmlComponent* delegate READ delegate WRITE setDelegate NOTIFY delegateChanged)
    Q_PROPERTY(Cartography::Quick::Mapnik::ViewTransform* viewTransform READ viewTransform WRITE
                 setViewTransform NOTIFY viewTransformChanged)
    Q_PROPERTY(QVariant model READ model WRITE setModel NOTIFY modelChanged)
  public:
    Markers(QQuickItem* _parent = nullptr);
    ~Markers();
    QQmlComponent* delegate() const;
    void setDelegate(QQmlComponent* _component);
    ViewTransform* viewTransform() const;
    void setViewTransform(ViewTransform* _view_transform);
    QVariant model() const;
    void setModel(const QVariant& _model);
  signals:
    void delegateChanged();
    void modelChanged();
    void viewTransformChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Quick::Mapnik
