#include "Rule.h"

#include <mapnik/config_error.hpp>
#include <mapnik/rule.hpp>

#include "Symbolizers.h"
#include "Workaround_p.h"

using namespace Cartography::Quick::Mapnik;

struct Rule::Private
{
  QList<SymbolizerBase*> symbolizers;
  QString expression;
  double min_scale = 0.0;
  double max_scale = std::numeric_limits<double>::max();
};

Rule::Rule(QObject* parent) : QObject(parent), d(new Private) {}

Rule::~Rule() { delete d; }

QQmlListProperty<QObject> Rule::symbolizers() const
{
  return qml_list_property_helper<
    Cartography::Quick::Mapnik::SymbolizerBase, QT_STRINGIFY_SIGNAL(symbolizerChanged()),
    QT_STRINGIFY_SIGNAL(ruleChanged())>::create(const_cast<Rule*>(this), &d->symbolizers);
}

mapnik::rule Rule::mapnikRule()
{
  mapnik::rule r(std::string(), d->min_scale, d->max_scale);
  for(SymbolizerBase* sb : d->symbolizers)
  {
    sb->appendMapnikSymbolizer(&r);
  }
  if(not d->expression.isEmpty())
  {
    try
    {
      r.set_filter(mapnik::parse_expression(d->expression.toStdString()));
#if 0
    } catch(mapnik::config_error& ce)
    {
      qWarning() << "mapnik::config_error: " << ce.what();
#endif
    }
    catch(const std::exception& re)
    {
      qWarning() << "Failed to set filter: " << re.what();
    }
  }
  return r;
}

QString Rule::filter() const { return d->expression; }

void Rule::setFilter(const QString& _filter)
{
  d->expression = _filter;
  emit(filterChanged());
  emit(ruleChanged());
}

double Rule::maxScale() const { return d->max_scale; }

void Rule::setMaxScale(double _scale)
{
  d->max_scale = _scale;
  emit(maxScaleChanged());
  emit(ruleChanged());
}

double Rule::minScale() const { return d->min_scale; }

void Rule::setMinScale(double _scale)
{
  d->min_scale = _scale;
  emit(minScaleChanged());
  emit(ruleChanged());
}

#include "moc_Rule.cpp"
