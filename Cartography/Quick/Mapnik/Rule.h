#ifndef _MAPNIKML_RULE_H_
#define _MAPNIKML_RULE_H_

#include <QQmlListProperty>

namespace mapnik
{
  class rule;
}

namespace Cartography::Quick::Mapnik
{
  class SymbolizerBase;
  class Rule : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<QObject /*Cartography::Quick::Mapnik::SymbolizerBase*/> symbolizers
                 READ symbolizers)
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY filterChanged)
    Q_PROPERTY(double maxScale READ maxScale WRITE setMaxScale NOTIFY maxScaleChanged)
    Q_PROPERTY(double minScale READ minScale WRITE setMinScale NOTIFY minScaleChanged)
    Q_CLASSINFO("DefaultProperty", "symbolizers")
  public:
    explicit Rule(QObject* parent = 0);
    virtual ~Rule();
    QQmlListProperty<QObject> symbolizers() const;
    mapnik::rule mapnikRule();
  public:
    QString filter() const;
    void setFilter(const QString& _filter);
    double maxScale() const;
    void setMaxScale(double _scale);
    double minScale() const;
    void setMinScale(double _scale);
  signals:
    void filterChanged();
    void maxScaleChanged();
    void minScaleChanged();
    void ruleChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Quick::Mapnik

#endif
