#include "Symbolizers.h"

#include "Keys.h"

#include "Workaround_p.h"

using namespace Cartography::Quick::Mapnik;

struct SymbolizerBase::Private
{
  QList<Cartography::Quick::Mapnik::KeyBase*> keys;
};

SymbolizerBase::SymbolizerBase(QObject* parent) : QObject(parent), d(new Private) {}

SymbolizerBase::~SymbolizerBase() { delete d; }

QQmlListProperty<QObject> SymbolizerBase::keys() const
{
  // return QQmlListProperty<KeyBase>(const_cast<SymbolizerBase*>(this), &d->keys);
  return qml_list_property_helper<KeyBase, QT_STRINGIFY_SIGNAL(valueChanged()),
                                  QT_STRINGIFY_SIGNAL(
                                    symbolizerChanged())>::create(const_cast<SymbolizerBase*>(this),
                                                                  &d->keys);
}

QList<KeyBase*> SymbolizerBase::keysList() const { return d->keys; }

#include "moc_Symbolizers.cpp"
