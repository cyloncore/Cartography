#include "Utils.h"

#include "UtilsAttachedProperty.h"

using namespace Cartography::Quick::Mapnik;

UtilsAttachedProperty* Utils::qmlAttachedProperties(QObject* object)
{
  return new UtilsAttachedProperty(object);
}
