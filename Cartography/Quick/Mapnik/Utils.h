#ifndef _MAPNIKML_UTILS_H_
#define _MAPNIKML_UTILS_H_

#include <QObject>

namespace Cartography::Quick::Mapnik
{
  class UtilsAttachedProperty;
  class Utils : public QObject
  {
    Q_OBJECT
  public:
    static UtilsAttachedProperty* qmlAttachedProperties(QObject* object);
  };
} // namespace Cartography::Quick::Mapnik

#include <QtQml>
QML_DECLARE_TYPEINFO(Cartography::Quick::Mapnik::Utils, QML_HAS_ATTACHED_PROPERTIES)

#endif
