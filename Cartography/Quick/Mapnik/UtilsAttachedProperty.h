#ifndef _MAPNIKML_UTILS_ATTACHED_PROPERTY_H_
#define _MAPNIKML_UTILS_ATTACHED_PROPERTY_H_

#include <QObject>

namespace Cartography::Quick::Mapnik
{
  class UtilsAttachedProperty : public QObject
  {
    Q_OBJECT
  public:
    UtilsAttachedProperty(QObject* _parent = nullptr);
    ~UtilsAttachedProperty();

    Q_INVOKABLE QString wktToProj(const QString& _srs);
  };
} // namespace Cartography::Quick::Mapnik

#endif
