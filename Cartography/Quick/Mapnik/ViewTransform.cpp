#include "ViewTransform.h"

#include <mapnik/proj_transform.hpp>

#include <QRect>
#include <QRectF>

using namespace Cartography::Quick::Mapnik;

struct ViewTransform::Private
{
  int width, height;
  QRectF extent;
  qreal scale_x, scale_y;
  mapnik::proj_transform* proj_trans = nullptr;
};

ViewTransform::ViewTransform(int _width, int _height, const QRectF& _extent, QObject* _parent)
    : QObject(_parent), d(new Private)
{
  set(_width, _height, _extent, "epsg:4326");
}

ViewTransform::~ViewTransform()
{
  delete d->proj_trans;
  delete d;
}

int ViewTransform::height() const { return d->height; }

int ViewTransform::width() const { return d->width; }

qreal ViewTransform::scaleX() const { return d->scale_x; }

qreal ViewTransform::scaleY() const { return d->scale_y; }

QRectF ViewTransform::extent() const { return d->extent; }

void ViewTransform::set(int _width, int _height, const QRectF& _extent,
                        const QString& _map_projection)
{
  d->width = _width;
  d->height = _height;
  d->extent = _extent;
  d->scale_x = _extent.width() > 0 ? d->width / _extent.width() : 1.0;
  d->scale_y = _extent.height() > 0 ? d->height / _extent.height() : 1.0;

  delete d->proj_trans;
  d->proj_trans = new mapnik::proj_transform(mapnik::projection("epsg:4326"),
                                             mapnik::projection(qPrintable(_map_projection)));

  emit(transformChanged());
}

QPointF ViewTransform::fromWgs84(qreal _longitude, qreal _latitude) const
{
  double x = _longitude;
  double y = _latitude;
  double z = 0.0;
  d->proj_trans->forward(x, y, z);
  return fromMap(x, y);
}

QPointF ViewTransform::toWgs84(qreal _x, qreal _y) const
{
  QPointF c_m = toMap(_x, _y);
  double x = c_m.x();
  double y = c_m.y();
  double z = 0.0;
  d->proj_trans->backward(x, y, z);
  return QPointF(x, y);
}

QPointF ViewTransform::fromMap(qreal x, qreal y) const
{
  return QPointF((x - d->extent.left()) * d->scale_x, (d->extent.bottom() - y) * d->scale_y);
}

QPointF ViewTransform::toMap(qreal x, qreal y) const
{
  return QPointF(d->extent.left() + x / d->scale_x, d->extent.bottom() - y / d->scale_y);
}

QRectF ViewTransform::fromMap(const QRectF& _pt) const
{
  return QRectF(fromMap(_pt.bottomLeft()), fromMap(_pt.topRight()));
}

QRectF ViewTransform::toMap(const QRectF& _pt) const
{
  return QRectF(toMap(_pt.bottomLeft()), toMap(_pt.topRight()));
}

#include "moc_ViewTransform.cpp"
