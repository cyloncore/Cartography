#ifndef MAPNIKML_VIEWTRANSFORM_H_
#define MAPNIKML_VIEWTRANSFORM_H_

#include <QObject>

#include <QPointF>

namespace Cartography::Quick::Mapnik
{

  class ViewTransform : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(qreal scaleX READ scaleX NOTIFY transformChanged)
    Q_PROPERTY(qreal scaleY READ scaleY NOTIFY transformChanged)
  public:
    ViewTransform(int _width, int _height, const QRectF& _extent, QObject* _parent);
    ~ViewTransform();
  public:
    int width() const;
    int height() const;
    QRectF extent() const;

    qreal scaleX() const;
    qreal scaleY() const;

    void set(int _width, int _height, const QRectF& _extent, const QString& _map_projection);
  public:
    Q_INVOKABLE QPointF toMap(qreal x, qreal y) const;
    Q_INVOKABLE QPointF toMap(const QPointF& _pt) const { return toMap(_pt.x(), _pt.y()); }
    Q_INVOKABLE QPointF fromMap(qreal x, qreal y) const;
    Q_INVOKABLE QPointF fromMap(const QPointF& _pt) const { return fromMap(_pt.x(), _pt.y()); }
    /// Convert from WGS84 coordinate to view coordinates (in pixels)
    Q_INVOKABLE QPointF fromWgs84(qreal _longitude, qreal _latitude) const;
    Q_INVOKABLE QPointF fromWgs84(const QPointF& _pt) const { return fromWgs84(_pt.x(), _pt.y()); }
    Q_INVOKABLE QPointF toWgs84(qreal _x, qreal _y) const;
    Q_INVOKABLE QPointF toWgs84(const QPointF& _pt) const { return toWgs84(_pt.x(), _pt.y()); }

    Q_INVOKABLE QRectF toMap(const QRectF& _pt) const;
    Q_INVOKABLE QRectF fromMap(const QRectF& _pt) const;
  signals:
    void transformChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Quick::Mapnik

#endif