namespace
{
  template<size_t N>
  struct FixedString
  {
    constexpr FixedString(const char (&str)[N]) { std::copy_n(str, N, value); }

    char value[N];
  };
} // namespace

template<typename _T_, FixedString _src_signal_, FixedString _dst_signal_>
struct qml_list_property_helper
{
  static QQmlListProperty<QObject> create(QObject* o, QList<_T_*>* _list)
  {
    return QQmlListProperty<QObject>{o, _list, &append, &count, &at, &clear};
  }
  static void append(QQmlListProperty<QObject>* _list, QObject* _key)
  {
    _T_* t = qobject_cast<_T_*>(_key);
    if(t)
    {
      QObject::connect(t, _src_signal_.value, _list->object, _dst_signal_.value);
      reinterpret_cast<QList<_T_*>*>(_list->data)->append(t);
    }
    else
    {
      qDebug() << "Cannot add object: " << _key << " invalid type.";
    }
  }

  static qsizetype count(QQmlListProperty<QObject>* _list)
  {
    return reinterpret_cast<QList<_T_*>*>(_list->data)->size();
  }

  static QObject* at(QQmlListProperty<QObject>* _list, qsizetype _index)
  {
    return (*reinterpret_cast<QList<_T_*>*>(_list->data))[_index];
  }

  static void clear(QQmlListProperty<QObject>* _list)
  {
    QList<_T_*>* l = reinterpret_cast<QList<_T_*>*>(_list->data);
    for(_T_* t : *l)
    {
      QObject::disconnect(t, _src_signal_.value, _list->object, _dst_signal_.value);
    }
    l->clear();
  }
};
