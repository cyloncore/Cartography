#include "CartographyRasterPlugin.h"

#include <QtQml>

#include <Cartography/Raster/Raster.h>

#include "Raster.h"
#include "RasterFactorySingleton.h"

using namespace Cartography::Quick::Raster;

void CartographyRasterPlugin::registerTypes(const char* /*uri*/)
{
  QMetaType::registerConverter<Raster*, Cartography::Raster::Raster>([](Raster* _raster)
                                                                     { return _raster->raster(); });
  QMetaType::registerConverter<QObject*, Cartography::Raster::Raster>(
    [](QObject* _object)
    {
      Raster* raster = qobject_cast<Raster*>(_object);
      return raster ? raster->raster() : Cartography::Raster::Raster();
    });

  const char* uri = "Cartography.Raster";

  qmlRegisterType<Raster>(uri, 2, 0, "Raster");

  qmlRegisterSingletonType<RasterFactorySingleton>(uri, 2, 0, "RasterFactory",
                                                   [](QQmlEngine*, QJSEngine*) -> QObject*
                                                   { return new RasterFactorySingleton; });
}
