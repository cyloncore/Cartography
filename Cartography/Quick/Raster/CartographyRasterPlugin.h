#pragma once

#include <QQmlExtensionPlugin>

class CartographyRasterPlugin : public QQmlExtensionPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "Cartography/Raster/2.0")
public:
  void registerTypes(const char* uri) override;
};
