#include "Raster.h"

#include <Cartography/Raster/Raster.h>

using namespace Cartography::Quick::Raster;

struct Raster::Private
{
  Cartography::Raster::Raster raster;
};

Raster::Raster(QObject* _parent) : QObject(_parent), d(new Private) {}

Raster::Raster(const Cartography::Raster::Raster& _raster, QObject* _parent) : Raster(_parent)
{
  d->raster = _raster;
}

Raster::~Raster() { delete d; }

Cartography::Raster::Raster Raster::raster() const { return d->raster; }

#include "moc_Raster.cpp"
