#pragma once

#include <QObject>

#include <Cartography/Forward.h>

namespace Cartography::Quick::Raster
{
  class RasterAttachedProperty;
  /**
   * Encapsulates a \ref Cartography::Raster::Raster for use in QML.
   */
  class Raster : public QObject
  {
    Q_OBJECT
  public:
    Raster(const Cartography::Raster::Raster& _raster, QObject* _parent = nullptr);
    Raster(QObject* _parent = nullptr);
    virtual ~Raster();
    Cartography::Raster::Raster raster() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Quick::Raster
