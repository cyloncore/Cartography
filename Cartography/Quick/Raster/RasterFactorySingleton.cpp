#include "RasterFactorySingleton.h"

#include <QUrl>

#include <Cartography/Raster/Raster.h>

#include "Raster.h"

using namespace Cartography::Quick::Raster;

RasterFactorySingleton::RasterFactorySingleton(QObject* _parent) : QObject(_parent) {}

RasterFactorySingleton::~RasterFactorySingleton() {}

Raster* RasterFactorySingleton::load(const QUrl& _url)
{
  Cartography::Raster::Raster r = Cartography::Raster::Raster::load(_url.toLocalFile());
  if(r.isValid())
  {
    return new Raster(r);
  }
  else
  {
    return nullptr;
  }
}

#include "moc_RasterFactorySingleton.cpp"
