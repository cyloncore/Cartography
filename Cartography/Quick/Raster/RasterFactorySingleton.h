#pragma once

#include <QObject>

#include <Cartography/Forward.h>

namespace Cartography::Quick::Raster
{
  class RasterFactorySingleton : public QObject
  {
    Q_OBJECT
  public:
    RasterFactorySingleton(QObject* _parent = nullptr);
    ~RasterFactorySingleton();

    Q_INVOKABLE Cartography::Quick::Raster::Raster* load(const QUrl& _url);
  };
} // namespace Cartography::Quick::Raster
