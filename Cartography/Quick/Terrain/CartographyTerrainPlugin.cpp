#include "CartographyTerrainPlugin.h"

#include <QtQml>

#include <Cartography/Terrain/Algorithms/Generators.h>
#include <Cartography/Terrain/HeightMap.h>

#include "HeightMapView.h"
#include "HeightMapWrapper.h"
#include "HillshadeView.h"

using namespace Cartography::Quick::Terrain;

void CartographyTerrainPlugin::registerTypes(const char* uri)
{
  if(strcmp(uri, "Cartography::Quick::Terrain::HeightMap") == 0)
  {
    qmlRegisterType<HeightMapView>(uri, 2, 0, "HeightMapView");
    qmlRegisterType<HillshadeView>(uri, 2, 0, "HillshadeView");
    qmlRegisterSingletonType(uri, 2, 0, "HeightMap",
                             [](QQmlEngine* engine, QJSEngine* scriptEngine) -> QJSValue
                             { return scriptEngine->newQObject(new HeightMapWrapper); });
  }
  else if(strcmp(uri, "Cartography.Terrain.Algorithms.Generators") == 0)
  {
    qmlRegisterType<Cartography::Terrain::Algorithms::Generators::TectonicUpliftFluvialErosion>(
      uri, 2, 0, "TectonicUpliftFluvialErosion");
  }
}
