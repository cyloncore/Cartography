#pragma once

#include <QQmlExtensionPlugin>

class CartographyTerrainPlugin : public QQmlExtensionPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "Cartography/Terrain/2.0")
public:
  void registerTypes(const char* uri) override;
};
