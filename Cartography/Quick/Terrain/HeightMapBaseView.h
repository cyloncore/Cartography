#ifndef _TERRAINML_HEIGHTMAPBASEVIEW_H_
#define _TERRAINML_HEIGHTMAPBASEVIEW_H_

#include <QQuickItem>

#include <Cartography/Forward.h>

namespace Cartography::Quick::Terrain
{
  class HeightMapBaseView : public QQuickItem
  {
    Q_OBJECT
    Q_PROPERTY(Cartography::Terrain::HeightMap heightMap READ heightMap WRITE setHeightMap NOTIFY
                 heightMapChanged)
    Q_PROPERTY(QRectF viewRect READ viewRect WRITE setViewRect NOTIFY viewRectChanged)
  protected:
    struct Private;
    HeightMapBaseView(Private* _d, QQuickItem* parent = 0);
  public:
    HeightMapBaseView(QQuickItem* parent = 0);
    virtual ~HeightMapBaseView();
  public:
    Cartography::Terrain::HeightMap heightMap() const;
    void setHeightMap(const Cartography::Terrain::HeightMap& _map);
    QRectF viewRect() const;
    void setViewRect(const QRectF& _viewRect);
    bool hasCustomViewRect() const;
  public:
    Q_INVOKABLE QPointF viewToMap(const QPointF& _point);
    Q_INVOKABLE QPointF mapToView(const QPointF& _point);
  signals:
    void heightMapChanged();
    void viewRectChanged();
  protected:
    virtual QImage image() const = 0;
    QSGNode* updatePaintNode(QSGNode* _oldNode, UpdatePaintNodeData* _upnd);
  protected:
    Private* const d;
  };
} // namespace Cartography::Quick::Terrain

#endif
