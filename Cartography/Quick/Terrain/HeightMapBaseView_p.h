#include "HeightMapBaseView.h"

class QSGTexture;

#include <Cartography/Terrain/HeightMap.h>

namespace Cartography::Quick::Terrain
{
  struct HeightMapBaseView::Private
  {
    Cartography::Terrain::HeightMap heightMap;
    bool update_map;
    QSGTexture* texture = nullptr;
    QRectF customViewRect;
  };
} // namespace Cartography::Quick::Terrain
