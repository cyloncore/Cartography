#include "HeightMapView.h"

#include <QImage>

#include <Cartography/Terrain/HeightMap.h>

using namespace Cartography::Quick::Terrain;

HeightMapView::HeightMapView(QQuickItem* parent) : HeightMapBaseView(parent) {}

HeightMapView::~HeightMapView() {}

QImage HeightMapView::image() const { return heightMap().toImage(); }

#include "moc_HeightMapView.cpp"
