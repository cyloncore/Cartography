#ifndef _TERRAINML_HEIGHTMAPVIEW_H_
#define _TERRAINML_HEIGHTMAPVIEW_H_

#include "HeightMapBaseView.h"

namespace Cartography::Quick::Terrain
{
  class HeightMapView : public HeightMapBaseView
  {
    Q_OBJECT
  public:
    HeightMapView(QQuickItem* parent = 0);
    virtual ~HeightMapView();
  protected:
    virtual QImage image() const;
  };
} // namespace Cartography::Quick::Terrain

#endif
