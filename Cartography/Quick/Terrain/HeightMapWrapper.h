#ifndef _TERRAINML_HEIGHTMAPWRAPPER_H_
#define _TERRAINML_HEIGHTMAPWRAPPER_H_
#include <QObject>

#include <Cartography/Forward.h>

namespace Cartography::Quick::Terrain
{
  class HeightMapWrapper : public QObject
  {
    Q_OBJECT
  public:
    Q_INVOKABLE Cartography::Terrain::HeightMap create(qreal _x, qreal _y, int _Width, int _height,
                                                       qreal _resolution);
    Q_INVOKABLE QRectF boundingBox(const Cartography::Terrain::HeightMap& _map);
    Q_INVOKABLE Cartography::Terrain::HeightMap fill(const Cartography::Terrain::HeightMap& _map,
                                                     float _altitude);
    Q_INVOKABLE Cartography::Terrain::HeightMap raise(const Cartography::Terrain::HeightMap& _map,
                                                      qreal _x, qreal _y, float _altitude,
                                                      float _radius, float _degree);
    Q_INVOKABLE Cartography::Terrain::HeightMap noise(const Cartography::Terrain::HeightMap& map,
                                                      qreal _min, qreal _max, qint32 _seed,
                                                      float _scale = 0.2);
  };
} // namespace Cartography::Quick::Terrain

#endif
