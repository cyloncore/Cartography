set(TERRAINML3D_SRCS
  HeightMapMesh.cpp
  CartographyTerrain3DPlugin.cpp)

add_library(CartographyTerrain3DPlugin SHARED ${TERRAINML3D_SRCS})
target_link_libraries(CartographyTerrain3DPlugin PRIVATE CartographyTerrain Qt6::Quick Qt6::3DRender clog)

install(TARGETS CartographyTerrain3DPlugin
  RUNTIME DESTINATION ${INSTALL_QML_DIR}/Cartography/Terrain3D
  LIBRARY DESTINATION ${INSTALL_QML_DIR}/Cartography/Terrain3D
)

install(DIRECTORY qml/
        DESTINATION ${INSTALL_QML_DIR}/)

install (CODE "execute_process(COMMAND qmlplugindump Cartography.Terrain3D 2.0 ${INSTALL_QML_DIR}  OUTPUT_FILE ${INSTALL_QML_DIR}/Cartography/Terrain3D/plugin.qmltypes)")
