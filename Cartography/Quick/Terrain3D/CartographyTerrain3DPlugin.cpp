#include "CartographyTerrain3DPlugin.h"

#include <QtQml>

#include "HeightMapMesh.h"

using namespace Cartography::Quick::Terrain3D;

void CartographyTerrain3DPlugin::registerTypes(const char* /*uri*/)
{
  const char* uri = "Cartography.Terrain3D";
  qmlRegisterType<HeightMapMesh>(uri, 2, 0, "HeightMapMesh");
}
