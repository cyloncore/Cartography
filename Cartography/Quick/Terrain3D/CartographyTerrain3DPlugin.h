#pragma once

#include <QQmlExtensionPlugin>

class CartographyTerrain3DPlugin : public QQmlExtensionPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "Cartography/Terrain3D/1.0")
public:
  void registerTypes(const char* uri) override;
};
