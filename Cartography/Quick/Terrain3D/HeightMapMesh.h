#ifndef _TERRAINML3D_HEIGHTMAPMESH_H_
#define _TERRAINML3D_HEIGHTMAPMESH_H_

#include <Qt3DRender/QGeometryRenderer>

#include <Cartography/Forward.h>

namespace Cartography::Quick::Terrain3D
{

  class HeightMapMesh : public Qt3DRender::QGeometryRenderer
  {
    Q_OBJECT
    Q_PROPERTY(Cartography::Terrain::HeightMap heightMap READ heightMap WRITE setHeightMap NOTIFY
                 heightMapChanged)
    Q_PROPERTY(
      qreal baseAltitude READ baseAltitude WRITE setBaseAltitude NOTIFY baseAltitudeChanged)
  public:
    HeightMapMesh(Qt3DCore::QNode* parent = nullptr);
    ~HeightMapMesh();
  public:
    Cartography::Terrain::HeightMap heightMap() const;
    void setHeightMap(const Cartography::Terrain::HeightMap& _map);
    qreal baseAltitude() const;
    void setBaseAltitude(qreal _baseAltitude);
  signals:
    void heightMapChanged();
    void baseAltitudeChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Quick::Terrain3D

#endif
