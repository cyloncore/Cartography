#include "Raster.h"

namespace Cartography::Raster
{
  class AbstractImageWritter
  {
  public:
    virtual quint8* prepare(quint64 _width, quint64 _height, quint64 _channels,
                            Raster::ChannelType _type)
      = 0;
    virtual std::size_t channelPos(std::size_t _index) const = 0;
    virtual std::size_t pixelSize() const = 0;
  };
} // namespace Cartography::Raster
