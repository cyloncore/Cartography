#pragma once

#include "Raster.h"

namespace Cartography::Raster
{
  class AbstractRasterSource
  {
  public:
    virtual ~AbstractRasterSource();
    /**
     * @return the number of bands in the raster
     */
    virtual std::size_t bandsCount() const = 0;
    /**
     * @return the @ref CoordinateSystem used with this raster
     */
    virtual CoordinateSystem coordinateSystem() const = 0;
    /**
     * @return true if the \ref Raster is valid
     */
    virtual bool isValid() const = 0;
    /**
     * Convert this raster to an image, for display purposes
     */
    virtual bool toImage(AbstractImageWritter* _writter) const = 0;
    /**
     * Expect an array with 6 double.
     * Xp = transform[0] + P*transform[1] + L*transform[2];
     * Yp = transform[3] + P*transform[4] + L*transform[5];
     */
    virtual void copyTransformationTo(double* _transform) const = 0;

    /**
     * Transform \ref _point into the pixel coordinates associated with this raster.
     */
    virtual QPointF transformToRaster(const Point& _point) const = 0;
    /**
     * Transform \ref _point into the pixel coordinates associated with this raster.
     */
    virtual Point transformFromRaster(const QPointF& _point) const = 0;
    virtual Point topLeft() const = 0;
    virtual Point bottomRight() const = 0;
    virtual double horizontalSize() const = 0;
    virtual double verticalSize() const = 0;
    /**
     * @return the maximum value for band \p _band.
     */
    virtual double maximumValue(std::size_t _band) const = 0;
    /**
     * @return the minim value for band \p _band.
     */
    virtual double minimumValue(std::size_t _band) const = 0;
    /**
     * Sample a rectangle between @ref _topLeft and @ref _bottomRight from the @ref _band
     *
     * @p _interpolation is used if @p _mode is @ref SampleMode::All
     */
    virtual double sampleRect(std::size_t _band, const Point& _topLeft, const Point& _bottomRight,
                              Raster::SampleMode _mode, Raster::Interpolation _interpolation) const
      = 0;
  public:
    /**
     * Convert this raster to a WKBRaster, as defined by postgis.
     *
     * The WKBRaster format is specified in the RFC2-WellKnownBinaryFormat text file in the postgis
     * source tarball.
     */
    virtual QByteArray toWKBRaster() const = 0;
  };
} // namespace Cartography::Raster
