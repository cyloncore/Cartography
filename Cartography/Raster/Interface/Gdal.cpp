#include "Gdal.h"

#include <clog_qt>

#include <gdal_priv.h>
#include <ogr_api.h>
#include <ogr_spatialref.h>

#include <QBuffer>
#include <QDataStream>
#include <QRectF>

#include <Cartography/CoordinateSystem.h>
#include <Cartography/Point.h>

#include <Cartography/Interface/Gdal.h>
#include <Cartography/Raster/AbstractImageWritter.h>

using namespace Cartography::Raster::Interface;

template<typename Char>
struct std::formatter<Cartography::Point, Char> : clog_format::base_ostream_formatter<Char>
{
};

struct Gdal::Private
{
  ~Private()
  {
    delete dataset;
    delete[] transform;
    delete[] inv_transform;
  }
  GDALDataset* dataset = nullptr;
  mutable CoordinateSystem cs;
  mutable double* transform = nullptr;
  mutable double* inv_transform = nullptr;
  void updateTransform() const;
  void updateInvTransform() const;
};

void Gdal::Private::updateTransform() const
{
  if(not transform and dataset)
  {
    transform = new double[6];
    dataset->GetGeoTransform(transform);
  }
}

void Gdal::Private::updateInvTransform() const
{
  if(not inv_transform)
  {
    updateTransform();
    if(transform)
    {
      inv_transform = new double[6];
      inv_transform[0] = transform[0];
      inv_transform[3] = transform[3];

      double det = transform[1] * transform[5] - transform[2] * transform[4];

      inv_transform[1] = transform[5] / det;
      inv_transform[2] = -transform[2] / det;
      inv_transform[4] = -transform[4] / det;
      inv_transform[5] = transform[1] / det;
    }
  }
}

Gdal::Gdal() : d(new Private) {}

Gdal::~Gdal() { delete d; }

std::size_t Gdal::bandsCount() const { return d->dataset->GetRasterCount(); }

Cartography::Raster::AbstractRasterSource* Gdal::load(const QString& _filename)
{
  GDALAllRegister();

  Gdal* r = new Gdal;
  r->d->dataset = (GDALDataset*)GDALOpenShared(qPrintable(_filename), GA_ReadOnly);
  return r;
}

#define BANDTYPE_FLAGS_MASK 0xF0
#define BANDTYPE_PIXTYPE_MASK 0x0F
#define BANDTYPE_FLAG_OFFDB (1 << 7)
#define BANDTYPE_FLAG_HASNODATA (1 << 6)
#define BANDTYPE_FLAG_ISNODATA (1 << 5)
#define BANDTYPE_FLAG_RESERVED3 (1 << 4)

#define BANDTYPE_PIXTYPE(x) ((x) & BANDTYPE_PIXTYPE_MASK)
#define BANDTYPE_IS_OFFDB(x) ((x) & BANDTYPE_FLAG_OFFDB)
#define BANDTYPE_HAS_NODATA(x) ((x) & BANDTYPE_FLAG_HASNODATA)
#define BANDTYPE_IS_NODATA(x) ((x) & BANDTYPE_FLAG_ISNODATA)

namespace
{
  int rasterDataSize(GDALDataType _type)
  {
    switch(_type)
    {
    default:
    case GDT_Unknown:
      return 0;
    case GDT_Byte:
      return 1;
    case GDT_UInt16:
    case GDT_Int16:
      return 2;
    case GDT_UInt32:
    case GDT_Int32:
    case GDT_Float32:
      return 4;
    case GDT_Float64:
      return 8;
    case GDT_CInt16:
      return 4;
    case GDT_CInt32:
    case GDT_CFloat32:
      return 8;
    case GDT_CFloat64:
      return 16;
    }
  }
  int rasterPixtype(GDALDataType _type)
  {
    switch(_type)
    {
    default:
    case GDT_Unknown:
      return -1;
    case GDT_Byte:
      return 3;
    case GDT_UInt16:
      return 6;
    case GDT_Int16:
      return 5;
    case GDT_UInt32:
      return 8;
    case GDT_Int32:
      return 7;
    case GDT_Float32:
      return 10;
    case GDT_Float64:
      return 11;
    }
  }
  GDALDataType gdalDataType(quint8 _header)
  {
    switch(BANDTYPE_PIXTYPE(_header))
    {
    case 3:
      return GDT_Byte;
    case 6:
      return GDT_UInt16;
    case 5:
      return GDT_Int16;
    case 8:
      return GDT_UInt32;
    case 7:
      return GDT_Int32;
    case 10:
      return GDT_Float32;
    case 11:
      return GDT_Float64;
    default:
      return GDT_Unknown;
    }
  }
  template<typename _RT_, typename _ST_>
  _RT_ read(QDataStream& _stream)
  {
    _ST_ val;
    _stream >> (_ST_&)val;
    return _RT_(val);
  }
} // namespace

QByteArray Gdal::toWKBRaster() const
{
  QByteArray data;
  int size = 61; // Size of the header
  for(int i = 0; i < d->dataset->GetRasterCount(); ++i)
  {
    GDALRasterBand* band = d->dataset->GetRasterBand(i + 1);
    size
      += (band->GetXSize() * band->GetYSize() + 1) * rasterDataSize(band->GetRasterDataType()) + 1;
  }
  data.reserve(size);
  QBuffer buffer(&data);
  buffer.open(QIODevice::WriteOnly);
  QDataStream stream(&buffer);

  // Get transform
  double gt[6] = {0};
  if(d->dataset->GetGeoTransform(gt) != CE_None)
  {
    gt[0] = 0;
    gt[1] = 1;
    gt[2] = 0;
    gt[3] = 0;
    gt[4] = 0;
    gt[5] = -1;
  }

  // Write WKBRaster header
  stream.setByteOrder(QSysInfo::ByteOrder == QSysInfo::BigEndian ? QDataStream::BigEndian
                                                                 : QDataStream::LittleEndian);
  stream << (quint8)1;  // endianess TODO this is weird, shouldn't we write something different for
                        // each endianess?
  stream << (quint16)0; // version
  stream << (quint16)d->dataset->GetRasterCount(); // number of bands
  stream << (double)gt[1];                         // scaleX
  stream << (double)gt[5];                         // scaleY
  stream << (double)gt[0];                         // ipX
  stream << (double)gt[3];                         // ipY
  stream << (double)gt[2];                         // skewX
  stream << (double)gt[4];                         // skewY
  stream << (qint32)coordinateSystem().srid();     // srid
  stream << (quint16)d->dataset->GetRasterXSize(); // width
  stream << (quint16)d->dataset->GetRasterYSize(); // height
  clog_assert(buffer.size() == 61);
  for(int i = 0; i < d->dataset->GetRasterCount(); ++i)
  {
    GDALRasterBand* band = d->dataset->GetRasterBand(i + 1);
    int band_header = rasterPixtype(band->GetRasterDataType());
    if(band_header == -1)
    {
      clog_error("Unsupported GDAL dataype when converting to WKBRaster %1",
                 band->GetRasterDataType());
      return QByteArray();
    }
    int hasnodataval;
    double nodataval = band->GetNoDataValue(&hasnodataval);
    if(hasnodataval)
    {
      band_header |= BANDTYPE_FLAG_HASNODATA;
    }
    stream << (uint8_t)band_header;
    switch(band->GetRasterDataType())
    {
    default:
    case GDT_Unknown:
      clog_fatal("Unsupported datatype");
    case GDT_Byte:
      stream << (qint8)nodataval;
      break;
    case GDT_UInt16:
      stream << (quint16)nodataval;
      break;
    case GDT_Int16:
      stream << (qint16)nodataval;
      break;
    case GDT_UInt32:
      stream << (quint32)nodataval;
      break;
    case GDT_Int32:
      stream << (qint32)nodataval;
      break;
    case GDT_Float32:
      stream.setFloatingPointPrecision(QDataStream::SinglePrecision);
      stream << (float)nodataval;
      stream.setFloatingPointPrecision(QDataStream::DoublePrecision);
      break;
    case GDT_Float64:
      stream << (double)nodataval;
      break;
    }
    QByteArray img_buffer(band->GetXSize() * band->GetYSize()
                            * rasterDataSize(band->GetRasterDataType()),
                          Qt::Uninitialized);
    CPLErr err
      = band->RasterIO(GF_Read, 0, 0, band->GetXSize(), band->GetYSize(), img_buffer.data(),
                       band->GetXSize(), band->GetYSize(), band->GetRasterDataType(), 0, 0);
    if(err != CPLE_None)
      return QByteArray();
    stream.writeRawData(img_buffer.data(), img_buffer.size());
  }
  clog_assert(data.size() == size);
  return data;
}

Cartography::Raster::AbstractRasterSource* Gdal::fromWKBRaster(const QByteArray& _data)
{
  GDALAllRegister();

  if(_data.size() < 61)
  {
    clog_error("Too small raster: %1", _data.size());
    return nullptr;
  }
  QByteArray data(_data);
  QBuffer buffer(&data);
  buffer.open(QIODevice::ReadOnly);

  quint8 endianess = _data.at(0);

  QDataStream stream(&buffer);
  stream.setByteOrder(QSysInfo::ByteOrder == QSysInfo::BigEndian ? QDataStream::BigEndian
                                                                 : QDataStream::LittleEndian);
  if(endianess != 1)
  {
    clog_error("Unsupported endianness");
    return nullptr;
  }

  stream >> (quint8&)endianess;
  quint16 version;
  stream >> (quint16&)version;

  if(version != 0)
  {
    clog_error("Unsupported WKB version: %1", version);
    return nullptr;
  }

  quint16 bands;
  stream >> (quint16&)bands;
  double gt[6];
  stream >> (double&)gt[1]; // scaleX
  stream >> (double&)gt[5]; // scaleY
  stream >> (double&)gt[0]; // ipX
  stream >> (double&)gt[3]; // ipY
  stream >> (double&)gt[2]; // skewX
  stream >> (double&)gt[4]; // skewY
  qint32 srid;
  stream >> (qint32&)srid;
  quint16 width, height;
  stream >> (quint16&)width;
  stream >> (quint16&)height;

  Gdal* r = new Gdal;
  r->d->dataset
    = (GDALDataset*)GDALCreate(GDALGetDriverByName("MEM"), "", width, height, 0, GDT_Byte, NULL);
  r->d->dataset->SetGeoTransform(gt);
  r->d->dataset->SetProjection(qPrintable(CoordinateSystem(srid).toWkt()));

  for(int i = 0; i < bands; ++i)
  {
    quint8 band_header;
    stream >> (quint8&)band_header;
    GDALDataType dt = gdalDataType(band_header);
    if(dt == GDT_Unknown)
    {
      clog_error("Unsupported data type: %1", BANDTYPE_PIXTYPE(band_header));
      return nullptr;
    }
    double nodataval;
    switch(dt)
    {
    default:
    case GDT_Unknown:
      qFatal("Unsupported datatype");
    case GDT_Byte:
      nodataval = read<double, qint8>(stream);
      break;
    case GDT_UInt16:
      nodataval = read<double, quint16>(stream);
      break;
    case GDT_Int16:
      nodataval = read<double, qint16>(stream);
      break;
    case GDT_UInt32:
      nodataval = read<double, quint32>(stream);
      break;
    case GDT_Int32:
      nodataval = read<double, qint32>(stream);
      break;
    case GDT_Float32:
      stream.setFloatingPointPrecision(QDataStream::SinglePrecision);
      nodataval = read<double, float>(stream);
      stream.setFloatingPointPrecision(QDataStream::DoublePrecision);
      break;
    case GDT_Float64:
      nodataval = read<double, double>(stream);
      break;
    }
    r->d->dataset->AddBand(dt);
    GDALRasterBand* band = r->d->dataset->GetRasterBand(i + 1);
    band->SetNoDataValue(nodataval);

    QByteArray data;
    data.resize(band->GetXSize() * band->GetYSize() * rasterDataSize(band->GetRasterDataType()));
    stream.readRawData(data.begin(), data.size());
    CPLErr err
      = band->RasterIO(GF_Write, 0, 0, band->GetXSize(), band->GetYSize(), data.data(),
                       band->GetXSize(), band->GetYSize(), band->GetRasterDataType(), 0, 0);
    if(err != CPLE_None)
      return nullptr;
  }

  return r;
}

Cartography::CoordinateSystem Gdal::coordinateSystem() const
{
  if(not d->cs.isValid())
  {
    d->cs = CoordinateSystem(d->dataset->GetProjectionRef());
  }
  return d->cs;
}

bool Gdal::isValid() const { return d->dataset; }

bool Gdal::toImage(AbstractImageWritter* _writter) const
{
  quint64 width = d->dataset->GetRasterXSize();
  quint64 height = d->dataset->GetRasterYSize();
  GDALDataType dt = d->dataset->GetRasterBand(1)->GetRasterDataType();
  Raster::ChannelType it;
#define DORasterCASE(_GT_, _KT_)                                                                   \
  case _GT_:                                                                                       \
    it = Raster::ChannelType::_KT_;                                                                \
    break;
  switch(dt)
  {
    DORasterCASE(GDT_Byte, UnsignedInteger8);
    DORasterCASE(GDT_UInt16, UnsignedInteger16);
    DORasterCASE(GDT_Int16, Integer16);
    DORasterCASE(GDT_UInt32, UnsignedInteger32);
    DORasterCASE(GDT_Int32, Integer32);
    DORasterCASE(GDT_Float32, Float32);
    DORasterCASE(GDT_Float64, Float64);
  default:
    clog_error("Unsupported image type");
    return false;
  }
#undef DORasterCASE
  quint8* data_ptr = _writter->prepare(width, height, d->dataset->GetRasterCount(), it);
  for(int i = 0; i < d->dataset->GetRasterCount(); ++i)
  {
    CPLErr err = d->dataset->GetRasterBand(i + 1)->RasterIO(
      GF_Read, 0, 0, width, height, data_ptr + _writter->channelPos(i), width, height, dt,
      _writter->pixelSize(), width * _writter->pixelSize());
    if(err != CPLE_None)
      return false;
  }

  return true;
}

void Gdal::copyTransformationTo(double* _transform) const
{
  d->dataset->GetGeoTransform(_transform);
}

QPointF Gdal::transformToRaster(const Point& _point) const
{
  Point pt = _point.transform(coordinateSystem());
  d->updateInvTransform();

  double x = pt.x() - d->inv_transform[0];
  double y = pt.y() - d->inv_transform[3];

  return QPointF(x * d->inv_transform[1] + y * d->inv_transform[2],
                 x * d->inv_transform[4] + y * d->inv_transform[5]);
}

Cartography::Point Gdal::transformFromRaster(const QPointF& _point) const
{
  d->updateTransform();
  double x = _point.x() * d->transform[1] + _point.y() * d->transform[2];
  double y = _point.x() * d->transform[4] + _point.y() * d->transform[5];
  return Point(x + d->transform[0], y + d->transform[3], coordinateSystem());
}

double Gdal::maximumValue(std::size_t _band) const
{
  double pdfMax;
  d->dataset->GetRasterBand(_band + 1)->GetStatistics(1, 0, nullptr, &pdfMax, nullptr, nullptr);
  return pdfMax;
}

double Gdal::minimumValue(std::size_t _band) const
{
  double pdfMin;
  d->dataset->GetRasterBand(_band + 1)->GetStatistics(1, 0, &pdfMin, nullptr, nullptr, nullptr);
  return pdfMin;
}

double Gdal::sampleRect(std::size_t _band, const Point& _topLeft, const Point& _bottomRight,
                        Raster::SampleMode _mode, Raster::Interpolation _interpolation) const
{
  const QRectF rectf(transformToRaster(_topLeft), transformToRaster(_bottomRight));
  const QRect img_rect(0, 0, d->dataset->GetRasterXSize(), d->dataset->GetRasterYSize());
  const QRect rect = rectf.normalized().toAlignedRect().intersected(img_rect);

  switch(_mode)
  {
  case Raster::SampleMode::All:
  {
    double r;

    GDALRasterIOExtraArg ioExtraArg;
    INIT_RASTERIO_EXTRA_ARG(ioExtraArg);

#define DO_INTERPOLATION_CASE(_RI_, _GI_)                                                          \
  case Raster::Interpolation::_RI_:                                                                \
    ioExtraArg.eResampleAlg = _GI_;                                                                \
    break
    switch(_interpolation)
    {
    case Raster::Interpolation::None:
      clog_fatal("Need to specify interpolation!");
      DO_INTERPOLATION_CASE(NearestNeighbour, GRIORA_NearestNeighbour);
      DO_INTERPOLATION_CASE(Bilinear, GRIORA_Bilinear);
      DO_INTERPOLATION_CASE(Cubic, GRIORA_Cubic);
      DO_INTERPOLATION_CASE(CubicSpline, GRIORA_CubicSpline);
      DO_INTERPOLATION_CASE(Lanczos, GRIORA_Lanczos);
      DO_INTERPOLATION_CASE(Average, GRIORA_Average);
      DO_INTERPOLATION_CASE(Vote, GRIORA_Mode);
      DO_INTERPOLATION_CASE(Gauss, GRIORA_Gauss);
    }

    CPLErr err = d->dataset->GetRasterBand(_band + 1)->RasterIO(GF_Read, rect.x(), rect.y(),
                                                                rect.width(), rect.height(), &r, 1,
                                                                1, GDT_Float64, 0, 0, &ioExtraArg);
    if(err != CPLE_None)
    {
      clog_error("Failed to access raster data with error '{}' from band '{}' from rect ({}, {})",
                 int(err), _band, _topLeft, _bottomRight);
      return NAN;
    }
    return r;
  }
  case Raster::SampleMode::AveragePositive:
  {
    std::vector<double> buffer(rect.width() * rect.height());
    CPLErr err = d->dataset->GetRasterBand(_band + 1)->RasterIO(
      GF_Read, rect.x(), rect.y(), rect.width(), rect.height(), buffer.data(), rect.width(),
      rect.height(), GDT_Float64, 0, 0);
    if(err != CPLE_None)
    {
      clog_error("Failed to access raster data with error '{}' from band '{}' from rect ({}, {})",
                 err, _band, _topLeft, _bottomRight);
      return NAN;
    }
    std::size_t count = 0;
    double r = 0.0;
    for(const double& v : buffer)
    {
      if(v > 0.0)
      {
        r += v;
        ++count;
      }
    }
    return count > 0 ? r / count : NAN;
  }
  }
  clog_fatal("Should not happen");
}

Cartography::Point Gdal::topLeft() const
{
  d->updateTransform();
  return Point(d->transform[0], d->transform[3], coordinateSystem());
}

Cartography::Point Gdal::bottomRight() const
{
  d->updateTransform();
  return Point(d->transform[0] + d->dataset->GetRasterXSize() * d->transform[1],
               d->transform[3] + d->dataset->GetRasterYSize() * d->transform[5],
               coordinateSystem());
}

double Gdal::horizontalSize() const
{
  d->updateTransform();
  return std::abs(d->dataset->GetRasterXSize() * d->transform[1]);
}

double Gdal::verticalSize() const
{
  d->updateTransform();
  return std::abs(d->dataset->GetRasterYSize() * d->transform[5]);
}
