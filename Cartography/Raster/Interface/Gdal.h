#include <Cartography/Raster/AbstractRasterSource.h>

namespace Cartography::Raster::Interface
{
  class Gdal : public AbstractRasterSource
  {
  public:
    Gdal();
    virtual ~Gdal();
    std::size_t bandsCount() const override;
    CoordinateSystem coordinateSystem() const override;
    bool isValid() const override;
    bool toImage(AbstractImageWritter* _writter) const override;
    void copyTransformationTo(double* _transform) const override;
    QPointF transformToRaster(const Point& _point) const override;
    Point transformFromRaster(const QPointF& _point) const override;
    double maximumValue(std::size_t _band) const override;
    double minimumValue(std::size_t _band) const override;
    double horizontalSize() const override;
    double verticalSize() const override;
    double sampleRect(std::size_t _band, const Point& _topLeft, const Point& _bottomRight,
                      Raster::SampleMode _mode,
                      Raster::Interpolation _interpolation) const override;
    QByteArray toWKBRaster() const override;
    Point topLeft() const override;
    Point bottomRight() const override;
    static AbstractRasterSource* fromWKBRaster(const QByteArray& _array);
    static AbstractRasterSource* load(const QString& _filename);
  private:
    struct Private;
    Private* const d;
  };
} // namespace Cartography::Raster::Interface
