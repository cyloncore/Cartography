#include "Raster.h"

#include <QBuffer>
#include <QDataStream>
#include <QPointF>
#include <QString>

#include <Cartography/CoordinateSystem.h>
#include <Cartography/Point.h>

#include "AbstractImageWritter.h"
#include "AbstractRasterSource.h"

#include <Cartography/config_p.h>

#ifdef CARTOGRAPHY_HAVE_GDAL
#include "Interface/Gdal.h"
#endif

using namespace Cartography::Raster;

struct Raster::Private : public QSharedData
{
  AbstractRasterSource* source;
};

Raster::Raster(AbstractRasterSource* _source) : d(new Private) { d->source = _source; }
Raster::Raster(const Raster& _rhs) : d(_rhs.d) {}

Raster& Raster::operator=(const Raster& _rhs)
{
  d = _rhs.d;
  return *this;
}

Raster::~Raster() {}
std::size_t Raster::bandsCount() const { return d->source->bandsCount(); }
Cartography::CoordinateSystem Raster::coordinateSystem() const
{
  return d->source->coordinateSystem();
}
bool Raster::operator==(const Raster& _raster) const { return d == _raster.d; }
bool Raster::isValid() const { return d->source and d->source->isValid(); }

bool Raster::toImage(AbstractImageWritter* _writter) const { return d->source->toImage(_writter); }

void Raster::copyTransformationTo(double* _transform) const
{
  d->source->copyTransformationTo(_transform);
}
QPointF Raster::transformToRaster(const Point& _point) const
{
  return d->source->transformToRaster(_point);
}

Cartography::Point Raster::transformFromRaster(const QPointF& _point) const
{
  return d->source->transformFromRaster(_point);
}

Cartography::Point Raster::topLeft() const { return d->source->topLeft(); }

Cartography::Point Raster::bottomRight() const { return d->source->bottomRight(); }

Cartography::Point Raster::center() const
{
  Point tL = topLeft();
  Point bR = bottomRight();
  return {0.5 * (tL.x() + bR.x()), 0.5 * (tL.y() + bR.y()), tL.coordinateSystem()};
}

double Raster::horizontalSize() const { return d->source->horizontalSize(); }

double Raster::verticalSize() const { return d->source->verticalSize(); }

double Raster::maximumValue(std::size_t _band) const { return d->source->maximumValue(_band); }

double Raster::minimumValue(std::size_t _band) const { return d->source->minimumValue(_band); }

double Raster::sampleRect(std::size_t _band, const Point& _topLeft, const Point& _bottomRight,
                          SampleMode _mode, Interpolation _interpolation) const
{
  return d->source->sampleRect(_band, _topLeft, _bottomRight, _mode, _interpolation);
}

QByteArray Raster::toWKBRaster() const { return d->source->toWKBRaster(); }

Raster Raster::fromWKBRaster(const QByteArray& _array)
{
  return Raster(Interface::Gdal::fromWKBRaster(_array));
}

Raster Raster::load(const QString& _filename) { return Raster(Interface::Gdal::load(_filename)); }
