#pragma once

#include <QSharedDataPointer>

class QPointF;
class QString;

#include <Cartography/Forward.h>

namespace Cartography::Raster
{
  /**
   * Wrap a raster (for example from GDAL) and provides convenient functions to access the data.
   *
   * The actual data is shared between multiple instance of this class and only deleted once the
   * last instance goes out of scope.
   */
  class Raster
  {
  public:
    enum class ChannelType
    {
      UnsignedInteger8,
      Integer8,
      UnsignedInteger16,
      Integer16,
      UnsignedInteger32,
      Integer32,
      Float32,
      Float64
    };
  public:
    /**
     * Construct an invalid raster
     */
    Raster(AbstractRasterSource* _source = nullptr);
    Raster(const Raster& _rhs);
    Raster& operator=(const Raster& _rhs);
    ~Raster();
    /**
     * @return the number of bands in the raster
     */
    std::size_t bandsCount() const;
    /**
     * @return the @ref CoordinateSystem used with this raster
     */
    CoordinateSystem coordinateSystem() const;
    bool operator==(const Raster& _raster) const;
    /**
     * @return true if the \ref Raster is valid
     */
    bool isValid() const;
    /**
     * Convert this raster to an image, for display purposes
     */
    bool toImage(AbstractImageWritter* _writter) const;
    /**
     * Expect an array with 6 double.
     * Xp = transform[0] + P*transform[1] + L*transform[2];
     * Yp = transform[3] + P*transform[4] + L*transform[5];
     */
    void copyTransformationTo(double* _transform) const;

    /**
     * Transform \ref _point into the \ref coordinateSystem associated with this raster.
     */
    QPointF transformToRaster(const Point& _point) const;
    Point transformFromRaster(const QPointF& _point) const;
    Point topLeft() const;
    Point bottomRight() const;
    Point center() const;

    /**
     * The horizontal size in (m) of the image.
     */
    double horizontalSize() const;
    /**
     * The vertical size in (m) of the image.
     */
    double verticalSize() const;
    enum class Interpolation
    {
      None,
      Bilinear,
      Cubic,
      CubicSpline,
      Lanczos,
      Average,
      NearestNeighbour,
      Vote, ///< select the most frequent value within sample
      Gauss
    };
    enum class SampleMode
    {
      All,            ///< Use all data
      AveragePositive ///< Average positive data
    };
    /**
     * @return the maximum value for band \p _band.
     */
    double maximumValue(std::size_t _band) const;
    /**
     * @return the minim value for band \p _band.
     */
    double minimumValue(std::size_t _band) const;
    /**
     * Sample a rectangle between @ref _topLeft and @ref _bottomRight from the @ref _band
     *
     * @p _interpolation is used if @p _mode is @ref SampleMode::All
     */
    double sampleRect(std::size_t _band, const Point& _topLeft, const Point& _bottomRight,
                      SampleMode _mode, Interpolation _interpolation = Interpolation::None) const;
  public:
    /**
     * Convert this raster to a WKBRaster, as defined by postgis.
     *
     * The WKBRaster format is specified in the RFC2-WellKnownBinaryFormat text file in the postgis
     * source tarball.
     */
    QByteArray toWKBRaster() const;
    static Raster fromWKBRaster(const QByteArray& _array);
  public:
    /**
     * @return a \ref Raster loaded from a local file @p _filename (an invalid \ref Raster is
     * returned in case of error while reading the file)
     */
    static Raster load(const QString& _filename);
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace Cartography::Raster

#include <QMetaType>

Q_DECLARE_METATYPE(Cartography::Raster::Raster)
