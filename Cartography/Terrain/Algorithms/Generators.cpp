#include "Generators.h"

#include <QPointF>
#include <QVariant>

#include "../HeightMap.h"

using namespace Cartography::Terrain::Algorithms::Generators;

struct TectonicUpliftFluvialErosion::Private
{
  Cartography::Terrain::HeightMap uplift;
  struct RiverNode
  {
    QPointF pt;
  };
  struct RiverEdge
  {
    RiverNode* src;
    RiverNode* dest;
  };
  QList<RiverNode*> nodes;
  QList<RiverEdge*> edges;
};

TectonicUpliftFluvialErosion::TectonicUpliftFluvialErosion() : d(new Private) {}

TectonicUpliftFluvialErosion::~TectonicUpliftFluvialErosion() { delete d; }

void TectonicUpliftFluvialErosion::setUplift(const Cartography::Terrain::HeightMap& _data)
{
  d->uplift = _data;
}

void TectonicUpliftFluvialErosion::generateRandomRiverNodes() {}

Cartography::Terrain::HeightMap TectonicUpliftFluvialErosion::generate() const
{
  return Cartography::Terrain::HeightMap();
}

QVariantList TectonicUpliftFluvialErosion::riverVertices() const
{
  QVariantList l;
  for(Private::RiverNode* node : d->nodes)
  {
    l.append(QVariant::fromValue(node->pt));
  }
  return l;
}

QVariantList TectonicUpliftFluvialErosion::riverEdges() const
{
  QVariantList l;
  for(Private::RiverEdge* edge : d->edges)
  {
    QVariantMap em;
    em["source"] = d->nodes.indexOf(edge->src);
    em["destination"] = d->nodes.indexOf(edge->dest);
    l.append(em);
  }
  return l;
}

#include "moc_Generators.cpp"
