#include "Hillshade.h"

#include <cmath>

#include <QDebug>
#include <QImage>

#include "../HeightMap.h"

namespace Cartography::Terrain
{
  class HeightMap;
  namespace Algorithms
  {
    namespace details
    {
      inline qreal hillshade_value_at(const HeightMap& _map, int _x, int _y)
      {
        return _map.altitude(qBound(0, _x, _map.columns() - 1), qBound(0, _y, _map.rows() - 1));
      }
    } // namespace details
    // From
    // http://edndoc.esri.com/arcobjects/9.2/net/shared/geoprocessing/spatial_analyst_tools/how_hillshade_works.htm
    QImage hillshade(const HeightMap& _map, qreal _altitude, qreal _azimuth, qreal _intensity)
    {
      qreal zenith = M_PI_2 - _altitude;
      qreal azimuth_math = 2 * M_PI - _azimuth + M_PI_2;
      if(azimuth_math > 2 * M_PI)
        azimuth_math -= 2 * M_PI;
      QImage image(_map.columns(), _map.rows(), QImage::Format_Grayscale8);
      for(int y = 0; y < image.height(); ++y)
      {
        for(int x = 0; x < image.width(); ++x)
        {
          qreal a = details::hillshade_value_at(_map, x - 1, y - 1);
          qreal b = details::hillshade_value_at(_map, x, y - 1);
          qreal c = details::hillshade_value_at(_map, x + 1, y - 1);
          qreal d = details::hillshade_value_at(_map, x - 1, y);
          //           qreal e = details::hillshade_value_at(_map, x    , y);
          qreal f = details::hillshade_value_at(_map, x + 1, y);
          qreal g = details::hillshade_value_at(_map, x - 1, y + 1);
          qreal h = details::hillshade_value_at(_map, x, y + 1);
          qreal i = details::hillshade_value_at(_map, x + 1, y + 1);

          qreal dzdx = ((c + 2 * f + i) - (a + 2 * d + g)) / (8 * _map.horizontalResolution());
          qreal dzdy = ((g + 2 * h + i) - (a + 2 * b + c)) / (8 * _map.verticalResolution());

          qreal slope = std::atan(/* z_factor* */ std::sqrt(dzdx * dzdx + dzdy * dzdy));
          qreal aspect = std::atan2(dzdy, -dzdx);
          if(aspect < 0)
            aspect = 2 * M_PI + aspect;

          qreal hillshade
            = _intensity
                * ((std::cos(zenith) * std::cos(slope))
                   + (std::sin(zenith) * std::sin(slope) * std::cos(azimuth_math - aspect)))
              + (255.0 - _intensity);

          if(hillshade < 0)
          {
            image.scanLine(y)[x] = 0;
          }
          else
          {
            image.scanLine(y)[x] = hillshade;
          }
        }
      }
      return image;
    }
    QImage hillshade(const QSize& _imageSize, const HeightMap& _map, qreal _altitude,
                     qreal _azimuth, qreal _intensity, const QRectF& _viewArea)
    {
      QRectF viewArea = _viewArea.isValid() ? _viewArea : _map.boundingBox();

      qreal zenith = M_PI_2 - _altitude;
      qreal azimuth_math = 2 * M_PI - _azimuth + M_PI_2;
      if(azimuth_math > 2 * M_PI)
        azimuth_math -= 2 * M_PI;
      QImage image(_imageSize.width(), _imageSize.height(), QImage::Format_Grayscale8);
      qreal hr = _map.horizontalResolution();
      qreal vr = _map.verticalResolution();
      qreal dx = _viewArea.width()
                 / _imageSize.width() /*/ _viewArea.width() * _map.boundingBox().width()*/;
      qreal dy = _viewArea.height()
                 / _imageSize.height() /*/ _viewArea.height() * _map.boundingBox().height()*/;

      for(int y_i = 0; y_i < image.height(); ++y_i)
      {
        qreal y = y_i * dy + viewArea.top();
        for(int x_i = 0; x_i < image.width(); ++x_i)
        {
          qreal x = x_i * dx + viewArea.left();

          qreal a = _map.altitude(x - hr, y - vr);
          qreal b = _map.altitude(x, y - vr);
          qreal c = _map.altitude(x + hr, y - vr);
          qreal d = _map.altitude(x - hr, y);
          // qreal e = _map.altitude(x    , y);
          qreal f = _map.altitude(x + hr, y);
          qreal g = _map.altitude(x - hr, y + vr);
          qreal h = _map.altitude(x, y + vr);
          qreal i = _map.altitude(x + hr, y + vr);

          qreal dzdx = ((c + 2 * f + i) - (a + 2 * d + g)) / (8 * _map.horizontalResolution());
          qreal dzdy = ((g + 2 * h + i) - (a + 2 * b + c)) / (8 * _map.verticalResolution());

          qreal slope = std::atan(/* z_factor* */ std::sqrt(dzdx * dzdx + dzdy * dzdy));
          qreal aspect = std::atan2(dzdy, -dzdx);
          if(aspect < 0)
            aspect = 2 * M_PI + aspect;

          qreal hillshade
            = _intensity
                * ((std::cos(zenith) * std::cos(slope))
                   + (std::sin(zenith) * std::sin(slope) * std::cos(azimuth_math - aspect)))
              + (255.0 - _intensity);

          if(hillshade < 0)
          {
            image.scanLine(y_i)[x_i] = 0;
          }
          else
          {
            image.scanLine(y_i)[x_i] = hillshade;
          }
        }
      }
      return image;
    }
  } // namespace Algorithms
} // namespace Cartography::Terrain
