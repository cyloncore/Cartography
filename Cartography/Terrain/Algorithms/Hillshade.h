#include <QRectF>

class QImage;

namespace Cartography::Terrain
{
  class HeightMap;
  namespace Algorithms
  {
    QImage hillshade(const HeightMap& _map, qreal _altitude, qreal _azimuth,
                     qreal _intensity = 80.0);
    QImage hillshade(const QSize& _imageSize, const HeightMap& _map, qreal _altitude,
                     qreal _azimuth, qreal _intensity = 80.0, const QRectF& _viewArea = QRectF());
  } // namespace Algorithms
} // namespace Cartography::Terrain
