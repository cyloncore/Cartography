#include "Terraforming.h"

#include <cmath>

#include <QRectF>

#include "../HeightMap.h"
#include "rand_p.h"
#include <QDebug>
namespace Cartography::Terrain
{
  namespace Algorithms
  {
    namespace Terraforming
    {
      void fill(HeightMap* map, float _altitude)
      {
        float* d = map->data();
        for(int i = 0; i < map->columns() * map->rows(); ++i)
        {
          d[i] = _altitude;
        }
      }
      template<typename _Tp>
      struct max
      {
        constexpr _Tp operator()(const _Tp& __x, const _Tp& __y) const
        {
          return std::max(__x, __y);
        }
      };
      template<typename _Tp>
      struct min
      {
        constexpr _Tp operator()(const _Tp& __x, const _Tp& __y) const
        {
          return std::min(__x, __y);
        }
      };
      template<typename _T_>
      void raise(HeightMap* map, const QPointF& _center, float _altitude, float _radius,
                 float _degree)
      {
        QRectF bb = map->boundingBox();
        QRectF rb(_center.x() - _radius, _center.y() - _radius, 2 * _radius, 2 * _radius);
        QRectF ir = (bb & rb).translated(-map->origin());
        ir = QRectF(ir.left() / map->horizontalResolution(), ir.top() / map->verticalResolution(),
                    ir.width() / map->horizontalResolution(),
                    ir.height() / map->verticalResolution());
        QRect iir = ir.toAlignedRect();
        QPointF offset = map->origin() - _center;
        for(int j = iir.top(); j <= iir.bottom(); ++j)
        {
          for(int i = iir.left(); i <= iir.right(); ++i)
          {
            QPointF pt_to_center
              = QPointF(i * map->horizontalResolution(), j * map->verticalResolution()) + offset;
            qreal dist = std::sqrt(QPointF::dotProduct(pt_to_center, pt_to_center));
            if(dist < _radius)
            {
              float offset
                = (1.0 - std::pow(std::sin(dist / _radius * M_PI * 0.5), _degree)) * _altitude;
              map->setAltitude(i, j, _T_()(map->altitude(i, j), offset));
            }
          }
        }
      }
      void raise(HeightMap* map, const QPointF& _center, float _altitude, float _radius,
                 float _degree, Op _op)
      {
        switch(_op)
        {
        case Op::Add:
          raise<std::plus<float>>(map, _center, _altitude, _radius, _degree);
          break;
        case Op::Sub:
          raise<std::minus<float>>(map, _center, _altitude, _radius, _degree);
          break;
        case Op::Max:
          raise<max<float>>(map, _center, _altitude, _radius, _degree);
          break;
        case Op::Min:
          raise<min<float>>(map, _center, _altitude, _radius, _degree);
          break;
        }
      }
      void raise(HeightMap* map, const QPointF& _center, float _altitude, float _radius,
                 float _degree)
      {
        raise(map, _center, _altitude, _radius, _degree, Op::Add);
      }

      namespace details_noise
      {
        float fade(float t) { return ((2.0 * std::abs(t) - 3.0) * (t) * (t) + 1.0); }
      } // namespace details_noise

      void noise(HeightMap* map, qreal _min, qreal _max, qint32 _seed, float _scale)
      {
        for(int j = 0; j < map->rows(); ++j)
        {
          for(int i = 0; i < map->columns(); ++i)
          {
            const QPointF grid_coord_f
              = (QPointF(i * map->horizontalResolution(), j * map->verticalResolution())
                 + map->origin())
                * _scale;
            const QPoint grid_coord(std::floor(grid_coord_f.x()), std::floor(grid_coord_f.y()));
            const QPointF weight = grid_coord_f - grid_coord;
            using namespace details_noise;

            qreal r = fade(weight.x())
                        * (fade(weight.y())
                             * Cartography::Quick::Terrain::Random::floatRandomAt(
                               grid_coord.x(), grid_coord.y(), _seed)
                           + fade(1 - weight.y())
                               * Cartography::Quick::Terrain::Random::floatRandomAt(
                                 grid_coord.x(), grid_coord.y() + 1, _seed))
                      + fade(1 - weight.x())
                          * (fade(weight.y())
                               * Cartography::Quick::Terrain::Random::floatRandomAt(
                                 grid_coord.x() + 1, grid_coord.y(), _seed)
                             + fade(1 - weight.y())
                                 * Cartography::Quick::Terrain::Random::floatRandomAt(
                                   grid_coord.x() + 1, grid_coord.y() + 1, _seed));
            map->setAltitude(i, j, map->altitude(i, j) + 0.25 * r * (_max - _min) + _min);
          }
        }
      }
    } // namespace Terraforming
  } // namespace Algorithms
} // namespace Cartography::Terrain
