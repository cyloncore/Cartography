#include <QtGlobal>

class QPointF;

namespace Cartography::Terrain
{
  class HeightMap;
  namespace Algorithms
  {
    namespace Terraforming
    {
      enum class Op
      {
        Add,
        Sub,
        Max,
        Min
      };
      void fill(HeightMap* map, float _altitude);
      void raise(HeightMap* map, const QPointF& _center, float _altitude, float _radius,
                 float _degree);
      void raise(HeightMap* map, const QPointF& _center, float _altitude, float _radius,
                 float _degree, Op _op);
      void noise(HeightMap* map, qreal _min, qreal _max, qint32 _seed, float _scale = 0.2);
    } // namespace Terraforming
  } // namespace Algorithms
} // namespace Cartography::Terrain
