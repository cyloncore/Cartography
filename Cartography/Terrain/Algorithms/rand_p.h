#include <QtGlobal>

namespace Cartography::Quick::Terrain
{
  namespace Random
  {
    float floatRandomAt(qint32 x, qint32 y, qint32 seed);
    qint32 intRandomAt(qint32 x, qint32 y, qint32 seed);
  } // namespace Random
} // namespace Cartography::Quick::Terrain
