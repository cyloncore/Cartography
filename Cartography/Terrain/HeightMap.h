#include <QSharedDataPointer>

class QImage;
class QPointF;
class QRectF;
class QVector3D;

namespace Cartography::Terrain
{
  /**
   * @ingroup Cartography_Terrain
   *
   * Represents a height map.
   */
  class HeightMap
  {
  public:
    static constexpr quint16 MAGIC_START = 0xFE43;
    static constexpr quint16 MAGIC_HEADER_END = 0x780A;
    static constexpr quint16 MAGIC_END = 0x12BC;
  public:
    HeightMap();
    HeightMap(qreal _width, qreal _height, qreal _resolution);
    HeightMap(qreal _origin_x, qreal _origin_y, qreal _width, qreal _height, qreal _resolution);
    HeightMap(const HeightMap& _rhs);
    HeightMap& operator=(const HeightMap& _rhs);
    ~HeightMap();
    HeightMap clone() const;
    /**
     * The number of columns
     */
    int columns() const;
    /**
     * The number of rows
     */
    int rows() const;
    qreal horizontalResolution() const;
    qreal verticalResolution() const;
    QPointF origin() const;
    QRectF boundingBox() const;
    void setAltitude(int _x, int _y, float _altitude);
    QPair<float, float> minmax() const;
    /**
     * Altitude of a given pixel.
     */
    float altitude(int _x, int _y) const;
    /**
     * Altitude of a given point, interpolated from the pixel grid.
     */
    float altitude(qreal _x, qreal _y) const;
    /**
     * Compute the normal of a given pixel.
     */
    QVector3D normal(int _x, int _y) const;
    float* data();
    QImage toImage() const;
    QByteArray toByteArray() const;
    static HeightMap fromByteArray(const QByteArray& _data);
    bool operator==(const HeightMap& _rhs) const;
    bool operator!=(const HeightMap& _rhs) const { return not(*this == _rhs); }
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace Cartography::Terrain

#include <QMetaType>

Q_DECLARE_METATYPE(Cartography::Terrain::HeightMap)
