#include "TestGeometryObject.h"

#include <clog_qt>
#include <cres_qtest>

#include "../CoordinateSystem.h"
#include "../GeometryObject.h"

#include <euclid/ops>

#include <QJsonDocument>

#define COMPARE_S(__KEY__, __VALUE__)                                                              \
  QCOMPARE(geobject[__KEY__].toString(), QStringLiteral(__VALUE__))

#define COMPARE_PT(__INDEX__, __X__, __Y__)                                                        \
  QCOMPARE(coordinates[__INDEX__].toList()[0].toInt(), __X__);                                     \
  QCOMPARE(coordinates[__INDEX__].toList()[1].toInt(), __Y__)

void TestGeometryObject::testJson()
{
  using namespace Cartography;

  QString wkt_00 = "POLYGON ((30.00 10.00, 40.00 40.00, 20.00 40.00, 10.00 20.00, 30.00 10.00))";
  QString wkt = "POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))";

  GeometryObject obj = CRES_QVERIFY(GeometryObject::fromWKT(wkt));
  QVERIFY(obj.isValid());
  QString json = obj.toJson();

  QJsonDocument document = QJsonDocument::fromJson(json.toLatin1());
  QVariantMap geobject = document.toVariant().toMap();
  COMPARE_S("type", "Polygon");
  QVariantList coordinates = geobject["coordinates"].toList().first().toList();
  QCOMPARE(coordinates.size(), 5);

  COMPARE_PT(0, 30, 10);
  COMPARE_PT(1, 40, 40);
  COMPARE_PT(2, 20, 40);
  COMPARE_PT(3, 10, 20);
  COMPARE_PT(4, 30, 10);

  GeometryObject obj2 = CRES_QVERIFY(GeometryObject::fromJson(json));
  QString obj2_toWKT = obj2.toWKT(2);
  if(obj2_toWKT != wkt and obj2_toWKT != wkt_00)
  {
    clog_debug("obj2.toWKT == {}", obj2_toWKT);
  }
  QVERIFY(obj2_toWKT == wkt or obj2_toWKT == wkt_00);
}

void TestGeometryObject::testWkt()
{
  using namespace Cartography;

  QString wkt = "POINT (30 10)";
  GeometryObject obj = CRES_QVERIFY(GeometryObject::fromWKT(wkt));
  QVERIFY(obj.isValid());

  EuclidSystem::point pt = obj.geometry().cast<EuclidSystem::point>();
  QCOMPARE(pt.x(), 30);
  QCOMPARE(pt.y(), 10);
  QVERIFY(std::isnan(pt.z()));
}

void TestGeometryObject::testFilterFixIt()
{
  using namespace Cartography;
  {
    QString goV_wkt = "POLYGON ((539873.890686628990807 6481119.320481460541487, "
                      "540067.526351127657108 6480836.615039764903486, 539971.490351781132631 "
                      "6480770.836346444673836, 540019.171643967623822 6480854.848114101216197, "
                      "539816.343298678169958 6481093.079311619512737, 539818.119882164290175 "
                      "6481090.997419313527644, 539873.890686628990807 6481119.320481460541487))";
    QString go_wkt = "POLYGON ((539893.517075355281122 6481569.943973865360022, "
                     "540717.025330100324936 6481436.670852212235332, 540574.577673597377725 "
                     "6480556.472207920625806, 539751.069418852333911 6480689.745329573750496, "
                     "539893.517075355281122 6481569.943973865360022))";

    GeometryObject goV = CRES_QVERIFY(GeometryObject::fromWKT(goV_wkt));
    GeometryObject go = CRES_QVERIFY(GeometryObject::fromWKT(go_wkt));

    GeometryObject god = goV.difference(go)
                           .filter(
                             [](const Cartography::EuclidSystem::geometry& _geom)
                             {
                               return _geom.is<Cartography::EuclidSystem::polygon>()
                                      or _geom.is<Cartography::EuclidSystem::multi_polygon>();
                             })
                           .fixIt()
                           .filter([](const Cartography::EuclidSystem::geometry& _geom)
                                   { return euclid::ops::area(_geom) > 2016.3892424093763; });
  }
  {
    QString geo_wkt
      = "POLYGON ((15.6841306360833794 58.4294138316326865, 15.6828425558032940 "
        "58.4293886751398404, 15.6823083039117730 58.4286475873724669, 15.6822949864810024 "
        "58.4286579448556864, 15.6832250423640236 58.4298991027730708, 15.6825557183203674 "
        "58.4300370880893780, 15.6837487414141972 58.4304922816094319, 15.6841306360833794 "
        "58.4294138316326865))";
    GeometryObject geo = CRES_QVERIFY(GeometryObject::fromWKT(geo_wkt));
    QVERIFY(not geo.isValid());
    geo = geo.fixIt();
    QVERIFY(geo.isValid());
  }
}

QTEST_MAIN(TestGeometryObject)
