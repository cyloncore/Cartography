#include <QtTest/QtTest>

class TestGeometryObject : public QObject
{
  Q_OBJECT
private slots:
  void testJson();
  void testWkt();
  void testFilterFixIt();
};
