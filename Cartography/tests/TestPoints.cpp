#include "TestPoints.h"

#include <Cartography/GeoPoint.h>
#include <Cartography/Point.h>

void TestPoints::testGeoPoint()
{
  Cartography::GeoPoint gp1(Cartography::Longitude(15.57065248), Cartography::Latitude(58.3948809),
                            Cartography::Altitude(74));
  QCOMPARE(gp1.longitude(), 15.57065248);
  QCOMPARE(gp1.latitude(), 58.3948809);
  QCOMPARE(gp1.altitude(), 74.0);
  QVERIFY(gp1.longitude() <= 15.57065248);
  QVERIFY(gp1.longitude() >= 15.57065248);
  QVERIFY(gp1.longitude() != 15.0);
  QVERIFY(gp1.longitude() > 15.0);
  QVERIFY(gp1.longitude() < 16.0);
  QCOMPARE(gp1.longitude() + 1.0, 16.57065248);
  QCOMPARE(gp1.longitude() - 1.0, 14.57065248);
  QCOMPARE(gp1.altitude() / 2.0, 37.0);
  QCOMPARE(gp1.altitude() * 2.0, 148.0);
}

void TestPoints::testTransform()
{
  Cartography::Point pt(10.0, 15.0, Cartography::CoordinateSystem::wgs84);
  Cartography::GeoPoint gp = Cartography::GeoPoint::from(pt);

  QCOMPARE(pt.x(), 10.0);
  QCOMPARE(pt.y(), 15.0);
  QVERIFY(std::isnan(pt.z()));

  QCOMPARE(gp.x(), 10.0);
  QCOMPARE(gp.y(), 15.0);
  QVERIFY(std::isnan(gp.z()));
}

QTEST_MAIN(TestPoints)
