#include <QtTest/QtTest>

class TestPoints : public QObject
{
  Q_OBJECT
private slots:
  void testGeoPoint();
  void testTransform();
};
