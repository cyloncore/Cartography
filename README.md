<div align="center">

[![stable pipeline](https://gitlab.com/cyloncore/cartography/badges/stable/pipeline.svg?key_text=stable)](https://gitlab.com/cyloncore/cartography/-/pipelines?ref=stable)
[![dev/1 pipeline](https://gitlab.com/cyloncore/cartography/badges/dev/1/pipeline.svg?key_text=dev/1)](https://gitlab.com/cyloncore/cartography/-/pipelines?ref=dev/1)
[![dev/2 pipeline](https://gitlab.com/cyloncore/cartography/badges/dev/2/pipeline.svg?key_text=dev/2)](https://gitlab.com/cyloncore/cartography/-/pipelines?ref=dev/2)
</div>

![Cartography logo](data/images/cartography.png) Cartography
============================================================

This library includes a set of components for building Geographic Information System (GIS) applications.
It supports C++17, QML and Python3.

Quick links:
* We recommend you start here: [Application examples using docker containers](docs/examples/examples.md).

* [Full documentation](docs/main.md).


Cartography source code is license under the [MIT License](LICENSE) and is free to everyone to use for any purpose. It is however necesserary to abide by the license of the dependencies of Cartography, such as Qt.


Overview
--------

Cartography contains the following components:

* Algorithms: geographic related algorithms, such as area polygon decomposition.
* Geometry: base geometric classes
* Raster: base raster classes
* Terrain: base terrain classes
* Quick/Mapnik: QML Component that uses the Mapnik library to draw a map.
* Quick/Cartographer: QML Components for editing geographic data.

Selected Features
-----------------

<table border="0">
  <tr>
    <td><img src="data/images/weighted_graph_decomposition.png"><br>
    Example of polygon decomposition<br> using AreaDecompose algorithm.</td>
    <td><img src="docs/examples/district_decomposition.png" width=250><br>
    Redistricting using <br>AreaDecompose algorithm.</td>
    <td><img src="data/images/world_viewer.png"><br>
    Viewing the world using Mapnik.</td>
 </tr>
</table>

Quick Start - example applications using docker containers!
-----------

For quick start and demonstration of the available functionalities a number of example applications have been setup using docker containers. We encourge all visitors to try them out! [Instructions are available here](docs/examples/examples.md).


Installation instructions
-------------------------

Installation instructions can be found in the [documentation](docs/installation.md)

### Dependencies

Hard dependencies:
* *euclid* which is available with cc-pkg or at https://gitlab.com/cyloncore/euclid
* *geos* which is usually available as a system package
* *proj*, *Qt6* which are usually available as a system package

Optional dependencies:
* *python-sip* for the python bindings
* *GDAL* for integration with GDAL primitives and its wide support of GIS file formats
* *Mapnik* for the QML components that render maps using Mapnik
* *Cyqlops::Toolbox* for the Cartographer QML Components
* *clog* for better logging of messages
* *libcmaes* for some variants of the algorithms (e.g. CMA-ES version of the `AreaDecompose` algorithm)

References
----------

The [WeightedGraphPolygonDecomposition](https://gitlab.com/cyloncore/Cartography/-/blob/stable/Cartography/Algorithms/WeightedGraphPolygonDecomposition.cpp?ref_type=heads) class implements the `AreaDecompose` algorithm presented in [[1]][ref1], which is currently submitted to the Soft Computing journal. The `AreaDecompose` uses the `simplifyBorders` algorithm also preseted in [[1]][ref1] and available in [euclid library](https://gitlab.com/cyloncore/euclid).

The [lawnMowerPattern](https://gitlab.com/cyloncore/Cartography/-/blob/stable/Cartography/Algorithms/LawnMowerPattern.h?ref_type=heads) method implements the algorithm for generating flight patterns for exploration missions using Unmanned Aerial Vehicles (UAVs) presented in [[2]][ref2].

When citing, use the following references:

[[1]][ref1] M. Wzorek, C. Berger, P. Doherty, Polygon Area Decomposition using a Compactness Metric. arXiv preprint arXiv:2110.04043 (2021).

[[2]][ref2] C. Berger, M. Wzorek, J. Kvarnström, G. Conte, P. Doherty, and A. Eriksson, “Area coverage with heterogeneous uavs using scan patterns,” in 2016 IEEE International Symposium on Safety, Security, and Rescue Robotics (SSRR), 10 2016, pp. 342–349.

[ref1]: <https://arxiv.org/abs/2110.04043> "[1]"
[ref2]: <https://ieeexplore.ieee.org/document/7784325> "[2]"

