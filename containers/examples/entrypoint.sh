#!/usr/bin/bash

echo ""
echo "+--------------------------------------------+"
echo "| Welcome to the Cartography examples docker |"
echo "+--------------------------------------------+"
echo ""

export CC_PKG_ROOT=/home/zero/cc-pkg
source $CC_PKG_ROOT/cc-pkg/shells/config.sh

EXAMPLE_ARGS=""

optspec=":h-:"
while getopts "$optspec" optchar; do
    case "${optchar}" in
        -)
            case "${OPTARG}" in
                vnc)
                    EXAMPLE_ARGS="$EXAMPLE_ARGS -platform vnc:size=1024x768"
                    echo "Run with vnc." >&2;
                    ;;
                novnc)
                    websockify -D \
                        --web /usr/share/novnc/ \
                        6080 \
                        localhost:5900
                    ;;
                list-examples)
                  echo ""
                  echo "Algorithms"
                  echo "=========="
                  echo " - district_decomposition: starts the weigthed graph decomposition example for district decomposition."
                  echo " - weighted_graph_decomposition: starts the weigthed graph decomposition example for general polygon decomposition."
                  echo " - polyline_fitting: starts the polyline fitting example."
                  echo ""
                  echo "Mapnik"
                  echo "======"
                  echo ""
                  echo " - geometry_viewer: starts the geometry viewer example."
                  echo " - world_viewer: starts the world vierwer example."
                  echo ""
                  echo "terrain"
                  echo "======"
                  echo ""
                  echo " - terrain_editor: starts the terrain editor exampl."
                  echo ""
                  ;;
                *)
                    if [ "$OPTERR" = 1 ] && [ "${optspec:0:1}" != ":" ]; then
                        echo "Unknown option --${OPTARG}" >&2
                    fi
                    ;;
            esac;;
        h)
            echo "usage: cartography_examples [--vnc] [--novnc] [--list-examples] [example_name]" >&2
            echo " --vnc start with vnc"
            echo " --novnc start a novnc server in the docker, to access with webbrowser (requires the --vnc option to work correctly)"
            exit 2
            ;;
        *)
            echo "${OPTARG}"
            if [ "$OPTERR" != 1 ] || [ "${optspec:0:1}" = ":" ]; then
                echo "Non-option argument: '-${OPTARG}'" >&2
            fi
            ;;
    esac
done

EXAMPLE_NAME=${@:$OPTIND:1}

# bash

if [ -z "$EXAMPLE_NAME" ]
then
  bash
else
  $CC_PKG_ROOT/host-build-release/Cartography/examples/$EXAMPLE_NAME/cartography_example_$EXAMPLE_NAME $EXAMPLE_ARGS
fi
