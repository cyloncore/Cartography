Docker Containers
=================

Run examples
------------

This docker allows to run all the [examples](examples/main.md) from Cartography:

```bash
docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=${DISPLAY} -h ${HOSTNAME}  -it cyrilleberger/cartography:examples
```

Specific examples can be run directly, more details in [Application Examples](examples/examples.md).

