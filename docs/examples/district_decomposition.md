District Decomposition
======================

The source code is available in [https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/district_decomposition](examples/district_decomposition).

This application demonstrates the weighted graph decomposition algorithm, combining the decomposition of a shape with population density data.

![Window showing the result of district decomposition](district_decomposition.png).

The application allows to create new polygon and run the weighted graph decomposition algorithm. After building Cartography, this example can be run from the build directory `examples/district_decomposition/cartography_example_district_decomposition`.
