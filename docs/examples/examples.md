Weighted Decomposition
======================

This application demonstrates the weighted decomposition algorithm (i.e. `AreaDecompose` [[1]][ref1]).

The source code is available at [examples/weighted_decomposition](https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/weighted_decomposition).

The application allows to create new polygons and run the weighted decomposition algorithm.
It also shows the resulting statitics, allows to set the weights and display generated trajectories for flying exploration patterns with UAVs. Trajectories were generated using the approach presented in [[2]][ref2].

[[1]][ref1] M. Wzorek, C. Berger, P. Doherty, Polygon Area Decomposition using a Compactness Metric. arXiv preprint arXiv:2110.04043 (2021), Under submission to the [Applied Soft Computing](https://www.sciencedirect.com/journal/applied-soft-computing/) journal.

[[2]][ref2] C. Berger, M. Wzorek, J. Kvarnström, G. Conte, P. Doherty, and A. Eriksson, “Area coverage with heterogeneous uavs using scan patterns,” in 2016 IEEE International Symposium on Safety, Security, and Rescue Robotics (SSRR), 10 2016, pp. 342–349.

[ref1]: <https://arxiv.org/abs/2110.04043> "[1]"
[ref2]: <https://ieeexplore.ieee.org/document/7784325> "[2]"

<img src="weighted_graph_decomposition.png" alt="Window showing the weighted graph decomposition algorithm" width="640"/>

This command will run the example application in a docker container:

```bash
docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=${DISPLAY} -h ${HOSTNAME} -it cyrilleberger/cartography:examples weighted_graph_decomposition
```
**A demo is accessible online at [weighted_graph_decomposition](http://terra8.ida.liu.se/cartography/weighted_graph_decomposition.html)** (username: `cartography` pasword: `cartography`). In case of problems with accessing the online demo, please [contact us](mailto:cyrille.berger@liu.se).

District Decomposition
======================
This application demonstrates the weighted decomposition algorithm (i.e. `AreaDecompose` [[1]][ref1]) applied to the problem of redistricting. The AreaDecompose algorithm is used to solve the problem of dividing a polygon based on the population density instead of the total surface of each partition.

The source code is available at [examples/district_decomposition](https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/district_decomposition).

The application allows to create new polygons and run the weighted decomposition algorithm. After building Cartography, this example can be run from the build directory `examples/district_decomposition/cartography_example_district_decomposition`.

<img src="district_decomposition.png" alt="Window showing the result of district decomposition" width="640"/>

This command will run the example application in a docker container:
```bash
docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=${DISPLAY} -h ${HOSTNAME} -it cyrilleberger/cartography:examples district_decomposition
```

**A demo is accessible online at [district_decomposition](http://terra8.ida.liu.se/cartography/district_decomposition.html)** (username: `cartography` pasword: `cartography`). In case of problems with accessing the online demo, please [contact us](mailto:cyrille.berger@liu.se).

Geometry Viewer
===============
This application demonstrates how to display geometries in QML applications.

The source code is available at [examples/geometry_viewer](https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/geometry_viewer).

The user enters a geometry definition as a WKT string which is then displayed in the window.
After building Cartography, this example can be run from the build directory `examples/geometry_viewer/cartography_example_geometry_viewer`.

<img src="geometry_viewer.png" alt="Window showing the geometry_viewer" width="640"/>

This command will run the example application in a docker container:
```bash
docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=${DISPLAY} -h ${HOSTNAME} -it cyrilleberger/cartography:examples geometry_viewer
```

Geo Tile OSM
============
This application demonstrates how to use the GeoTileDatasource to display a map using tiles from an OpenStreetMap tiles server.

The source code is available at [examples/geometry_viewer](https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/geo_tile_osm).

<img src="geo_tile_osm.png" alt="Window showing the geo_tile_osm" width="640"/>

This command will run the example application in a docker container:
```bash
docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=${DISPLAY} -h ${HOSTNAME} -it cyrilleberger/cartography:examples geo_tile_osm
```

Shape Viewer
============

This application demonstrates how to display SHP file in a QML application.

The source code is available at [examples/shape_viewer](https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/shape_viewer).

The user can load new shape files from the menu.
After building Cartography, this example can be run from the build directory `examples/shape_viewer/cartography_example_shape_viewer`.

<img src="shape_viewer.png" alt="Window showing the shape_viewer" width="640"/>

This command will run the example application in a docker container:
```bash
docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=${DISPLAY} -h ${HOSTNAME} -it cyrilleberger/cartography:examples shape_viewer
```


Terrain Editor
==============

This application demonstrates how the editing of terrain and display using hill shading.

The source code is available at [examples/terrain_editor](https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/terrain_editor).

The user can edit the terrain in the left panel and update the rendering options on the right.
After building Cartography, this example can be run from the build directory `examples/terrain_editor/cartography_example_terrain_editor`.

<img src="terrain_editor.png" alt="Window showing the terrain_editor" width="640"/>

This command will run the example application in a docker container:
```bash
docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=${DISPLAY} -h ${HOSTNAME} -it cyrilleberger/cartography:examples terrain_editor
```

World Viewer
============

This application demonstrates the mapnik intgegration to show a map of the world, the capital and some makor cities.

The source code is available at [examples/world_viewer](https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/world_viewer).

This example can be run from the build directory `examples/world_viewer/cartography_example_world_viewer`.

<img src="world_viewer.png" alt="Window showing the world_viewer" width="640"/>

This command will run the example application in a docker container:
```bash
docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=${DISPLAY} -h ${HOSTNAME} -it cyrilleberger/cartography:examples world_viewer
```


