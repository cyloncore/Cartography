Geometry Viewer
===============

The source code is available in [https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/geometry_viewer](examples/geometry_viewer).

This application demonstrates how to display geometries in QML applications.

![Window showing the geometry_viewer](geometry_viewer.png).

The user enter a geometry definition as a WKT string which is then displayed in the window.
After building Cartography, this example can be run from the build directory `examples/geometry_viewer/cartography_example_geometry_viewer`.
