Examples
========

* [district_decomposition](district_decomposition.md) an application demonstrating the weighted graph decomposition algorithm for district decomposition
* [geometry_viewer](geometry_viewer.md) an application demonstrating the geometry qml module, to display WKT geometry.
* [shape_viewer](shape_viewer.md) an application demonstrating the mapnik qml module, to display a shapes as different layers.
* [terrain_editor](terrain_editor.md) an application demonstrating the terrain qml module.
* [weighted_graph_decomposition](weighted_graph_decomposition.md) an application demonstrating the weighted graph decomposition algorithm
* [world_viewer](world_viewer.md) an application demonstrating the mapnik qml module, to display a map of the world.
