Shape Viewer
============

The source code is available in [https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/shape_viewer](examples/shape_viewer).

This application demonstrates how to display SHP file in a QML application.

![Window showing the shape_viewer](shape_viewer.png).

The user can load new shape files from the menu.
After building Cartography, this example can be run from the build directory `examples/shape_viewer/cartography_example_shape_viewer`.
