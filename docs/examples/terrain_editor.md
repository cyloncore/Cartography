Terrain Editor
==============

The source code is available in [https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/terrain_editor](examples/terrain_editor).

This application demonstrates how the editing of terrain and display using hill shading.

![Window showing the terrain_editor](terrain_editor.png).

The user can edit the terrain and the left panel and update the rendering option on the right.
After building Cartography, this example can be run from the build directory `examples/terrain_editor/cartography_example_terrain_editor`.
