Weighted Graph Decomposition
============================

The source code is available in [https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/weighted_decomposition](examples/weighted_decomposition).

This application demonstrates the weighted graph decomposition algorithm.

![Window showing the weighted graph decomposition algorithm](weighted_graph_decomposition.png).

The application allows to create new polygon and run the weighted graph decomposition algorithm. It also shows the resulting statitics, allows to set the weights and display a generated trajectory for flying an exploration pattern with an UAV.
