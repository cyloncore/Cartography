World Viewer
============

The source code is available in [https://gitlab.com/cyloncore/Cartography/-/tree/stable/examples/world_viewer](examples/world_viewer).

This application demonstrates the mapnik intgegration to show a map of the world, the capital and some makor cities.

![Window showing the result of district decomposition](district_decomposition.png).

This example can be run from the build directory `examples/world_viewer/cartography_example_world_viewer`.
