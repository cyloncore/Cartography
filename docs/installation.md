Installation
============

Dependencies
------------

On Ubuntu 22.04:

```bash
sudo apt install cmake libgdal-dev qtbase5-dev libeigen3-dev python3-pyqt6 sip-dev qtpositioning5-dev pyqt6-dev
```

Compile from source
--------------------

```bash
git clone https://gitlab.com/cyloncore/Cartography.git
cd Cartography
mkdir build
cd build
cmake ..
make -j4
make install
```

cc-pkg
-------

Using `cc-pkg`:

```bash
cc-pkg get --repo cyloncore-core https://gitlab.com/cyloncore/cc-pkg-core-repositories.git
cc-pkg get Cartography
cc-pkg build Cartography
```

This will install some of the dependencies, such as euclid.
