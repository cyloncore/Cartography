include_directories(BEFORE ${CMAKE_BINARY_DIR}/extensions/ ${CMAKE_SOURCE_DIR}/extensions/)

qt6_add_resources(DISTRICT_DECOMPOSITION_QML_SRC qml.qrc)

add_executable(cartography_example_district_decomposition main.cpp ${DISTRICT_DECOMPOSITION_QML_SRC} )
target_link_libraries(cartography_example_district_decomposition Qt6::Quick)
