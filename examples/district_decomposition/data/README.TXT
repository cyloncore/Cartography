Data from: https://sedac.ciesin.columbia.edu/data/collection/usgrid

https://sedac.ciesin.columbia.edu/data/set/usgrid-summary-file1-2010

Recommended Citation(s)*:

    Center for International Earth Science Information Network - CIESIN - Columbia University. 2017. U.S. Census Grids (Summary File 1), 2010. Palisades, NY: NASA Socioeconomic Data and Applications Center (SEDAC). https://doi.org/10.7927/H40Z716C. Accessed DAY MONTH YEAR. 

