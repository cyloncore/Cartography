#include <QGuiApplication>

#include <QAbstractItemModel>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QTemporaryDir>

int main(int argc, char* argv[])
{
#ifdef HAVE_QT6WIDGETS
  QApplication app(argc, argv);
#else
  QGuiApplication app(argc, argv);
#endif
  QTemporaryDir tmpdir;
  QFile::copy(":/data/State.shx", tmpdir.path() + "/State.shx");
  QFile::copy(":/data/State.shp", tmpdir.path() + "/State.shp");
  QFile::copy(":/data/State.prj", tmpdir.path() + "/State.prj");
  QFile::copy(":/data/nypop10.tif", tmpdir.path() + "/nypop10.tif");
  QFile::copy(":/data/nypop10.tif.aux.xml", tmpdir.path() + "/nypop10.tif.aux.xml");

  app.setApplicationName("cartography_district_decomposition");

  QQmlApplicationEngine engine;
  engine.addImportPath("qrc:/qml/");
  engine.globalObject().setProperty("TMP_DIRECTORY", tmpdir.path());
  engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

  return app.exec();
}
