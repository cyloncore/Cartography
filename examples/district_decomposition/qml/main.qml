import Cartography.Algorithms
import Cartography.Geometry
import Cartography.Geometry.Controls
import Cartography.Raster
import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts

ApplicationWindow
{
  id: root
  visible: true
  width: 1024
  height: 768
  title: qsTr("Cartography District Decomposition")

  property var startPoints: []
  property var weights: Algorithms.equalWeights(districtsSpinBox.value)
  property Geometry geometry: __select_geometry(loadedGeometry, loadedGeometryCollectionIndex.value)
  property Geometry loadedGeometry: null
  property Raster raster: null
  property var __envelope: root.geometry ? geometry.envelope : Qt.rect(0, 0, 0, 0)
  property var __offset: Qt.point(-__envelope.left, -__envelope.top)
  property var __scale: Math.min(graph_decompo.width, graph_decompo.height) / Math.max(__envelope.width, __envelope.height)
  property real tolerance: toleranceSpinBox.realValue * 0.1

  function __select_geometry(geometry, index)
  {
    if(__is_geometry_collection(geometry))
    {
      return geometry.elements[index]
    } else {
      return geometry
    }
  }
  function __is_geometry_collection(geometry)
  {
    return geometry && geometry.type == Geometry.Collection
  }
  
  GridLayout
  {
    id: buttonRow
    columns: 3
    Button
    {
      text: "Open shape file"
      onClicked: geometryDialog.open()
    }
    Button
    {
      text: "Open population file"
      onClicked: rasterDialog.open()
    }
    Item
    {
      Layout.fillWidth: true
    }
    Label
    {
      text: "Geometry index in collection:"
      visible: __is_geometry_collection(loadedGeometry)
    }
    SpinBox
    {
      id: loadedGeometryCollectionIndex
      visible: __is_geometry_collection(loadedGeometry)
      from: 0
      to: __is_geometry_collection(loadedGeometry) ? loadedGeometry.elements.length - 1 : 0
    }
    Item
    {
      visible: __is_geometry_collection(loadedGeometry)
      Layout.fillWidth: true
    }
    Label
    {
      text: "Tolerance:"
    }
    SpinBox
    {
      id: toleranceSpinBox
      editable: true
      from: 0
      to: 1000
      value: 0.1 * to
      property int decimals: 3
      property real realValue: value * 1.0 / to
      validator: DoubleValidator {
          bottom: toleranceSpinBox.from
          top:  toleranceSpinBox.to
      }

      textFromValue: function(value, locale) {
        return Number(value / toleranceSpinBox.to).toLocaleString(locale, 'f', toleranceSpinBox.decimals)
      }

      valueFromText: function(text, locale) {
        return Number.fromLocaleString(locale, text) * toleranceSpinBox.to
      }
    }
    Item
    {
      Layout.fillWidth: true
    }
    Label
    {
      text: "Districts:"
    }
    SpinBox
    {
      id: districtsSpinBox
      editable: true
      from: 2
      to: 20
      value: 6
    }
    Item
    {
      Layout.fillWidth: true
    }

    Button
    {
      text: "Compute"
      enabled: root.geometry != null
      onClicked:
      {
        _calculation_text_.opacity = 0.9
        _calculation_timer_.start()
      }
      Timer
      {
        id: _calculation_timer_
        onTriggered:
        {
          graph_decompo.geometry = Algorithms.weightedGraphPolygonDecomposition(root.geometry, weights, tolerance, startPoints, {"raster": root.raster})
          _calculation_text_.opacity = 0.1
        }
      }
    }
    Text
    {
      id: _calculation_text_
      opacity: 0.1
      text: "Calculation can take some time, and user interface will appear frozen while it happens..."
    }
    Item
    {
      Layout.fillWidth: true
    }
  }
  
  FileDialog
  {
    id: geometryDialog
    title: "Open shape file"
    onAccepted: {
      root.loadedGeometry = GeometryFactory.load(geometryDialog.selectedFile)
    }
  }
  FileDialog
  {
    id: rasterDialog
    title: "Open raster file"
    onAccepted: {
      root.raster = RasterFactory.load(rasterDialog.selectedFile)
    }
  }
  

  Timer
  {
    id: switch_geometry
    interval: 1000
    repeat: true
    running: graph_decompo.geometry
    property int geometry_index: 0
    onTriggered: geometry_index = (geometry_index + 1) % graph_decompo.geometry.elements.length
  }
  // Display of the original state shape
  GeometryView
  {
    id: graph_decompo
    width: parent.width
    anchors.top: buttonRow.bottom
    anchors.bottom: parent.bottom
    strokeStyle: "blue"
    lineWidth: 2
    offset: root.__offset
    scale: root.__scale
  }
  // Display of the original state shape
  GeometryView
  {
    anchors.fill: graph_decompo
    strokeStyle: "red"
    lineWidth: 1
    offset: root.__offset
    scale: root.__scale
    geometry: root.geometry
  }
  // Highlight one district at a time
  GeometryView
  {
    id: graph_decompo_alternate
    anchors.fill: graph_decompo
    geometry: graph_decompo.geometry ?  graph_decompo.geometry.elements[switch_geometry.geometry_index] : null
    strokeStyle: "orange"
    lineWidth: 2
    offset: root.__offset
    scale: root.__scale
  }
  Component.onCompleted:
  {
    root.loadedGeometry = GeometryFactory.load("file:///" + TMP_DIRECTORY + "/State.shp")
    root.raster = RasterFactory.load("file:///" + TMP_DIRECTORY + "/nypop10.tif")
    loadedGeometryCollectionIndex.value = 2
  }
}

