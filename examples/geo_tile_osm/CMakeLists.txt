qt6_add_resources(GEO_TILE_OSM_QML_SRC qml.qrc)

add_executable(cartography_example_geo_tile_osm main.cpp ${GEO_TILE_OSM_QML_SRC})
target_link_libraries(cartography_example_geo_tile_osm Qt6::Qml Qt6::Quick )
