import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Dialogs
import Cartography.Mapnik

ApplicationWindow
{
  id: root
  visible: true
  width: 1024
  height: 768

  RowLayout
  {
    anchors.fill: parent
    Slider
    {
      id: zoomSlider
      orientation: Qt.Vertical
      from: 1.0
      to: 10.0
      value: Math.sqrt(mapView.zoom)
      onValueChanged: mapView.setZoom(value*value)
      Layout.fillHeight: true
    }
    MapView
    {
      id: mapView
      map: Map
      {
        srs: '+proj=merc +a=6378137 +b=6378137 +lat_ts=0 +lon_0=0 +x_0=0 +y_0=0 +k=1 +units=m +nadgrids=@null +wktext +no_defs +type=crs'
        backgroundColor: "#ccc"
        styles: [
          Style
          {
            name: "raster"
            Rule
            {
              RasterSymbolizer
              {
                ScalingKey { value: Mapnik.SCALING_BILINEAR }
              }
            }
          }
        ]
        Layer
        {
          name: "OSM"
          // Projection as to be web mercator
          srs: '+proj=merc +a=6378137 +b=6378137 +lat_ts=0 +lon_0=0 +x_0=0 +y_0=0 +k=1 +units=m +nadgrids=@null +wktext +no_defs +type=crs'
          styles: ["raster"]
          datasource: GeoTileDatasource {
            // Pick from https://wiki.openstreetmap.org/wiki/Raster_tile_providers
            sources: ["https://tile.openstreetmap.org/{z}/{x}/{y}.png"]
            userAgent: "cartography_example_geo_tile_osm/2.0 (CylonCore; Cartography)"
            onTilesUpdated: mapView.updateMap(true)
          }
        }
      }
      MouseArea
      {
        anchors.fill: parent
        property real zoomFactor: 1.2
        property real panFactor: 0.3

        property int __isPanning: 0 // 0 no panning 1 mouse pressed 2 panning in progress
        property int  __lastX: -1
        property int  __lastY: -1
        property int __isWheeling: 0 // 0 no wheeling 1 wheeling in progress
        
        hoverEnabled: true
        
        onPressed: mouse =>
        {
          __isPanning = 1
          __lastX = mouse.x
          __lastY = mouse.y
        }
  
        onReleased: mouse =>
        {
          if(__isPanning != 2)
          {
            let center = mapView.viewTransform.toMap(Qt.point(mouse.x, mouse.y))
            mapView.centerTo(center.x, center.y)
          }
          __isPanning = 0
        }
  
        onPositionChanged: mouse =>
        {
          {
            let center = mapView.viewTransform.toWgs84(mouse.x, mouse.y)
          __coordinate__label__.text = "(" + center.x + " ," + center.y + ")"
          }
          if (__isPanning > 0)
          {
            __isPanning = 2
            var dx = mouse.x - __lastX
            var dy = mouse.y - __lastY
            mapView.centerX -= dx * mapView.pixelScaleX
            mapView.centerY += dy * mapView.pixelScaleY
            __lastX = mouse.x
            __lastY = mouse.y
          }
          if(__isWheeling == 1)
          { // If we are zooming then we should stop
            __isWheeling = 0
          }
        }
  
        onWheel: wheel =>
        {
          if(wheel.angleDelta.y > 0)
          {
            // First time we zoom in? Then we should record mouse pointer position
            if(__isWheeling == 0)
            {
              __isWheeling = 1
              __lastX = wheel.x
              __lastY = wheel.y
            }
            let center = mapView.viewTransform.toMap(Qt.point(__lastX, __lastY))
            // Zoom to point
            mapView.zoomTo(center.x, center.y, mapView.zoom * zoomFactor, panFactor)
            
            // Adjust zooming point
            __lastX = (1-panFactor)* zoomFactor*(__lastX - 0.5*mapView.width) + 0.5*mapView.width
            __lastY = (1-panFactor)* zoomFactor*(__lastY - 0.5*mapView.height) + 0.5*mapView.height
          } else {
            mapView.zoomOut(zoomFactor)
          }
        }
      }
      Label
      {
        id: __coordinate__label__
        anchors.left: parent.left
        anchors.bottom: parent.bottom
      }
      Label
      {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        text: "© <a href='https://www.openstreetmap.org/copyright'>OpenStreetMap</a> contributors."
        onLinkActivated: link => Qt.openUrlExternally(link)
      }
      Layout.fillWidth: true
      Layout.fillHeight: true
    }
  }
}
