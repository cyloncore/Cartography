#include <QGuiApplication>

#include <QAbstractItemModel>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char* argv[])
{
  QGuiApplication app(argc, argv);
  app.setApplicationName("cartography_example_geometry_viewer");

  QQmlApplicationEngine engine;
  engine.addImportPath("qrc:/qml/");

  engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

  return app.exec();
}
