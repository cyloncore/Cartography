import Cartography.Geometry
import Cartography.Algorithms
import Cartography.Geometry.Controls
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

ApplicationWindow
{
  id: root
  visible: true
  width: 1024
  height: 768

  title: qsTr("Cartography Geometry Fixer Example")

  property var wkts: [
  ]
  
  function from_wkts(wkts)
  {
    var gs = []
    for(var i = 0; i < wkts.length; ++i)
    {
      gs.push(Algorithms.fixIt(GeometryFactory.fromWKT(wkts[i])))
    }
    return gs
  }

  property var geometries: from_wkts(wkts)
  
  property var __envelope: geometries.length > 0 ? geometries[0].envelope : null
  property var __offset: __envelope ? Qt.point(-__envelope.left+ __extra_offset.x, -__envelope.top+ __extra_offset.y) : Qt.point(0,0)
  property var __scale: __envelope ? root.width / __envelope.width : 1.0
  property var __extra_offset: Qt.point(0, 0)
  
  ColumnLayout
  {
    anchors.fill: parent
    RowLayout
    {
      TextArea
      {
        id:text_area
        placeholderText: "Geometry as a WKT string, ex: POLYGON((10 10, ...))"
        wrapMode: TextEdit.Wrap
        Layout.minimumHeight: 100
        Layout.fillWidth: true
      }
      ColumnLayout
      {
        Button
        {
          text: "Add"
          onClicked:
          {
            var wkts = root.wkts
            wkts.push(text_area.text)
            text_area.text = ""
            root.wkts = wkts
          }
        }
        Button
        {
          text: "Clear"
          onClicked: root.wkts = []
        }
      }
    }
    Rectangle
    {
      color: "lightgray"
      Repeater
      {
        model: root.geometries
        GeometryView
        {
          anchors.fill: parent
          strokeStyle: Qt.rgba(Math.random(), Math.random(), Math.random(), 1.0)
          lineWidth: 2
          offset: root.__offset
          scale: root.__scale
          geometry: modelData
        }
      }
      MouseArea
      {
        id: ma
        anchors.fill: parent
        property real startX
        property real startY
        property real lastX
        property real lastY
        hoverEnabled: true
        onPressed: {
          startX = mouse.x
          startY = mouse.y
        }
        onPositionChanged: {
          if(mouse.buttons == Qt.LeftButton)
          {
            __extra_offset = Qt.point(__extra_offset.x + (mouse.x - startX) / root.__scale, __extra_offset.y + (mouse.y - startY) / root.__scale)
            startX = mouse.x
            startY = mouse.y
          }
          lastX = mouse.x
          lastY = mouse.y
        }
        onWheel:
        {
          if(wheel.angleDelta.y > 0)
          {
            root.__scale *= 1.2
          } else {
            root.__scale = Math.max(root.__scale *0.8, 0.1)
          }
        }
      }
      Label
      {
        anchors.bottom: parent.bottom
        text: (ma.lastX / __scale - __offset.x) + ", " + (ma.lastY / __scale - __offset.y)
      }
      Layout.fillHeight: true
      Layout.fillWidth: true
    }
  }
}
