include_directories(BEFORE ${CMAKE_BINARY_DIR}/extensions/ ${CMAKE_SOURCE_DIR}/extensions/)

qt6_add_resources(CARTOGRAPHY_POLYLINE_FITTING_QML_SRC qml.qrc)

add_executable(cartography_example_polyline_fitting main.cpp ${CARTOGRAPHY_POLYLINE_FITTING_QML_SRC} )
target_link_libraries(cartography_example_polyline_fitting Qt6::Quick )

# install(TARGETS cartography_example_polyline_fitting ${INSTALL_TARGETS_DEFAULT_ARGS} )

