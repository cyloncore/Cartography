import Cartography.Algorithms
import Cartography.Geometry
import Cartography.Geometry.Controls
import QtQuick
import QtQuick.Controls

ApplicationWindow
{
  id: root
  visible: true
  width: 1024
  height: 768

  title: qsTr("Cartography Examples Polyline Fitting")

  property var startPoints: []

//   property Geometry geometry: GeometryFactory.points([[0, 0], [0, 100], [100, 100], [100, 200], [200, 200]])
//   property Geometry geometry: GeometryFactory.points([[0, 0], [100, 0], [100, 100], [100, 200], [200, 200], [300, 200], [300, 300], [300, 400], [400, 400], [400, 500], [500, 500], [500, 600], [600, 600], [700, 600], [700, 700], [800, 700], [800, 800], [900, 800]])
//   property Geometry geometry: GeometryFactory.points([[0, 0], [100, 0], [100, 100], [100, 200], [200, 200], [300, 200], [300, 300], [300, 400], [400, 400], [400, 500], [500, 500], [500, 600], [600, 600], [700, 600], [700, 700], [800, 700], [800, 800], [900, 800], [1000, 800], [1100, 800], [1200, 800], [1300, 800], [1400, 800], [1500, 800]])
  property Geometry geometry: GeometryFactory.points([[320,320], [320,340], [320,360], [340,360], [340,380], [340,400], [340,420], [340,440], [340,460], [340,480], [340,500]])
  property var tolerance: 10

  GeometryView
  {
    anchors.fill: parent
    strokeStyle: "red"
    lineWidth: 4
    geometry: root.geometry
  }
  GeometryView
  {
    anchors.fill: parent
    strokeStyle: "blue"
    lineWidth: 2
    geometry: Algorithms.polylineFitting(root.geometry, tolerance)
  }
}
