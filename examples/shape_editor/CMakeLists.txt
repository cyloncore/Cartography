qt6_add_resources(SHAPE_EDITOR_QML_SRC qml.qrc)

add_executable(cartography_example_shape_editor main.cpp ${SHAPE_EDITOR_QML_SRC})
target_link_libraries(cartography_example_shape_editor Qt6::Quick ${QT6_WIDGET_LIBRARIES})
