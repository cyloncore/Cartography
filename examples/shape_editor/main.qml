import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs

import Cartography.Geometry
import Cartography.Mapnik
import Cartography.Cartographer
import Cartography.Cartographer.Tools
import QtQuick.Layouts
import Cyqlops.Toolbox
import Cyqlops.Toolbox.Controls as TC

ApplicationWindow
{
  id: root
  visible: true
  width: 1024
  height: 768

  
  MessageDialog
  {
    id: errorMessageDialog
  }
  
  FileDialog
  {
    id: openDialog
    title: "Choose a shape file"
    nameFilters: [ "Shape files (*.shp)" ]
    onAccepted: {
      
      gdalFeatureSource.url = selectedFile
      if(!gdalFeatureSource.load())
      {
        errorMessageDialog.text = "Error when opening " + selectedFile + ": " + gdalFeatureSource.errorMessage
        errorMessageDialog.open()
      }
    }
  }
  FileDialog
  {
    id: newDialog
    title: "Choose a shape file"
    nameFilters: [ "Shape files (*.shp)" ]
    acceptLabel: "Create"
    fileMode: FileDialog.SaveFile
    onAccepted: {
      
      gdalFeatureSource.url = selectedFile
      if(!gdalFeatureSource.create("ESRI Shapefile"))
      {
        errorMessageDialog.text = "Error when creating " + selectedFile + ": " + gdalFeatureSource.errorMessage
        errorMessageDialog.open()
      }
    }
  }

  menuBar: MenuBar {
    Menu {
      title: "&File"
      MenuItem { action: file_new }
      MenuItem { action: file_open }
      MenuItem { action: file_save }
    }
    Menu
    {
      title: "&Tools"
      MenuItem { action: tools_navigation }
      MenuItem { action: tools_selection }
      MenuItem { action: tools_create_point }
      MenuItem { action: tools_create_line_string }
      MenuItem { action: tools_create_polygon }
    }
  }
  Action
  {
    id: file_new
    text: "&New..."
    onTriggered: newDialog.open()
  }
  Action
  {
    id: file_open
    text: "&Open..."
    onTriggered: openDialog.open()
  }
  Action
  {
    id: file_save
    text: "&Save"
    onTriggered:
    {
      if(!gdalFeatureSource.save())
      {
        errorMessageDialog.text = "Error when saving " + gdalFeatureSource.url + ": " + gdalFeatureSource.errorMessage
        errorMessageDialog.open()
      }
    }
  }
  MapViewInterface
  {
    id: view_interface
    mapView: map_view
  }
  TC.ToolActionsGroup {
    id: tools
    TC.ToolAction
    {
      id: tools_navigation
      text: "&Navigation"
      tool: NavigationTool
      {
        id: navigation_tool
        mapView: map_view
      }
    }
    TC.ToolAction
    {
      id: tools_selection
      text: "&Selection"
      tool: SelectionTool
      {
        id: selection_tool
        featuresSource: gdalFeatureSource
      }
    }
    TC.ToolAction
    {
      id: tools_create_point
      text: "Create &Point"
      tool: CreatePointFeatureTool
      {
        id: create_point_tool
        featuresSource: gdalFeatureSource
      }
    }
    TC.ToolAction
    {
      id: tools_create_line_string
      text: "Create &Line String"
      tool: CreateLineStringFeatureTool
      {
        id: create_line_string_tool
        featuresSource: gdalFeatureSource
        viewInterface: view_interface
      }
    }
    TC.ToolAction
    {
      id: tools_create_polygon
      text: "Create &Polygon"
      checked: true
      tool:  CreatePolygonFeatureTool
      {
        id: create_polygon_tool
        featuresSource: gdalFeatureSource
        viewInterface: view_interface
      }
    }
  }
  SplitView
  {
    enabled: gdalFeatureSource.valid
    anchors.fill: parent
    orientation: Qt.Horizontal
    Loader
    {
      property QtObject tool: tool_controller.tool
      property MapView mapView: map_view
      sourceComponent: tool_controller.tool.optionsComponent
      SplitView.minimumWidth: 150
    }
    MapView
    {
      id: map_view
      map: Map
      {
        backgroundColor: "white"
        styles: [
          Style
          {
            name: "default"
            Rule
            {
              filter: "[mapnik::geometry_type]=point"
              DotSymbolizer
              {
                FillKey { value: "black" }
                WidthKey { value: 5 }
                HeightKey { value: 5 }
              }
            }
            Rule
            {
              filter: "[mapnik::geometry_type]=linestring"
              LineSymbolizer
              {
                StrokeKey { value: "black" }
              }
            }
            Rule
            {
              filter: "[mapnik::geometry_type]=polygon"
              LineSymbolizer
              {
                StrokeKey { value: "black" }
              }
            }
          }
        ]
        Layer
        {
          name: "shape_editor"
          styles: ["default"]
          datasource: EditableDatasource
          {
            featuresSource: GDALFeaturesSource {
              id: gdalFeatureSource
            }
          }
        }
      }
      Loader
      {
        anchors.fill: parent
        property QtObject tool: tool_controller.tool
        property MapView mapView: map_view
        sourceComponent: tool_controller.tool.overlayComponent
      }
      ToolController
      {
        id: tool_controller
        anchors.fill: parent
        viewInterface: view_interface
        tool: tools.tool
        onViewInterfaceChanged: console.log(viewInterface, view_interface)
      }
      SplitView.fillWidth: true
      SplitView.fillHeight: true
    }
  }
  Component.onCompleted:
  {
    if(CMD_LINE_FILENAME.length > 0)
    {
      gdalFeatureSource.url = "file:///" + CMD_LINE_FILENAME
      if(!gdalFeatureSource.load())
      {
        errorMessageDialog.text = "Error when opening " + CMD_LINE_FILENAME + ": " + gdalFeatureSource.errorMessage
        errorMessageDialog.open()
      }
    }
  }
}
