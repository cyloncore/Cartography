qt6_add_resources(SHAPE_VIEWER_QML_SRC qml.qrc)

add_executable(cartography_example_shape_viewer main.cpp ${SHAPE_VIEWER_QML_SRC})
target_link_libraries(cartography_example_shape_viewer CartographyQuickMapnik Qt6::Quick ${QT6_WIDGET_LIBRARIES})
