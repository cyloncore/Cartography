import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import Cartography.Mapnik

ApplicationWindow
{
  id: root
  visible: true
  width: 1024
  height: 768

  
  property variant layer_list: []

  FileDialog
  {
    id: openDialog
    title: "Choose a shape file"
    nameFilters: [ "Shape files (*.shp)" ]
    onAccepted: {
      console.log("Loading file: ", selectedFile)
      
      var new_layer = Qt.createQmlObject("import Cartography.Mapnik; Layer {}", map, "new layer")
      var new_datasource = Qt.createQmlObject("import Cartography.Mapnik; Datasource { property string type: 'shape'; property url file }", new_layer, "new layer")
      new_datasource.file = selectedFile
      new_layer.datasource = new_datasource
      new_layer.styles = ["default"]
      var map_layers = []
      for(var i = 0; i < map.layers.length; ++i)
      {
        map_layers.push(map.layers[i])
      }
      map_layers.push(new_layer)
      map.layers = map_layers
    }
  }
  menuBar: MenuBar {
    Menu {
      title: "Layers"
      MenuItem { action: layer_add_shp }
    }
  }
  Action
  {
    id: layer_add_shp
    text: "Add shp file..."
    onTriggered: openDialog.open()
  }
  MapView
  {
    anchors.fill: parent
    map: Map
    {
      id: map
      backgroundColor: "white"
      styles: [
        Style
        {
          name: "default"
          Rule
          {
            filter: "[mapnik::geometry_type]=point"
            DotSymbolizer
            {
              FillKey { value: "black" }
              WidthKey { value: 5 }
              HeightKey { value: 5 }
            }
          }
          Rule
          {
            filter: "[mapnik::geometry_type]=linestring"
            LineSymbolizer
            {
              StrokeKey { value: "black" }
            }
          }
          Rule
          {
            filter: "[mapnik::geometry_type]=polygon"
            LineSymbolizer
            {
              StrokeKey { value: "black" }
            }
          }
        }
      ]
    }
  }
}
