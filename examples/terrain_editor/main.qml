import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts
import Cartography.Terrain

ApplicationWindow
{
  id: root
  visible: true
  width: 1024
  height: 768

  
  SplitView
  {
    anchors.fill: parent
    HeightMapView
    {
      id: view
      Component.onCompleted: {
        var hm = HeightMap.create(-10, -10, 20, 20, 20/600)
        hm = HeightMap.fill(hm, 100.0)
        view.heightMap = hm
      }
      MouseArea
      {
        anchors.fill: parent
        onClicked: mouse =>
        {
          var hm = view.heightMap
          var pt = view.viewToMap(Qt.point(mouse.x, mouse.y))
          hm = HeightMap.raise(hm, pt.x, pt.y, 10, 5, 2)
          view.heightMap = hm
        }
      }
      SplitView.fillWidth: true
      SplitView.fillHeight: true
    }
    ColumnLayout
    {
      RowLayout
      {
        Text { text: "Azimuth: " }
        Slider {
          id: azimuthSlider
          from: 0
          to: 360
        }
        Text { text: azimuthSlider.value }
      }
      RowLayout
      {
        Text { text: "Altitude: " }
        Slider {
          id: altitudeSlider
          value: 45
          from: 0
          to: 90
        }
        Text { text: altitudeSlider.value }
      }
      RowLayout
      {
        Text { text: "Intensity: " }
        Slider {
          id: intensitySlider
          value: 80.0
          from: 0
          to: 255.0
        }
        Text { text: intensitySlider.value }
      }
      HillshadeView
      {
        azimuth: azimuthSlider.value
        altitude: altitudeSlider.value
        intensity: intensitySlider.value
        heightMap: view.heightMap
        Layout.fillWidth: true
        Layout.fillHeight: true
      }
    }
  }
}
