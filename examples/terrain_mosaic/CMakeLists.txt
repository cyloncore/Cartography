qt6_add_resources(TERRAIN_MOSAIC_QML_SRC qml.qrc)

add_executable(cartography_example_terrain_mosaic TestDatasource.cpp main.cpp ${TERRAIN_MOSAIC_QML_SRC})
target_link_libraries(cartography_example_terrain_mosaic CartographyTerrain Qt6::Qml ${QT6_WIDGET_LIBRARIES})

