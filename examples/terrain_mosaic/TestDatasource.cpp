#include "TestDatasource.h"

#include <QRectF>

#include <Cartography/Terrain/Algorithms/Terraforming.h>

TestDatasource::TestDatasource(QObject* _parent) : AbstractDatasource(_parent)
{
  m_maps.append(Cartography::Terrain::HeightMap(-2.0, -2.0, 2.0, 2.0, 0.01));
  m_maps.append(Cartography::Terrain::HeightMap(0.0, -2.0, 2.0, 2.0, 0.01));
  m_maps.append(Cartography::Terrain::HeightMap(-2.0, 0.0, 2.0, 2.0, 0.01));
  m_maps.append(Cartography::Terrain::HeightMap(0.0, 0.0, 2.0, 2.0, 0.01));
  for(Cartography::Terrain::HeightMap& hm : m_maps)
  {
    Cartography::Terrain::Algorithms::Terraforming::fill(&hm, 0.0);
    Cartography::Terrain::Algorithms::Terraforming::raise(&hm, QPointF(0.0, 0.0), 0.5, 1.0, 2.0);
  }
}

TestDatasource::~TestDatasource() {}

QList<Cartography::Terrain::HeightMap> TestDatasource::heightMaps(const QRectF& _rect) const
{
  QList<Cartography::Terrain::HeightMap> maps;

  for(const Cartography::Terrain::HeightMap& hm : m_maps)
  {
    if(hm.boundingBox().intersects(_rect))
    {
      maps.append(hm);
    }
  }

  return maps;
}
