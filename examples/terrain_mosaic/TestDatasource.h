#include <Cartography/Terrain/AbstractDatasource.h>

#include <Cartography/Terrain/HeightMap.h>

class TestDatasource : public Cartography::Terrain::AbstractDatasource
{
public:
  TestDatasource(QObject* _parent = nullptr);
  ~TestDatasource();
public:
  QList<Cartography::Terrain::HeightMap> heightMaps(const QRectF& _rect) const override;
private:
  QList<Cartography::Terrain::HeightMap> m_maps;
};
