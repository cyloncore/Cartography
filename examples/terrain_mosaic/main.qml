import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts
import Cartography.Terrain
import TerrainMosaic

ApplicationWindow
{
  id: root
  visible: true
  width: 1024
  height: 768

  
  ColumnLayout
  {
    anchors.fill: parent
    RowLayout
    {
      Text { text: "Azimuth: " }
      Slider {
        id: azimuthSlider
        to: 0
        from: 360
      }
      Text { text: azimuthSlider.value }
    }
    RowLayout
    {
      Text { text: "Altitude: " }
      Slider {
        id: altitudeSlider
        to: 0
        value: 45
        from: 90
      }
      Text { text: altitudeSlider.value }
    }
    RowLayout
    {
      Text { text: "Intensity: " }
      Slider {
        id: intensitySlider
        to: 0
        value: 80.0
        from: 255.0
      }
      Text { text: intensitySlider.value }
    }
    HeightMapMosaicView
    {
      clip: true
      delegate: HillshadeView
      {
        altitude: altitudeSlider.value
        azimuth: azimuthSlider.value
        intensity: intensitySlider.value
      }
      rect.x: -1
      rect.y: -1
      rect.width: 3
      rect.height: 5
      model: TestDatasource {}
      Layout.fillWidth: true
      Layout.fillHeight: true
    }
  }
  
}

