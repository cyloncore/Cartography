import Cartography.Algorithms
import Cartography.Geometry
import Cartography.Geometry.Controls
import QtQuick.Controls

ApplicationWindow
{
  id: root
  visible: true
  width: 1024
  height: 768

  title: qsTr("Cartography Examples Triangulation")
//   property Geometry geometry: GeometryFactory.polygon([[0, 0], [0, 100], [100, 100], [100, 200], [200, 200], [200, 0], [150, 0], [150, 50], [50, 50], [50, 0], [0, 0]], [])
  property Geometry geometry: GeometryFactory.fromWKT("POLYGON ((172.23141501 496.113839 132.9,389.78036 495.31041 132.9,389.78036 440.9259 132.9,084.00853 448.77526 132.9,85.01271269 471.07765132 132.9,175.20364 470.64125 132.9,172.23141501 496.113839 132.9))")
//   property Geometry geometry: GeometryFactory.fromWKT("POLYGON ((35 10, 45 45, 15 40, 10 20, 35 10), (20 30, 35 35, 30 20, 20 30))")
//   property Geometry geometry: GeometryFactory.fromWKT("POLYGON ((350 100, 450 450, 150 400, 100 200, 350 100), (200 300, 350 350, 300 200, 200 300))")
//   property Geometry geometry: GeometryFactory.fromWKT("POLYGON((200.00 300.00, 300.00 200.00, 350.00 100.00, 100.00 200.00, 150.00 400.00, 450.00 450.00, 350.00 350.00, 200.00 300.00, 200.00 300.00))")
  GeometryView
  {
    anchors.fill: parent
    strokeStyle: "blue"
    lineWidth: 7
    geometry: Algorithms.intersection(root.geometry, Algorithms.delaunayTriangulation(root.geometry, 0.1))
  }
  GeometryView
  {
    anchors.fill: parent
    strokeStyle: "orange"
    lineWidth: 4
    geometry: Algorithms.convexPolygonDecomposition(root.geometry, 0.1)
    onGeometryChanged: console.log(GeometryFactory.toWKT(geometry))
  }
  GeometryView
  {
    anchors.fill: parent
    strokeStyle: "red"
    lineWidth: 1
    geometry: root.geometry
  }
}
