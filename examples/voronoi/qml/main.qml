import Cartography.Algorithms
import Cartography.Geometry
import Cartography.Geometry.Controls
import QtQuick.Controls

ApplicationWindow
{
  id: root
  visible: true
  width: 1024
  height: 768

  title: qsTr("Cartography Examples Voronoi")
  property var points: [[0, 0], [0, 100], [100, 100], [100, 200], [200, 200], [200, 0], [150, 0], [150, 50], [50, 50], [50, 0], [0, 0]]
  property Geometry geometry: GeometryFactory.polygon(points, [])
  GeometryView
  {
    anchors.fill: parent
    strokeStyle: "blue"
    lineWidth: 7
    geometry: Algorithms.intersection(root.geometry, Algorithms.voronoiDiagram(GeometryFactory.points(root.points), root.geometry, 0.1))
  }
//   GeometryView
//   {
//     anchors.fill: parent
//     strokeStyle: "blue"
//     lineWidth: 7
//     geometry: Algorithms.voronoiDiagram(GeometryFactory.points(root.points), root.geometry, 0.1)
//   }
  GeometryView
  {
    anchors.fill: parent
    strokeStyle: "red"
    lineWidth: 1
    geometry: root.geometry
  }
}
