#include <QGuiApplication>

#include <QAbstractItemModel>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char* argv[])
{
#ifdef HAVE_QT6WIDGETS
  QApplication app(argc, argv);
#else
  QGuiApplication app(argc, argv);
#endif
  app.setApplicationName("cartography_example_weighted_decomposition");

  QQmlApplicationEngine engine;
  engine.addImportPath("qrc:/qml/");

  engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

  return app.exec();
}
