import Cartography.Algorithms
import Cartography.Geometry
import Cartography.Geometry.Controls
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

ApplicationWindow
{
  id: root
  visible: true
  width: 1024
  height: 768

  title: qsTr("Cartography Examples Weighted Graph Polygon Decomposition")

  property Geometry geometry: polygon
  // GeometryFactory.fromWKT("POLYGON ((0 400, 400 400, 400 0, 0 0, 0 400), (120 120, 120 240, 240 240, 240 120, 120 120))")
  property LinearRing exterior: LinearRing {}
  property LinearRing current: exterior
  property Polygon polygon: Polygon {}

  Component.onCompleted:
  {
    polygon.exteriorRing = exterior

    exterior.append(1685, 118)
    exterior.append(980, 56)
    exterior.append(663, 341)
    exterior.append(731, 1040)
    exterior.append(1274, 1280)
    exterior.append(1807, 1049)
    exterior.append(1756, 574)
    exterior.append(1168, 556)
    exterior.append(1155, 739)
    exterior.append(1557, 804)
    exterior.append(1548, 982)
    exterior.append(998, 931)
    exterior.append(991, 388)
    exterior.append(1782, 344)

    var envelope = polygon.envelope
    var horizontalRatio = _views_.width / envelope.width * 0.7
    var verticalRatio = _views_.height / envelope.height * 0.7
    root.__scale = Math.min(horizontalRatio, verticalRatio)
  }
  
  property var weights: JSON.parse(_weights_string_.text)
  property var startPoints: []
  
  property var tolerance: 0.01

  property var __offset: Qt.point(0, 0)
  property var __scale: 1

  property var decomposition

  ColumnLayout
  {
    anchors.fill: parent
    GridLayout
    {

      Label
      {
        text: "Weights:"
      }
      TextField
      {
        id: _weights_string_
        text: "[0.1, 0.4, 0.3, 0.2]"
        Layout.fillWidth: true
      }
      Button
      {
        text: "Decompose"
        enabled: root.current.points.length > 2
        onClicked:
        {
          root.decomposition = Algorithms.weightedGraphPolygonDecomposition(root.geometry, root.weights, root.tolerance, root.startPoints, {}, true)
          if(root.decomposition)
          {
            var stats = Algorithms.lastStatistics
            var txt = "idx | weight | polsby_popper\n"
            for(var i = 0; i < stats["areas"].length; ++i)
            {
              txt += String(i).padStart(3,' ') + " |  " + String(stats["areas"][i]/stats["areas_sum"]).substr(0, 4) + "  | " + String(stats["polsby_popper"][i]).substr(0, 4)  + "\n"
            }
            _statistics_.text = txt
            _error_msg_.text = ""
          } else {
            _statistics_.text = ""
            _error_msg_.text = Algorithms.lastError
          }
        }
      }
      Button
      {
        text: "Clear"
        onClicked:
        {
          root.exterior.clear()
          root.polygon.clearHoles()
          current = root.exterior
          root.decomposition = null
          _statistics_.text = ""
        }
      }
      Button
      {
        text: "start hole"
        enabled: root.current.points.length > 2
        onClicked:
        {
          root.current = root.polygon.createHole()
        }
      }
      CheckBox
      {
        id: _show_trajectories_
        text: "Show trajectories"
      }
      Layout.fillWidth: true
    }
    Item
    {
      id: _views_
      clip: true
      Text
      {
        id: _statistics_
        font.family: "mono"
        text: ""
      }
      Text
      {
        id: _error_msg_
        color: "red"
        font.family: "mono"
        text: ""
      }
      Timer
      {
        id: switch_geometry
        interval: 1000
        repeat: true
        running: root.decomposition != null
        property int geometry_index: 0
        onTriggered: geometry_index = (geometry_index + 1) % root.decomposition.elements.length
      }
      GeometryView
      {
        anchors.fill: parent
        strokeStyle: "green"
        lineWidth: 3
        geometry: GeometryFactory.points(graph_decompo.geometry)
        offset: root.__offset
        scale: root.__scale
      }      
      GeometryView
      {
        id: graph_decompo
        geometry: root.decomposition ? root.decomposition : null
        anchors.fill: parent
        strokeStyle: "blue"
        lineWidth: 2
        offset: root.__offset
        scale: root.__scale
      }
      GeometryView
      {
        id: graph_decompo_alternate
        geometry: root.decomposition ? root.decomposition.elements[switch_geometry.geometry_index] : null
        anchors.fill: parent
        strokeStyle: "orange"
        lineWidth: 2
        offset: root.__offset
        scale: root.__scale
      }
      GeometryView
      {
        anchors.fill: parent
        strokeStyle: "black"
        lineWidth: 2
        offset: root.__offset
        scale: root.__scale
        geometry: GeometryFactory.points(startPoints)
      }
      GeometryView
      {
        anchors.fill: parent
        strokeStyle: "red"
        lineWidth: 1
        offset: root.__offset
        scale: root.__scale
        geometry: root.geometry
      }
      Repeater
      {
        model: root.decomposition ? root.decomposition.elements : []
        GeometryView
        {
          visible: _show_trajectories_.checked
          strokeStyle: "black"
          lineWidth: 1
          offset: root.__offset
          scale: root.__scale
          geometry: Algorithms.lawnMowerPattern(modelData, 30)
        }
      }
      MouseArea
      {
        anchors.fill: parent
        property real startX
        property real startY
        property bool hasMoved: false
        onPressed: mouse => {
          startX = mouse.x
          startY = mouse.y
          hasMoved = false
        }
        onReleased: {
          if(!hasMoved)
          {
            root.current.append(mouse.x/root.__scale - root.__offset.x, mouse.y/root.__scale - root.__offset.y)
          }
        }
        onPositionChanged: {
          hasMoved = true
          __offset = Qt.point(__offset.x + (mouse.x - startX) / root.__scale, __offset.y + (mouse.y - startY) / root.__scale)
          startX = mouse.x
          startY = mouse.y
        }
        onWheel:
        {
          if(wheel.angleDelta.y > 0)
          {
            root.__scale *= 1.01
            if(root.__scale > 2) root.__scale = 2
          } else {
            root.__scale /= 1.011
            if(root.__scale < 0.2) root.__scale = 0.2
          }
        }
      }
      Layout.fillWidth: true
      Layout.fillHeight: true
    }
  }


}
