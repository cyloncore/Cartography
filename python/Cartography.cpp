#include "Common.h"

#include <Cartography/CoordinateSystem.h>
#include <Cartography/GeoPoint.h>
#include <Cartography/GeometryObject.h>
#include <Cartography/Point.h>

namespace py = pybind11;

PYBIND11_MODULE(Cartography_, m)
{
  m.doc() = R"pbdoc(
    Cartography
    -----------

    .. currentmodule:: Cartography

    .. autosummary::
      :toctree: _generate
  )pbdoc";

  py::class_<Cartography::Point>(m, "Point")
    .def(py::init())
    .def(py::init<double, double, double, Cartography::CoordinateSystem>())
    .def("x", &Cartography::Point::x)
    .def("y", &Cartography::Point::y)
    .def("z", &Cartography::Point::z)
    .def("coordinateSystem", &Cartography::Point::coordinateSystem)
    .def("transform", &Cartography::Point::transform);
  py::class_<Cartography::GeoPoint>(m, "GeoPoint")
    .def(py::init())
    .def(py::init(
           [](double _longitude, double _latitude, double _altitude)
           {
             return new Cartography::GeoPoint(Cartography::Longitude(_longitude),
                                              Cartography::Latitude(_latitude),
                                              Cartography::Altitude(_altitude));
           }),
         py::arg("longitude"), py::arg("latitude"), py::arg("altitude") = NAN)
    .def("longitude", &Cartography::GeoPoint::longitude)
    .def("latitude", &Cartography::GeoPoint::latitude)
    .def("altitude", &Cartography::GeoPoint::altitude);

  py::class_<Cartography::CoordinateSystem>(m, "CoordinateSystem")
    .def(py::init())
    .def("toWkt", &Cartography::CoordinateSystem::toWkt)
    .def("toProj", &Cartography::CoordinateSystem::toProj)
    .def("srid", &Cartography::CoordinateSystem::srid)
    .def("isValid", &Cartography::CoordinateSystem::isValid)
    // .def_static("wgs84", &Cartography::CoordinateSystem::wgs84)
    .def_static("utm", &Cartography::CoordinateSystem::utm);

  py::class_<Cartography::GeometryObject>(m, "GeometryObject")
    .def(py::init())
    .def("isValid", &Cartography::GeometryObject::isValid)
    .def("toList", &Cartography::GeometryObject::toList)
    .def("toEWKT", pybind11_qt::overload<int>(&Cartography::GeometryObject::toEWKT))
    .def("toWKT", pybind11_qt::overload<int>(&Cartography::GeometryObject::toWKT))
    .def("toGML", &Cartography::GeometryObject::toGML)
    .def("toJson", &Cartography::GeometryObject::toJson)
    .def("toWKB", &Cartography::GeometryObject::toWKB)
    .def("toEWKB", &Cartography::GeometryObject::toEWKB)
    .def("toGeoVariant", &Cartography::GeometryObject::toGeoVariant)
    .def("coordinateSystem", &Cartography::GeometryObject::coordinateSystem)
    .def_static("fromWKB", &Cartography::GeometryObject::fromWKB)
    .def_static("fromEWKB", &Cartography::GeometryObject::fromEWKB)
    .def_static("fromWKT", &Cartography::GeometryObject::fromWKT)
    .def_static("fromGML", &Cartography::GeometryObject::fromGML)
    .def_static("fromJson", &Cartography::GeometryObject::fromJson)
    .def_static("fromGeoVariant", &Cartography::GeometryObject::fromGeoVariant)
    .def_static("polygon", &Cartography::GeometryObject::polygon)
    .def_static("load", &Cartography::GeometryObject::load);
}
