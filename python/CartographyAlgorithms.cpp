#include <pybind11-qt/core.h>
#include <pybind11/stl.h>

#include <euclid/extra/compactness>
#include <euclid/extra/convex_polygon_decomposition>
#include <euclid/extra/curve_value>

#include <QRectF>

#include <Cartography/Algorithms/LawnMowerPattern.h>
#include <Cartography/Algorithms/WeightedGraphPolygonDecomposition.h>
#include <Cartography/Point.h>

namespace py = pybind11;

PYBIND11_MODULE(Algorithms, m)
{
  m.doc() = R"pbdoc(
    Cartography.Algorithms
    ----------------------

    .. currentmodule:: Cartography.Algorithms

    .. autosummary::
      :toctree: _generate
  )pbdoc";

  m.def("convexPolygonDecomposition",
        [](const Cartography::GeometryObject& _object, qreal _tol)
        {
          return Cartography::GeometryObject(
            _object.coordinateSystem(),
            euclid::extra::convex_polygon_decomposition(_object.geometry(), _tol));
        });
  m.def(
    "weightedGraphPolygonDecomposition",
    [](const Cartography::GeometryObject& _object, const QList<qreal>& _weights, qreal _tol,
       const QList<QPointF>& _starts, const QVariantHash& _options,
       Cartography::Algorithms::WeightedGraphPolygonDecompositionStatistics* _statistics)
    {
      Cartography::Algorithms::WeightedGraphPolygonDecompositionParameters wgpdp;
      wgpdp.read(_options);
      wgpdp.weights = _weights;
      wgpdp.tol = _tol;
      QList<euclid::simple::point> starts;
      for(const QPointF& pt : _starts)
      {
        wgpdp.starts.append(euclid::simple::point(pt.x(), pt.y()));
      }
      return Cartography::Algorithms::weightedGraphPolygonDecomposition(_object, wgpdp,
                                                                        _statistics);
    },
    py::arg("object"), py::arg("weights"), py::arg("tol"), py::arg("starts"),
    py::arg("options") = QVariantHash(), py::arg("statistics") = nullptr);

  m.def("lawnMowerPattern",
        pybind11_qt::overload<const Cartography::GeometryObject&, const QVariantHash&>(
          &Cartography::Algorithms::lawnMowerPattern));
  m.def("curveValue", [](const Cartography::GeometryObject& _go, double _distance)
        { return Cartography::Point(euclid::extra::curve_value(_go.geometry(), _distance)); });
  m.def("length",
        [](const Cartography::GeometryObject& _go) { return euclid::ops::length(_go.geometry()); });
  m.def("area",
        [](const Cartography::GeometryObject& _go) { return euclid::ops::area(_go.geometry()); });
  m.def("envelope",
        [](const Cartography::GeometryObject& _go)
        {
          euclid::simple::box envelope = euclid::ops::envelope(_go.geometry());
          return QRectF(envelope.left(), envelope.top(), envelope.width(), envelope.height());
        });
  struct Compactness
  {
  };
  py::class_<Compactness>(m, "Compactness")
    .def_static("schwartzberg", [](const Cartography::GeometryObject& _object)
                { return euclid::extra::compactness::schwartzberg(_object.geometry()); })
    .def_static("reock", [](const Cartography::GeometryObject& _object)
                { return euclid::extra::compactness::reock(_object.geometry()); })
    .def_static(
      "minimum_rotated_rectangle_ratio", [](const Cartography::GeometryObject& _object)
      { return euclid::extra::compactness::minimum_rotated_rectangle_ratio(_object.geometry()); })
    .def_static("two_balls",
                [](const Cartography::GeometryObject& _object)
                {
#ifdef EUCLID_GEOS_HAS_MAXIMUM_INSCRIBED_CIRCLE
                  return euclid::extra::compactness::two_balls(_object.geometry());
#else
        throw std::exception("Two balls is not available with geos < 3.9.0");
#endif
                })
    .def_static("polsby_popper", [](const Cartography::GeometryObject& _object)
                { return euclid::extra::compactness::polsby_popper(_object.geometry()); })
    .def_static("moment_of_inertia", [](const Cartography::GeometryObject& _object)
                { return euclid::extra::compactness::moment_of_inertia(_object.geometry()); });
}
