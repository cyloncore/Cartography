#include <pybind11-qt/core.h>
#include <pybind11/stl.h>

#include <Cartography/Raster/Raster.h>

namespace py = pybind11;

PYBIND11_MODULE(Raster, m)
{
  m.doc() = R"pbdoc(
    Cartography.Raster
    ----------------------

    .. currentmodule:: Cartography.Raster

    .. autosummary::
      :toctree: _generate
  )pbdoc";
  py::class_<Cartography::Raster::Raster>(m, "Raster")
    .def(py::init())
    .def("isValid", &Cartography::Raster::Raster::isValid)
    .def_static("fromWKBRaster", &Cartography::Raster::Raster::fromWKBRaster)
    .def_static("load", &Cartography::Raster::Raster::load);
  pybind11_qt::register_qvariant_converter<Cartography::Raster::Raster>();
}
