#include <pybind11-qt/core.h>

#include <cext_nt>
#include <clog_qt>
#include <cres_qt>

namespace Cartography::pybind11
{
  template<typename _T_>
  _T_ handle_result(const cres_qresult<_T_>& _result)
  {
    if(_result.is_successful())
    {
      return _result.get_value();
    }
    else
    {
      throw std::runtime_error(_result.get_error().get_message().toStdString());
    }
  }
  template<>
  void handle_result(const cres_qresult<void>& _result)
  {
    if(not _result.is_successful())
    {
      throw std::runtime_error(_result.get_error().get_message().toStdString());
    }
  }
} // namespace Cartography::pybind11

namespace pybind11::detail
{
  template<typename _T_, typename _Tag_, int _Options_>
  struct type_caster<cext_named_type<_T_, _Tag_, _Options_>>
  {
  public:
    using NamedType = cext_named_type<_T_, _Tag_, _Options_>;
    PYBIND11_TYPE_CASTER(NamedType, const_name("cext_named_type<_T_, _Tag_, _Options_>"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      value = pybind11::cast<_T_>(src);
      return true;
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(NamedType src, return_value_policy policy, handle parent)
    {
      return pybind11::cast(src.get_value(), policy, parent).release();
    }
  };
  template<>
  struct type_caster<cres_qresult<void>>
  {
  public:
    PYBIND11_TYPE_CASTER(cres_qresult<void>, const_name("cres_qresult<void>"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle, bool) { return false; }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(cres_qresult<void> src, return_value_policy, handle)
    {
      if(src.is_successful())
      {
        return pybind11::none();
      }
      else
      {
        throw std::runtime_error(src.get_error().get_message().toStdString());
      }
    }
  };
  template<typename _T_>
  struct type_caster<cres_qresult<_T_>>
  {
  public:
    using t_conv = make_caster<_T_>;
    PYBIND11_TYPE_CASTER(cres_qresult<_T_>,
                         const_name("cres_qresult<") + t_conv::name + const_name(">"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool) { return false; }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(cres_qresult<_T_> src, return_value_policy policy, handle parent)
    {
      return pybind11::cast(Cartography::pybind11::handle_result(src), policy, parent).release();
    }
  };
} // namespace pybind11::detail
