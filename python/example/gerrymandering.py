#!/usr/bin/env python3

import Cartography
import Cartography.Algorithms
import Cartography.Raster
import sys
import PyQt6

if len(sys.argv) != 3:
  raise Exception("Invalid number of arguments: gerrymandering.py [shape file] [population as tif]")

shape_filename = sys.argv[1]
pop_filename   = sys.argv[2]

shapes  = Cartography.GeometryObject.load(shape_filename)
if len(shapes) == 0:
  raise Exception("Invalid shape file")
if len(shapes) > 1:
  raise Exception("Too many shapes")

shape   = shapes[0].toList()[2]

pop     = Cartography.Raster.Raster.load(pop_filename)

print(PyQt6.QtCore.QVariant(pop).typeName())

if not pop.isValid():
  print("Population file is not valid.")
  sys.exit(-1)

stats = Cartography.Algorithms.WeightedGraphPolygonDecompositionStatistics()

decomp  = Cartography.Algorithms.weightedGraphPolygonDecomposition(shape, [0.25, 0.25, 0.25, 0.25], 0.01, [], {"raster": pop, "optimisation1": "CMAES"}, stats)

print("===== Final decomposition =====")
print(decomp.toWKT())

print("===== Areas =====")
print(stats.areas)

for gvar in decomp.toList():
  print("===== Individual area compactness =====")
  print(gvar.toWKT())
  
  Compactness = Cartography.Algorithms.Compactness
  
  area = Cartography.Algorithms.area(gvar)
  schwartzberg = Compactness.schwartzberg(gvar)
  reock = Compactness.reock(gvar)
  minimum_rotated_rectangle_ratio = Compactness.minimum_rotated_rectangle_ratio(gvar)
  two_balls = Compactness.two_balls(gvar)
  polsby_popper = Compactness.polsby_popper(gvar)

  print(area, schwartzberg, reock, minimum_rotated_rectangle_ratio, two_balls, polsby_popper)

#gdb --args python /home/cyrille/cc-pkg/src/Cartography/python/example/gerrymandering.py /home/cyrille/Data/PolygonDecomposition/NewYork/State.shp /home/cyrille/Data/PolygonDecomposition/NewYork/nypop10.tif
