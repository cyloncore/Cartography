#!/usr/bin/env python3

import unittest

import Cartography
import Cartography.Algorithms
import knowCore
class TestAlgorithms(unittest.TestCase):
    def test_decomposition(self):
        self.maxDiff = None
        go = Cartography.GeometryObject.fromWKT("POLYGON ((35 10, 45 45, 15 40, 10 20, 35 10), (20 30, 35 35, 30 20, 20 30))")
        do = Cartography.Algorithms.convexPolygonDecomposition(go, 0.1)
        self.assertEqual(do.toWKT(2), u'GEOMETRYCOLLECTION (LINEARRING (15 40, 20 30, 10 20, 15 40), LINEARRING (35 35, 20 30, 15 40, 45 45, 35 35), LINEARRING (35 35, 45 45, 35 10, 30 20, 35 35), LINEARRING (30 20, 35 10, 10 20, 20 30, 30 20))')

    def test_lawnMowerPattern(self):
        go = Cartography.GeometryObject.fromWKT("POLYGON ((35 10, 45 45, 15 40, 10 20, 35 10), (20 30, 35 35, 30 20, 20 30))")
        do = Cartography.Algorithms.lawnMowerPattern(go, {"strideWidth": 5})
        self.assertEqual(do.toWKT(2), u'LINESTRING (37.4 9.31, 47.4 44.31, 42.54 45.68, 32.55 10.71, 27.84 12.59, 37.03 44.76, 33.04 43.37, 30.34 32.58, 25.6 30.45, 28.82 43.31, 23.38 42.41, 16.99 16.84, 21.72 14.95, 24.75 27.06, 27.62 23.23, 25.28 13.85, 12.26 18.73, 17.95 41.5)')
        do = Cartography.Algorithms.lawnMowerPattern(go, {"strideWidth": 5, "startPoint": { "x": 42.54, "y": 45.68 }})
        self.assertEqual(do.toWKT(2), u'LINESTRING (47.4 44.31, 37.4 9.31, 32.55 10.71, 42.54 45.68, 37.03 44.76, 27.84 12.59, 25.28 13.85, 27.62 23.23, 24.75 27.06, 21.72 14.95, 16.99 16.84, 23.38 42.41, 17.95 41.5, 12.26 18.73, 25.6 30.45, 28.82 43.31, 33.04 43.37, 30.34 32.58)')


if __name__ == '__main__':
    unittest.main()
