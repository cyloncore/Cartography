#include "TestAlgorithms.h"

#include <clog_qt>

#include <euclid/io>
#include <euclid/ops>

#include <Cartography/Algorithms/WeightedGraphPolygonDecomposition.h>
#include <Cartography/CoordinateSystem.h>

void TestAlgorithms::initMain() { qputenv("QTEST_FUNCTION_TIMEOUT", "900000"); }

void TestAlgorithms::testWeightedGraphPolygonDecomposition()
{
  testWeightedGraphPolygonDecomposition(
    "POLYGON ((125.141853704345 -228.845183503804,113.806505936974 "
    "-150.160152385757,100.654252043437 -58.8629528534784,-115.710197553785 "
    "-71.9179172950728,-155.981145026017 -74.3477794478341,-205.772136007354 "
    "-77.3520605022455,-215.619878361139 -77.9462520457176,125.141853704345 -228.845183503804))",
    {0.60672, 0.000109487, 0.285839, 0.107331}, 0.0, true, 0.01);
  testWeightedGraphPolygonDecomposition(
    "POLYGON ((146.245129613789 -203.442310219414,205.275229808581 "
    "4.16642303943411,212.471904170068 52.6586610658695,214.23269537054 "
    "64.5231281404228,8.29027801126438 171.921542761458,-202.01409339061 "
    "197.170991979814,-259.953181001678 168.028678452108,-160.720327687085 "
    "-139.637933809548,-88.3813581949135 -238.677428330928,28.1139371277873 "
    "-255.955105777773,42.1889570456879 -258.042603418994,127.138124834692 "
    "-270.641604231607,146.245129613789 -203.442310219414))",
    {0.772443, 0.223768, 0.00286273, 0.000925805}, 0.0, true, 0.01);
  testWeightedGraphPolygonDecomposition(
    "POLYGON ((-276.762980240966 -47.3548644902596,-233.62098273842 "
    "-108.810311503229,-145.706190773017 -234.044263384277,-108.449715951857 "
    "-287.115828142648,205.105082327193 -139.560977239494,265.685962479963 "
    "-111.052395047208,269.658051767877 213.531302148975,-279.011740571387 "
    "237.484615628653,-290.79991607179 237.999252787573,-276.762980240966 -47.3548644902596))",
    {0.544798, 0.341153, 0.112127, 0.00192189}, 0.0, true, 0.01);
  testWeightedGraphPolygonDecomposition(
    "POLYGON ((295.80440672145 22.0931363879088,270.99118713202 119.36552780574,253.344981754003 "
    "188.541903341507,244.189555179737 203.891740760832,206.961665654514 "
    "266.307407255603,-19.19599307308 244.747331819907,-95.4165727244159 "
    "234.126192558601,-226.330111547855 37.1680232158076,-278.185587793979 "
    "-159.837947762411,-222.808157134068 -215.163615144093,150.306345333345 "
    "-273.552355071136,247.422185634701 -272.124812926504,287.373052509666 "
    "-271.537560157012,295.80440672145 22.0931363879088))",
    {0.00648749, 0.376728, 0.616784}, 0, true, 0.01);
  testWeightedGraphPolygonDecomposition(
    "POLYGON ((220.492966985828 294.788121071572,140.016324384714 "
    "269.951354791916,11.5639773856032 230.308287966884,-79.1958920196087 "
    "202.297903945742,-184.872584226139 -253.112449256232,24.2398782478091 "
    "-227.130628918413,150.70862627086 -211.417130781528,271.601051868015 "
    "-196.396479956091,231.181128345495 192.067381578609,220.492966985828 294.788121071572))",
    {0.254546, 0.249138, 0.496274, 2.29822e-5, 2.01161e-5}, -0.095707, false, 0.1);
  testWeightedGraphPolygonDecomposition(
    "POLYGON ((-33.1067826203181 208.97464494354,-63.7896100278542 "
    "190.998034688247,-197.896964694818 112.426538808952,-197.932526899395 "
    "112.405703444498,-35.0907143638819 -84.2799193884751,-0.344485833451643 "
    "228.169585246241,-33.1067826203181 208.97464494354))",
    {0.95612, 0.0362478, 0.00763239}, 0.0, false, 0.01);
}

void TestAlgorithms::testWeightedGraphPolygonDecomposition(const QString& _wkt,
                                                           const QList<qreal>& _weights,
                                                           qreal _area_adjust, bool _check_bounds,
                                                           qreal _tolerance)
{
  clog_info("Testing weighted graph with {}", _wkt);
  Cartography::EuclidSystem::polygon object
    = euclid::io::from_wkt<Cartography::EuclidSystem::polygon>(_wkt.toStdString());
  QVERIFY(not object.is_null());
  QVERIFY(not object.is_empty());
  Cartography::Algorithms::WeightedGraphPolygonDecompositionParameters wgp;
  wgp.weights = _weights;
  wgp.tol = _tolerance;
  Cartography::EuclidSystem::multi_polygon result
    = Cartography::Algorithms::weightedGraphPolygonDecomposition(
      object, Cartography::CoordinateSystem(), wgp);
  QVERIFY(not result.is_null());
  QVERIFY(not result.is_empty());
  QCOMPARE((int)result.size(), _weights.size());
  qreal object_area = euclid::ops::area(object);
  qreal total_area = 0.0;
  for(int i = 0; i < _weights.size(); ++i)
  {
    Cartography::EuclidSystem::polygon poly = result.at(i);
    qreal p_area = euclid::ops::area(poly);
    if(_check_bounds)
    {
      QVERIFY(p_area > 0.9 * _weights[i] * object_area);
      QVERIFY(p_area < 1.1 * _weights[i] * object_area);
    }
    total_area += p_area;
  }
  QCOMPARE(total_area, object_area + _area_adjust);
}

QTEST_MAIN(TestAlgorithms)
