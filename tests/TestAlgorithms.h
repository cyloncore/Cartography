#include <QtTest/QtTest>

class TestAlgorithms : public QObject
{
  Q_OBJECT
public:
  static void initMain();
private slots:
  void testWeightedGraphPolygonDecomposition();
private:
  void testWeightedGraphPolygonDecomposition(const QString& _wkt, const QList<qreal>& _weights,
                                             qreal _area_adjust, bool _check_bounds,
                                             qreal _tolerance);
};
